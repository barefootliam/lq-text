# not finished
%define name lq-text
%define version 1.18.1
%define release 4lqt

Summary:        Text retrieval database

Name:           %{name}
Version:        %{version}
Release:        %{release}
Source: http://www.holoweb.net/~liam/lq-text/%{name}-%{version}-%{release}.tar.gz
Prefix:		/usr/local

URL:            http://www.holoweb.net/~liam/lq-text/

Group:          Applications/Database
Vendor:		Barefoot Computing, Toronto
Packager:       Liam Quin <liam@w3.org>
Requires: 	gcc libdb-devel libncurses-devel
Copyright:      Barefoot License (see COPYING)

BuildRoot:      /var/tmp/%{name}-root

%description
A text retrieval database with a command-line UI; use this to index
text or HTML files and then search them later.

%prep

%setup

%build
cd src && make DESTDIR=$RPM_BUILD_ROOT/%{prefix} docdir=$RPM_BUILD_ROOT/%{_docdir}/%{name}

%install
install -d $RPM_BUILD_ROOT%{_mandir}/man1
(cd src && make install DESTDIR=$RPM_BUILD_ROOT/%{prefix} docdir=$RPM_BUILD_ROOT/%{_docdir}/%{name} )
cp doc/man/*.1 $RPM_BUILD_ROOT%{_mandir}/man1/

%files
%doc COPYING-Barefoot
%doc COPYING-LGPL
%doc COPYRIGHT
%doc INSTALL
%doc README
%doc TODO
%doc BugReport
%doc config.txt
%doc 1.4-makingAnIndex.html
%doc Sample/config.txt
%doc Sample/CommonWords

%doc %attr(644, root, man) %{_mandir}/man1/lqaddfile.1*
%doc %attr(644, root, man) %{_mandir}/man1/lqfile.1*
%doc %attr(644, root, man) %{_mandir}/man1/lqkwic.1*
%doc %attr(644, root, man) %{_mandir}/man1/lq.1*
%doc %attr(644, root, man) %{_mandir}/man1/lqmkwidtable.1*
%doc %attr(644, root, man) %{_mandir}/man1/lqphrase.1*
%doc %attr(644, root, man) %{_mandir}/man1/lqquery.1*
%doc %attr(644, root, man) %{_mandir}/man1/lqrank.1*
%doc %attr(644, root, man) %{_mandir}/man1/lqrename.1*
%doc %attr(644, root, man) %{_mandir}/man1/lqshow.1*
%doc %attr(644, root, man) %{_mandir}/man1/lq-text.1*
%doc %attr(644, root, man) %{_mandir}/man1/lqtext.1*
%doc %attr(644, root, man) %{_mandir}/man1/lqunindex.1*
%doc %attr(644, root, man) %{_mandir}/man1/lqwordlist.1*
%doc %attr(644, root, man) %{_mandir}/man1/lqword.1*

# %attr(755,root,root) %{prefix}/bin/*
# %doc %attr(644,root,root) %{_mandir}/man1/dot.1.gz

%{prefix}/bin/addbyline
%{prefix}/bin/lq
%{prefix}/bin/lqaddfile
%{prefix}/bin/lqbyteline
%{prefix}/bin/lqcat
%{prefix}/bin/lqclean
%{prefix}/bin/lqfile
%{prefix}/bin/lqkwic
%{prefix}/bin/lqmkindex
%{prefix}/bin/lqphrase
%{prefix}/bin/lqquery
%{prefix}/bin/lqrank
%{prefix}/bin/lqrename
%{prefix}/bin/lqsed
%{prefix}/bin/lqshow
%{prefix}/bin/lqsimilar
%{prefix}/bin/lqsort
%{prefix}/bin/lqtext
%{prefix}/bin/lqunindex
%{prefix}/bin/lqword
%{prefix}/bin/lqwordlist
%{prefix}/bin/mkwidtable
%{prefix}/bin/sortwids

# documentation

# devel package:
# %{prefix}/lib/liblqtext.a


%clean
rm -rf $RPM_BUILD_ROOT

%post
exit 0

%changelog
* Fri Aug 29 2003 Liam Quin <liam@w3.org>

- now compiles with gcc 2.3.2

* Fri May 23 2003 Liam Quin <liam@w3.org>

- use LQTEXTDIR in current directory if $LQTEXTDIR is unset,
  before trying $HOME/LQTEXTDIR

* Thu May 22 2003 Liam Quin <liam@w3.org>

- stop lqword looping forever in default mode with dbnative

* Wed May  7 2003 Liam Quin <liam@w3.org>

- made first release of RPM package
- fixed some Linux compile warnings
