.ds Revision "@(#) $Revision: 1.24 $
.
.
'\" Page Proportions
'\" ================
'\" 
'\" See Jan Tschicholde's essay on the Form of the Book,
'\" and note especially the diagrams.
'\" 
'\" The proportions are also influenced by the Golden Cockerell Press
'\" Four Gospels.  Had I been able to measure it, I should have used
'\" the Pseudo-Cicero incunablum that I saw recently, or any other
'\" similar 15th C. early printed commentary with the mixed sizes of text,
'\" as that is exactly the effect I wish to achieve.
'\" 
'\" The Golden Cockerel Four Gospels (GCFG) uses a 1:sqrt(2) page width/height
'\" ratio.
'\" However, I have used 3:4 for now, which gives a wider text area, because
'\" there are so many long words (actually function names) in the text.
'\" 
'\" 
'\" Proportions of 3:4:
.nr Page.Width 20c
.nr Page.Height \n[Page.Width]*4/3
.
'\" The page is divided into ninths:
'\" A horizontal page unit is an X, and a vertical one a Y.
.nr tmp \n[Page.Width]/9
.scale X \n[tmp]u 1 round
.nr tmp \n[Page.Height]/9
.scale Y \n[tmp]u 1 round
.
.\" put the page on the paper:
.nr Paper.Width 8.5i
.nr Paper.Height 11i
'pl 17i
.nr Margin.Paper.left (\n[Paper.Width]-\n[Page.Width])/2
.nr Margin.Paper.Top (\n[Paper.Height]-\n[Page.Height])/3
.nr Margin.Paper.Bottom (\n[Paper.Height]-\n[Page.Height])*2/3
.
'po \n[Margin.Paper.left]u+1X
'nr Measure 6X
'll \n[Measure]u
.tm Page \n[Page.Width]x\n[Page.Height], inner \I#[1X], ll \n[.l] Y=\I#[1Y]
.
'\" 
'\" 
.
.de Page.Top
' ch Restore.ch
' rs
' sp |\\n[Margin.Paper.Top]u
' ev ev.Page.Footers
'  po \n[Margin.Paper.left]u
\D[w 0.5]\D[b \\n[Page.Width]u \\n[Page.Height]u]\D[w 0]
.  br
' ev
'
' ie o 'po \n[Margin.Paper.left]u+1X
' el   'po \n[Margin.Paper.left]u+2X
'
' sp |\\n[Margin.Paper.Top]u
' sp 1Y
' mk Page.Top
' if d Restore.ch \{.
'  Restore.ch
. \}
' frm Restore.ch
. RestoreMeasure
' frr endOfDecl
' ie d Page.LastEntry \{.
'  ds Page.FirstEntry \\*[Page.LastEntry]
' \}
' el \{.
'  frm Page.FirstEntry
' \}
' mk NextMarginNote.top
.
' ns
..
.
.wh 0 Page.Top
.
.de Page.Bottom
' sy echo -n '[\\n%]' 1>&2
' PrintFootNotes
' ev ev.Page.Footers
'  lt \\n[.l]u
'  ps 9
'  nr tmp \\n[Page.Height]+\\n[Margin.Paper.Top]-1v
'  sp |\\n[tmp]u
'  if d Page.FirstEntry \{.
'   ie d Page.LastEntry \{.
'    ds tmp "\\*[Page.FirstEntry]\[em]\\*[Page.LastEntry]
'   \}
'   el \{.
'    ds tmp "\\*[Page.FirstEntry]
'   \}
'   if o '  tl '''\\*[tmp]\h[1P]%'
'   if e '  tl '%\h[1P]\\*[tmp]'''
'  \}
' ev
' frr closeFit
' bp
..
.
.wh (u; \n[Margin.Paper.Top]+1Y+6Y) Page.Bottom
.
.de _End
' if !\\n[List.Level]=0 .tm List.Level \\n[List.Level] at end of processing!
' xclose sqtroff.pagelist
. bp
' sy echo '**** \\n% pages.' 1>&2
..
.em _End
.
.de Head
. nr P 0
\\*[HeadMark]\|\\$1: \c
..
.
.ds textFont Celestia
.ds smallCapsFont CelestiaCapsR
.ds smallCapsFontBold BemboB
.ffam \*[textFont]
.hy 14
.hxd on
.
.ft R
.ps 12
.vs 14
.nr Body.vs \n[.v]
.nr Measure \n[.l]u
.\" . ds +sc\"
.\" . ds -sc\"
.ds +sc "\<\f[\*[smallCapsFont]]
.ds -sc "\>
.ds +fn "\<\s-1\f[\*[smallCapsFont]]
.ds -fn "\>
.de Font.FunctionName
. ie '\\$1'bold' .ft \\*[smallCapsFontBold]
. el .            ft \\*[smallCapsFont]
..
.ds +h "\<\s-1\f[CCR]<
.ds -h >\>
.ds +q "\[od]
.ds -q "\[cd]
.\".ds +var "\<\s-1\f[CCR]
.\".ds -var "\>
.ds +var "`
.ds -var "'
.ds HeadMark "\<\f[CelestiaOrnamentsR]\s-2Z\s0\>
.
.wbsym -/\-\[em]\[en]_\[hy]
.
.copye \n[.ev] ev.Page.Footers
.copye \n[.ev] ev.MarginNote
.copye \n[.ev] ev.FootNotes
.
.
.ev ev.MarginNote
'  ll 2X-3P
'  ps 8
'  vs 9
'  ad l
.ev
.
.de +MarginNote
. mk MarginNote.top
. ev ev.MarginNote
.  if \\n[MarginNote.top]<\\n[NextMarginNote.top] \{.
.   sp |\\n[NextMarginNote.top]u
.  \}
.  di MarginNote.text
.   nh
.   if e .ad r
.   if o .ad l
..
.
.de -MarginNote
.   br
.  di
.  nf
.  sp \\n[Body.vs]u-\\n[.v]u\" baseline uncertainty correction
.  ie e \{.\" put it in the left margin
.   po -(u; 2X-2P)
.   MarginNote.text
.   po
.  \}
.  el \{.\"odd (recto) page, use the right margin
.   po +(u; 6X+1P)
.   MarginNote.text
.   po
.  \}
.  rm MarginNote.text
.
.  mk NextMarginNote.top
.  nr NextMarginNote.top +1v
.  fi
. ev
. sp |\\n[MarginNote.top]u
. rr MarginNote.top
..
.
.\" *************** footnotes
.
.\" Actually, these are not so much footnotes as cross references.
.\" Each cross reference consists of:
.\" a name, such as the name of a function or of a section
.\" a description
.\" a page number
.\"
.\" There is only ever one footnote on the page with a given name,
.\" and the footnotes are sorted alphabetically.  I don't like things
.\" to be too easy!
.\"
.\" The notes are set in the bottom margin, in two columns.
.\"
.\" Example usage is
.\" .+FootNote LQT_GetDatabaseDirectoryName
.\" LQT_GetDatabaseDirectoryName [liblqutil; returns char *]
.\" page 56, Database/Defaults;
.\" page 78, Database/Defaults;
.\" src/myfile.c
.\" .-FootNote
.\"
.\" Footnotes are stored in diversions, Foot.text.NAME.
.\" We save the name of each to a file, /tmp/feet.$$;
.\" when we get to the end of the page, we sort this file and
.\" read it in again, and it makes a big diversion containing
.\" the footnotes, split into two columns.
.\"
.
.nr FootNote.Gutter 1P
.nr FootNote.Columns 2
.
.ev ev.FootNote
' ps 8
' vs 9.5
' ft R
.ev
.
.de +FootNote \" $1=name 
. nr _Gutters ( (\\n[FootNote.Columns] - 1) * \\n[FootNote.Gutter] )
. nr FootNote.Measure (\\n[Measure] - \\n[_Gutters])/\\n[FootNote.Columns]
. ev ev.FootNotes
. \" see if this footnote is already defined
. if d Foot.text.\\$1 \{.
.  nr FootNote.Ignore 1
.  di FootNote.DiscardLaterPlease
.  return
. \}
. ds FootNote.Name \\$1
. if !r FootNote.Count \{.\" open the aux file for the first one
.  nr FootNote.Count 0
.  \" open a pipe to the program that massages the footnotes, sorts them.
.  \" and determines where the column break should be:
.  xopen file.feet wp bin/footfondler > /tmp/feet.\n[$$]
.  xwrite file.feet .COLS \\n[FootNote.Columns] \\I#[1Y] 0
. \}
. nr FootNote.Count +1
. di Foot.text.\\$1
.  ps 8
.  vs 9.5
.  ft R
.  ll \\n[FootNote.Measure]u
.  in 1m
.  ti -1m
..
.de -FootNote
.   br
.  di
. ev
. if r FootNote.Ignore \{.
.  rr FootNote.Ignore
.  rm FootNote.DiscardLaterPlease
.  return
. \}
. xwrite file.feet \\*[FootNote.Name] \\n[dn]
..
.
.de PrintFootNotes
. if !r FootNote.Count .return \" no feet saved
. xclose file.feet
. vpt off
. ev ev.FootNotes
.  so /tmp/feet.\n[$$]
.  sy /bin/rm -f /tmp/feet.\n[$$]
. ev
. vpt on
. rr FootNote.Count
..
.
.de FootPrint \" print one footnote, $1=NAME
. br
. Foot.text.\\$1
. rm Foot.text.\\$1
. br
..
.
.de +FootCol \" col height
. in 0
. sp |(u; \\n[Margin.Paper.Top] + \\n[Page.Height] - (\\$2 + 0.5Y + 2P))
. ll (u; (\\n[FootNote.Measure] * \\$1)+(\\n[FootNote.Gutter] * (\\$1 - 1)))
. in (u; (\\n[FootNote.Measure] + \\n[FootNote.Gutter]) * (\\$1 - 1))
..
.
.de -FootCol \" end of printing a footnote column
. in 0
. ll \\n[FootNote.Measure]u
. sp |(u; \\n[Margin.Paper.Top] + \\n[Page.Height] - 1Y - 2P)
..
.
.de Overview
. if r WithinOverview .tm Warning, last overview didn't end, this is \\$*
. if \\n%>1 \{.
.  bp
.  rm Page.FirstEntry
.  rm Page.LastEntry
.  if e \{.
.   rs
.   bp
.  \}
. \}
. ps +2
. vs +2
. ad r
\&\<\fI\\$1\>
. sp
. ad b
. ds Page.FirstEntry "\\$1
. nr WithinOverview 1
. P
. ft I
. dpush P
. dpush /P
. de P
.  br
.  ti 1m
\\..
. de /P
.  br
\\..
..
.
.de /Overview
. sp
. ft R
. vs -2
. ps -2
. MakeContents \\*[Page.FirstEntry]
. sp
. rr WithinOverview
. dpop P
. dpop /P
..
.
.de MakeContents \" category
. sy cd "Categories/\\$1"; ls | sed -e 's/^/.contents-item /' > /tmp/ct.\n[$$]
. copye \\n[.ev] ev.contents
. ev ev.contents
.  di MakeContents.text
.  ps 9
.  vs 10.5
.  ll 3.5X \" i.e., 6X-2.5X
.  in 0
.  ta \\n[.l]uR
.  cs TR 36
.   nf
.   so /tmp/ct.\n[$$]
.  di
.  in 2.5X
.  MakeContents.text
.  rm MakeContents.text
.  fi
. ev
. sy /bin/rm -f /tmp/ct.\n[$$]
..
.
.de contents-item
\&\\$1 \f[TR]\a\fP \\*[@Page.\\$1]
..
.
.de Entry
. br
. if r endOfDecl \{.
.   tm Moving entry on... .d \\n[.d], .h \\n[.h], end is \\n[endOfDecl]
.   sp |\\n[endOfDecl]u+1v
. \}
. in 0
. br
. ne 5
. rs
\D[l \\n[.l]u 0]
. br
. frm DidNote
. ds F.Library "\\$1
..
.
.de /Entry
. br
..
.
.de InputBreak
. br
. it 1 InputBreak
. nr InputBreak.LineCount +1
. if d InputBreak.DoAfterLine_\\n[InputBreak.LineCount] \{.
.  InputBreak.DoAfterLine_\\n[InputBreak.LineCount] \\n[InputBreak.LineCount]
. \}
..
.
.de InputBreak.DoAfterLine_2
. it
. di
. br
. frr closeFit
. if (\\n[dl]+1P<2.5X) \{.
.  nr closeFit 1
. \}
. +MarginNote
\&\\*[F.Class]
.br
\&\\*[F.File]
. -MarginNote
. nr DidNote 1
.\" . nr nameLineWidth \\n[dl]
. sp \\n[Body.vs]u-\\n[.v]u\" baseline uncertainty correction
. _Decl
. rm _Decl
. di _Decl
..
.
.de Decl.setup
' ps -2
' vs -2
..
.
.de Decl.restore
' ps +2
' vs +2
' in 0
..
.
.de Decl
. br
. in 0
. if !d F.Class .ds F.Class "[unknown class]
. IndexFunction "\\*[F.Name]" "\\*[F.Class]" "\\*[F.Library]" "\\*[F.File]" 
. br
. dpush +fn
. dpush -fn
. ds +fn "\<\s-1\f[BemboB]
. ds -fn "\>
. frr closeFit
. Decl.setup
. nr InputBreak.LineCount 0
.\" . nr nameLineWidth 0
. it 1 InputBreak
. di _Decl
..
.
.de /Decl
. br
. it
. di
. if !r DidNote \{.
.  +MarginNote
\&\\*[F.Class]
.br
\&\\*[F.File]
.  -MarginNote
. \}
. frm DidNote
. nr declWidth \\n[dl]u
. if (\\n[dl]+1P<2.5X) \{.
.  nr declWidth 2.5X-1P
. \}
. if (\\n[.l]-\\n[declWidth]<2X) \{.
.   nr declWidth 10X \" i.e. huge, so we won't use it
. \}
. in 0
. ne \\n[dn]u+1v
. mk topOfDecl
. nf
. _Decl
. fi
. in
. ll \\n[dl]u+1m
. rm _Decl
. dpop -fn
. dpop +fn
..
.
.de RestoreMeasure
' in 0
' ll \\n[Measure]u
' frr endOfDecl
..
.
.de prepareRestoration
. br
. if d Restore.ch \{.
.  tm \\n[.F]: \\n[.c]: oops - Restore.ch defined! in \\n[.clist]
.  sp \\n[.t]u+1u
. \}
. if d Restore.ch \{.
.  Restore.ch
.  frm Restore.ch
. \}
.\" . if (\\n[declWidth]<\\n[nameLineWidth]) \{.
.\" .  nr closeFit 1
.\" . \}
. if (\\n[declWidth]+1P<2.5X) \{.
.  nr declWidth 2.5X-1P
. \}
. ie ((\\n[declWidth]+1P)<=2.5X) \{.
.  mk endOfDecl
.  sp |\\n[topOfDecl]u
.  if r closeFit \{.
.   sp -2v
.   sp \\n[.v]u-\\n[Body.vs]u\" upwards baseline uncertainty correction
. \}
. 
.  Decl.restore
.  ll \\n[Measure]u
.  in \\n[declWidth]u+1P
.  ds maxSpace 6\" huge because we have short measures.
.  de Restore.ch
'   ch Restore.ch
.   rn Restore.ch Restore.doomed
'   wh \\n[endOfDecl]u
.   RestoreMeasure
'   in 0\" was: \\n[Decl.Indent]u
.   ds maxSpace 3.5\" still fairly large cos of long names
.   rm Restore.doomed
\\..
.  nr endOfDecl +0.5v
.  wh \\n[endOfDecl]u Restore.ch
.  nr endOfDecl -0.5v
. \}
. el \{.\" doesn't fit
.  Decl.restore
.  ll \\n[Measure]u
.  in 0
.  frr closeFit
. \}
. 
..
.
.de StartTextBlock
. nr P 0
..
.
.de Function
. if r endOfDecl \{.
.  if (\\n[.h] < \\n[endOfDecl]) \{.
.   sp |\\n[endOfDecl]u+1v
.  \}
. \}
. frm F.Class
. frm F.Name
. StartTextBlock
. ds F.File "\\$1
..
.
.de /Function
. br
. if r endOfDecl \{.
.  if (\\n[.h] < \\n[.d]) \{.
.   sp |\\n[.h]u+1v
.  \}
. \}
. if d Restore.ch \{.
.  sp 12i
. \}
..
.
.de Errors
. Head Errors
..
.
.de /Errors
..
.
.de File
..
.
.de /File
..
.
.nr List.Level 0
.de List
. nr List.Level +1
. nr Counter.\\n[List.Level] 0
..
.
.de /List
. nr List.Level -1
. if \\n[List.Level]<0 .tm oops, List.Level < 0, \\n[List.Level]
..
.
.de LI
. nr Counter.\\n[List.Level] +1
\\n[Counter.\\n[List.Level]].\~\&\c
. if !''\\$1' \&\\$1
..
.
.de /LI
..
.
.de Name
. ds F.Name "\\$1
. ie !d Page.FirstEntry .ds Page.FirstEntry "\\$1
. el .ds Page.LastEntry "\\$1
..
.
.de Notes
. Head Notes
..
.
.de /Notes
..
.
.de Example
. br
. ne 2
Example:
. nf
..
.de /Example
. br
. fi
..
.
.de Restrictions
. Head Restrictions
..
.
.de Bugs
. Head Bugs
..
.
.
.de Class
. ds F.Class "\\$*
..
.
.de P
. nr P +1
. if \\nP>1 \{.
\\[pp]\|\c
. \}
. if !''\\$1' \&\\$1
..
.
.de /P
..
.
.de Purpose
. br
. StartTextBlock
. prepareRestoration
..
.
.de Returns
. Head Returns
..
.
.de SeeAlso
. Head SeeAlso
..
.
.de I
\&\fI\\$*\fP
..
.
.de B
\&\fI\\$*\fP
..
.
.de CPP
. br
\&\\$*
. br
..
.
.
.so tmac.page
.
.ds bestSpace 1.5
.ds maxSpace 3.5\" fairly large cos of long names
.
.hw Word-Info File-Info lq-text
.
.ds ellipsis "\[ellipsis]
.
.\" *********** index stuff
.copye \n[.ev] ev.Index
.
.de IndexFunction \" Name Class Library File 
.\" first, remember the information
. if !'\\n%'\\*[@Page.\\$1]' \{.
.  tm may need to run troff again, \\$1 changed from p. \\*[@Page.\\$1] to \\n%
. \}
. ds @Page.\\$1 \\n%
. ds @Place.\\$1 \\n[nl]
. ds @Class.\\$1 "\\$2
. ds @File.\\$1 "\\$4
. ds @Lib.\\$1 "\\$3
. xwrite sqtroff.pagelist .ds @Page.\\$1 \\n%
. xwrite sqtroff.pagelist .ds @Place.\\$1 \\n[nl]
. xwrite sqtroff.pagelist .ds @Class.\\$1 "\\$2
. xwrite sqtroff.pagelist .ds @File.\\$1 "\\$4
. xwrite sqtroff.pagelist .ds @Lib.\\$1 "\\$3
..
.
.ds Xref.ThisIsEven.-1 "previous page
.ds Xref.ThisIsEven.1 "opposite
.ds Xref.ThisIsOdd.-1 "opposite
.ds Xref.ThisIsOdd.1 "overleaf
.
.\" Xref -- cross reference to a function
.de LastXref
. ds Xref.sep ".
. Xref \\$@
..
.
.ds Xref.sep ";
.
.de Xref \" .Xref [punct-before] name [punct-after]
. ie d @Page.\\$1 \{.
.  ds Xref.P \\*[@Page.\\$1]
.  ie '\\*[@Page.\\$1]'\\n%' \{.
.    ie (\\*[@Place.\\$1]>\\n[nl]) \{.
.      ds Xref.where "below
.    \}
.    el \{.
.      ds Xref.where "above
.    \}
.  \}
.  el \{. \" different pages
.   if e .ds Xref.This "Even
.   if o .ds Xref.This "Odd
.   nr _i \\*[Xref.P]-\\n%
.   ds _tmp Xref.ThisIs\\*[Xref.This].\\n[_i] 
.   ie d \\*[_tmp] \{.\" 1 page away
.     ds Xref.where "\\*[\\*[_tmp]]
.   \}
.   el \{.
.     ds Xref.where "p.\~\\*[Xref.P]
.   \}
.  \}
\&\\*[+fn]\\$1\\*[-fn]\s-4\u\^\[dd]\d\s+4 (\\*[Xref.where])\\*[Xref.sep]
. +FootNote \\$1
. set \<
. Font.FunctionName bold
\\$1
. set \>
\\*[@File.\\$1];
\\*[@Class.\\$1], p. \\*[@Page.\\$1]
. -FootNote
. \}
. el \{.
.  tm page \\n%: \\$4::\\*[F.Name]: unresolved xref to \\$1
\\$1 (undocumented);
. \}
. ds Xref.sep ";
..
.
.sy touch sqtroff.pagelist
.sy cp sqtroff.pagelist sqtroff.pagelist.bak
.so sqtroff.pagelist
.xopen sqtroff.pagelist wf sqtroff.pagelist
.\" .ctrace on
.\" .ntrace on
.\" .mtrace on
