.\" t
.
.\" use sqtbl % | troff -man
.
.\" $Id: lqaddfile.man,v 1.1 96/05/30 22:02:09 lee Exp $
.
.TH lqaddfile l "1.14, May 1996" "\(co Liam Quin 1989, 1993, 1996" "lq-text programs"
.
.SH NAME
lqaddfile \- add files to a lq-text full-text retrieval inverted index
.
.SH SYNOPSIS
.
.de oB
. RB [ \\$1 ]
..
.de oI
. RB [ \\$1
. IR "\\$2" ]
..
.B lqaddfile
.oI \-d dir
.oI \-M n
.oI \-W n
.oI \-H n
.oI \-w n
.oI \-f file
[
.I file
] .\|.\|.
.SH DESCRIPTION
Use
.I lqaddfile
to add files to an existing
.I lq-text
database, or to create a new database.
A database is a directory containing a configuration file, by default
named
.IR config.txt ,
and described in more detail in
.IR lqconfig "\^(5)."
A minimal configuration file is included in the
.I sample
directory in the
.I lq-text
source distribution;
An example might be:
.RS
#
.I "A list of places to search for files:"
.br
docpath "/a/place/docs/are/kept:/an/other/place"
.br
#
.I "A file containing words to ignore, one per line:"
.br
stoplist "/usr/lib/eign"
.br
#
.I "Don't index words shorter than this:"
.br
minwordlength 3
.br
#
.I "Truncate words longer than this:"
.br
maxwordlength 16
.br
#
.I "Enable the use of lqwordlist(1)"
.br
wordlist on
.br
#
.I "do not index content before the first <div> element in"
.br
#
.I "XML, HTML or other SGML files:"
.br
IgnoreHTMLhead "div"
.br
end
.RE
Each file whose name is given to
.I lqaddfile
is added to the index in the given directory.
If a file given to
.I lqaddfile
is not an absolute pathname, it is searched for along each entry
in
.I docpath
in turn.
In this way, if you are indexing a large number of files,
you can avoid storing a common prefix.
You can also move the files around later, as long as the component that
was stored (e.g. the actual filename) doesn't change.
.PP
The options (described below) for setting the cache size can make
a dramatic difference to the speed of
.IR lqaddfile .
The general idea is maximise the use of physical memory; if the system
starts frantic paging or swapping activity, use a smaller cache size
next time.
Unfortunately, there's no good way of determining this number in advance,
partly because it depends on the distribution of word frequencies in the
data, and partly because it's difficult to manage physical memory usage
on a virtual memory system such as Unix.
.
.PP
Note that while
.I lqaddfile
is running, the database is not accessible by other users.
.
.PP
For convenience, the
.I lqclean
shell script removes all of the files in a databse that are generated
by
.IR lqaddfile ,
and leaves other files intact.
.
.PP
You can interrupt
.I lqaddfile
with control-C, or by sending it
.SM SIGINT
with
.IR kill (1),
and this will make
.I lqaddfile
write out its current in-memory cache, and then exit.
This will leave the database in a clean state, although if
.I lqaddfile
had been part-way through reading a file, that file won't be completely
indexed.
If you send 
.I lqaddfile
four or more such signals,
it will quit immediately.
This generally leaves the database in a corrupt state.
.
.SH "OPTIONS"
All of the options documented in
.IR lq-text "\^(1)"
are interepreted by
.IR lqaddfile ,
although some of them apply only to matching rather than to indexing,
and are simply ignored.
.PP
The options specific to
.I lqaddfile
are given here:
.TP
.BI \-f \^file
Read the names of files to add to the index from
.IR file ;
the files should be one per line.
Blank lines are ignored, and a leading # is taken to start a comment.
As with all
.I lq-text
programs, a value of \- for a filename means that the program is
to read standard input; see below under
.SM EXAMPLES
for a typical use of this.
.TP
.BI \-H n
Set the size of the hash table to
.I n
slots;
each slot holds information about every occurrence of a single word.
.
.TP
.BI \-w n
Specifies the maximum total number of occurrences of words that can
be held in the cache's in-memory hash table at one time.
Typically, the overhead in memory is about 12 bytes for each occurrence,
with an additional 20\-30 bytes per slot, depending on the overhead
of
.IR malloc "\^(3)"
and on the length of the particular word.
.
.TP
.BI -W n
When
.I lqaddfile
writes out its cache, it tries to write out words that have not been
used in the most recent two files added, on the grounds that they are
less likely to be needed again; after that, it writes out the least
frequently occurring words, as this is a useful way to free a large number
of slots from the in-memory hash table.
Finally, the most common 30 or so words are written out, as this
typically frees a large amount of memory.
Although
.I lqaddfile
varies the order in which these strageies are used depending on which
resources are in most demand, it can be helpful to give it a hint.
The
.I -W
option indicates that a word is considered
.I infrequent
if there are fewer than
.I n
occurrences of it in the in-memory hash table.
A value of three or so is good for files with an average vocabulary
suitable for reading by old grey-haired professers in ancient
dust-covered libraries;
a value of 100 or higher might be appropriate for back-issues of the
.IR "Daily Scandal" .
The default is a yawnsome 50 occurrences.
.
.SH EXAMPLES
.RS
find reports memos \-type f \-print | \-lqaddfile \-w3000000 \-f \-
.RE
This adds all of the files in the
.I reports
or
.I memos
directories to the index.
The cache size is set about ten times higher than the default, but
no extra in-memory hash table slots are allocated, using
the default of 32768, so presumably each
word occurs an average of 1,000 times.
.PP
.RS
lqaddfile \-t Verbose \-w\s-1\&1000000\s0 \-H\s-1\&5000000\s0 \-W2 war+peace waverly
.RE
The two enormous text files
.I war+peace
and
.I waverly
are indexed.
Memory and hash slots are allocated, and the \-W option indicates that
many words will occur only one or two times in a million (the million
comes from the \-w option).
The
.B "\-t Verbose"
is a generic
.I lq-file
option that produces some user-level tracing.
.
.SH ENVIRONMENT
All the environment variables described in
.IR lq-text "\^(l)"
are understood; in particular,
.SM LQTEXTDIR
can contain the name of a directory containing the database to be used.
.PP
The
.SM TMPDIR
environment variable is used to determine where to place temporarily
uncompressed or unarchived copies of files in order to index them.
If not set, this defaults to
.IR /tmp .
This is only used for files that could not be read directly.
See also the note under
.SM BUGS
below.
.
.SH BUGS
.I lqaddfile
is supposed to recognise compressed, packed, gzip'd or archived files,
which it does; however, it is
.I also
supposed to unpack the files appropriately before indexing them,
which it does not.
If you give only the basename of a file, however,
.I lqaddfile
is more likely to do the right thing.
For example, if you want to index a file called
.I ichabod.Z
which is compressed, pass only the name
.I ichabod
to
.IR lqaddfile ,
without the
.I \&.Z
suffix.
In this case,
.I lqaddfile
will uncompress
.I ichabod.Z
into a temporary file and then index that.
.SH "SEE ALSO"
.IR lq-text "\^(l),"
.IR lq "\^(l),"
.IR lqkwic "\^(l),"
.IR lqwordlist "\^(l),"
.IR lqword "\^(l),"
.IR lqclean "\^(l),"
.IR lqfile "\^(l),"
.IR find "\^(l)."
.
.SH AUTHOR
Liam R. E. Quin, 1989, 1993, 1996
