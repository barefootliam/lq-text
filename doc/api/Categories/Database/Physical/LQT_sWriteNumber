<!DOCTYPE File PUBLIC "-//Liam Quin//DTD C API Documentation v1.1//EN" "doc.dtd"><File>
<Entry dir="h">
<Function File="h/numbers.h">
<Name>LQT_sWriteNumber</Name>
<Class>Database/Physical
<Purpose>
Writes a compressed binary representation of the given Number into
the given string.  The pointer pointed to by Sp is advanced to point
to the first unwritten byte of the buffer.
<Returns>
<LIST><LI>-1 if the string doesn't fit; in this case, the pointer
referred to by Sp will have been advanced by the amount of the number
that fitted;
<LI>Zero is returned if the number was written successfully.</LI>
</LIST>
<Notes>
<P>This function and the companion LQT_sReadNumber are central to the
operation of the lq-text database package.  If it were not for the
use of compressed numbers, the index would be too large to be useful.
</P>
<P>The function is designed to work best with small numbers; a number
less than 127 is written out in a single byte, for example, and a
number less than 16383 is written in two bytes.  For this reason,
LQT_sWriteNumber is most effectively used when writing a sorted
sequence of numbers, as then you can write only the difference between
successive values, saving space.  This form of delta coding is used
extensively by lq-text.
<SeeAlso>
LQT_sReadNumber
</Function>
<Decl>
INLINE int
LQT_sWriteNumber(Sp, Number, Base, Maxlen)
    unsigned char **Sp;
    unsigned long Number;
    unsigned char *Base;
    unsigned int Maxlen;
</Decl>
</Entry>
