<!DOCTYPE File PUBLIC "-//Liam Quin//DTD C API Documentation v1.1//EN" "doc.dtd"><File>
<Entry dir="liblqtext">
<Function File="liblqtext/pbcache.c">
<Name>LQT_BlockIsFree</Name>
<Class>Database/Physical
<Purpose>
<P>Determine the status of the block at a given byte offset from the
start of the data overflow file (data).
An external file, freelist, is kept in the database directory; this
file uses a single bit to represent the status of each block, either
in use or free.  If the freelist file is removed, subsequent
attempts to write to the database will fail.  Read-only
access will still work unless LQTRACE_READAFTERWRITE is set,
whereupon LQT_ReadBlock checks the status of each
block before returning it;
it is an error to attempt to read an unallocated block,
although this not normally checked, for performance reasons.</P>
<Returns>
Non-zero if the block is available, zero if it is free
<Notes>
<P>The first few blocks are reserved for storing information about
the database; they are marked as used automatically whenever a
database is created.</P>
<P>The freelist file can be rebuilt by the lqmkfreelist program.</P>
<P>The test program `free' contains examples of using the Block
Status functions LQT_BlockIsFree and LQT_SetBlockStatus.
It can also be used to edit the contents of the freelist file.</P>
<Errors>
Fatal error if the freelist file could not be opened
<SeeAlso>
LQT_SetBlockStatus
LQT_FindFreeBlock
LQT_ReadBlock
</Function>
<Decl>
API int
LQT_BlockIsFree(db, Offset)
    t_LQTEXT_Database *db;
    unsigned long Offset;
</Decl>
</Entry>
