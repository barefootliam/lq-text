<!DOCTYPE File PUBLIC "-//Liam Quin//DTD C API Documentation v1.1//EN" "doc.dtd"><File>
<Entry dir="liblqtext">
<Function File="liblqtext/rpblock.c">
<Name>LQT_GetpblockWhere</Name>
<Class>Database/Retrieval, Database/Update, Database/Physical
<Purpose>
Look up a word in the database...
and return a list of all the WordPlaces where it's found.
The AcceptFunc is called for each place as it is read off the
disk, with the given db, the WID and the new WordPlace as arguments.
If the
AcceptFunc returns a positive value, the WordPlace is accepted;
otherwise, it is not included in the returned t_pblock.  Note that
it is possible to end up with a pblock with no WordPlaces at all
if the AcceptFunc never returns a positive value.
An AcceptFunc of NULL is considered to return 1 in every case.
<Returns>
a freshly malloc'd t_pblock containing all of the WordPlaces from
the disk that the AcceptFunc accepted, and with NumberOfWordPlaces 
set to the number of such places.
<Notes>
Normally you would use LQT_MakeMatches instead of this function.
This function is used internally, and also by lq-text clients that
update the database efficiently.
<Errors>
Database format errors are nearly always fatal.
<SeeAlso>
LQT_MakeMatches
</Function>
<Decl>
API t_pblock *
LQT_GetpblockWhere(db, WordInfo, AcceptFunc)
    t_LQTEXT_Database *db;
    t_WordInfo *WordInfo;
    int (* AcceptFunc)(
#ifdef HAVE_PROTO
	t_LQTEXT_Database *,
	t_WID,
	t_WordPlace *
#endif
    );
</Decl>
</Entry>
