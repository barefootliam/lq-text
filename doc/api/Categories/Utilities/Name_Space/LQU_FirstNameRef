<!DOCTYPE File PUBLIC "-//Liam Quin//DTD C API Documentation v1.1//EN" "doc.dtd"><File>
<Entry dir="liblqutil">
<Function File="liblqutil/namespace.c">
<Name>LQU_FirstNameRef</Name>
<Class>Utilities/Name Space
<Purpose>
Used in conjunction with LQU_NextNameRef to iterate over all
of the Names in a Name Space.
<Returns>
A reference to the first Name in the given Name Space, if there are
any.  Use LQU_NameRefIsValid() to determine if the returned reference
is valid; if not, LQU_NameRefIsError will determine if there was
an error, and LQU_GetNameError will handle the error using Error().
<Example>
 *    	t_NameRef NameRef;
 *
 *    	for (
 *    	    NameRef = LQU_FirstNameRef(NameSpace);
 *    	    LQU_NameRefIsValid(NameSpace, NameRef);
 *    	    NameRef = LQU_NextNameRef(NameSpace, NameRef)
 *    	) {
 *    	    <I>now use the Name Reference:</I>
 *    	    printf("%s\n", LQU_GetNameFromNameRef(NameRef));
 *    	}
</Example>
<SeeAlso>
LQU_GetNameFromNameRef
LQU_GetTypeFromNameRef
LQU_NameRefIsValid
LQU_NameRefIsError
</Function>
<Decl>
API t_NameRef
LQU_FirstNameRef(NameSpace)
    t_NameSpace *NameSpace;
</Decl>
</Entry>
