<!DOCTYPE File PUBLIC "-//Liam Quin//DTD C API Documentation v1.1//EN" "doc.dtd"><File>
<Entry dir="liblqutil">
<Function File="liblqutil/isnzfile.c">
<Name>LQU_IsNonEmptyFile</Name>
<Class>Utilities/Files
<Purpose>
Determines whether the given Path names a regular file that contains
data.
In other words, the file must have the its stat st_mode's S_IFMT field
set to S_IFREG, and must also have a non-zero st_size field;
see the stat(2) man page.
<Returns>
Non-zero if and only if Path names a regular file of non-zero length
<Notes>
<P>There is tracing in here so that you can see which files are being
investigated by the calling program; tracing is available if
the liblqutil library was compiled with -DASCIITRACE; if so, you
can set the FindFile trace flag (LQTRACE_FINDFILE) to see tracing for
this routine.
The -t "FindFile|Verbose" command-line option will do this.</P>
<P>On systems that have the trace, strace or truss utility,
investigate using that instead.</P>
</Notes>
<SeeAlso>
LQU_IsFile
LQU_IsDir
</Function>
<Decl>
API int
LQU_IsNonEmptyFile(Path)
    CONST char *Path;
</Decl>
</Entry>
