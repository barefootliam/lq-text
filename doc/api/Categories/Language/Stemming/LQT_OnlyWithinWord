<!DOCTYPE File PUBLIC "-//Liam Quin//DTD C API Documentation v1.1//EN" "doc.dtd"><File>
<Entry dir="liblqtext">
<Function File="liblqtext/wordrule.c">
<Name>LQT_OnlyWithinWord</Name>
<Class>Language/Stemming
<Purpose>
<P>Returns non-zero only if the given character ch can appear within
a word but not at the start or end, and not repeated consecutively.
For English, an apostrophe (') is normally considered to be
the only such character; it's found in wouldn't, can't, and o'clock.
You could also include the hyphen if you
wanted, but it turns out to be best to index `match-box' as two
separate words with punctuation between them, rather than as
a single word.</P>
<P>This function is normally a macro declared in
the header file <h>wordrules.h</h> but can also be defined as a C
function is greater complexity is needed and the indexing speed loss
is not a concern.</P>
<Returns>
zero or non-zero.
<Bugs>
This routine is only sensible for English.
<SeeAlso>
LQT_StartsWord
</Function>
<Decl>
API int
LQT_OnlyWithinWord(db, ch)
    t_LQTEXT_Database *db;
    int ch;
</Decl>
</Entry>
