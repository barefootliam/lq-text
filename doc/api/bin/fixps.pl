#! /usr/bin/perl -w

$printerfontline = "";

while (<>) {
    if (/^%%Page/) {
	undef $pendingfontline;
	$printerfontline =  "";
	print;
	next;
    }

    chop($_);

    if (/ N$/) {
	$pendingfontline = $_;
	next;
    } elsif ($pendingfontline) {
	if ($pendingfontline ne $printerfontline) {
	    $printerfontline = $pendingfontline;
	    print "$pendingfontline\n";
	}
	undef $pendingfontline;
    }
    print "$_\n";
}
