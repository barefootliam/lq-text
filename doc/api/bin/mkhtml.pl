#! /usr/bin/perl -w

use strict;

# We want the output in this order:
my @WantedOrder = (
    "Entry", "Function", "Name", "Class", "Decl", "Returns",
    "Purpose", "Other"
);

# Strategy:
# We build up a hash of all the main sections we're interested in,
# and then process them in the order given by WantedOrder.
# Any remaining sections are put out at the end.


my $tmpFile = "/tmp/mkhtml.tmp";
open(TMPFILE, ">$tmpFile") || die("Can't create tmp file $tmpFile\n");

my $dir = "misc";
my $realFileName = "oops";

my $savedLineCount = 0;
my %savedLines = ();

sub saveStuff {
    my ($key,$value) = @_;
    if ($savedLines{$key}) {
	$savedLines{$key} .= "\n" . $value;
    } else {
	$savedLines{$key} = $value;
    }
}

my $inSeeAlso = 0;

while (<>) {
    if (/<!DOCTYPE/) {
	next
    }

    if (/<Name>(.*)<\/Name>/) {
	saveStuff("Name", $1);
	next;
    }

    if (/<Purpose>/) {
	$0 = "<h2 class=Purpose>Purpose</H2>";
    }

    if (/<Decl>/) {
	# $inDecl = 1;
	print TMPFILE "<pre class=\"decl\">\n";
	next;
    }

    if (/<\/Decl>/) {
	print TMPFILE "</pre>\n";
	# PrintSavedLines();
	# $inDecl = 0;
	next;
    }


    # put cross-references in
    if ($inSeeAlso) {
	if (/^[^a-zA-Z]/) {
	    $inSeeAlso = 0;
	} else {
	    s@\b([a-zA-Z][^,;]*)\b@<Xref>$1</Xref>@g;
	    s@</Xref>[^<]+<Xref>@</Xref><Xref>@g;
	}
    }

    if (/<Entry dir="([^"]+)"/) {
	$dir = $1;
    }

    if (/<H1 class=name>(.*)<\/H1>/) {
	$realFileName = $1;
    }

    if (/<Function File="(.*)">/) {
	$realFileName = $1;
    }


    # escape angle brackets in included files:
    #	#include <strings.h>
    # this should be done by mkdocfromc
    # Note: uses control-B for backslash so that it does not
    # get escaped later!
    s@&@\&amp;@g;

    # put tags on individual lines:
    s/></>\n</g;

    if (/<Decl>/ .. /\/Decl>/) {
	# within a Decl, use special fonts for function name:
	s@([A-Za-z][^ ]*)\(@<TT>$1</TT>(@;
    } elsif (/Purpose/ .. /<\/Function/) {
	# in the main body, use functions and small caps appropriately:
	# s@\b(LQ[A-Z]*_[a-zA-Z\\%0-9]*)\b@<tt>$1</tt>@g;
	# s@\b([A-Z][A-Z\\%][A-Z_0-9\\%]*[A-Z])\b@<tt>$1</tt>@g;
    }

    # newline before end list item:
    s@(</LI>)@\n$1@g;

    # remove troff hyphens!
    s@\\%@@g;

    if (/<Notes*>/) {
	print TMPFILE "<H2 class=\"notes\">Notes</H2>\n";
	next;
    }

    if (/<Class>/) {
	s@<Class> *@@i;
	s@ *</Class>@@i;
	s@ +$@@;
	print TMPFILE "<H2 class=\"category\">Class: $_</H2>\n";
	next;
    }

    # check for cross references last, so we miss the <SeeAlso> line:
    if (/<SeeAlso>/) {
	$inSeeAlso = 1;
	print TMPFILE "<H2 class=\"SeeAlso\">See Also</H2>\n";
	next;
    }

    print TMPFILE;
}

close(TMPFILE);

# rename the tmp file
if (!-d "html/$dir") {
    system("mkdir -p html/$dir");
}
system("mv $tmpFile html/${realFileName}.html");
