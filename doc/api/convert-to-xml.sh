#! /bin/bash

find Categories -type f -print |
    while read f
    do
	echo "processing $f"
	bn=`basename "$f"`
	dn=`dirname "$f"`

	mkdir -p "xml/$dn" || exit 1
	osx -c /usr/share/sgml/openjade-1.3.3/catalog  -D. -f/dev/null "$f" > "xml/$dn/${bn}.xml"
    done

