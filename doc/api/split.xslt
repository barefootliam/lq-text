<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet
  version="3.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns="http://www.w3.org/1999/xhtml"
  xmlns:array="http://www.w3.org/2005/xpath-functions/array"
  xmlns:file="http://expath.org/ns/file"
  xmlns:h="http://www.w3.org/1999/xhtml"
  xmlns:lq="http://www.barefootliam.org/"
  xmlns:map="http://www.w3.org/2005/xpath-functions/map"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  exclude-result-prefixes="file map xsl lq"
  expand-text="yes"
  >

  <!--* Make a Web version of the lq-text API documentation.
      * Requires an XSLT 3 engine such as Saxon 11 or later.
      *
      * First run ./mkhtml
      * Then ./convert-to-xml.sh
      * Then e.g. saxon-ee split.xslt split.xslt
      * Output will be in the "html" folder.
      *
      *-->

  <xsl:output method="html" indent="no"
    html-version="5" cdata-section-elements="script" />

  <xsl:variable name="functions-to-filenames" as="map(xs:string, xs:string)">
    <xsl:variable name="keys" as="element(lq:fn)*">
      <xsl:for-each select="uri-collection('xml?recurse=yes;select=*.xml')">
	<xsl:variable name="filename" select="." />
	<xsl:for-each select="doc($filename)//FUNCTION">
	  <lq:fn name="{NAME}" file="{replace($filename, '^.*/Categories/', 'Categories/')}" />
	</xsl:for-each>
      </xsl:for-each>
    </xsl:variable>
    <xsl:map>
      <xsl:for-each select="1 to count($keys)">
	<xsl:variable name="dot" as="xs:integer" select="xs:integer(.)" />
	<xsl:variable name="me" select="$keys[$dot]" as="element(lq:fn)"/>
	<xsl:if test="not($keys[position() lt $dot]/@name = $me/@name)">
	  <!--*
	  <xsl:message select="'name ' || $me/@name || ' maps to ' || $me/@file"/>
	  *-->
	  <xsl:map-entry key="string($me/@name)" select="replace(string($me/@file), 'xml', 'html')" />
	</xsl:if>
      </xsl:for-each>
    </xsl:map>
  </xsl:variable>

  <xsl:variable name="category-list" as="xs:string*" select="
      ('Overview',
      sort(
	distinct-values(
	    uri-collection('xml?recurse=yes;select=*.xml') ! doc(.) ! .//CLASS ! tokenize(., '\s*[,&#xa;]\s*') !
	    replace(., ',', '')
	    )
      )
      )"/>

  <xsl:variable name="category-to-docs" as="map(*)">
    <xsl:variable name="keys" as="element(lq:fn)*">
      <xsl:for-each select="uri-collection('xml?recurse=yes;select=*.xml')">
	<xsl:variable name="filename" select="." />
	<xsl:for-each select="doc($filename)//FUNCTION">
	  <xsl:variable name="F" select="." as="element(FUNCTION)" />
	  <xsl:for-each select="tokenize($F/CLASS, '\s*[,&#xa;]\s*')">
	    <!--* an fn element for each class (category) value: *-->
	    <lq:fn name="{$F/NAME}" file="{$filename}" class="{.}" />
	  </xsl:for-each>
	</xsl:for-each>
      </xsl:for-each>
    </xsl:variable>
    <xsl:map>
      <xsl:for-each select="distinct-values($keys/@class)">
	<xsl:variable name="this" select="." />
	<xsl:map-entry key="." select="$keys[@class = $this]" />
      </xsl:for-each>
    </xsl:map>
  </xsl:variable>

  <xsl:template match="/">
    <xsl:for-each select="uri-collection('xml?recurse=yes;select=*.xml')">
      <xsl:variable name="mainfilename" select="replace(., 'xml', 'html')" />

      <xsl:for-each select="doc(.)/FILE/ENTRY"> <!--* there can be more than one ENTRY in a file *-->
	<xsl:variable name="entry" select="." />
	<xsl:variable name="filename" select="replace($mainfilename, '/[^/]*$', '/' || FUNCTION/NAME || '.html')" />
	<xsl:for-each select="tokenize(FUNCTION/CLASS, '\s*[,&#xa;]\s*')">
	  <xsl:variable name="thisfile" select="replace(
	      $filename,
	      'Categories/.*/([^/]+)$',
	      'Categories/' || . || '/$1'
	    )" />
	  <!--*
	  <xsl:message select=" 'write to ' || $thisfile "/>
	  *-->
	  <xsl:result-document href="{translate($thisfile, ' ', '_')}">
	    <xsl:apply-templates select="$entry"/>
	  </xsl:result-document>
	</xsl:for-each>
      </xsl:for-each>
    </xsl:for-each>

    <xsl:for-each select="$category-list">
      <xsl:variable name="fn" select="translate(., ' ', '_')" />
      <xsl:variable name="overview" select="('Overviews/Categories/' || $fn || '/Overview')"/>
      <xsl:choose>
	<xsl:when test="unparsed-text-available($overview)">
	  <xsl:variable name="overview-raw" select="unparsed-text($overview)" />
	  <xsl:result-document href="html/Categories/{$fn}/index.html">
	    <html>
	      <head>
		<title>lq-text: {.}</title>
		<link rel="preconnect" href="https://fonts.googleapis.com" />
		<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin="crossorigin" />
		<link href="https://fonts.googleapis.com/css2?family=Merriweather+Sans&amp;family=Merriweather:ital,wght@0,400;0,700;1,400&amp;display=swap" rel="stylesheet" />
		<xsl:sequence select="$css" />
	      </head>
	      <body>
		<h1>{.}</h1>
		<nav class="leftbit">
		  <xsl:if test="not($category-to-docs(.))">
		    <xsl:message select=" '    no category record for ' || ." />
		  </xsl:if>
		  <xsl:where-populated>
		    <div class="inthiscategory">
		      <xsl:on-non-empty>
			<p>In this category:</p>
		      </xsl:on-non-empty>
		      <xsl:where-populated>
			<ul>
			  <xsl:for-each select="$category-to-docs(.)">
			    <li><a class="noul" href="{
			      lq:replace-all(@file, (
				['^.*/', ''],
				  [ 'xml', 'html'],
				  [ ' ', '_' ]
				)
			      )
			      }">{@name}</a></li>
			  </xsl:for-each>
			</ul>
		      </xsl:where-populated>
		    </div>
		  </xsl:where-populated>

		  <xsl:variable name="to-top" select="lq:replace-all(current-output-uri(), (
		    [ ' ', '_' ],
		    [ '/[^/]+$', '/' ],
		    [ '^.*Categories/', '' ],
		    [ '[^/]+/', '../' ]
		  )) "/>

		  <div class="categories">
		    <p>All Categories:</p>
		    <ul>
		      <xsl:for-each select="$category-list">
			<li>
			  <a class="noul" href="{$to-top}{translate(., ' ', '_')}">{.}</a>
			</li>
		      </xsl:for-each>
		    </ul>
		  </div>
		</nav>

		<main>
		  <p><xsl:sequence select="lq:troff-to-html($overview-raw, current-output-uri())" /></p>
		</main>
	      </body>
	    </html>
	  </xsl:result-document>

	</xsl:when>
	<xsl:otherwise>
	  <xsl:message select="'no overview for ' || ." />
	  <xsl:message>could not read {$overview}</xsl:message>
	</xsl:otherwise>
      </xsl:choose>
    </xsl:for-each>
  </xsl:template>

  <xsl:function name="lq:replace-all" as="xs:string">
    <xsl:param name="input" as="xs:string" />
    <xsl:param name="replacements" as="array(*)*" />

    <xsl:sequence select="
      fold-left($replacements, $input, 
	function($input, $replacement) {
	    replace(
	      $input,
	      $replacement(1),
	      $replacement(2),
	      if (array:size($replacement) ge 3)
	      then $replacement(3)
	      else 'm'
	    )
	}
      )" />
    </xsl:function>

  <xsl:mode name="overview"
    on-no-match="shallow-copy" />

  <xsl:function name="lq:troff-to-html" as="node()*">
    <xsl:param name="input" as="xs:string"/>
    <xsl:param name="output-uri" as="xs:string"/>

      <!--* now the pairs, inside to outside. *-->

    <!--* i have not tried to turn .P into a p element,
        * as there isn't a ./P except in the overview.
	*-->
    <xsl:variable name="overview-str" as="xs:string" select="
        '&lt;h:div xmlns:h=&quot;http://www.w3.org/1999/xhtml&quot;>' ||
	lq:replace-all($input, (
	      ['\\[&amp;%]', '&#xad;'], (: \% was line break point with no hyphen :)
	      ['^\.tm.*?$', ''],
	      ['^\./?Overview.*?$', ''],
	      ['^\. *P *$(.*?)^\. */P *$', '&lt;p>$1&lt;/p>' ],
	      ['^\. *P', '&lt;br />'],
	      ['^\. */P', '&lt;br />'],
	      ['^\. *$', ''],

	      [ '^\.I\s+(.+)$', '&lt;i>$1&lt;/i>' ],
	      (: lists :)
	      ['^\. *LI(.*?)^\. */LI', '&lt;li>$1&lt;/li>', 'sm'],
	      ['^\.List(.*?)\./List', '&lt;ul>$1&lt;/ul>', 'sm'],

	      (:  \*[+c]README\*[-c] :)
	      ['\\\*\[\+c\]([^\\]+)\\\*\[-c\]', '&lt;h:span class=&quot;code&quot;>$1&lt;/h:span>'],

	      ['\\\*\[\+h\]([^\\]+)\\\*\[-h\]', '&lt;h:span class=&quot;header&quot;>$1&lt;/h:span>'],

	      ['\\\*\[\+fn\]([^\\]+)\\\*\[-fn\]', '&lt;h:span class=&quot;fn&quot;>$1&lt;/h:span>'],
	      ['\\\*\[\+sc\]([^\\]+)\\\*\[-sc\]', '&lt;h:span class=&quot;sc&quot;>$1&lt;/h:span>'],
	      ['\\\*\[\+var\]([^\\]+)\\\*\[-var\]', '&lt;h:span class=&quot;var&quot;>$1&lt;/h:span>'],

	      ['\\f\[I\]*([^\\]+)\\fP', '&lt;i>$1&lt;/i>']
	    )
	) || '&lt;/h:div>'
      " />
      <xsl:variable name="overview" select="parse-xml-fragment($overview-str)" as="node()*" />

      <xsl:apply-templates mode="overview" select="$overview">
	<xsl:with-param name="output-uri" select="$output-uri" tunnel="yes" />
      </xsl:apply-templates>
  </xsl:function>

  <xsl:template mode="overview" match="text()">
    <xsl:param name="output-uri" tunnel="yes" as="xs:string" />

    <xsl:sequence select="lq:add-links-to-text(., $output-uri)" />
  </xsl:template>

  <xsl:template match="*">
    <xsl:message terminate="yes" select="'unmatched element ' || name()" />
  </xsl:template>

  <xsl:template match="FILE">
    <xsl:apply-templates />
  </xsl:template>

  <xsl:template match="FILE/ENTRY">
    <!--* top-level element *-->
    <html>
      <xsl:call-template name="output.head" />
      <xsl:call-template name="output.body" />
    </html>
  </xsl:template>

  <xsl:variable name="css" expand-text="no" as="element(h:style)">
    <style xsl:expand-text="no">
      html {
	font-family: "Merriweather", serif;
	line-height: 1.5;
      }

      body {
	display: grid;
	grid-template-columns: 1fr 3fr;
      }

      nav.leftbit {
	background-color: wheat;
	min-height: 70vh;
	max-width: 50em;
	border-right: 1px solid black;
	grid-column-start: 1;
	padding-left: 0.5rem;
      }

      main {
	grid-column-start: 2;
	margin-left: 2em;
	margin-right: 1em;
      }

      h1 {
	/* span both columns */
	grid-column-start: 1;
	grid-column-end: 3;
      }

      h2 {
	font-size: 1rem;
	margin-bottom: 0;
      }

      .decl {
	width: content;
	margin-right: 1em;
	font-size: 1.2rem;
	white-space: pre-wrap;
      }

      p {
	margin-top: 0;
      }

      ul {
	margin-top: 0;
      }

      span.headerfile {
	font-style: italic;
      }

      a.noul {
	text-decoration: none;
      }

      a.funcref {
	font-style: italic;
      }

      span.funcdeclname {
	color: purple;
	white-space: normal;
	font-weight: bolder;
      }

      span.fn {
	color: purple;
	font-style: italic;
      }

      span.code {
	font-family: "Merriweather Sans";
	font-size: 90%;
      }

      span.h {
	/* header */
	font-style: italic;
      }

      span.sc {
	font-variant: small-caps;
      }

      span.var {
	font-family: "Merriweather Sans";
	font-size: 90%;
      }

    </style>
  </xsl:variable>

  <xsl:template name="output.head">
    <head>
      <title>lq-text: {.//NAME[1]}</title>
      <link rel="preconnect" href="https://fonts.googleapis.com" />
      <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin="crossorigin" />
      <link href="https://fonts.googleapis.com/css2?family=Merriweather+Sans&amp;family=Merriweather:ital,wght@0,400;0,700;1,400&amp;display=swap" rel="stylesheet" />
      <xsl:sequence select="$css" />
    </head>
  </xsl:template>

  <xsl:variable name="sortkeys" as="map(xs:string, xs:integer)"
    select="
      map {
	'NAME' : 1,
	'CLASS' : 2,
	'DECL' : 3,
	'PURPOSE' : 4,
	'RETURNS' : 5,
	'ERRORS' : 6,
	'RESTRICTIONS' : 7,
	'BUGS' : 8,
	'SEEALSO' : 9
      }
  " />


  <xsl:template name="output.body">
    <body>
      <h1>lq-text: {FUNCTION/NAME}</h1>
      <xsl:call-template name="output.leftbit"/>
      <main>
	<xsl:apply-templates select="(FUNCTION/*,DECL) except FUNCTION/(NAME|SEEALSO)">
	  <xsl:sort
	    order="ascending"
	    data-type="number"
	    select="if ($sortkeys(name())) then $sortkeys(name()) else 999" />
	</xsl:apply-templates>
      </main>
    </body>
  </xsl:template>

  <xsl:template match="NAME">
    <h1>lq-text: {.}</h1>
  </xsl:template>

  <xsl:template name="output.leftbit">
    <xsl:variable name="to-top" select="lq:replace-all(current-output-uri(), (
      [ ' ', '_' ],
      [ '/[^/]+$', '/' ],
      [ '^.*Categories/', '' ],
      [ '[^/]+/', '../' ]
    )) "/>

    <nav class="leftbit">
      <p>File:</p>
      <ul>
	<li><xsl:value-of select="ancestor-or-self::ENTRY//FUNCTION/@FILE" /></li>
      </ul>
      <p>Category:</p>
      <ul class="class">
	<xsl:for-each select="tokenize(ancestor-or-self::ENTRY//CLASS, ',\s*')">
	  <li>
	    <a class="noul" href="{$to-top}{translate(., ' ', '+')}">{.}</a>
	  </li>
	</xsl:for-each>
      </ul>
      <xsl:where-populated>
	<div class="seealso">
	  <xsl:on-non-empty>
	    <p>See Also:</p>
	  </xsl:on-non-empty>
	  <xsl:where-populated>
	    <ul>
	      <xsl:for-each select="tokenize(ancestor-or-self::ENTRY//SEEALSO, '\s+')">
		<li>
		  <xsl:sequence select="lq:make-function-link(., current-output-uri())" />
		</li>
	      </xsl:for-each>
	    </ul>
	  </xsl:where-populated>
	</div>
      </xsl:where-populated>
      <div class="categories">
	<p>All Categories:</p>
	<ul>
	  <xsl:for-each select="$category-list">
	    <li>
	      <a class="noul" href="{$to-top}{translate(., ' ', '_')}">{.}</a>
	    </li>
	  </xsl:for-each>
	</ul>
      </div>
    </nav>
  </xsl:template>

  <xsl:function name="lq:make-function-link">
    <xsl:param name="name" as="xs:string" />
    <xsl:param name="here" as="xs:string" />
    <xsl:variable name="here2" select="replace($here, '^.*Categories/', 'Categories/')" />
    <xsl:variable name="upness" select="lq:replace-all(
      $here2, (
	['///*', '/' ],
	['/[^/]+$', '/' ],
	['[^/]+/', '../']
      )
    )"/>

    <xsl:variable name="remote" select="$functions-to-filenames($name)" />
    <xsl:choose>
      <xsl:when test="$remote">
	<a class="noul" href="{$upness}{translate($remote, ' ', '_')}">{$name}</a>
	<!--* 
	<xsl:message select="'MFL ' || $name || ' ⇒ ' || $remote" />
	*-->
      </xsl:when>
      <xsl:otherwise>
	<xsl:text>{$name}</xsl:text>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:function>

  <xsl:function name="lq:relative-link-from-to">
    <xsl:param name="from-uri" as="xs:string" />
    <xsl:param name="target-uri" as="xs:string" />

    <xsl:variable name="from-words" as="xs:string*"
	      select="tokenize($from-uri, '/')" />

    <xsl:variable name="to-words" as="xs:string*"
	      select="tokenize($target-uri, '/')" />

    <!--* if from is a b c d.html and to is a b e f.html, we need ../../e/f.html
        *-->
    <xsl:choose>
      <xsl:when test="count($to-words) le 1">
      <!--* simple case, to="ankles.html in same folder. This is unlikely;
          * usually we will get e.g. file://full/path/to/ankles.html as input.
	  *-->
	<xsl:sequence select="$target-uri" />
      </xsl:when>
      <xsl:otherwise>
	<xsl:sequence select="$target-uri" />
      </xsl:otherwise>
    </xsl:choose>

    <!--*
    <xsl:sequence select="lq:remove-common-prefixes-from($from-uri, $target-uri)" />
      *-->
  </xsl:function>

  <xsl:template match="CLASS" />
  <xsl:template match="SEEALSO" />

  <xsl:template match="DECL">
    <div class="decl">
      <xsl:if test="matches(., '^\s*VARARGS')">
	<xsl:text>/*</xsl:text>
      </xsl:if>
      <xsl:apply-templates />
    </div>
  </xsl:template>

  <xsl:template match="DECL/text()">
    <xsl:analyze-string select="." regex="^[A-Z][A-Za-z0-9_]*[(]" flags="m">
      <xsl:matching-substring>
	<span class='funcdeclname'>
	  {replace(., '[(]', '')}
	</span>
	<xsl:value-of select="'('" />
      </xsl:matching-substring>
      <xsl:non-matching-substring>
	<xsl:value-of select="." />
      </xsl:non-matching-substring>
    </xsl:analyze-string>
  </xsl:template>


  <xsl:template match="PURPOSE">
    <div class="purpose">
      <h2>Purpose</h2>
      <xsl:apply-templates/>
    </div>
  </xsl:template>

  <xsl:template match="NOTES">
    <div class="notes">
      <h2>Notes</h2>
      <xsl:apply-templates/>
    </div>
  </xsl:template>

  <xsl:template match="EXAMPLE">
    <div class="example">
      <h2>Example</h2>
      <xsl:apply-templates/>
    </div>
  </xsl:template>

  <xsl:template match="H">
    <span class="headerfile">
      <xsl:text>&lt;</xsl:text>
      <xsl:apply-templates/>
      <xsl:text>&gt;</xsl:text>
    </span>
  </xsl:template>

  <xsl:template match="I">
    <i class="i">
      <xsl:apply-templates/>
    </i>
  </xsl:template>

  <xsl:template match="E">
    <em>
      <xsl:apply-templates/>
    </em>
  </xsl:template>

  <xsl:template match="LIST">
    <ul>
      <xsl:apply-templates/>
    </ul>
  </xsl:template>

  <xsl:template match="VAR">
    <i class="var">
      <xsl:apply-templates/>
    </i>
  </xsl:template>

  <xsl:template match="LI|P">
    <xsl:element name="{lower-case(name())}">
      <xsl:apply-templates/>
    </xsl:element>
  </xsl:template>

  <xsl:template match="RETURNS">
    <div class="returns">
      <h2>Returns</h2>
      <p>
	<xsl:apply-templates/>
      </p>
    </div>
  </xsl:template>

  <xsl:template match="ERRORS">
    <div class="errors">
      <h2>Errors</h2>
      <xsl:apply-templates/>
    </div>
  </xsl:template>

  <xsl:template match="RESTRICTIONS">
    <div class="errors">
      <h2>Restrictions</h2>
      <xsl:apply-templates/>
    </div>
  </xsl:template>

  <xsl:template match="BUGS">
    <div class="bugs">
      <h2>Bugs</h2>
      <xsl:apply-templates/>
    </div>
  </xsl:template>

  <xsl:function name="lq:add-links-to-text" as="node()*">
    <xsl:param name="input" as="xs:string" />
    <xsl:param name="from" as="xs:string" />

    <xsl:variable name="here" select="replace($from, '^.*Categories/', 'Categories/')" />
    <xsl:variable name="upness" select="lq:replace-all(
      $here, (
	['///*', '/' ],
	['/[^/]+$', '/' ],
	['[^/]+/', '../']
      )
    )"/>

    <xsl:analyze-string
      select="replace($input, '&#xAD;', '')"
      regex="[A-Za-z\d_]+">

      <xsl:matching-substring>
	<xsl:choose>
	  <xsl:when test="$functions-to-filenames(.)">
	    <!--*
	    <xsl:message select="'in ' || $here || ', upness is ' || $upness || ', and v is ' || $functions-to-filenames(.)"/>
	    *-->

	    <a href="{$upness}{$functions-to-filenames(.) ! translate(., ' ', '_')}" class="noul funcref">
	      <xsl:value-of select="replace(., '([a-z])([A-Z])', '$1&#xAD;$2')" />
	    </a>
	  </xsl:when>
	  <xsl:otherwise>
	    <xsl:value-of select="." />
	  </xsl:otherwise>
	</xsl:choose>
      </xsl:matching-substring>

      <xsl:non-matching-substring>
	<xsl:value-of select="." />
      </xsl:non-matching-substring>

    </xsl:analyze-string>
  </xsl:function>

  <xsl:template match="(PURPOSE|RETURNS|NOTES|BUGS|P|LI)//text()">
    <xsl:sequence select="lq:add-links-to-text(., current-output-uri())" />
  </xsl:template>


</xsl:stylesheet>
<!--*
vi:sw=2:
  *-->


