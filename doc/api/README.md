# lq-text C API documentation

The output is now generated in HTML with XSLT (use Saxon 9 or later),
rather than in PDF by _sqtroff_. See notes near the start of _split.xsl_
for running it.

The documentation uses SGML markup before the declaration of each
function in the C files (this predates doxygen).



