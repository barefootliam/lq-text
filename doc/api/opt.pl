#! /usr/local/bin/perl -w

# improve the PostScript that sqdps generates...

# syntax:
# 0 174 rm -> rmoveto
# 0 -66 rm -> should be coalesced with previous
# 1091 hm -> absolute horizontal position, could be
# 112 rv 1091 hm

# the preamble
while (<>) {
    if (/^%%EndProlog/) {
	print "/rv{0 exch rmoveto}B %added by sqpsopt.pl\n";
	print "/rh{0 rmoveto}B %added by sqpsopt.pl\n";
	print;
	last;
    }
    print;
}

$rvmot = 0;
$rhmot = 0;
undef $hpos;

# the main text
while (<>) {
    if (/^%%Page:/) {
	$rvmot = 0;
	$rhmot = 0;
	undef $hpos;
    }

    if (/^(-?[0-9]+) (-?[0-9]+) rm$/) {
	$rhmot += $1;
	$rvmot += $2;
    } elsif (/^(-?[0-9]+) hm/) {
	$hpos = $1 + $rhmot;
	$rhmot = 0;
    } elsif (/ N$/) {
	$fontLine = $_;
    } else {
	if ($fontLine) {
	    print "$fontLine";
	    undef $fontLine;
	}

	if ($hpos) {
	    $hpos += $rhmot;
	    $rhmot = 0;
	    if ($rvmot != 0) {
		print "${rvmot} rv\n";
	    }
	    print "${hpos} hm\n";
	} elsif ($rhmot != 0 || $rvmot != 0) {
	    if ($rvmot != 0) {
		if ($rhmot != 0) {
		    print "$rhmot $rvmot rm\n";
		} else {
		    print "$rvmot rv\n";
		}
	    } else { # rvmot 0, rhmot != 0
		print "$rhmot rh\n";
	    }
	}
	undef $hpos;
	$rvmot = 0;
	$rhmot = 0;
	print;
    }
}
