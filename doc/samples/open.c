/* $Id: open.c,v 1.2 95/10/08 21:32:52 lee Exp $
 * Liam R. E. Quin, 1995; This file is in the public domain.
 */

#include <stdio.h>
#include <lqtext.h>

static t_LQTEXT_Database *db;

int
main(argc, argv)
    int argc;
    char *argv[];
{
    t_lqdbOptions *Options;

    /* process command-line options to determine where
     * the database should reside:
     */
    Options = LQT_InitFromArgv(argc, argv);

    /* open the database: */
    db = LQT_OpenDatabase(Options, O_RDONLY, 0);
    if (!db) {
	/* Currently it's a fatal error if the database is not
	 * opened successfully, so this is never reached, but
	 * perhaps it could be in a future release....
	 */
	Error(E_FATAL,
	    "couldn't open database %s", LQT_GetOption(Options, "directory")
	);
    }

    /* use db */

    printf("opened lq-text database %s\n",
	LQT_GetOption(Options, "directory")
    );

    /* . . . */

    /* close the database */
    LQT_CloseDatabase(db);

    /* return to the operating system... */
    return 0;
}
