/* $Id: open.c,v 1.2 95/10/08 21:32:52 lee Exp $
 * Liam R. E. Quin, 1995; This file is in the public domain.
 *
 * getfile.c -- find a file
 */

#include <stdio.h>
#include <lqtext.h>

static t_LQTEXT_Database *db;

int
main(argc, argv)
    int argc;
    char *argv[];
{
    t_lqdbOptions *Options;
    Options = LQT_InitFromArgv(argc, argv);

    db = LQT_OpenDatabase(Options, O_RDONLY, 0);

    while (--argc > 0) {
	t_FID theFID;

	++argv;

	theFID = LQT_NameToFID(db, *argv);
	if (theFID) {
	    t_FileInfo *FileInfo;

	    FileInfo = LQT_FIDToFileInfo(db, theFID);
	    if (FileInfo) {
		printf("%s -> FID %ld -> %s as %s\n",
		    *argv,
		    theFID,
		    FileInfo->Name,
		    LQT_FindFile(db, *argv)
		);
		LQT_DestroyFileInfo(db, FileInfo);
	    } else {
		Error(E_WARN|E_INTERNAL, "FID %d has no FileInfo!?", theFID);
	    }
	} else {
	    Error(E_WARN, "%s: file is not indexed in database %s",
		*argv,
		LQT_GetOption(Options, "directory")
	    );
	}
    }

    LQT_CloseDatabase(db);

    /* return to the operating system... */
    return 0;
}
