/* $Id: open.c,v 1.2 95/10/08 21:32:52 lee Exp $
 * Liam R. E. Quin, 1995; This file is in the public domain.
 */

#include <stdio.h>
#include <lqtext.h>

static t_LQTEXT_Database *db;

int
main(argc, argv)
    int argc;
    char *argv[];
{
    t_lqdbOptions *Options;
    Options = LQT_InitFromArgv(argc, argv);

    db = LQT_OpenDatabase(Options, O_RDONLY, 0);

    while (--argc > 0) {
	t_Phrase *onePhrase;

	++argv;
	
	onePhrase = LQT_StringToPhrase(db, argv[0]);

	if (onePhrase && LQT_NumberOfWordsInPhrase(db, onePhrase) > 0) {
	    /* We will print the matches, but reject them -- i.e.,
	     * we won't keep them in memory:
	     */
	    LQT_MakeMatchesWhere(db, onePhrase, LQT_PrintAndRejectOneMatch);
	}

	if (onePhrase) { /* reclaim storage */
	    LQT_DestroyPhrase(db, onePhrase);
	}
    }

    LQT_CloseDatabase(db);

    /* return to the operating system... */
    return 0;
}
