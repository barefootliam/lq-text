#include <sys/types.h>
#include <stdio.h>
#include <db.h>

main(argc, argv)
    int argc;
    char *argv[];
{
    DB *dbp;
    DBT key, data;
    int ret;

    if ((ret = db_create(&dbp, NULL, 0)) != 0) {
	fprintf(stderr, "%s: db_create: %s\n", argv[0], db_strerror(ret));
	exit(1);
    }

    if ((ret = dbp->open(
		    dbp, "/tmp/db.db", NULL, DB_HASH, DB_CREATE, 0664)) != 0) {
	dbp->err(dbp, ret, "%s", "/tmp/db.db");
	exit(1);
    }

    memset(&key, 0, sizeof(key));
    memset(&data, 0, sizeof(data));
    key.data = "boy";
    key.size = sizeof("boy");
    data.data = "Simon";
    data.size = sizeof("Simon");

    if ((ret = dbp->put(dbp, NULL, &key, &data, 0)) == 0) {
	printf("db: %s: key stored\n", (char *) key.data);
    } else {
	dbp->err(dbp, ret, "DBP->put");
	exit(1);
    }

    if ((ret = dbp->get(dbp, NULL, &key, &data, 0)) == 0) {
	printf("db: %s: key found, data was \"%s\"\n",
		(char *) key.data, (char *) data.data);
    } else {
	dbp->err(dbp, ret, "DBP->get");
	exit(1);
    }

    if ((ret = dbp->close(dbp, 0)) != 0) {
	dbp->err(dbp, ret, "close");
	exit(1);
    }
    exit(0);
}
