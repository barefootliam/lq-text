#!/usr/local/bin/mawk

# E new 1 now 2 for appropriate
# E new 2 now 1 for elj2
# E new 3 now 1 for debugged
# 1 2   3 4   5  6  7

/new/ {
    if ($7 in WordCounts) {
	printf "%d: WID %d %s: last was %d, but marked as new with %d\n",
		NR, $3, $7, WordCounts[$7], $5 | "cat 1>&2"
    }
    WordCounts[$7] = $5
    next
}

/update/ {
    if ($7 in WordCounts) {
	if (WordCounts[$7] != $5) {
	    printf "%d: WID %d %s: last was %d, now %d\n",
		    NR, $3, $7, WordCounts[$7], $5 | "cat 1>&2"
	}
    } else {
	printf "%d: WID %d %s: updating to %d but had no entry",
		NR, $3, $7, $5 | "cat 1>&2"
    }
    WordCounts[$7] = $5
}
