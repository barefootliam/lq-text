/* $Id: charfreq.c,v 1.1 2019/04/21 06:14:20 lee Exp $ */

#include <stdio.h>
#include <malloc.h>

typedef struct s_fby {
    int MyCount;
    short Allocated;
    char *CharsAfter;
    struct s_fby *Things;
} t_fby;

t_fby Empty = {
    0,
};

t_fby Root = {
    0,
};

t_fby *
AddChar(Node, c)
    t_fby *Node;
    int c;
{
    int i;

    if (Node->Allocated == 0) {
	Node->Allocated = 4;
	Node->CharsAfter = malloc(4);
	Node->Things = malloc(sizeof(t_fby) * 4);
	Node->Things[0] = Empty;

	Node->CharsAfter[0] = c;
	Node->CharsAfter[1] = '\0';
	Node->CharsAfter[2] = '\0';
	Node->CharsAfter[3] = '\0';
	i = 0;
    }

    for (i = 0; i < Node->Allocated; i++) {
	if (Node->CharsAfter[i] == c) {
	    Node->Things[i].MyCount++;
	    return &(Node->Things[i]);
	} else if (Node->CharsAfter[i] == '\0') {
	    break;
	}
    }

    if (Node->CharsAfter[Node->Allocated - 1] != '\0') {
	int j;

	Node->CharsAfter = realloc(Node->CharsAfter, Node->Allocated + 4);
	for (j = Node->Allocated; j < Node->Allocated + 4; j++) {
	    Node->CharsAfter[j] = '\0';
	}
	Node->Allocated += 4;
	Node->Things = realloc(Node->Things, Node->Allocated*sizeof(t_fby));
	Node->Things[i] = Empty;
    }

    Node->CharsAfter[i] = c;
    Node->Things[i] = Empty;
    Node->Things[i].MyCount = 1;
    return &Node->Things[i];
}

t_fby *
AddTriplet(c1, c2, c3)
    int c1, c2, c3;
{
    t_fby *fp;
    int i;

    fp = &Root;
    fp = AddChar(fp, c1);
    if (c2) {
	fp = AddChar(fp, c2);
	if (c3) {
	    (void) AddChar(fp, c3);
	}
    }
}

void
PrintNode(Node)
    t_fby *Node;
{
    int i;

    for (i = 0; i < Node->Allocated; i++) {
	t_fby *Child;
	int j;

	if (Node->CharsAfter[i] == '\0') {
	    break;
	} else if (Node->CharsAfter[i] == '\n') {
	    continue;
	}

	Child = &(Node->Things[i]);
	for (j = 0; j < Node->Allocated; j++) {
	    t_fby *GrandKid;
	    int k;

	    if (!Child->Things) {
		continue;
	    } else if (Child->CharsAfter[j] == '\0') {
		break;
	    } else if (Child->CharsAfter[j] == '\n') {
		continue;
	    }

	    GrandKid = &(Child->Things[j]);
	    for (k = 0; k < Node->Allocated; k++) {
		if (!GrandKid->Things) {
		    break;
		} else if (GrandKid->CharsAfter[k] == '\0') {
		    break;
		} else if (GrandKid->CharsAfter[k] == '\n') {
		    continue;
		}

		printf("%c%c%c %d\n",
		    GrandKid->CharsAfter[k],
		    Child->CharsAfter[j],
		    Node->CharsAfter[i],
		    GrandKid->Things[k].MyCount
		);
	    }
	}
    }
}

main()
{
    int c1, c2 = 0, c3 = 0;

    while ((c1 = getchar()) != EOF) {
	AddTriplet(c1, c2, c3);
	c3 = c2;
	c2 = c1;
    }
    PrintNode(&Root);
}
