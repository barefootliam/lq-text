#! /bin/sh

LQTEXTDIR=test$$
export LQTEXTDIR

state='INTERRUPTED'
trap 'echo "**** $state"; /bin/rm -rf $LQTEXTDIR; exit' 0 1 2 3 15

mkdir $LQTEXTDIR

# tests that do not need lq-text to be built
test/dbmtry 1000
test/dbmtry 5000

# tests that need only the libraries
test/NumberTest 1000

# tests that use the clients
cd test/testdb; ./do-test

state='FINISHED'
