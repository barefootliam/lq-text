/* NumberTest.c -- Copyright 1989 Liam R. Quin.  All Rights Reserved.
 * This code is NOT in the public domain.
 * See the file COPYRIGHT for full details.
 */

/*
 * This version of the test program prints messages even when things are OK.
 *
 * $Id: NumberTest.c,v 1.3 2019/04/21 06:14:20 lee Exp $
 *
 */

#include "error.h"
#include "globals.h"

#include <stdio.h>
#include <sys/types.h>

#ifdef HAVE_STRING_H
# include <string.h>
#else
# include <strings.h>
#endif

#ifdef HAVE_STDLIB_H
# include <stdlib.h>
#endif

#ifdef HAVE_UNISTD_H
# include <unistd.h>
#endif

#include <malloc.h>

#include "lqtrace.h"
#include "numbers.h"
#include "liblqtext.h"

char *progname = "NumberTest";

/** Unix system calls: **/
/** Unix Library Functions: **/
/** liblqtext Library Functions: **/

/** Routines in this file which need declaring: **/
PRIVATE void Test(
#ifdef HAVE_PROTO
    int level
#endif
);

/** **/

static int ErrorCount = 0;

int
main(ac, av)
    int ac;
    char *av[];
{
    int Level = 1;

    if (ac > 2 || (ac == 2 && !strcmp(av[1], "-x"))) {
	fprintf(stderr, "Usage: %s [level, default is 1]\n", av[0]);
	exit(1);
    } else if (ac == 2) {
	Level = atoi(av[1]);
	if (Level <= 0) {
	    Error(E_FATAL, "optional argument must be a positive number.");
	}
    } else {
	Level = 1;
    }

    fprintf(stderr, "%s: ** Starting to test Compressed Number package\n",
	progname
    );

    do {
	Test(Level);
    } while (--Level);

    if (ErrorCount) {
	Error(E_FATAL,
	    "**** FAIL **** %d failures",
	    ErrorCount
	);
    }

    fprintf(stderr, "%s: ** Test complete - PASS\n", progname);
    exit(0);
    /*NOTREACHED*/
    return 0; /* for lint and gcc */
}

static unsigned char *Buffer = 0;

PRIVATE void
ClearBuffer(Byte, Len)
    unsigned char Byte;
    int Len;
{
    register unsigned char *p;

    /* could use memset but this is better for debugging */
    for (p = Buffer; p - Buffer < Len; p++) {
	*p = Byte;
    }
}

PRIVATE void
OneTest(i, Level, N, pad)
    int i;
    int Level;
    unsigned long N;
    unsigned char pad;
{
    unsigned long L;
    int Length;
    unsigned char *p;

#define BufLEN ((size_t) 40)
    if (!Buffer) {
	Buffer = malloc(BufLEN);
    }
    if (LQT_TraceFlagsSet(LQTRACE_DEBUG|LQTRACE_VERBOSE)) {
	(void) fprintf(stderr, "\r%ld    ", N);
    }
    ClearBuffer(pad, BufLEN);
    p = Buffer;
    if (LQT_sWriteNumber(&p, N, Buffer, BufLEN) == -1) {
	Error(E_WARN,
	    "**** FAIL -- WriteNumber failed, writing %d",
	    N
	);
	++ErrorCount;
    }

    Length = p - Buffer;
    if ((unsigned char)*p != (unsigned char) pad) {
	Error(E_WARN,
	    "**** FAIL -- 2^%d+%d len %d: sentinel changed on write from 0%o to 0%o",
	    i,
	    Level,
	    Length,
	    pad,
	    *p
	);
	++ErrorCount;
    }

    p = Buffer;

    L = 0L;
    if (LQT_sReadNumber(&p, &L, p, BufLEN) == -1) {
	Error(E_WARN,
	    "**** FAIL -- ReadNumber failed, reading %d",
	    N
	);
	++ErrorCount;
    } else if (L != N) {
	Error(E_WARN,
	    "**** FAIL -- 2^%d+%d: wrote %ld read %ld, bytes %d",
	    i,
	    Level,
	    L,
	    N,
	    Length
	);
	++ErrorCount;
    } else if (p - Buffer != Length) {
	Error(E_WARN,
	    "**** FAIL -- 2^%d+%d: wrote %ld bytes, read %ld bytes",
	    i,
	    Level,
	    Length,
	    p - Buffer
	);
	++ErrorCount;
    }
    if ((unsigned char)*p != (unsigned char) pad) {
	Error(E_WARN,
	    "**** FAIL -- 2^%d+%d: sentinel changed from 0%o to 0%o",
	    i,
	    Level,
	    pad,
	    *p
	);
	++ErrorCount;
    }

    if (LQT_TraceFlagsSet(LQTRACE_DEBUG|LQTRACE_VERBOSE)) {
	(void) fputc('\r', stderr);
    }
}

PRIVATE void
Test(Level)
    int Level;
{
    int i;

    for (i = 0; i < 32; i++) {
	unsigned long N = (unsigned long) (1L << i) + (unsigned long) Level;

	OneTest(i, Level, N, (unsigned char) 0);
	OneTest(i, Level, N, (unsigned char) 0xff);
    }
}

