/* $Header: /home/lee/src/lq-text-cvs/src/test/RCS/TryHash.c,v 1.2 2019/04/21 06:14:20 lee Exp $
 *
 * $Log: TryHash.c,v $
 * Revision 1.2  2019/04/21 06:14:20  lee
 * for migration to CVS
 *
 * Revision 1.2  2001/05/31 03:50:55  liam
 * for release 1.17
 *
 * Revision 1.1  90/08/09  19:17:43  lee
 * Initial revision
 * 
 *
 */


#include <stdio.h>

#include "globals.h" /* defines and declarations for database filenames */

#define HASHSIZ 4096

char *progname = "TryHash";

/** Unix library functions: **/
extern int strlen();

int
Hash0(String)
    char *String;
{
    register int Result = strlen(String);
    register char *q;

    for (q = String; *q; q++) {
	Result = ((Result * 211) | *q) & (HASHSIZ - 1);
    }
    return Result;
}

int
Hash1(str) /* Ozan's ... */
    register char *str;
{
    register unsigned long n = 0;
    register int len = strlen(str);

#ifdef DUFF

#define HASHC	n = *str++ + 65599 * n

    if (len > 0) {
	register int loop = (len + 8 - 1) >> 3;

	switch(len & (8 - 1)) {
	case 0:	do {
		HASHC;	case 7:	HASHC;
	case 6:	HASHC;	case 5:	HASHC;
	case 4:	HASHC;	case 3:	HASHC;
	case 2:	HASHC;	case 1:	HASHC;
		} while (--loop);
	}

    }
#else
    while (len--)
	n = *str++ + 65599 * n;
#endif
    return n & (HASHSIZ - 1);
}

typedef int (* t_ifp)();
t_ifp HashTab[] = {
    Hash0,
    Hash1
#define MAXHASHF 2 /* CHANGE ME: number of hash functions */
};

int
main()
{
    char Buffer[1000];
    int i;
    t_ifp f;

    while (gets(Buffer) != (char *) 0) {
	printf("%s:", Buffer);
	for (i = 0; i < MAXHASHF; i++) {
	    f = HashTab[i];
	    printf("\t%d", (* f)(Buffer));
	}
	putchar('\n');
    }
    return 0;
}
