head	1.3;
access;
symbols;
locks;
comment	@ * @;


1.3
date	2019.04.21.06.14.20;	author lee;	state Exp;
branches;
next	1.2;

1.2
date	96.08.08.21.46.19;	author lee;	state Exp;
branches;
next	1.1;

1.1
date	96.07.01.21.37.28;	author lee;	state Exp;
branches;
next	;


desc
@LQU_fReadLine no longer allocates each line...
@


1.3
log
@for migration to CVS
@
text
@/* test speed of readword
 */

static char *Version =
    "@@(#) $Id: readword.c,v 1.2 96/08/08 21:46:19 lee Exp $";

#include "globals.h" /* defines and declarations for database filenames */
#include "error.h"

#include <stdio.h>
#include <malloc.h>
#include <ctype.h>
#include <sys/types.h>

#ifdef HAVE_FCNTL_H
# ifdef HAVE_SYSV_FCNTL_H
#  include <sys/stat.h>
# endif
# include <fcntl.h>
#endif

#ifdef HAVE_STRING_H
# include <string.h>
#else
# include <strings.h>
#endif

#ifdef HAVE_STDLIB_H
# include <stdlib.h>
#endif

#ifdef HAVE_UNISTD_H
# include <unistd.h>
#endif

#include "fileinfo.h"
#include "wordinfo.h"
#include "wordrules.h"
#include "emalloc.h"
#include "addfile.h"
#include "lqutil.h"
#include "revision.h"
#include "liblqtext.h"
#include "lqtrace.h"
#include "filter.h"

#include <sys/mman.h>


/** Functions within this file that need declaring: **/
PRIVATE void AddStream(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *,
    t_FileInfo *FileInfo
#endif
);

PRIVATE void AddFrom(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *,
    char *Name
#endif
);

/* Symbol Table Interface */
static void PrintWord(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *,
    t_WordInfo *WordInfo
#endif
);

PRIVATE void AddFile(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *,
    char *Name
#endif
);


/**/

char *progname = "@@(#) $Id: readword.c,v 1.2 96/08/08 21:46:19 lee Exp $";

static int SignalFlag = 0;

int
main(argc, argv)
    int argc;
    char *argv[];
{
    extern int getopt();
    extern char *optarg;
    extern int optind;

    t_LQTEXT_Database *db;
    t_lqdbOptions *Options;
    int c;
    int ErrorFlag = 0;
    int DoNothing = 0;
    char *InputFile = (char *) 0;

    progname = argv[0]; /* retain the full path at first */

    Options = LQT_InitFromArgv(argc, argv);

    while ((c = getopt(argc, argv, "w:f:H:M:xVZz:")) != -1) {
	switch (c) {
	case 'Z':
	case 'z':
	    break; /* work done in SetDefault() */
	case 'V':
	    fprintf(stderr, "%s: Release: %s\n", progname, LQTEXTREVISION);
	    fprintf(stderr, "%s: Revision: %s\n", progname, Version);
	    DoNothing = 1;
	    break;
	case 'f':
	    if (InputFile) {
		Error(E_USAGE|E_XHINT|E_FATAL,
		    "only one -f option allowed; use -xv for explanation"
		);
	    }
	    InputFile = optarg;
	    break;
	case 'x':
	    ErrorFlag = (-1);
	    break;
	default:
	case '?':
	    ErrorFlag = 1;
	}
    }

    if ((progname = strrchr(progname, '/')) != (char *) NULL) {
	++progname; /* step over the last / */
    } else {
	progname = argv[0];
    }

    if (ErrorFlag > 0) {
	fprintf(stderr, "use %s -x or %s -xv for an explanation.\n",
							progname, progname);
	exit(1);
    } else if (ErrorFlag < 0) { /* -x was used */
	fprintf(stderr, "%s -- read words\n", progname);

	LQT_PrintDefaultUsage(Options);

	exit(0);
    }

    if (DoNothing) {
	if (optind < argc) {
	    Error(E_WARN|E_XHINT,
		"%d extra argument%s ignored...",
		argc - optind,
		argc - optind == 1 ? "" : "%s"
	    );
	}
	exit(0);
    }

    if (!(db = LQT_OpenDatabase(Options, O_RDONLY, 0))) {
	Error(E_FATAL, "couldn't open database for reading.");
    }
    LQT_InitFilterTable(db);

    if (InputFile) {
	if (optind < argc) {
	    Error(E_FATAL|E_USAGE|E_XHINT,
		"cannot give filenames after -f %s",
		InputFile
	    );
	}
	AddFrom(db, InputFile);
    } else for (; optind < argc; ++optind) {
	AddFile(db, argv[optind]);
    }

    LQT_CloseDatabase(db);

    return 0;
}

static void
AddFrom(db, Name)
    t_LQTEXT_Database *db;
    char *Name;
{
    FILE *fp;
    char *Line;

    if (Name[0] == '-' && Name[1] == '\0') {
	fp = stdin;
    } else {
	fp = LQU_fEopen(E_FATAL, Name, "list of files to add", "r");
    }

    while (LQU_fReadLine(fp, &Line, LQUF_NORMAL) > 0) {
	/* Note:
	 * LQU_fReadFile will silently swallow blank lines.
	 * if we use LQUF_NORMAL it will swallow lines that start with a #,
	 * but we don't want that here!
	 */
	AddFile(db, Line);
    }

    if (fp != stdin) {
	(void) fclose(fp);
    }
}

PRIVATE void
AddFile(db, Name)
    t_LQTEXT_Database *db;
    char *Name;
{
    t_FileInfo *theFileInfo;
    t_FID FID;

    if (!Name || !*Name) {
	return;
    }

    if ((FID = LQT_NameToFID(db, Name)) == (t_FID) 0) {
	return;
    }

    if ((theFileInfo = LQT_FIDToFileInfo(db, FID)) == (t_FileInfo *) 0) {
	return;
    }

    theFileInfo->Stream = LQT_MakeInput(db, theFileInfo);

    AddStream(db, theFileInfo);
    LQT_DestroyFileInfo(db, theFileInfo);

    return;
}

PRIVATE void
AddStream(db, FileInfo)
    t_LQTEXT_Database *db;
    t_FileInfo *FileInfo;
{
    /* I have to mark the last word in the block.
     * I do that by marking the previous word if it was in a differant block
     * than the current one.
     */
    char *Base;
    char *Start, *End;
    t_WordInfo *WordInfo;
    t_WordInfo *LastWord = 0;

    if (!FileInfo->FileSize) {
	struct stat s;

	if (fstat(fileno(FileInfo->Stream), &s) < 0) {
	    Error(E_WARN|E_SYS, "Can't get size of %s", FileInfo->Name);
	    return;
	}

	FileInfo->FileSize = s.st_size;
    }

#ifndef MAP_FILE
# define MAP_FILE 0
#endif

    Base = mmap(
	0,
	FileInfo->FileSize,
	PROT_READ,
	MAP_FILE|MAP_SHARED,
	fileno(FileInfo->Stream),
	0
    );

    if (Base == (caddr_t) -1) {
	Error(E_WARN|E_SYS, "can't mmap input for %s", FileInfo->Name);
	return;
    }

    /* reset the word-reading routine */
    (void) LQT_ReadWordFromStringPointer(
	db,
	(char **) NULL,
	(char **) NULL,
	(char *) NULL,
	0
    );

    /* add the words in this file, one at a time.
     * We are always one word behind, because when ReadWord
     * finds punctuation after a word, it sets the flag in the
     * previous word's WordPlace... so we have to leave it in place
     * to get set!
     */

    Start = Base;
    End = &Base[FileInfo->FileSize];
    LastWord = (t_WordInfo *) 0;

    while (SignalFlag <= 1) {
	/* needs more than one signal to quit in the middle of a file */

	WordInfo = LQT_ReadWordFromStringPointer(
	    db,
	    &Start,
	    (char **) NULL,
	    End,
	    LQT_READWORD_IGNORE_COMMON
	);

	if (WordInfo == (t_WordInfo *) NULL) {
	    break;
	} else {
	    WordInfo->WordPlace.FID = FileInfo->FID;

	    if (LastWord) {
		LastWord->WordPlace.FID = FileInfo->FID;
		PrintWord(db, LastWord);
	    }

	    LastWord = WordInfo;
	}
    }

    if (LastWord) {
	/* ensure that the WPF_LASTINBLOCK flag is not set */
	LastWord->WordPlace.Flags &= ~WPF_LASTINBLOCK;
	LastWord->WordPlace.FID = FileInfo->FID;
	PrintWord(db, LastWord);
	LastWord = (t_WordInfo *) 0;
    }

    (void) munmap(Base, FileInfo->FileSize);
}

PRIVATE void
PrintWord(db, Word)
    t_LQTEXT_Database *db;
    t_WordInfo *Word;
{
    /* print enough information to allow the word to be indexed */
    printf("%d\t%*.*s\t%ld\t%ud\t%lu\t%lu\n",
	(int) Word->Length,
	(int) Word->Length, (int) Word->Length, Word->Word,
	(unsigned long) Word->WordPlace.Flags,
	(unsigned int) Word->WordPlace.StuffBefore,
	Word->WordPlace.BlockInFile,
	Word->WordPlace.WordInBlock
    );
}
@


1.2
log
@prints out useful stuff now.
@
text
@d5 1
a5 1
    "@@(#) $Id: readword.c,v 1.1 96/07/01 21:37:28 lee Exp $";
d47 3
d53 1
a53 1
    t_LQTEXT_Database *db,
d60 1
d68 1
a68 1
    t_LQTEXT_Database *db,
d75 1
d83 1
a83 1
char *progname = "@@(#) $Id: readword.c,v 1.1 96/07/01 21:37:28 lee Exp $";
a85 1
t_LQTEXT_Database *db;
d96 1
d175 1
a175 1
	AddFrom(InputFile);
d177 1
a177 1
	AddFile(argv[optind]);
d186 2
a187 1
AddFrom(Name)
d205 1
a205 1
	AddFile(Line);
d214 2
a215 1
AddFile(Name)
d250 19
a268 2
    char *Line;
    long lineNumber;
d270 8
a277 1
    lineNumber = 0;
d279 19
a297 3
    /* Read lines one at a time into Line and then read the words from
     * those lines.
     * Note: LQU_fReadFile() uses a private (but growable) buffer.
a298 4
    while (LQU_fReadLine(FileInfo->Stream, &Line, 0) > 0) {
	char *Start;
	t_WordInfo *WordInfo;
	t_WordInfo *LastWord = 0;
d300 6
a305 1
	++lineNumber;
d307 1
a307 6
	if (!Line || !*Line) {
	    continue;
	}
    
	/* reset the word-reading routine */
	(void) LQT_ReadWordFromStringPointer(
d309 1
d311 2
a312 2
	    (char **) NULL,
	    0
d315 4
a318 6
	/* add the words in this line, one at a time.
	 * We are always one word behind, because when ReadWord
	 * finds punctuation after a word, it sets the flag in the
	 * previous word's WordPlace... so we have to leave it in place
	 * to get set!
	 */
d320 4
a323 2
	Start = Line;
	LastWord = (t_WordInfo *) 0;
d325 1
a325 22
	while (SignalFlag <= 1) {
	    /* needs more than one signal to quit in the middle of a file */

	    WordInfo = LQT_ReadWordFromStringPointer(
		db,
		&Start,
		(char **) NULL,
		LQT_READWORD_IGNORE_COMMON
	    );

	    if (WordInfo == (t_WordInfo *) NULL) {
		break;
	    } else {
		WordInfo->WordPlace.BlockInFile = lineNumber;
		WordInfo->WordPlace.FID = FileInfo->FID;

		if (LastWord) {
		    PrintWord(db, LastWord);
		}

		LastWord = WordInfo;
	    }
d327 1
d329 6
a334 8
	if (LastWord) {
	    /* ensure that the WPF_LASTINBLOCK flag is not set */
	    LastWord->WordPlace.Flags &= ~WPF_LASTINBLOCK;
	    LastWord->WordPlace.BlockInFile = lineNumber;
	    LastWord->WordPlace.FID = FileInfo->FID;
	    PrintWord(db, LastWord);
	    LastWord = (t_WordInfo *) 0;
	}
d336 2
@


1.1
log
@Initial revision
@
text
@d5 1
a5 1
    "@@(#) $Id: addbyline.c,v 1.4 96/06/20 14:22:08 lee Exp $";
d78 1
a78 1
char *progname = "@@(#) $Id: addbyline.c,v 1.4 96/06/20 14:22:08 lee Exp $";
a79 4
#ifdef USE_LINENUMBERS
static int UseLineNumbers = 0;
#endif

d321 9
a329 2
    fwrite(Word->Word, (int) Word->Length, 1, stdout);
    putchar('\n');
@
