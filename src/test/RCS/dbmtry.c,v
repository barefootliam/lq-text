head	1.9;
access;
symbols;
locks;
comment	@ * @;


1.9
date	2019.04.21.06.14.20;	author lee;	state Exp;
branches;
next	1.8;

1.8
date	94.02.26.15.02.32;	author lee;	state Exp;
branches;
next	1.7;

1.7
date	92.05.10.23.33.03;	author lee;	state Exp;
branches;
next	1.6;

1.6
date	92.05.10.22.04.29;	author lee;	state Exp;
branches;
next	1.5;

1.5
date	91.03.03.00.47.56;	author lee;	state Exp;
branches;
next	1.4;

1.4
date	90.08.09.19.17.47;	author lee;	state Exp;
branches;
next	1.3;

1.3
date	90.07.27.17.41.54;	author lee;	state Exp;
branches;
next	;


desc
@Simple test for dbm routines
@


1.9
log
@for migration to CVS
@
text
@/* dbmtry.c -- Copyright 1989, 1994 Liam R. Quin.
 * All Rights Reserved.
 * This code is NOT in the public domain.
 * See the file COPYRIGHT for full details.
 */

/* If you have problems with the dbm interface, try this program.
 * If it fails with ALERT messages when given an argument of 300 or so,
 * you almost certainly have a faulty dbm.
 *
 * On SysV, by the way, check for delitem() calling bcopy() with
 * overlapping arguments...
 *
 * This version of the test program prints messages even when things are OK.
 *
 * $Id: dbmtry.c,v 1.8 94/02/26 15:02:32 lee Exp $
 *
 */

#include "globals.h" /* defines and declarations for database filenames */
#include "error.h"

#include <stdio.h>
#include <sys/types.h>
#ifdef HAVE_FCNTL_H
# ifdef HAVE_SYSV_FCNTL_H
#  include <sys/stat.h>
# endif
# include <fcntl.h>
#endif

#include "smalldb.h"
#include "lqutil.h"
#include "liblqtext.h"

#ifdef HAVE_UNISTD_H
# include <unistd.h>
#endif

#ifdef HAVE_STDLIB_H
# include <stdlib.h>
#endif

char *progname = "dbmtry";

PRIVATE void printvalues(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *db,
    int max,
    char *note
#endif
);

PRIVATE int FindMax(
#ifdef HAVE_PROTO
    DBM *kvpdb
#endif
);

PRIVATE void SetMax(
#ifdef HAVE_PROTO
    DBM *kvpdb,
    int max
#endif
);

/** **/

static char *TestFile = "/tmp/trydbm";
static int ErrorCount = 0;

int
main(argc, argv)
    int argc;
    char *argv[];
{
    DBM *kvpdb;
    int max;
    int i;
    datum key, data;
    char dbuf[200];
    int min;

    t_lqdbOptions *Options;
    t_LQTEXT_Database *db = 0;

    if (argc <= 1) {
	fprintf(stderr, "Usage: %s maxkey\n", argv[0]);
	exit(1);
    }

    Options = LQT_InitFromArgv(argc, argv);
    max = atoi(argv[1]);

    db = LQT_OpenDatabase(Options, O_RDWR|O_CREAT, 0644);
    if (!db || LQT_ObtainWriteAccess(db) < 0) {
	Error(E_FATAL, "couldn't open lq-text database for writing");
    }
    fprintf(stderr, "%s: ** Using test database \"%s\"\n", progname, TestFile);

    if ((kvpdb = LQT_OpenKeyValueDatabase(db, TestFile)) == (DBM *) 0) {
	Error(E_FATAL|E_SYS, "Couldn't open test database %s", TestFile);
    }

    if ((min = FindMax(kvpdb)) < 0) {
	min = 0;
    } else {
	if (min + 1 >= max) {
	    Error(E_WARN, "%s previously had a stored max of %d, using %d",
		TestFile,
		min,
		max + min
	    );
	    max += min;
	}
	printvalues(db, min, "previously stored");
	++min; /* start one above the last max */
    }

    fprintf(stderr, "%s: ** writing from %d up to %d.\n", progname, min, max);

    for (i = min; i <= max; i++) {
	char buf[20];
	int s_val;

	sprintf(buf, "%d", i);
	sprintf(dbuf, "%d data item here", i);
	    /* Note: the number is at the start to help speed the
	     * strcmp, as it is most likely to differ
	     * (yes, this mattered a long time ago, testing 30,000 items
	     * on a VAX. Now, it doesn't really make a difference)
	     */
	bzero(&key, sizeof key);
	key.dsize = strlen(buf) + 1; /* include the \0 so we can strcmp() */
	key.dptr = buf;
	bzero(&data, sizeof data);
	data.dptr = dbuf;
	data.dsize = strlen(dbuf) + 1;
	s_val = dbm_store(kvpdb, key, data, DBM_REPLACE);
	if (s_val != 0) {
	    Error(E_WARN|E_SYS, "ALERT! dbm_store %d returned %d, not 0 [%s]",
		i, s_val, LQT_dberror(s_val)
	    );
	    ++ErrorCount;
	}
    }

    fprintf(stderr, "%s: ** write test complete: %d error%s\n",
	progname,
	ErrorCount,
	(ErrorCount == 1) ? "" : "s"
    );

    SetMax(kvpdb, max);

    LQT_CloseKeyValueDatabase(kvpdb);
    LQT_SyncAndCloseAllKeyValueDatabases(db);

    printvalues(db, max, "all");

    if (ErrorCount) {
	Error(E_FATAL, "**** ALERT **** Total of %d errors, should be 0",
	    ErrorCount
	);
    } else {
	fprintf(stderr, "%s: test passed.\n", progname);
    }
    exit(0);
    return -1; /* for lint, gcc */
}

PRIVATE void
printvalues(db, max, note)
    t_LQTEXT_Database *db;
    int max;
    char *note;
{
    DBM *kvpdb;
    int i;
    char buf[20];
    datum key, data;

    LQT_ObtainReadOnlyAccess(db);

    kvpdb = LQT_OpenKeyValueDatabase(db, TestFile);
    fprintf(stderr, "*** got database 0x%x\n", kvpdb);

    if (!kvpdb) {
	Error(E_FATAL|E_SYS, "Unable to open database %s", TestFile);
    }
    (void) bzero(&key, sizeof key);
    (void) bzero(&data, sizeof data);

    i = FindMax(kvpdb);
    if (i != max) {
	Error(E_WARN, "FindMax() returned %d, but %d was expected", i, max);
    }

    fprintf(stderr, "%s: ** Checking %s stored data from 0 up to %d.\n",
	progname, note, max
    );

    /* Note: always start at zero */
    for (i = 0; i <= max; i++) {

	sprintf(buf, "%d", i);
	key.dsize = strlen(buf) + 1;
	key.dptr = buf;
	data = dbm_fetch(kvpdb, key);

	if (data.dsize == 0) {
	    Error(E_WARN, "ALERT! Item %d has been lost! ALERT!", i);
	    ++ErrorCount;
	} else {
	    char *Buf[100];
	    (void) sprintf(Buf, "%d data item here", i);
	    if (strncmp(Buf, data.dptr, data.dsize) != 0) {
		Error(E_WARN, "ALERT! %d: Corrupt: \"%s\" != \"%s\" ALERT!",
				i, data.dptr, Buf);
		++ErrorCount;
	    } else {
		fprintf(stderr, "%s: ** %d: %*.*s\n", 
		    progname, i, data.dsize, data.dsize, data.dptr
		);
	    }
	}
    }
    LQT_CloseKeyValueDatabase(kvpdb);
}

static char *MaxValueString = "MAX VALUE";

PRIVATE int
FindMax(kvpdb)
    DBM *kvpdb;
{
    int i;
    static datum key, data;

    fprintf(stderr, "findmax(0x%x)\n", kvpdb);
    (void) bzero(&key, sizeof key);
    (void) bzero(&data, sizeof data);

    key.dptr = MaxValueString;
    key.dsize = strlen(MaxValueString);

    data = dbm_fetch(kvpdb, key);

    if (data.dsize == 0 || !data.dptr) return -1;
    i = atoi(data.dptr);
    if (i <= 0) {
	Error(E_WARN, "Max Value stored in %s was as %d, strange", TestFile, i);
	return -1;
    }

    return i;
    
}

PRIVATE void
SetMax(kvpdb, max)
    DBM *kvpdb;
    int max;
{
    int i;
    char buf[20];
    static datum key, data;

    (void) bzero(&key, sizeof key);
    (void) bzero(&data, sizeof data);
    key.dptr = MaxValueString;
    key.dsize = strlen(MaxValueString);

    (void) sprintf(buf, "%d", max);
    data.dptr = buf;
    data.dsize = strlen(buf) + 1; /* include the \0 so atoi() will work */

    i = dbm_store(kvpdb, key, data, DBM_REPLACE);
    if (i < 0) {
	Error(E_WARN|E_SYS,
	    "Failed to insert Max marker (%d) into %s",
	    max,
	    TestFile
	);
    }
}
@


1.8
log
@API change
@
text
@d1 2
a2 1
/* dbmtry.c -- Copyright 1989 Liam R. Quin.  All Rights Reserved.
d16 1
a16 1
 * $Id: dbmtry.c,v 1.7 92/05/10 23:33:03 lee Exp $
d24 6
a29 3
#include <fcntl.h>
#ifdef bsdhash
# include <sys/types.h>
d31 1
d33 10
a44 1
int AsciiTrace = 0;
d46 13
a58 2
/** Unix system calls: **/
extern void exit();
d60 6
a65 13
/** Unix Library Functions: **/
extern int atoi();
extern int strlen();
extern void perror();

/** liblqtext Library Functions: **/
extern void LQT_ObtainWriteAccess();
extern void LQT_ObtainReadOnlyAccess();

/** Routines in this file which need declaring: **/
void printvalues();
static int FindMax();
static void SetMax();
d73 3
a75 3
main(ac, av)
    int ac;
    char *av[];
d77 2
a78 2
    DBM *db;
    int max = atoi(av[1]);
d81 1
a81 1
    char dbuf[30];
d84 5
a88 2
    if (ac <= 1) {
	fprintf(stderr, "Usage: %s maxkey\n", av[0]);
d92 2
a93 1
    LQT_ObtainWriteAccess();
d95 4
d101 1
a101 1
    if ((db = LQT_OpenKeyValueDatabase(TestFile)) == (DBM *) 0) {
d105 1
a105 1
    if ((min = FindMax(db)) < 0) {
d116 1
a116 1
	printvalues(min, "previously stored");
d124 1
a124 1
	register int s_val;
d130 2
d133 1
d136 1
d139 1
a139 1
	s_val = dbm_store(db, key, data, DBM_INSERT);
d141 3
a143 1
	    Error(E_WARN, "ALERT! dbm_store %d returned %d, not 0", i, s_val);
d154 1
a154 1
    SetMax(db, max);
d156 2
a157 2
    LQT_CloseKeyValueDatabase(db);
    LQT_SyncAndCloseAllKeyValueDatabases();
d159 1
a159 1
    printvalues(max, "all");
d169 1
d172 3
a174 2
void
printvalues(max, note)
d178 1
a178 1
    DBM *db;
d183 1
a183 1
    LQT_ObtainReadOnlyAccess();
d185 2
a186 1
    db = LQT_OpenKeyValueDatabase(TestFile);
d188 1
a188 1
    if (!db) {
d191 2
d194 1
a194 1
    i = FindMax(db);
d209 1
a209 1
	data = dbm_fetch(db, key);
d221 4
d228 1
a228 1
    LQT_CloseKeyValueDatabase(db);
d233 3
a235 3
static int
FindMax(db)
    DBM *db;
d238 5
a242 1
    datum key, data;
d247 1
a247 1
    data = dbm_fetch(db, key);
d260 3
a262 3
static void
SetMax(db, max)
    DBM *db;
d267 1
a267 1
    datum key, data;
d269 2
d278 1
a278 1
    i = dbm_store(db, key, data, DBM_REPLACE);
@


1.7
log
@More verbose now.
@
text
@d15 1
a15 1
 * $Id$
d41 2
a42 2
extern void lqWriteAccess();
extern void lqReadOnlyAccess();
d71 1
a71 1
    lqWriteAccess();
d75 1
a75 1
    if ((db = startdb(TestFile)) == (DBM *) 0) {
d124 2
a125 2
    enddb(db);
    cleanupdb();
d149 1
a149 1
    lqReadOnlyAccess();
d151 1
a151 1
    db = startdb(TestFile);
d157 1
a157 1
    i = FindMax();
d187 1
a187 1
    enddb(db);
@


1.6
log
@added lqWriteAccess()
@
text
@d13 1
a13 1
 * $Header: /usr/src/cmd/lq-text/src/test/RCS/dbmtry.c,v 1.5 91/03/03 00:47:56 lee Exp $
d15 2
a16 16
 * $Log:	dbmtry.c,v $
 * Revision 1.5  91/03/03  00:47:56  lee
 * added sys/types.h for ozmahash
 * 
 * Revision 1.4  90/08/09  19:17:47  lee
 * *** empty log message ***
 * 
 * Revision 1.3  90/07/27  17:41:54  lee
 * *** empty log message ***
 * 
 * Revision 1.2  89/09/16  21:16:15  lee
 * First demonstratable version.
 * 
 * Revision 1.1  89/09/07  21:05:54  lee
 * Initial revision
 * 
d20 1
d40 4
d46 2
d51 2
a58 1
    extern lqWriteAccess();
d64 1
a65 1

d73 4
a76 3
    if ((db = startdb("/tmp/trydbm")) == (DBM *) 0) {
	fprintf(stderr, "dbmopen 1 failed\n");
	exit(1);
d79 18
a96 1
    for (i = 1; i <= max; i++) {
a99 4
	if ((i % 500) == 0) {
	    (void) fprintf(stderr, "\r%d ", i);
	    (void) fflush(stderr);
	}
d105 1
a105 1
	key.dsize = strlen(buf) + 1; /* include th nul so we can strcmp() */
d111 2
a112 1
	    printf("store %d returned %d\n", i, s_val);
d116 8
d125 1
d127 10
a136 2
    printvalues(max);
    return 0;
d140 1
a140 1
printvalues(max)
d142 1
d146 1
d149 1
a149 1
    db = startdb("/tmp/trydbm");
d151 2
d154 1
a154 3
	fprintf(stderr, "Unable to open database ");
	perror("/tmp/trydbm");
	exit(1);
d157 4
a160 2
    for (i = 1; i <= max; i++) {
	char buf[20];
d162 7
d175 2
a176 1
	    printf("ALERT! Item %d has been lost! ALERT!\n", i);
d180 2
a181 2
	    if (strcmp(Buf, data.dptr) != 0) {
		printf("%d: Corrupt: \"%s\" != \"%s\"\n",
d183 1
a183 3
		/* NOTE: no use using STREQ, as the strings are usually
		 * the same.
		 */
d187 2
d190 49
a238 1
    enddb(db);
@


1.5
log
@added sys/types.h for ozmahash
@
text
@d13 1
a13 1
 * $Header: /usr/src/cmd/lq-text/src/test/RCS/dbmtry.c,v 1.4 90/08/09 19:17:47 lee Exp Locker: lee $
d16 3
d37 1
a37 1
#ifdef ozmahash
d43 1
d64 1
d76 2
@


1.4
log
@*** empty log message ***
@
text
@d13 1
a13 1
 * $Header: /home/lee/lq-text/src/test/RCS/dbmtry.c,v 1.3 90/07/27 17:41:54 lee Exp $
d16 3
d34 3
@


1.3
log
@*** empty log message ***
@
text
@d13 1
a13 1
 * $Header: dbmtry.c,v 1.2 89/09/16 21:16:15 lee Locked $
d16 3
a24 1
 *
d73 1
d75 4
d80 5
a84 2
	sprintf(dbuf, "This is the data for %d", i);
	key.dsize = strlen(buf) + 1;
d88 4
a91 2
	printf("store %d returned %d\n",
			i, dbm_store(db, key, data, DBM_INSERT));
d111 1
a111 1
	fprintf(stderr, "Aaaaaaaaaaaargh2: ");
d127 9
a135 1
	    printf("%d: %s\n", i, data.dptr);
@
