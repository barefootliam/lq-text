/*
 * Try compiling this program with -DAND and without, and
 * timing the two.
 * Give it a numerical argument, e.g. 1000000, big enough that it takes
 * several seconds.
 *
 * On my Sun 4/110, I get times of about 5 seconds (real and user) and 0 sys,
 * with -UAND, and about 1 second with -DAND.
 *
 * Then, edit lqtext/wordtable.c::Hash() to use the appropriate function.
 *
 * When I start using automatic configuring, this will be automated....
 *
 * Lee
 *
 */

#include <stdio.h>

static int theNumber = 65536;

static int
theFunc(j)
    register int j;
{
#ifdef AND
    return j & (theNumber - 1);
#else
    return j % theNumber;
#endif
}

main(argc, argv)
    int argc;
    char *argv[];
{
    register int i;

    if (argc > 1) {
	i = atoi(argv[1]);
    } else {
	i = 100000;
    }

    while (i > 0) {
	if (theFunc(i) == -1) {
	    exit(1);
	}
	i--;
    }
}


