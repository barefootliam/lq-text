/* MaxWid.c -- Copyright 1989, 1994 Liam R. Quin.  All Rights Reserved.
 * This code is NOT in the public domain.
 * See the file COPYRIGHT for full details.
 */

#include "globals.h" /* defines and declarations for database filenames */
#include "error.h"

#include <stdio.h>
#include <sys/types.h>
#include "liblqtext.h"

#ifdef HAVE_FCNTL_H
# ifdef HAVE_SYSV_FCNTL_H
#  include <sys/stat.h>
# endif
# include <fcntl.h>
#endif

int
main(argc, argv)
    int argc;
    char *argv[];
{
    t_LQTEXT_Database *db;
    t_lqdbOptions *Options;

    Options = LQT_InitFromArgv(argc, argv);
    db = LQT_OpenDatabase(Options, O_RDONLY, 0);
    if (!db) {
	Error(E_FATAL,
	    "unable to open lq-text database in directory \"%s\"",
	    LQT_GetOption(Options, "directory")
	);
    }

    printf("Max Word Identifier (MaxWID) is %lu\n", LQT_GetMaxWID(db));
    return 0;
}
