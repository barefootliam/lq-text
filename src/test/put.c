/* put.c -- Copyright 1989 Liam R. Quin.  All Rights Reserved.
 * This code is NOT in the public domain.
 * See the file COPYRIGHT for full details.
 */

/* This is useful if your echo doesn't grok \ddd.
 * Use put [string|number]*, numbers wih a leading 0 are octal.
 * Use -s to mean that the next argument is a string.  Use -s -s to print
 * the string "-s".
 *
 * $Header: /home/lee/src/lq-text-cvs/src/test/RCS/put.c,v 1.3 2019/04/21 06:14:20 lee Exp $
 *
 * $Log: put.c,v $
 * Revision 1.3  2019/04/21 06:14:20  lee
 * for migration to CVS
 *
 * Revision 1.2  92/01/31  00:06:31  lee
 * added call to exit();
 * 
 * Revision 1.1  90/08/09  19:17:50  lee
 * Initial revision
 * 
 * Revision 1.2  89/09/16  21:18:32  lee
 * First demonstratable version.
 * 
 * Revision 1.1  89/09/07  21:06:10  lee
 * Initial revision
 * 
 *
 */

#include <stdio.h>
#include <ctype.h>

/** Unix/C Library Functions: **/
extern int atoi(), strcmp();
/** **/

int
main(ac, av)
    int ac;
    char *av[];
{
    while (--ac) {
	++av;
	if (isdigit(**av)) {
	    if (**av == '0') {
		int o;

		sscanf(*av, "%o", &o);
		putchar(o);
	    } else {
		putchar(atoi(*av));
	    }
	} else {
	    if (ac > 1 && !strcmp(*av, "-s")) {
		--ac;
		++av;
	    }
	    printf("%s", *av);
	}
    }
    exit(0);
}
