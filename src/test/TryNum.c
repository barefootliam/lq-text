/* TryNum.c -- Copyright 1989 Liam R. Quin.  All Rights Reserved.
 * This code is NOT in the public domain.
 * See the file COPYRIGHT for full details.
 */

/* For testing number routines from numbers.c */

/* $Header: /home/lee/src/lq-text-cvs/src/test/RCS/TryNum.c,v 1.3 2019/04/21 06:14:20 lee Exp $
 *
 * $Log: TryNum.c,v $
 * Revision 1.3  2019/04/21 06:14:20  lee
 * for migration to CVS
 *
 * Revision 1.3  2001/05/31 03:50:55  liam
 * for release 1.17
 *
 * Revision 1.2  90/08/29  21:53:25  lee
 * Test for variable-sozed numbers
 * 
 * Revision 1.1  90/08/09  19:17:44  lee
 * Initial revision
 * 
 * Revision 1.2  89/09/16  21:16:04  lee
 * First demonstratable version.
 * 
 * Revision 1.1  89/09/07  21:05:50  lee
 * Initial revision
 * 
 */

#include <stdio.h>
#include "globals.h"
#include "numbers.h"

#include "emalloc.h"

/** Unix system calls that need to be declared **/
extern void exit();

/** Unix Library Functions that need to be declared **/
extern int strcmp();
extern void perror();
extern int strlen();
extern char *strcpy();

/** Functions from this file that need to be declared: **/
void dostrings();

/** **/

char *progname = "@(#) $Header: /home/lee/src/lq-text-cvs/src/test/RCS/TryNum.c,v 1.3 2019/04/21 06:14:20 lee Exp $";

int
main(ac, av)
    int ac;
    char *av[];
{
    FILE *f;
    unsigned long L;

    progname = av[0]; /* leave the full path */

    if (ac == 3 && !strcmp(av[1], "-s")) {
	dostrings(av[2]);
	exit(0);
    }

    if (ac != 2) {
	fprintf(stderr, "Usage: %s file, or %s -s string\n", av[0], av[0]);
	exit(1);
    }

    if ((f = fopen(av[1], "r")) == (FILE *) 0) {
	extern int errno;
	int e = errno;
	fprintf(stderr, "%s: can't open file ", av[0]);
	errno = e;
	perror(av[1]);
	exit(1);
    }

    for (;;) {
	L = fReadNumber(f);
	printf("%ld\n", L);
	fflush(stdout);
	if (feof(f)) break;
    }

    (void) fclose(f);

    exit(0);
    /*NOTREACHED*/
    return 1; /* for lint */
}

void
dostrings(string)
    char *string;
{
    int len;
    char *Buf;
    char *End;

    len = strlen(string);

    /* the extra bytes allow sReadNumber to over-run */
    if ((Buf = emalloc(len + sizeof(unsigned long) + 1)) == (char *) 0) {
	fprintf(stderr, "%s: Not enough memory\n", progname);
	exit(1);
    }

    (void) strcpy(Buf, string);

    End = &Buf[len];
    string = Buf;

    while (string < End) {
	printf("%lu\n", sReadNumber(&string));
    }
}
