/* TryRoot.c -- Copyright 1989, 1994 Liam R. Quin.  All Rights Reserved.
 * This code is NOT in the public domain.
 * See the file COPYRIGHT for full details.
 *
 * $Header: /home/lee/src/lq-text-cvs/src/test/RCS/TryRoot.c,v 1.3 1996/05/25 22:45:22 lee Exp $
 *
 */

#include "error.h"
#include <stdio.h>
#include <ctype.h>
#include <sys/types.h>
#include "globals.h"
#ifdef HAVE_STRING_H
# include <string.h>
#else
# include <strings.h>
#endif
#include "fileinfo.h" /* this is needed by wordinfo.h */
#include "wordinfo.h"
#include "wordrules.h" /* for the flags, etc */
#include "liblqtext.h" /* for the flags, etc */

#ifdef HAVE_FCNTL_H
# include <sys/stat.h>
# include <sys/fcntl.h>
#endif

char *progname = "TryRoot";

/** Unix/C Library Functions: **/
#ifndef tolower
extern int tolower();
#endif

/** **/

int
main(argc, argv)
    int argc;
    char *argv[];
{
    t_WordInfo W;

    t_lqdbOptions *Options;
    t_LQTEXT_Database *db = 0;

    Options = LQT_InitFromArgv(argc, argv);

    db = LQT_OpenDatabase(Options, O_RDWR, 0);
    if (!db || LQT_ObtainReadOnlyAccess(db) < 0) {
	Error(E_FATAL, "couldn't open lq-text database for writing");
    }

    while (--argc) {
	char *s;
	W.Word = *++argv;
	W.Length = strlen(*argv);
	W.WordPlace.Flags = 0;

	printf("%s --> ", *argv);
	if (isupper(W.Word[0])) {
	    W.WordPlace.Flags |= WPF_UPPERCASE;
	}
	for (s = W.Word; *s; s++) {
	    *s = tolower(*s);
	}
	s = LQT_WORDROOT(db, &W);
	printf("%s [%d] -->", s, W.WordPlace.Flags);
	printf("%s", LQT_GenerateWordFromRoot(db, &W, W.WordPlace.Flags));
	printf(" [%d]\n", W.WordPlace.Flags);
    }
    return 0;
}

