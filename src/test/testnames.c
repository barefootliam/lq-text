/* lqphrase.c -- Copyright 1989, 1990, 1994 Liam R. E. Quin.
 * All Rights Reserved.
 * This code is NOT in the public domain.
 * See the file COPYRIGHT for full details.
 *
 * $Id: testnames.c,v 1.2 2019/04/21 06:14:20 lee Exp $
 */

#include "error.h"
#include "globals.h" /* defines and declarations for database filenames */

#include <stdio.h> /* stderr, also for fileinfo.h */
#include <sys/types.h>
#ifdef HAVE_SYSV_FCNTL_H
# include <sys/file.h>
#endif
#ifdef HAVE_FCNTL_H
#include <fcntl.h>
#endif
#include <malloc.h>
#ifdef HAVE_STRING_H
# include <string.h>
#else
# include <strings.h>
#endif

#include "emalloc.h" /* for efree() */
#include "fileinfo.h" /* for wordinfo.h */
#include "wordinfo.h"
#include "pblock.h"
#include "phrase.h"
#include "lqutil.h"
#include "liblqtext.h"
#include "lqtrace.h"
#include "namespace.h"

#include <stdlib.h>

static char *Revision = "@(#) $Id: testnames.c,v 1.2 2019/04/21 06:14:20 lee Exp $";

char *progname = "namespace.test";

static int One =1;
static int Fourteen = 14;
static char *Simon = "Simon Whitehead";

static char *
intToString(i)
    int i;
{
    static char tmp[30];

    (void) sprintf(tmp, "%d", i);
    return tmp;
}

static char *
boyHeight()
{
    return "five feet exactly";
}

static int
boyAge(n)
    int n;
{
    return n;
}

static t_NameSpaceTableEntry NameSpaceTable[] = {
    { "One", LQU_NameType_Integer, &One },
    { "Fourteen", LQU_NameType_Integer, &Fourteen },
    { "NameSpace", LQU_NameType_NameSpace, 0 },
    { "NameRef", LQU_NameType_NameRef, 0 },
    { "Simon.Name", LQU_NameType_String, &Simon },
    { "Simon.NameRef", LQU_NameType_NameRef, 0 },
    { "Simon.Height", LQU_NameType_String, boyHeight, LQU_NS_FunctionNoArguments },
    { "Simon.Age", LQU_NameType_Integer, boyAge, LQU_NS_FunctionOneArgument, 11 },
    { 0, 0 }
};

int
main(argc, argv)
    int argc;
    char *argv[];
{
    extern int optind, getopt();
    /* extern char *optarg; */
    int ch;
    int ErrorFlag = 0;
    int ignoreCase = 0;
    int noNesting = 0;
    int status = 0;

    t_NameSpace *NameSpace;
    t_NameRef NameRef;

    t_lqdbOptions *Options;
    t_LQTEXT_Database *db = 0;

    Options = LQT_InitFromArgv(argc, argv);

    progname = argv[0];

    while ((ch = getopt(argc, argv, "Zz:inxVv")) != EOF) {
	switch (ch) {
	case 'z':
	case 'Z':
	    break; /* done by LQT_InitFromArgv(); */
	case 'i':
	    ignoreCase = 1;
	    break;
	case 'n':
	    noNesting = 1;
	    break;
	case 'V':
	    fprintf(stderr, "%s version %s\n", progname, Revision);
	    break;
	case 'x':
	    ErrorFlag = (-1);
	    break;
	case '?':
	    ErrorFlag = 1;
	}
    }

    if (ErrorFlag) {
	fprintf(stderr, "Usage: %s [options] \"Name\" [...]\n", progname);
	fprintf(stderr, "\t-i: ignore case\n\t-n: no.nested.names\n");
	exit(ErrorFlag > 0 ? 1 : 0); /* 0 means -x was used */
    }

    db = LQT_OpenDatabase(Options, O_RDWR, 0);
    if (!db || LQT_ObtainReadOnlyAccess(db) < 0) {
	Error(E_FATAL, "couldn't open lq-text database for writing");
    }

    
    /* try to create name space */

    NameSpace = LQU_NameSpaceTableToNameSpace("Options Table", NameSpaceTable);
    if (!NameSpace) {
	Error(E_FATAL, "LQU_NameSpaceTableToNameSpace failed.");
    }

    if (ignoreCase) {
	NameSpace->NamesAreCaseSensitive = 0;
    }

    if (noNesting) {
	NameSpace->FollowNestedReferences = 0;
    }

    /* set the values */
    for (
	NameRef = LQU_FirstNameRef(NameSpace);
	LQU_NameRefIsValid(NameSpace, NameRef);
	NameRef = LQU_NextNameRef(NameSpace, NameRef)
    ) {
	static t_NameRef NR;

	NR = LQU_StringToNameRef(NameSpace, "Simon.Name");

	if (STREQ(LQU_GetNameFromNameRef(NameRef), "NameSpace")) {
	    NameRef = LQU_SetNameVariable(NameRef, &NameSpace);
	} else if (STREQ(LQU_GetNameFromNameRef(NameRef), "NameRef")) {
	    static t_NameRef recursive;

	    recursive = NR;
	    NameRef = LQU_SetNameVariable(NameRef, &recursive);
	} else if (STREQ(LQU_GetNameFromNameRef(NameRef), "Simon.NameRef")) {
	    NameRef = LQU_SetNameVariable(NameRef, &NR);
	}
    }

    while (optind < argc) {
	NameRef = LQU_StringToNameRef(NameSpace, argv[optind]);

	if (LQU_NameRefIsValid(NameSpace, NameRef)) {
	    char *value;

	    printf("%s: **** Found %s\n",
		progname, LQU_GetNameFromNameRef(NameRef)
	    );
	    value = LQU_NameRefToString(NameRef);
	    printf("Value: %s\n", value);
	    efree(value);
	} else {
	    Error(E_WARN, "LQU_StringToNameRef(%s, %s) failed",
		NameSpace->Name,
		argv[optind]
	    );
	    status = 1; /* there were errors */
	}
	++optind;
    }

    (void) LQT_CloseDatabase(db);
    return status;
}
