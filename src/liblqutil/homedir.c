/* gethome.c -- Copyright 1989, 1995, 1996 Liam R. Quin.
 * All Rights Reserved.
 * This code is NOT in the public domain.
 * See the file COPYRIGHT for full details.
 *
 * $Id: homedir.c,v 1.2 2019/04/21 06:06:55 lee Exp $
 *
 * Get a user's home (login) directory
 *
 */

#include "error.h"
#include "globals.h" /* defines and declarations for database filenames */

#include <stdio.h>
#include <sys/types.h>
#include <stdlib.h>

#ifdef HAVE_GETPWUID
# include <pwd.h>
#endif

#ifdef HAVE_STRING_H
# include <string.h>
#else
# include <strings.h>
#endif

#ifdef HAVE_STDLIB_H
# include <stdlib.h>
#else
# include <malloc.h>
#endif

#include "emalloc.h"
#include "lqutil.h"
#include "lqtrace.h"

/* <Function>
 *   <Name>LQU_GetLoginDirectory
 *   <Class>Utilities/System
 *   <Purpose>
 *      Determines the home directory of the current user.
 *	It returns the value of the environment variable $HOME if it is set.
 *	If this isn't set, or is empty, or does not point to a directory,
 *	the password file (or Yellow pages) is consulted instead.
 *   <Returns>
 *	The directory name in a malloc's string; it is the caller's
 *	responsibility to free this string if it should no longer be needed.
 *	If the home directory cannot be determined, a NULL pointer is
 *	returned; this might happen if the user's entry in /etc/passwd was
 *	removed while the program was running, or if the Yellow Pages (NIS)
 *	service became unavailable.
 * </Function>
 */
API char *
LQU_GetLoginDirectory()
{
    char *home = getenv("HOME");

# ifdef HAVE_GETPWUID
    if (!home || !*home || !LQU_IsDir(home)) {
	uid_t Me = getuid();
	struct passwd *pw = getpwuid(Me);

	if (pw && pw->pw_dir && pw->pw_dir[0] != '\0') {
	    home = pw->pw_dir;
	} else {
	    return (char *) NULL; /* fail */
	}
    }
# endif

    /* save the result, since this is necessary in the getuid() case,
     * and the caller must always free the string...
     */
    return strcpy(emalloc("$HOME...", strlen(home) + 1), home);
}
