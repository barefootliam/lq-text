/* readfile.c -- Copyright 1992, 1994, 1995 Liam R. E. Quin.
 * All Rights Reserved.
 * This code is NOT in the public domain.
 * See the file COPYRIGHT for full details.
 */

/* $Id: readfile.c,v 1.11 2019/04/21 06:06:55 lee Exp $
 * readfile - read files or lines into memory.
 */

#include "error.h"
#include <stdio.h>
#include <sys/types.h>
#include "globals.h"
#include <ctype.h>

#include <ctype.h>	/* isspace() etc */

#ifdef HAVE_STDLIB_H
# include <stdlib.h>
#else
# include <malloc.h>
#endif

#ifdef HAVE_UNISTD_H
# include <unistd.h>
#endif

#include "emalloc.h"


#include "lqutil.h"
#include "lqtrace.h"

/* <Function>
 *   <Name>LQU_ReadFile
 *   <Class>Utilities/Files
 *   <Purpose>
 *      <P>Reads the file named by the Name argument, and returns a
 *	pointer to an array of pointers to the start of
 *	each line in the file.</P>
 *	<P>The Flags argument is any combination of flags from
 *	<h>lqutil.h</h> combined with bitwise or; in practice,
 *	however, LQUF_NORMAL is the most frequently used flag, which is
 *	a bitwise or of all of the flags described below.
 *	<P>The flags are as follows:
 *	<LIST><LI>LQUF_IGNBLANKS to throw away blank lines;</LI>
 *	<LI>LQUF_IGNSPACES to discard leading and trailing spaces;</LI>
 *	<LI>LQUF_IGNHASH to discard leading comments (# with a hash-sign);</LI>
 *	<LI>LQUF_IGNALLHASH to discard comments (# with a hash-sign);</LI>
 *	<LI>LQUF_ESCAPEOK to accept \# and \\ as # and \.</LI></LIST>
 *	<P>This is the file descriptor version of LQU_fReadFile.</P>
 *      <P>In the event of an error, the given Severity argument is passed
 *	to Error, along with the given What argument, which should be a
 *	brief English description, perhaps of the order of three words long,
 *	of the file.
 *   <Returns>
 *      the number of lines read, if any.  The char ** pointed to by
 *	the Lines argument is set to point to an array of strings, each
 *	containing one line of text, NUL-terminated with trailing
 *	newlines removed.
 *	If E_FATAL was given, LQU_fReadFile does not return after an error.
 *   <Example>
 *	int numberOfLines;
 *      char **theLines;
 *	int i;
 *
 *      numberOfLines = LQU_fReadFile(E_FATAL,
 *	    "julian.txt",
 *	    "Book of Meditations",
 *	    &theLines,
 *	    LQUF_NORMAL
 *	);
 *
 *	for (i = 0; i < numberOfLines; i++) {
 *	    printf("Line %d was: %s\n", i, Lines[i]);
 *	    efree(Lines[i]);
 *	}
 *	efree((char *) Lines);
 *   </Example>
 *   <Errors>
 *      Generates a Warning or Error of the given Severity if the file
 *	can't be opened, and attempts to diagnose the cause.
 *   <SeeAlso>
 *	LQU_fReadFile
 * </Function>
 */
API long
LQU_ReadFile(Severity, Name, What, Lines, Flags)
    int Severity;
    CONST char *Name;
    CONST char *What;
    char ***Lines;
    int Flags;
{
    FILE *f;
    long NLines;

    f = LQU_fEopen(Severity, Name, What, "r");

    if (f == (FILE *) 0) {
	if (Lines) {
	    *Lines = (char **) 0;
	}
	return -1L;
    }

    NLines = LQU_fReadFile(Severity, f, Name, What, Lines, Flags);

    LQU_fEclose(Severity, f, Name, What);

    return NLines;
}
