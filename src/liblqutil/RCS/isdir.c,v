head     1.5;
branch   ;
access   ;
symbols  ;
locks    ;
comment  @ * @;


1.5
date     96.08.14.16.57.23;  author lee;  state Exp;
branches ;
next     1.4;

1.4
date     95.09.05.00.59.50;  author lee;  state Exp;
branches ;
next     1.3;

1.3
date     94.06.26.17.35.51;  author lee;  state Exp;
branches ;
next     1.2;

1.2
date     94.02.26.14.54.38;  author lee;  state Exp;
branches ;
next     1.1;

1.1
date     92.02.15.03.37.05;  author lee;  state Exp;
branches ;
next     ;


desc
@Check to see if a pathname represents a directory.
@


1.5
log
@for beta 6.
@
text
@/* isdir.c -- Copyright 1989, 1994 Liam R. Quin.  All Rights Reserved.
 * This code is NOT in the public domain.
 * See the file COPYRIGHT for full details.
 */

/* $Id: isdir.c,v 1.4 95/09/05 00:59:50 lee Exp $ */

#include "globals.h"
#include "error.h"

#include <stdio.h>

#include <sys/types.h>
#include <sys/stat.h>

#include "lqutil.h"

/* <Function>
 *   <Name>LQU_IsDir
 *   <Class>Utilities/Files
 *   <Purpose>
 *      returns 1 if and only if the given path is a directory.
 *	See the description for stat(2) for more details.
 *   <Errors>
 *	A fatal error is issued if LQU_IsDir is called with a null string;
 *	a warning is issued if the string is of length zero.
 * </Function>
 */
API int
LQU_IsDir(Dir)
    CONST char *Dir;
{
    struct stat statbuf;

    if (!Dir) {
	Error(E_FATAL|E_INTERNAL, "LQU_IsDir(<NULL pointer>) disallowed");
    } else if (!*Dir) {
	/* If the name has zero characters, on Unix it's the current directory,
	 * but that's probably not what was intended - you can use . for the
	 * current directory.
	 */
	Error(E_WARN, "LQU_IsDir(\"\") seems a little odd");
    }

    /* If the stat failed, chances are it doesn't exist.
     *
     * Another implementation is to look for "Directory/.", but that won't
     * work over NFS, and may generate error messages on the system console
     * with some versions of NFS!
     */
    if (stat(Dir, &statbuf) < 0) return 0;

    if ((statbuf.st_mode & S_IFMT) != S_IFDIR) {
	/* We could also check to see if it's a symbolic link that points
	 * at a directory, but that is unlikely, as stat(2) is supposed to
	 * return information about the target of the link, not about the
	 * file containing the link - see lstat(2).
	 * The only time it might say it's a symlink is if it points to a
	 * directory that isn't there, in which case we should return 0
	 * anyway!
	 */

	return 0;
    }

    return 1;
}
@


1.4
log
@added class to doc, made empty string an error.
@
text
@d6 1
a6 1
/* $Id: isdir.c,v 1.3 94/06/26 17:35:51 lee Exp $ */
a11 1
#include <ctype.h>
d20 1
a20 1
 *   <Class>Utilities/File System
d36 1
a36 3
	Error(E_FATAL|E_INTERNAL,
	    "LQU_IsDir(<invalid NULL pointer>) disallowed",
	);
d42 1
a42 3
	Error(E_WARN,
	    "LQU_IsDir(\"\") seems a little odd",
	);
@


1.3
log
@Added <DOC>
@
text
@d6 1
a6 1
/* $Id: isdir.c,v 1.2 94/02/26 14:54:38 lee Exp $ */
d21 1
d24 4
d36 13
a48 8
    /* If the Directory is the null string, it isn't a directory.
     * We could return -1 in this case, but there's no need.
     *
     * If the name has zero characters, on Unix it's the current directory,
     * but that's probably not what was intended - you can use . for the
     * current directory.
     */
    if (!Dir || !*Dir) return 0;
@


1.2
log
@API change
@
text
@d6 1
a6 1
/* $Id: isdir.c,v 1.1 92/02/15 03:37:05 lee Exp $ */
d19 6
@


1.1
log
@Initial revision
@
text
@d1 1
a1 1
/* IsDir.c -- Copyright 1989 Liam R. Quin.  All Rights Reserved.
d6 1
a6 1
/* $Id$ */
d8 6
d17 5
a21 3
int
IsDir(Dir)
    char *Dir;
@
