head	1.6;
access;
symbols;
locks;
comment	@ * @;


1.6
date	2019.04.21.06.06.55;	author lee;	state Exp;
branches;
next	1.5;

1.5
date	96.08.14.16.57.20;	author lee;	state Exp;
branches;
next	1.4;

1.4
date	96.07.01.21.36.20;	author lee;	state Exp;
branches;
next	1.3;

1.3
date	95.04.30.18.51.36;	author lee;	state Exp;
branches;
next	1.2;

1.2
date	94.06.26.18.07.50;	author lee;	state Exp;
branches;
next	1.1;

1.1
date	94.02.26.14.54.23;	author lee;	state Exp;
branches;
next	;


desc
@LQU_fReadLine no longer allocates each line...
@


1.6
log
@for migration to CVS
@
text
@/* readline.c -- Copyright 1993, 1994 Liam R. E. Quin.
 * All Rights Reserved.
 * This code is NOT in the public domain.
 * See the file COPYRIGHT for full details.
 */

/* $Id: freadln.c,v 1.6 2001/05/31 03:50:13 liam Exp $
 */

#include "error.h"
#include <stdio.h>
#include <sys/types.h>
#include "globals.h"

#include <ctype.h>

#ifdef HAVE_STDLIB_H
# include <stdlib.h>
#else
# include <malloc.h>
#endif

#include "emalloc.h"
#include "lqutil.h"

#ifndef LQT_READLINE_SLOP
  /* the buffer for lines can be this much larger than
   * the last actually read line.
   */
# define LQT_READLINE_SLOP 30
#endif

static unsigned int LineLength = 0;
static char *LQTpReadLineBuffer = 0;

/* <Function>
 *   <Name>LQU_fReadLine
 *   <Class>Utilities/Files
 *   <Purpose>
 *      <P>Reads the next input line from the given file into a static buffer.
 *	The buffer is allocated with malloc and resized dynamically, but
 *	is owned by LQU_fReadLine and should not be free'd or overwritten.</P>
 *	<P>The LQU_StealReadLineBuffer function can be used to obtain the
 *	buffer; LQU_fReadLine will allocate a new one the next time it
 *	is called.</P>
 *	<P>The given Flags are treated as for
 *	LQU_fReadFile, which currently calls this routine directly.
 *	Note that, as for LQU_fReadFile, blank lines are skipped if the
 *	corresponding flag is given.  In this case, LQU_fReadLine will never
 *	return a pointer to a blank line, but will continue reading lines
 *	from the file until a non-blank one is found.</P>
 *   <Returns>
 *      a pointer to the line, in Line, and also the number of bytes in
 *	the line; -1 is returned on EOF, in which case the Line pointer should
 *	not be used.
 * </Function>
 */
API int
LQU_fReadLine(f, Linep, Flags)
    FILE *f;
    char **Linep;
    int Flags;
{
    register char *p;
    static int LinesRead = 0;
    unsigned long TotalSoFar = 0L;
    int c;

    /* NOTE: we mantain a running average of linelengths, and use
     * this as a heuristic for the initial buffer size.  At the end,
     * we realloc() if we over-estimated by more than one byte.
     */

TryAgain:

    *Linep = 0;
    p = (*Linep);

    while ((c = getc(f)) != EOF) {
	/* Look for things to skip before allocating any space: */

	if (c == '\\' && (Flags & LQUF_ESCAPEOK)) {
	    if ((c = getc(f)) == EOF) break;
	    else if (c == '\n') continue; /* join lines together */
	} else if (c == '#' && ((Flags & LQUF_IGNALLHASH) ||
				((Flags & LQUF_IGNHASH) && p == (*Linep)))) {
	    while ((c = getc(f)) != EOF && c != '\n') {
		/*NULLBODY*/ ;
	    }
	    if (c == EOF) {
		break;
	    } else {
		(void) ungetc(c, f);
		continue;
	    }
	} else if (
	    p == (*Linep) &&
	    (Flags & LQUF_IGNSPACES) &&
	    (isascii(c) && isspace(c))
	) {
	    continue; /* ignore leading blanks */
	}

	/* Now ensure that we have allocated enpough space: */
	if (!p) {
	    /* the -1 is to leave room for a trailing \0 */

	    if (LineLength == 0) {
		LineLength = TotalSoFar / (LinesRead ? LinesRead : 1);
		if (LineLength < LQT_READLINE_SLOP) {
		    LineLength = LQT_READLINE_SLOP;
		    LinesRead = 0;
		}
		p = LQTpReadLineBuffer = *Linep =
				emalloc("fReadLine Buffer", LineLength + 1);
	    } else {
		p = (*Linep) = LQTpReadLineBuffer;
	    }
	} else if (p - (*Linep) >= LineLength - 1) {
	    /* save our position... */
	    int WhereWeWere = p - (*Linep);

	    /* increase linelength */
	    if (LineLength < 20) {
		LineLength += 10;
	    } else {
		LineLength += (LineLength / 2);
	    }

	    *Linep = LQTpReadLineBuffer =
				erealloc(LQTpReadLineBuffer, LineLength + 1);
	    if (!*Linep) return WhereWeWere + 1;

	    p = (*Linep);
	    p = &p[WhereWeWere];
	}

	*p++ = c;
	*p = '\0';  /* p a r a n o i a */
	if (c == '\n') {
	    /* delete trailing newline if asked to do so */
	    if (Flags & LQUF_IGNNEWLINE) {
		*--p = '\0';
	    }
	    break;
	}
    }

    if (!p) {
	if (c == EOF) {
	    return -1;
	} else if (Flags & LQUF_IGNBLANKS) {
	    *Linep = (char *) 0;
	    goto TryAgain;
	} else {
	    /* empty line */
	    *Linep = (char *) 0;
	    return 0;
	}
    }

    *p = '\0';

    /* Discard a trailing \r, as these are usually the result of
     * file transfers and are unwanted. (is this a bogus mis-feature?
     * I don't think so, but then, I wrote it...  Another flag?)
     */
    if ((Flags & (LQUF_IGNNEWLINE|LQUF_IGNSPACES)) &&
	p > *Linep && p[-1] == '\r'
    ) {
	*--p = '\0';
    }

    if (Flags & LQUF_IGNSPACES) {
	/* delete trailing spaces, being careful to keep "\ " if requested */
	while (
	    p > *Linep &&
	    (isascii(p[-1]) && isspace(p[-1]))
	) {
	    if (&p[-1] > *Linep && p[-2] == '\\' && (Flags & LQUF_ESCAPEOK)) {
		break; /* This is the case where we must keep "\ " */
	    }
	    *--p = '\0';
	}
    }

    TotalSoFar += p - *Linep;
    ++LinesRead;
	/* Note: we include blank lines in the average */

    if (p == *Linep) {
	if (c == EOF) {
	    return -1;
	} else if (Flags & LQUF_IGNBLANKS) {
	    *Linep = (char *) 0;	/* p a r a n o i a */
	    goto TryAgain;
	 }
    }

    /* ASSERT: p > *Linep */

    if (p - *Linep < LineLength + 1 + LQT_READLINE_SLOP) {
	/* The +1 in the test above is one for a \0; LQT_READLINE_SLOP is to
	 * reduce the number of relloc() calls, as realloc almost always
	 * copies its argument on most systems, and is slow.
	 */
	LineLength = p - *Linep;
	    /* ASSERT: LineLength != 0 */

	*Linep = LQTpReadLineBuffer = erealloc(*Linep, LineLength + 1);
	if (LQTpReadLineBuffer == (char *) 0) {
	    return -1;
	    /* is this right??  no error? */
	}

	p = (*Linep) + LineLength;

    }
    return p - *Linep;
}

/* <Function>
 *   <Name>LQU_StealReadLineBuffer
 *   <Class>Utilities/Files
 *   <Purpose>
 *      <P>Returns the internal line buffer used by LQU_fReadLine, and also
 *	causes LQU_fReadLine to allocate a new buffer the next time it is
 *	called.  In this way, you can read lines with LQU_fReadLine, and
 *	save any that you are interested in keeping by calling
 *	LQU_StealReadLineBuffer, without having to copy the data.</P>
 *	<P>The buffer returned may be longer than necessary to contain
 *	the line that was last stored there by LQU_fReadLine by up to
 *	LQT_READLINE_SLOP bytes; use erealloc to shrink it if desired.
 *	The LQT_READLINE_SLOP constant is defined in freadln.c as 30 bytes.</P>
 *   <Returns>
 *      a pointer to the buffer, or NULL if there isn't one yet.
 * </Function>
 */
API char *
LQU_StealReadLineBuffer()
{
    char *Result;

    Result = LQTpReadLineBuffer;

    LQTpReadLineBuffer = 0;
    LineLength = 0;

    return Result;
}
@


1.5
log
@for beta 6.
@
text
@d7 1
a7 1
/* $Id: freadln.c,v 1.4 96/07/01 21:36:20 lee Exp $
d16 6
a21 1
#include <malloc.h>
d79 1
a79 1
    while ((c = getc(f)) != EOF && c != '\n') {
d98 2
a99 2
	    (isascii(c) && isspace(c)) &&
	    (Flags & LQUF_IGNSPACES)
d140 7
d156 1
d158 1
a158 1
	    return -1;
d168 3
a170 1
    if (p > *Linep && p[-1] == '\r') {
d180 1
a180 1
	    if (&p[-1] > *Linep && p[-2] == '\\' && (Flags && LQUF_ESCAPEOK)) {
d202 1
a202 1
    if (p - *Linep < LineLength + 1 - LQT_READLINE_SLOP) {
d209 5
a213 2
	if ((*Linep = realloc(*Linep, LineLength + 1)) == (char *) 0) {
	    return LineLength;
@


1.4
log
@LQU_fReadLine no longer allocates each line...
@
text
@d7 1
a7 1
/* $Id: freadln.c,v 1.3 95/04/30 18:51:36 lee Exp $
d25 1
a25 1
# define LQT_READLINE_SLOP 128
d100 1
a100 1
	if (!p || p - (*Linep) >= LineLength - 1) {
a102 2
	    int WhereWeWere;

a103 1
		WhereWeWere = 0;
d105 2
a106 3
		if (LineLength < 4) {
		    LineLength = 4;
			/* it seems silly allocating less than this... */
d109 2
a110 1
		LQTpReadLineBuffer = *Linep = malloc(LineLength);
d112 5
a116 2
		/* save our position... */
		WhereWeWere = p - (*Linep);
d118 5
a122 7
		/* increase linelength */
		if (LineLength < 20) {
		    LineLength += 10;
		} else {
		    LineLength += (LineLength / 2);
		}
		*Linep = realloc(LQTpReadLineBuffer, LineLength);
d125 2
d132 1
d213 4
@


1.3
log
@rearrange header files;
call isascii before isspace.
@
text
@d7 1
a7 1
/* $Id: freadln.c,v 1.1 94/02/26 14:54:23 lee Exp $
a18 1
#include "readfile.h"
d21 10
d33 1
d35 7
a41 2
 *      Reads the next input line from the given file into a freshly
 *	malloc-allocated buffer.  The given Flags are treated as for
d46 1
a46 1
 *	from the file until a non-blank one is found.
d48 3
a50 5
 *      <LIST>
 *        <LI>a pointer to the line, in Line
 *        <LI>the number of bytes in the line, on success
 *        <LI>-1 on EOF
 *	</LIST>
a60 1
    unsigned int LineLength = 0;
d77 1
a77 1
	if (c == '\\' && (Flags & UF_ESCAPEOK)) {
d80 2
a81 2
	} else if (c == '#' && ((Flags & UF_IGNALLHASH) ||
				((Flags & UF_IGNHASH) && p == (*Linep)))) {
d94 1
a94 1
	    (Flags & UF_IGNSPACES)
d103 1
a103 1
	    int WhereWeWere = p - (*Linep);
d106 1
d108 2
a109 2
		if (LineLength < 2) {
		    LineLength = 10;
d113 1
a113 1
		*Linep = malloc(LineLength);
d115 2
a116 2
		if (LineLength < 20) LineLength += 10;
		else LineLength += (LineLength / 2);
d118 7
a124 1
		*Linep = realloc(*Linep, LineLength);
d139 1
a139 2
	} else if (Flags & UF_IGNBLANKS) {
	    if (*Linep) (void) free(*Linep);
d151 2
a152 4
     * file transfers (or using script(1) on 4.2 BSD Unix) and are
     * unwanted.
     * I hope.  (is this a bogus mis-feature?  I don't think so, but then,
     * I wrote it...)
d158 1
a158 1
    if (Flags & UF_IGNSPACES) {
d164 1
a164 1
	    if (&p[-1] > *Linep && p[-2] == '\\' && (Flags && UF_ESCAPEOK)) {
d178 1
a178 2
	} else if (Flags & UF_IGNBLANKS) {
	    (void) free(*Linep);
d186 4
a189 4
    if (p - *Linep < LineLength - 2) {
	/* The +2 in the test above is one for a \0 and one extra to reduce
	 * the number of relloc() calls -- an extra byte per line is no
	 * disaster, since we discard the \n anyway.
d201 26
@


1.2
log
@Added DOC.
@
text
@d10 3
a13 1
#include "error.h"
a14 1
#include <stdio.h>
d16 1
a17 2
#include <malloc.h>	/* This declares malloc(), realloc() etc... */

d79 5
a83 1
	} else if (p == (*Linep) && isspace(c) && (Flags & UF_IGNSPACES)) {
d144 4
a147 1
	while (p > *Linep && isspace(p[-1])) {
@


1.1
log
@API change
@
text
@d1 2
a2 1
/* readline.c -- Copyright 1993 Liam R. E. Quin.  All Rights Reserved.
d7 1
a7 1
/* $Id: readfile.c,v 1.6 92/07/05 12:50:06 lee Exp $
d22 18
@
