/* LQU_cknatstr.c -- Copyright 1989 Liam R. Quin.  All Rights Reserved.
 * This code is NOT in the public domain.
 * See the file COPYRIGHT for full details.
 */

/* $Id: cknatstr.c,v 1.5 1995/09/04 23:48:29 lee Exp $
 *
 * LQU_cknatstr - check that a string represents a natural number
 */

#include "error.h"
#include <stdio.h> /* required by ANSI C */
#include <sys/types.h>
#include "globals.h"

#include <ctype.h>
#include "lqutil.h"

/* <Function>
 *   <Name>LQU_cknatstr
 *   <Class>Utilities/Strings
 *   <Purpose>
 *      Checks whether the given string argument represents a natural
 *	number; that is, an optional plus or minus sign followed by
 *	one or more decimal digits.
 *	Leading whitespace, as reported by the isspace macro, is ignored,
 *	but no trailing whitespace is allowed.
 *   <Returns>
 *      Zero if the match fails, and one if it succeeds.
 *   <Bugs>
 *	<LIST>
 *        <LI>Should return a pointer to the first implausible character.</LI>
 *	  <LI>Should probably allow trailing whitespace.</LI>
 *	  <LI>Does not check its argument for a NULL pointer.</LI>
 *	</LIST>
 * </Function>
 */
API int
LQU_cknatstr(str)
    CONST char *str;
{
    /* check that a string represents a positive or 0 number */
    register CONST char *p = str;

    /* skip leading white space */
    while (isspace(*p)) {
	p++;
    }

    if (!*p) {
	/* Nothing there... */
	return 0;
    }

    /* allow a leading sign */
    if (*p == '-' || *p == '+') {
	p++;
    }

    if (!*p) {
	/* A possible + or - sign, but nothing after it! */
	return 0;
    }

    /* now skip digits... */
    while (isdigit(*p)) {
	p++;
    }

    return (p > str && *p == '\0');
}
