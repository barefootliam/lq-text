/* elseek.c -- Copyright 1994 Liam R. E. Quin.  All Rights Reserved.
 * This code is NOT in the public domain.
 * See the file COPYRIGHT for full details.
 */

/* $Id: elseek.c,v 1.5 2019/04/21 06:06:55 lee Exp $
 * Error checking version of lseek(), taken from Liam Quin's
 * unreleased error handling library and stripped down somewhat.
 */

#include "globals.h"
#include "error.h"

#include <stdio.h>

#include <errno.h>
#ifdef HAVE_STRING_H
# include <string.h>
#else
# include <strings.h>
#endif

#include <sys/types.h> /* for off_t */
#include <unistd.h>

#ifndef SEEK_SET
# define SEEK_SET 0
# define SEEK_CUR 1
# define SEEK_END 2
#endif

#include "lqutil.h"

/* <Function>
 *   <Name>LQU_Elseek
 *   <Class>Utilities/Files
 *   <Purpose>
 *      <P>This is a wrapper for the lseek(2) system call.  On an error,
 *	the given Name (which should reflect the corresponding file name,
 *	but need not be suitable to access that file) and What, which should
 *	be a terse description of the way in which the program is using the
 *	file, are used to construct a message passed to Error with the
 *	given Severity.</P>
 *	<P>The fd, Position and Whence arguments are as for the lseek(2)
 *	system call.</P>
 *   <Returns>
 *      The new file offset on success, or -1 on failure.
 *   <Errors>
 *      Generates an Error at the given Severity if lseek fails, adding
 *	(with bitwise or) E_SYS if appropriate.
 *   <Example>
 *	Where = LQU_lseek(E_FATAL, "passwd", "user database", 0, 0L, SEEK_SET);
 *   </Example>
 * </Function>
 */
API off_t
LQU_Elseek(Severity, Name, What, fd, Position, Whence)
    int Severity;
    CONST char *Name;
    CONST char *What;
    int fd;
    off_t Position;
    int Whence;
{
    off_t Result;
    int e;

    errno = 0;

    Result = lseek(fd, Position, Whence);

    if (Result >= 0L) {
	return Result;
    }

    if (fd < 0) {
	e = errno;

	Error(Severity|E_BUG,
	    "%s (%s) - LQU_Elseek(): file descriptor %d invalid",
	    Name, What,
	    fd
	);

	errno = e;
	return -1;
    }

    /* Save errno so we can restore it again after printing an error message,
     * in case Error() changes it:
     */
    e = errno;

    switch (errno) {
#ifdef EBADF
    case EBADF:
	Error(Severity|E_BUG,
	    "%s (%s): LQU_Elseek(): %d is not an open file descriptor",
	    Name, What,
	    fd
	);
	errno = e;
	return -1;
#endif

#ifdef EINVAL
    case EINVAL:
	if (Whence != SEEK_SET && Whence != SEEK_CUR && Whence != SEEK_END) {
	    Error(Severity|E_BUG,
"%s (%s): LQU_Elseek(%d, %ld, %d): Whence (%d) illegal value;\n\
Allowed lseek values are %d (SEEK_SET), %d (SEEK_CUR) and %d (SEEK_END)",
		Name, What,
		fd, Position, Whence,
		SEEK_SET, SEEK_CUR, SEEK_END
	    );
	} else if (Position < 0L) {
	    Error(Severity|E_SYS,
"%s (%s): LQU_Elseek(%d, %ld, %d) would result in invalid file position",
		Name, What,
		fd, Position, Whence
	    );
	}
	errno = e;
	return -1;
#endif

#ifdef ESPIPE
    case ESPIPE:
	Error(Severity|E_SYS,
	    "%s (%s): LQU_Elseek(%d ...): can't seek on a pipe, socket or FIFO",
	    Name, What,
	    fd, Position, Whence
	);
	errno = e;
	return -1;
#endif
    }

    Error(Severity|E_SYS,
	"%s (%s) - LQU_Elseek(): couldn't lseek to position %ld (%s)",
	Name, What,
	Position,
	((Whence == SEEK_SET) ? 
	    (
		"from start of file"
	    ) : (
		(Whence == SEEK_CUR) ?
		    "further into the file" :
		    "beyond the end of the file"
	    )
	)
    );

    errno = e;
    return -1;
}

