/* isdir.c -- Copyright 1989, 1994 Liam R. Quin.  All Rights Reserved.
 * This code is NOT in the public domain.
 * See the file COPYRIGHT for full details.
 */

/* $Id: isdir.c,v 1.5 1996/08/14 16:57:23 lee Exp $ */

#include "globals.h"
#include "error.h"

#include <stdio.h>

#include <sys/types.h>
#include <sys/stat.h>

#include "lqutil.h"

/* <Function>
 *   <Name>LQU_IsDir
 *   <Class>Utilities/Files
 *   <Purpose>
 *      returns 1 if and only if the given path is a directory.
 *	See the description for stat(2) for more details.
 *   <Errors>
 *	A fatal error is issued if LQU_IsDir is called with a null string;
 *	a warning is issued if the string is of length zero.
 * </Function>
 */
API int
LQU_IsDir(Dir)
    CONST char *Dir;
{
    struct stat statbuf;

    if (!Dir) {
	Error(E_FATAL|E_INTERNAL, "LQU_IsDir(<NULL pointer>) disallowed");
    } else if (!*Dir) {
	/* If the name has zero characters, on Unix it's the current directory,
	 * but that's probably not what was intended - you can use . for the
	 * current directory.
	 */
	Error(E_WARN, "LQU_IsDir(\"\") seems a little odd");
    }

    /* If the stat failed, chances are it doesn't exist.
     *
     * Another implementation is to look for "Directory/.", but that won't
     * work over NFS, and may generate error messages on the system console
     * with some versions of NFS!
     */
    if (stat(Dir, &statbuf) < 0) return 0;

    if ((statbuf.st_mode & S_IFMT) != S_IFDIR) {
	/* We could also check to see if it's a symbolic link that points
	 * at a directory, but that is unlikely, as stat(2) is supposed to
	 * return information about the target of the link, not about the
	 * file containing the link - see lstat(2).
	 * The only time it might say it's a symlink is if it points to a
	 * directory that isn't there, in which case we should return 0
	 * anyway!
	 */

	return 0;
    }

    return 1;
}
