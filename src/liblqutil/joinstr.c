/* joinstr.c -- Copyright 1989, 1993, 1994 Liam R. E. Quin.
 * All Rights Reserved.
 * This code is NOT in the public domain.
 * See the file COPYRIGHT for full details.
 */

/* $Id: joinstr.c,v 1.4 1996/08/14 16:57:29 lee Exp $ */

#include "error.h"
#include <stdio.h>
#include <sys/types.h>
#include "globals.h"
#include "emalloc.h"

#ifdef HAVE_STRING_H
# include <string.h>
#else
# include <strings.h>
#endif

#include "lqutil.h"

/* <Function>
 *   <Name>LQU_joinstr3
 *   <Class>Utilities/Strings
 *   <Purpose>
 *      Returns a string consisting of the concatenation of the three
 *	given strings.  The result is freshly malloc'd, and it is the
 *	caller's responsibility to free this storage.
 *	Null strings are treated as if they were empty strings.
 *   <Returns>
 *	The concatenation of the three given arguments.
 *   <Errors>
 *      Fatal error if memory is exhausted.
 *   <Bugs>
 *	The name is a little odd.
 *   <Example>
 *	char *theFile = LQU_joinstr3(directoryName, "/", fileName);
 *   </Example>
 * </Function>
 */
API char *
LQU_joinstr3(s1, s2, s3)
    CONST char *s1, *s2, *s3;
{
    char *p;
    int len1, len2;

    /* treat null strings as if they were empty, since
     * we can't do good error reporting from a function this low-level:
     */
    if (!s1) s1 = "";
    if (!s2) s2 = "";
    if (!s3) s3 = "";
    
    /* We need the string lengths, but optimise for the case that the
     * the middle string is only 1 character long.  This is actually
     * the usual case, for
     * LQU_joinstr3(theDirectory, "/", theFile)
     * is a common usage.
     */

    len1 = strlen(s1);
    if (s2[0] != '\0' && s2[1] == '\0') {
	len2 = 1;
    } else {
	len2 = strlen(s2);
    }

    p = emalloc("LQU_joinstr3", len1 + len2 + strlen(s3) + 1);
    /* ASSERT: p != 0 */
    (void) strcpy(p, s1);
    (void) strcpy(&p[len1], s2);
    (void) strcpy(&p[len1 + len2], s3);

    return p;
}

