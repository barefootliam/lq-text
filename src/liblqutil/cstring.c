/* cstring.c -- Copyright 1994 Liam R. E. Quin.
 * All Rights Reserved.
 * This code is NOT in the public domain.
 * See the file COPYRIGHT for full details.
 */

/* $Id: cstring.c,v 1.3 1996/08/14 16:57:10 lee Exp $ */

#include "error.h"
#include "globals.h"

#include <stdio.h>
#include "emalloc.h"

#ifdef HAVE_STRING_H
# include <string.h>
#else
# include <strings.h>
#endif

#include "lqutil.h"

PRIVATE int
OctalDigit(spp, DigitsSoFar)
    char *

/* <Function>
 *   <Name>LQU_cstring
 *   <Class>Utilities/Strings
 *   <Purpose>
 *      Converts any C escape sequences in the given string, and
 *	returns the result in a freshly malloc'd copy.
 *	The escape sequences currently recognised are
 *	\a (audible alert), \e (escape), \n (newline), \t (tab),
 *	\b (backspace), \r (return), \f (form feed), \\ (backslash),
 *	\' (single quote) and \" (double quote).
 *	The vertical tab (\v) is converted into a newline.
 *	The octal \ddd notation is understood; there can be up to three
 *	octal digits after the backslash.  If you need to follow an octal
 *	escape with an ASCII digit, you should use all three digits, with
 *	leading zeros if necessary.
 *	The ANSI C \xDD hexadecimal notation is not supported.
 *   <Returns>
 *      A pointer to a freshly allocated buffer; it is the caller's
 *	responsibility to free this.
 *	If a null pointer was passed as an argument, however, a null
 *	pointer is returned.
 *   <Errors>
 *      Warns if an unrecognised escape sequence or trigraph was found
 *   <Bugs>
 *	Has support neither for hexadecimal escapes (\xDD) nor for trigraphs
 *	(perhaps this is a feature).
 *	There is no way to include ASCII NUL (\000) into a string, as this
 *	terminates it.
 * </Function>
 */
API char *
LQU_cstring(theString)
    CONST char *theString
{
    register char *p, *q;
    char *Result;

    if (!theString) {
	Error(E_WARN,
	    "%s: %d: LQU_cstring: null pointer passed",
	    __FILE__, __LINE__
	);
	return (char *) 0;
    }

    q = Result = emalloc(theString, strlen(theString) + 1);

    for (p = theString; *p; p++) {
	if (*p == '\\') {
	    ++p;

	    if (!*p) {
		Error(E_WARN,
		    "Illegal trailing \\ in C-style string \"%s\"",
		    theString
		);
		*q = '\0';
		return Result;
	    }

	    switch (*p) {
	    case 'a': *q++ = '\007'; break; /* alert */
	    case 'e': *q++ = '\037'; break; /* ESCape */
	    case 'n': *q++ = '\n'; break;
	    case 't': *q++ = '\t'; break;
	    case 'b': *q++ = '\b'; break;
	    case 'r': *q++ = '\r'; break;
	    case 'f': *q++ = '\f'; break;
	    case 'v': *q++ = '\n'; break;
	    case '\\': *q++ = '\\'; break;
	    case '\'': *q++ = '\''; break;
	    case '\"': *q++ = '\"'; break;

	    case '0': case '1': case '2':
	    case '3': case '4': case '5':
	    case '6': case '7':
		{
		    int theValue = *p - '0';

		    ++p;

		    switch
		*q++ = '\n'; break;
		}
	    case '8': case '9':
		Error(E_WARN,
		    "illegal octal digit \\%c in C-style string \"%s\"",
		    *p, theString
		);
		*p = *q++;
		break;
	    default:
		*p = *q++;
	    }
	} else {
	    *p = *q++;
	}
    }
    *p = '\0';
    return Result;
}
