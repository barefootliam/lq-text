/* Isnzfile.c -- Copyright 1993, 1994 Liam R. Quin.
 * All Rights Reserved.
 * This code is NOT in the public domain.
 * See the file COPYRIGHT for full details.
 */

/* $Id: isnzfile.c,v 1.4 1996/08/14 16:57:27 lee Exp $ */

#include "globals.h"
#include "error.h"

#include <stdio.h>
#include <ctype.h>

#include <sys/types.h>
#include <sys/stat.h>

#include "lqutil.h"
#include "lqtrace.h"

/* <Function>
 *   <Name>LQU_IsNonEmptyFile
 *   <Class>Utilities/Files
 *   <Purpose>
 *      Determines whether the given Path names a regular file that contains
 *	data.
 *	In other words, the file must have the its stat st_mode's S_IFMT field
 *	set to S_IFREG, and must also have a non-zero st_size field;
 *	see the stat(2) man page.
 *   <Returns>
 *	Non-zero if and only if Path names a regular file of non-zero length
 *   <Notes>
 *	<P>There is tracing in here so that you can see which files are being
 *	investigated by the calling program; tracing is available if
 *	the liblqutil library was compiled with -DASCIITRACE; if so, you
 *	can set the FindFile trace flag (LQTRACE_FINDFILE) to see tracing for
 *	this routine.
 *	The -t "FindFile|Verbose" command-line option will do this.</P>
 *	<P>On systems that have the trace, strace or truss utility,
 *	investigate using that instead.</P>
 *   </Notes>
 *   <SeeAlso>
 * 	LQU_IsFile
 *	LQU_IsDir
 * </Function>
 */
API int
LQU_IsNonEmptyFile(Path)
    CONST char *Path;
{
    struct stat statbuf;

    if (stat(Path, &statbuf) < 0) {

#ifdef ASCIITRACE
	if (LQT_TraceFlagsSet(LQTRACE_FINDFILE)) {
	    Error(E_WARN|E_SYS,
		"IsNonZeroFile couldn't get file information (stat) for \"%s\"",
		Path
	    );
	}
#endif
	return 0;
    }

    if ((statbuf.st_mode & S_IFMT) != S_IFREG) {
#ifdef ASCIITRACE
	LQT_Trace(LQTRACE_FINDFILE,
	    "LQU_IsNonEmptyFile(%s) -> false (NOTFILE)",
	    Path
	);
#endif
	return 0;
    }

    if (statbuf.st_size == 0) {
#ifdef ASCIITRACE
	LQT_Trace(LQTRACE_FINDFILE,
	    "LQU_IsNonEmptyFile(%s) -> false (EMPTY)",
	    Path
	);
#endif
	return 0;
    }

#ifdef ASCIITRACE
    LQT_Trace(LQTRACE_FINDFILE,
	"LQU_IsNonEmptyFile(%s) -> true",
	Path
    );
#endif
    return 1;
}
