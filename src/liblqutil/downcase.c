/* downcase.c -- Copyright 1993, 1994 Liam R. E. Quin.
 * All Rights Reserved.
 * This code is NOT in the public domain.
 * See the file COPYRIGHT for full details.
 */

/* $Id: downcase.c,v 1.6 1995/09/05 00:18:55 lee Exp $
 */

#include "error.h"
#include <stdio.h> /* for ANSI C (sigh) */
#include <sys/types.h>
#include "globals.h"

#include <ctype.h>
#include "lqutil.h"
#include "emalloc.h"

#ifdef HAVE_STDLIB_H
# include <stdlib.h>
#endif
#ifndef tolower
 extern int tolower();
#endif

#ifdef HAVE_STRING_H
# include <string.h>
#else
# include <strings.h>
#endif 


/* <Function>
 *   <Name>LQU_DownCase
 *   <Class>Utilities/Strings
 *   <Purpose>
 *      Returns a pointer to a static buffer containing a copy of the
 *	given string in which all upper-case characters have been converted
 *	to lower case.
 *	The buffer grows automatically, and requires that the given String
 *	be nul-terminated.
 *   <Notes>
 *	Relies on correct support from isupper, as described in ctype(3).
 *	On some systems, this function returns garbage if a character with
 *	the top bit set is tested, and LOCALE has not been set.
 *   <Bugs>
 *	The argument is not checked to see if it is a NULL pointer.
 * </Function>
 */
API char *
LQU_DownCase(String)
    CONST char *String;
{
    static char *Buffer = 0;
    static unsigned int BufferLength = 0;
    register CONST char *p;
    register char *q;

    if (!Buffer) {
	BufferLength = strlen(String) + 1;
	Buffer = emalloc("LQU_DownCase Buffer", BufferLength);
    }

    for (p = String, q = Buffer; *p; p++, q++) {
	if (q - Buffer >= BufferLength - 1) {
	    int pos = q - Buffer;

	    BufferLength += 5;
	    Buffer = erealloc(Buffer, BufferLength);
	    q = &Buffer[pos];
	}
	if (isalnum(*p) && isupper(*p)) {
	    *q = tolower(*p);
	} else {
	    *q = (*p);
	}
    }

    *q = '\0';
    return Buffer;
}

