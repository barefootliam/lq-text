/* glue.c -- Copyright 2008 Liam R. E. Quin.  All Rights Reserved.
 * This code is NOT in the public domain.
 * See the file COPYRIGHT for full details.
 */

/* $Id: glue.c,v 1.1 2019/04/21 06:06:55 lee Exp $
 *
 * Glue: functions to bind C variables to scripting/formatting
 * variables.
 *
 * The Glue functions maintain only points to values.  The
 * values can be stored in local variables but not registers.
 */

#include "error.h"
#include <stdio.h> /* required by ANSI C */
#include <sys/types.h>
#include "globals.h"

#include "lqutil.h"

/* glue.h */

typedef enum {
    e_GlueType_Unknown = 0,
    e_GlueType_char,
    e_GlueType_int,
    e_GlueType_long,
    e_GlueType_string
} t_GlueVariableType;

/* A binding for a single variable */
typedef struct {
    char *Name;
    t_GlueVariableType VariableType;
    void *VariablePointer;
} t_GlueBinding;

/* A symbol table has an array of current bindings.
 * Symbol tables are searched in most-recently-added-first
 * order for variables.
 */
typedef struct s_GlueSymbolTable {
    int EntriesAllocated; /* unused ones are NUL pointers */
    t_GlueBinding **entries;
    struct s_GlueSymbolTable *Parent;
    struct s_GlueSymbolTable *Child;
} t_GlueSymbolTable;

PRIVATE t_GlueSymbolTable *
LQUp_GetLeastChild(t_GlueSymbolTable *Top, char *caller)
{
    int maxdepth = 10000;

    if (!caller) {
	Error(E_FATAL|E_INTERNAL, "LQUp_GetLeastChild: NULL caller");
    }
    if (!Top) {
	Error(E_FATAL|E_INTERNAL, "%s: LQUp_GetLeastChild: null Top", caller);
    }

    while (Top) {
	if (!Top->Child) {
	    return Top;
	}
	if (--maxdepth <= 0) {
	    Error(E_FATAL|E_INTERNAL, "%s: LQUp_GetLeastChild: loop!", caller);
	}
	Top = Top->Child;
    }
    
}


/* <Function>
 *   <Name>LQU_NewSymbolTable
 *   <Class>Utilities/Glue
 *   <Purpose>
 *      Creates a new t_GlueSymbolTable that can be used to hold
 *      bindings between C variables and externally-visible names.
 *      This function does not make the new symbol table active,
 *      unless a parent symbol table pointer is supplied, in which
 *      case the symbol table is appended to the chain.
 *   <Returns>
 *      A pointer to the new table, or NULL on failure.
 * </Function>
 */
API t_GlueSymbolTable *
LQU_NewSymbolTable(t_GlueSymbolTable *Parent)
{
    int i;
    t_GlueSymbolTable *result = (t_GlueSymbolTable *) emalloc(
	"new Glue symbol table",
	sizeof(t_GlueSymbolTable);
    );

    if (!result) {
	/* will not happen unless you have overridden the default
	 * error handler
	 */
	return 0;
    }

    /* the usual case is that you are going to allocate some
     * entries, so let's start off
     */
    result->EntriesAllocated = 6;
    result->entries = (t_GlueBinding **) ecalloc(
	"Glue table",
	6,
	sizeof (t_GlueBinding *)
    );

    for (i = 0; i < result->EntriesAllocated; i++) {
	result->entries[i] = 0;
    }

    result->Parent = 0;
    result->Child = 0;

    if (Parent) {
	(void) LQU_PushGlueTable(Parent, result);
    }

    return result;
}

/* <Function>
 *   <Name>LQU_PushGlueTable
 *   <Class>Utilities/Glue
 *   <Purpose>
 *      Pushes the given child symbol table onto the stack for
 *      the given parent.
 *      This makes all of the bindings in the child visible,
 *      overriding any bindings of the same name in the parent.
 *   <Returns>
 *      The given parent pointer.
 * </Function>
 */
API t_GlueSymbolTable *
LQU_PushGlueTable(
    t_GlueSymbolTable *Parent,
    t_GlueSymbolTable *NewChild
)
{
    int maxdepth = 10000;
    t_GlueSymbolTable *p;


    if (!Parent) {
	Error(E_FATAL|E_INTERNAL, "LQU_PushGlueTable called with NULL Parent");
    }

    if (!Child) {
	Error(E_FATAL|E_INTERNAL, "LQU_PushGlueTable called with NULL Child");
    }

    if (Parent == Child) {
	Error(E_FATAL|E_INTERNAL, "LQU_PushGlueTable: Child is own parent?!");
    }

    for (pp = Parent; pp; pp = pp->Child) {
	if (!pp->Child) {
	    pp->Child = NewChild;
	    if (NewChild->Parent && NewChild->Parent != pp) {
		Error(E_WARN|E_INTERNAL, "LQU_PushGlueTable: child reparented");
		/* should we free the old parent? I don't think so */
		if (NewChild->Parent->Child != NewChild) {
		    Error(E_FATAL|E_INTERNAL,
			"LQU_PushGlueTable: reparented child was not associated with its parent");
		}
		NewChild->Parent->Child = 0;
	    }
	    NewChild->Parent = pp;
	    return Parent;
	}
	if (--maxdepth <= 0) {
	    Error(E_FATAL|E_INTERNAL, "LQU_PushGlueTable: loop detected");
	}
    }
    return Parent;
}

/* <Function>
 *   <Name>LQU_PopSymbolTable
 *   <Class>Utilities/Glue
 *   <Purpose>
 *      Removes a symbol table from the given chain.
 *   <Returns>
 *      A pointer to the removed symbol table.
 * </Function>
 */
API t_GlueSymbolTable *
LQU_PopSymbolTable(
    t_GlueSymbolTable *Parent,
    t_GlueSymbolTable *UnwantedChild
)
{
    int maxdepth = 10000;
    t_GlueSymbolTable *pp;

    if (!Parent) {
	Error(E_FATAL|E_INTERNAL, "LQU_popGlueTable called with NULL Parent");
    }

    if (!UnwantedChild) {
	Error(E_FATAL|E_INTERNAL, "LQU_popGlueTable called with NULL Child");
    }

    if (UnwantedChild == parent) {
	Error(E_FATAL|E_INTERNAL, "LQU_popGlueTable: child is its own parent");
    }

    if (!UnwantedChild->Parent) {
	Error(E_FATAL|E_INTERNAL, "LQU_popGlueTable called with an orphan");
    }

    if (!Parent->Child) {
	Error(E_FATAL|E_INTERNAL, "LQU_popGlueTable called with childless table");
    }

    if (UnwantedChild->Child) {
	Error(E_FATAL|E_INTERNAL, "LQU_popGlueTable: child has children");
    }

    for (pp = Parent; pp; pp = pp->Child) {
	if (!pp->Child) {
	    Error(E_FATAL|E_INTERNAL, "LQU_popGlueTable: child not found");
	}
	if (pp->Child == NewChild) {
	    break;
	}

	if (--maxdepth <= 0) {
	    Error(E_FATAL|E_INTERNAL, "LQU_popGlueTable: loop detected");
	}
    }

    if (pp->Child != UnwantedChild) {
	Error(E_FATAL|E_INTERNAL, "LQU_popGlueTable: adoption detected");
    }

    if (pp != UnwantedChild->Parent) {
	Error(E_FATAL|E_INTERNAL, "LQU_popGlueTable: fostering detected");
    }

    UnwantedChild->Parent = 0;
    pp->Child = 0;

    return Parent;
}

/**************************************
 * Bindings
 *************/

/* <Function>
 *   <Name>LQU_AddBindingToTable
 *   <Class>Utilities/Glue
 *   <Purpose>
 *      Associates a variable name with a pointer to its value
 *      in the given Glue Table
 *   <Returns>
 *      The given symbol table
 * </Function>
 */
API t_GlueSymbolTable *
LQU_AddBindingToTable(
    t_GlueSymbolTable *Parent,
    unsigned char *Name,
    t_GlueVariableType ValueType,
    void *Value
)
{
    t_GlueBinding *NewBinding;
    int i;
    t_GlueSymbolTable *tp;
    char *lower;

    if (!Parent) {
	Error(E_FATAL|E_INTERNAL,
	    "LQU_AddBindingToTable called with NULL Parent pointer"
	);
    }

    if (!Name) {
	Error(E_FATAL|E_INTERNAL,
	    "LQU_AddBindingToTable called with NULL Name pointer"
	);
    }

    if (!Value) {
	Error(E_FATAL|E_INTERNAL,
	    "LQU_AddBindingToTable called with NULL Value pointer"
	);
    }

    if (ValueType == e_GlueType_Unknown) {
	Error(E_FATAL|E_INTERNAL,
	    "LQU_AddBindingToTable called with unknown ValueType "
	);
    }

    lower = LQU_DownCase(Name);

    /* OK, allocate a new slot in the parent... */
    tp = LQUp_GetLeastChild(Top, "LQU_AddBindingToTable");
    if (!tp) {
	/* cannot happen with code as designed... */
	Error(E_FATAL|E_INTERNAL, "LQU_AddBindingToTable: tp is 0");
    }

    if (!tp->entries || tp->EntriesAllocated == 0) {
	result->entries = (t_GlueBinding **) ecalloc(
	    "Glue table",
	    6,
	    sizeof (t_GlueBinding *)
	);

	for (i = 0; i < result->EntriesAllocated; i++) {
	    result->entries[i] = 0;
	}
    } else if (tp->entries[tp->EntriesAllocated - 1]) {
	tp->entries = (t_GlueBinding **) erealloc(
	    (void *) tp->entries,
	    sizeof(t_GlueBinding *) * (tp->EntriesAllocated + 6)
	);
	if (!tp->Entries) {
	    Error(E_FATAL|E_INTERNAL|E_MEMORY,
		"LQU_AddBindingToTable %s: realloc %d bytes failed",
		sizeof(t_GlueBinding *) * (tp->EntriesAllocated + 6)
	    );
	}
	for (i = 0; i < 6; i++) {
	    tp->entries[tp->EntriesAllocated + i] = 0;
	}
	tp->EntriesAllocated += 6;
    }

    /* now we have at least one available entry */

    for (i = 0; i < tp->EntriesAllocated; i++) {
	if (!tp->entries[i]) {
	    break;
	}
	if (STREQ(tp->entries[i]->Name, lower)) {
	    /* already there */
	    if (tp->entries[i]->VariableType != ValueType) {
		/* Disallow changing the type of a variable:
		 * you must remove the binding and then add it
		 * again.  The attempt is most likely a bug.
		 * NOTE: we allow shadowing of a variable by
		 * another of the same name but a different type.
		 */
		Error(E_FATAL|E_INTERNAL,
		    "LQU_AddBindingToTable %s: unsupported type change %d/%d",
		    tp->entries[i]->VariableType, ValueType
		);
	    }
	    tp->entries[i]->VariablePointer = Value;
	    return Top;
	}
    }

    NewBinding = LQU_NewGlueBinding(Name, ValueType, Value);
    tp->entries[i] = NewBinding;


    return Top;
}
