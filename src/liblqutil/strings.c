/* strings.c -- Copyright 1989, 1994 Liam R. Quin.
 * All Rights Reserved.
 * This code is NOT in the public domain.
 * See the file COPYRIGHT for full details.
 */

/* $Id: strings.c,v 1.7 2019/04/21 06:06:55 lee Exp $ */

/* LQU_StringContainedIn(s1, s2) - true if s2 contains an instance of s1;
 *
 * Also LQU_UTF8FirstNonPartialOctet to find the first byte in a string past
 * a given point is not the tail end of a multi-byte UTF8 sequence.
 */


#include "error.h"
#include <stdio.h>
#include <sys/types.h>
#include "globals.h"
#include "lqutil.h"

#ifdef HAVE_STRING_H
# include <string.h>
#else
# include <strings.h>
#endif /*HAVE_STRING_H*/


# if 0

/* A little helper function... but not needed because it's included
 * inline in Strcontains().  It's here in case I ever want it again!
 * Modern Unix systems have a library routine to do this, see strstr.
 */

PRIVATE_INLINE int
strprefix(Prefix, String)
    register char *Prefix;
    register char *String;
{
    while (*String++ == *Prefix++) {
	if (!*Prefix) return 1;
    }
    return 0;
}

#endif

/* <Function>
 *   <Name>LQU_StringContainedIn
 *   <Class>Utilities/Strings
 *   <Purpose>
 *      Determines whether the given ShortString is contained anywhere 
 *	in the LongString, and, if so, returns non-zero.
 *   <Returns>
 *      <LIST>
 *        <LI>1 if the shorter string is contained in the longer, or
 *	      if the strings are equal, of if ShortString is of length zero
 *        <LI>0 otherwise
 *	</LIST>
 *   <Notes>
 *	See strstr for a more efficient way to do this.  Some Unix systems
 *	do not have strstr, though.
 *   </Notes>
 * </Function>
 */
API int
LQU_StringContainedIn(ShortString, LongString)
    CONST char *ShortString;
    CONST char *LongString;
{
    register CONST char *p;
    unsigned int LongStringLength;
    unsigned int ShortStringLength;
    int LastShortCharacter;

    /* This is basically a simple "grep" algorithm.
     * Adding Boyer-Moore style delta tables would speed it up, but
     * it isn't used all that often in lq-text - at most a dozen or so
     * times per input file.  I've gone as far as putting strprefix() inline,
     * to save a few function calls, but that's all.
     */

    if (!LongString || !*LongString || !ShortString) {
	return 0;
    } else if (!ShortString) {
	return 1; /* null prefix */
    }

    LongStringLength = strlen(LongString);

    ShortStringLength = 0;
    for (p = ShortString; *p; p++) {
	++ShortStringLength;
    }

    LastShortCharacter = p[ShortStringLength - 1];

    for (
	p = LongString;
	p - LongString <= LongStringLength - ShortStringLength;
	p++
    ) {
	/* check both the first and last strings are equal.
	 * This is a slight burden on 1-character prefixes, but
	 * potentially a big win on what is otherwise an n^2
	 * algorithm if the two strings are of nearly equal lengths.
	 */
	if (
	    *p == *ShortString &&
	    p[ShortStringLength - 1] == LastShortCharacter
	) {
	    register CONST char *String = p;
	    register CONST char *Prefix = ShortString;

	    while (*String++ == *Prefix++) {
		if (!*Prefix) {
		    return 1;
		}
	    }
	}
    }
    return 0;
}

/* <Function>
 *   <Name>LQU_UTF8FirstNonPartialOctet
 *   <Class>Utilities/Strings
 *   <Purpose>
 *      Avoid splitting strings in the middle of multi-byte characters by
 *      finding the first suitable point.
 *   <Returns>
 *        A pointer to the first place to split; this may be the end of
 *        the string.
 * </Function>
 */
API unsigned char *
LQU_UTF8FirstNonPartialOctet(Start)
    unsigned char *Start;
{
    /* In UTF-8, you can tell the position of a chacater by looking at
     * its value range:
     * 00-7F = single-byte character
     * 80-BF = trailing octet
     * C0-DF = leading byte of 2-octet character
     * E0-EF = leading byte of 3-octet character
     * F0-F7 = leading byte of 4-octet character
     * F8-FB = leading byte of 5-octet character (now illegal)
     * FC-FD = leading byte of 6-octet character (now illegal)
     * FE-FF = illegal, byte order mark at start of file
     */
    if (*Start <= 0x7f) {
	return Start;
    }

    /* Start points to a character with the top bit set, either the
     * start or in the middle
     */
    for (; *Start; Start++) {
	if (*Start <= 0xbf) { /* trailing byte, so next is start */
	    return ++Start;
	}

	if (*Start >= 0xc0 && *Start <= 0xfd) {
	    /* found beginning char */
	    return Start;
	}

	if (((*Start) & 0075) == 0) {
	    return Start;
	}
    }

    return Start;
}

/* <Function>
 *   <Name>LQU_UTF8LengthFromFirstByte
 *   <Class>Utilities/Strings
 *   <Purpose>
 *      Given the first byte of a UTF character sequence, return the
 *      total expected length in bytes (at least one byte of course)
 *   <Returns>
 *        The number of bytes, from 1 to 6.
 *   <Notes>
 *        Does not handle diacritical marks: a following Unicode character
 *        might overlay the current character.
 * </Function>
 */
API unsigned long
LQU_UTF8LengthFromFirstByte(c)
    CONST unsigned char c;
{
    if (c == (unsigned char) 0xff || c == (unsigned char) 0xfe) {
	return 1; /* an illegal character */
    }

    if (c >= (unsigned char) 0xfc) {
	return 6;
    } else if (c >= (unsigned char) 0xf8) {
	return 5;
    } else if (c >= (unsigned char) 0xf0) {
	return 4;
    } else if (c >= (unsigned char) 0xe0) {
	return 3;
    } else if (c >= (unsigned char) 0xc0) {
	return 2;
    }
    return 1;
}

/* <Function>
 *   <Name>LQU_UTF8CharLength
 *   <Class>Utilities/Strings
 *   <Purpose>
 *      Find the length in displayed characters of a UTF-8 string.
 *   <Returns>
 *        The number of characters.  Multi-byte characters count as
 *        one, regardless of their length.
 *   <Notes>
 *        Does not (yet) handle diacritical marks.
 * </Function>
 */
API unsigned long
LQU_UTF8CharLength(String)
    CONST unsigned char *String;
{
    /* In UTF-8, you can tell the position of a chacater by looking at
     * its value range:
     * 00-7F = single-byte character
     * 80-BF = trailing octet
     * C0-DF = leading byte of 2-octet character
     * E0-EF = leading byte of 3-octet character
     * F0-F7 = leading byte of 4-octet character
     * F8-FB = leading byte of 5-octet character (now illegal)
     * FC-FD = leading byte of 6-octet character (now illegal)
     * FE-FF = illegal, byte order mark at start of file
     */
    unsigned char *Start = LQU_UTF8FirstNonPartialOctet(String);
    unsigned long result = 0L;

    while (*Start) {
	int skip = 1;

	if (*Start == (unsigned char) 0xff || *Start == (unsigned char) 0xfe) {
	    continue; /* BOM or illegal */
	}

	if (*Start >= (unsigned char) 0xfc) {
	    skip = 6;
	} else if (*Start >= (unsigned char) 0xf8) {
	    skip = 5;
	} else if (*Start >= (unsigned char) 0xf0) {
	    skip = 4;
	} else if (*Start >= (unsigned char) 0xe0) {
	    skip = 3;
	} else if (*Start >= (unsigned char) 0xc0) {
	    skip = 2;
	}

	while (skip-- > 0) {
	    if (*Start) {
		++Start;
	    } else {
		return result;
	    }
	}
	++result;
    }

    return result;
}
