/* freadf.c -- Copyright 1992, 1994 Liam R. E. Quin.
 * All Rights Reserved.
 * This code is NOT in the public domain.
 * See the file COPYRIGHT for full details.
 */

/* $Id: freadf.c,v 1.5 2019/04/21 06:06:55 lee Exp $
 * read files into memory.
 */

#include "error.h"
#include <stdio.h>
#include <sys/types.h>

#include "globals.h"

#include <ctype.h>
#include <stdio.h>
#include <ctype.h>	/* isspace() etc */

#ifdef HAVE_STDLIB_H
# include <stdlib.h>
#else
# include <malloc.h>
#endif

#include "emalloc.h"

#include "lqutil.h"

/* <Function>
 *   <Name>LQU_fReadFile
 *   <Class>Utilities/Files
 *   <Purpose>
 *	<P>Reads the named file (Name), and mallocs an array of char * pointers
 *	to the start of each line read.  The number of lines returned may
 *	be less than the number in the file, since by default LQU_fReadFile
 *	ignores blank or commented lines.  Comments are denoted by a # as
 *	the first non-blank character on the line.  If the file can't be
 *	opened, memory is exhausted, LQU_ReadFile() calls Error() with the
 *	given Severity, and with an error message constructed out of What,
 *	which should be a short (e.g. 3-word) description of the purpose of
 *	the file.  The Flags argument can contain any of the following,
 *	combined with or (|):</P>
 *	<LIST>
 *	  <LI>UF_IGNBLANKS to throw away blank lines,
 *	  <LI>UF_IGNSPACES to discard leading and trailing spaces,
 *	  <LI>UF_IGNHASH   to discard leading comments (# with a hash-sign)
 *	  <LI>UF_IGNALLHASH   to discard comments (# with a hash-sign)
 *	  <LI>UF_ESCAPEOK  to accept "\#" and "\\" as "#" and "\"
 *	</LIST>
 *	<P>In addition, UF_NORMAL is defined to be
 *	UF_IGNBLANKS | UF_IGNSPACES | UF_IGNHASH | UF_ESCAPEOK
 *	and use of this in reading files is strongly encouraged to provide
 *	a consistent file format.</P>
 *   <Returns>
 *      <LIST>
 *        <LI>a pointer to the array of lines, in Lines
 *        <LI>the number of lines allocated.
 *        <LI>-1 if the file couldn't be opened.
 *	</LIST>
 *   <Errors>
 *      Warns (with the given severity | E_SYS) if the file can't be opened.
 * </Function>
 */
API long
LQU_fReadFile(Severity, f, Name, What, Lines, Flags)
    int Severity;
    FILE *f;
    CONST char *Name;
    CONST char *What;
    char ***Lines;
    int Flags;
{
    /* This is where the real work is done... */

    long NumberOfLines = 0L;
    long LinesAllocated = 0L;
    char *Linep;

    LinesAllocated = 20;
    *Lines = (char **) emalloc("line buffer array", sizeof(char *) * LinesAllocated);

    while (LQU_fReadLine(f, &Linep, Flags) >= 0) {
	if (NumberOfLines >= LinesAllocated) {
	    LinesAllocated += 50;
	    *Lines = (char **) erealloc((char *) *Lines,
				(unsigned) (LinesAllocated * sizeof(char *)));
	    if (!*Lines) {
		Error(Severity|E_MEMORY, "Lines from \"%s\" (%s)", Name, What);
		return -1;
	    }
	}

	(*Lines)[NumberOfLines] = LQU_StealReadLineBuffer();
	++NumberOfLines;
    }

    return NumberOfLines;
}

