/* progname.c -- Copyright 1989 Liam R. Quin.  All Rights Reserved.
 * This code is NOT in the public domain.
 * See the file COPYRIGHT for full details.
 *
 * This file simply declares progname.
 * This variable MUST be set, usually by main() from argv[0].
 */

char *progname = (char *) 0;

/* $Id: progname.c,v 1.3 1994/02/26 15:01:41 lee Exp $
 *
 */
