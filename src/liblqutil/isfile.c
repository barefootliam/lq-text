/* isfile.c -- Copyright 1989, 1994, 1995 Liam R. Quin.
 * All Rights Reserved.
 * This code is NOT in the public domain.
 * See the file COPYRIGHT for full details.
 */

/* $Id: isfile.c,v 1.4 1996/08/14 16:57:25 lee Exp $ */

#include "globals.h"
#include "error.h"

#include <stdio.h>

#include <sys/types.h>
#include <sys/stat.h>

#include "lqutil.h"
#include "lqtrace.h"

/* <Function>
 *   <Name>LQU_IsFile
 *   <Class>Utilities/Files
 *   <Purpose>
 *      <P>Determines whether the given Path refers to a regular file.
 *	Devices (such as /dev/null or a terminal), and directories in
 *	particular are not regular files.  The Unix command</P>
 *	<P>find filename -type f -print</P>
 *	<P>will print out filename if and only if LQU_IsFile would
 *	return 1 for the same filename.
 *   <Returns>
 *      <LIST>
 *	  <LI>1 if the given Path represents a regular file</LI>
 *	  <LI>zero otherwise</LI>
 *	</LIST>
 *   <Notes>
 *	<P>There is tracing in here so that you can see which files are being
 *	investigated by the calling program; tracing is available if
 *	the liblqutil library was compiled with -DASCIITRACE; if so, you
 *	can set the FindFile trace flag (LQTRACE_FINDFILE) to see tracing for
 *	this routine.
 *	The -t "FindFile|Verbose" command-line option will do this.</P>
 *	<P>On systems that have the trace, strace or truss utility,
 *	investigate using that instead.</P>
 *   </Notes>
 * </Function>
 */
API int
LQU_IsFile(Path)
    CONST char *Path;
{
    struct stat statbuf;

    if (stat(Path, &statbuf) < 0) {

#ifdef ASCIITRACE
	if (LQT_TraceFlagsSet(LQTRACE_FINDFILE)) {
	    Error(E_WARN|E_SYS,
		"LQU_IsFile couldn't get file information (stat) for \"%s\"",
		Path
	    );
	}
#endif
	return 0;
    }

    if ((statbuf.st_mode & S_IFMT) != S_IFREG) {
#ifdef ASCIITRACE
	LQT_Trace(LQTRACE_FINDFILE,
	    "LQU_IsFile(%s) -> false",
	    Path
	);
#endif
	return 0;
    }

#ifdef ASCIITRACE
    LQT_Trace(LQTRACE_FINDFILE,
	"LQU_IsFile(%s) -> true",
	Path
    );
#endif
    return 1;
}
