/* joinstr2.c -- Copyright 1995, 1996 Liam R. E. Quin.
 * All Rights Reserved.
 * This code is NOT in the public domain.
 * See the file COPYRIGHT for full details.
 */

/* $Id: joinstr2.c,v 1.1 1996/05/14 23:47:47 lee Exp $ */

#include "error.h"
#include <stdio.h>
#include <sys/types.h>
#include "globals.h"
#include "emalloc.h"

#ifdef HAVE_STRING_H
# include <string.h>
#else
# include <strings.h>
#endif

#include "lqutil.h"

/* <Function>
 *   <Name>LQU_joinstr2
 *   <Class>Utilities/Strings
 *   <Purpose>
 *      Returns a string consisting of the concatenation of the two
 *	given strings.  The result is freshly malloc'd, and it is the
 *	caller's responsibility to free this storage.
 *	Null strings are treated as if they were empty strings.
 *   <Returns>
 *	The concatenation of the three given arguments.
 *   <Errors>
 *      Fatal error if memory is exhausted.
 *   <Bugs>
 *	The name is a little odd.
 *   <Example>
 *	char *theFile = LQU_joinstr2(directoryName, "/fileName");
 *   </Example>
 *   <SeeAlso>
 *	LQU_joinstr3
 * </Function>
 */
API char *
LQU_joinstr2(s1, s2)
    CONST char *s1, *s2;
{
    char *p;
    int len1;

    /* treat null strings as if they were empty, since
     * we can't do good error reporting from a function this low-level:
     */
    if (!s1) s1 = "";
    if (!s2) s2 = "";
    
    len1 = strlen(s1);

    p = emalloc("LQU_joinstr3", len1 + strlen(s2) + 1);
    /* ASSERT: p != 0 */
    (void) strcpy(p, s1);
    (void) strcpy(&p[len1], s2);

    return p;
}

