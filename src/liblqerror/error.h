/** error.h $Id: error.h,v 1.10 96/07/04 22:10:29 lee Exp $
 ** Liam Quin,
 **
 ** Include this header file if you plan to call Error().
 **
 ** The various E_ values are passed as the first argument to Error():
 ** 	Error(E_FATAL|E_INTERNAL, "%s forgot to write the program", Person);
 **
 ** Remaining arguments are passed to printf.
 **
 ** If you are going to include <errno.h> (not normally necessary), do so
 ** before including this file.
 **
 **/

extern void Error(
# ifdef HAVE_PROTO
    unsigned int flags,
    CONST char *format,
    ...
# endif
);

#ifndef E_FATAL
    /* This ifndef is so that you can include error.h twice if you want.
     */

/* #define NEEDERRNO */
    /* See ErrTest.c for more details of this.
     * Define it if you do not have errno, and Error() will no longer
     * look at sys_errlist
     */

# define E_FATAL	001
	/* Fatal error -- call exit() */
# define E_WARN		002
	/* Warning -- we'll try & continuel; Error() will not call
	 * exit() if the E_WARN bit is set.
	 */
# define E_INTERNAL	004
	/* the error was caused by an internal or programmer error;
	 * this is suitable for use with an assertion failure, or
	 * just after a NOTREACHED comment...
	 */
# define E_MEMORY	010 /* e.g. malloc returns zero */
	/* note that a memory error is not fatal, so you should use
	 *    E_MEMORY|E_FATAL
	 * to make it so if you can't recover.
	 * The E_MEMORY flag prevents Error() from opening pop-up windows
	 * (if compiled with System V curses, in some implementations),
	 * or from any other action that might invole malloc().
	 */
# define E_SYS		020
	/* Use this if a system call or library routine returned an error.
	 * For example, fopen() returned (FILE *) NULL, or putc returned
	 * EOF, or read returned -1 and errno != EINTR.
	 */
# define E_USAGE	040
	/* Use this if you are using Error() to give a command-line usage
	 * message.  I am not sure whether this is not a mis-feature.
	 */
# define E_XHINT	0100
	/* If you use this, the error message will be followed by a single
	 * line saying
	 * cmdname: progname: use the -x option for an explanation.
	 */

# define E_ABORT	(0200|E_FATAL)
	/* Use this to force a core dump.  It's good for debugging...
	 */

# define E_BUG	(0400|E_ABORT|E_INTERNAL|E_FATAL)
	/* If you use this Error() will look at the external variable
	 * AsciiTrace, and, if set to a value of 3 or more, will abort.
	 * Otherwise, it's like E_INTERNAL.
	 * This interface to AsciiTrace is likely to change
	 * when I add the tracing module...
	 */

# define E_MULTILINE 01000
# define E_LASTLINE  02000
	/* This is for all except the last of a multi-line error message.
	 * The idea is that only the last (E_LASTLINE) will actually
	 * cause an exit.
	 */

# ifdef NEEDERRNO
#  ifndef _ERROR_C
	/* _ERROR_C is set in error.c to minimise loading grief with
	 * multilple definitions of errno, or with spurious externs.
	 */
extern int errno;
#  endif

# ifndef ENOENT
#  define ENOENT 2 /* this is the usual Unix value */
# endif

# define open   _e_open
# define fopen  _e_fopen
    /* If necessary, we define variants of open() and fopen() in error.c
     * These are * functions in error.c; you can take their addresses, etc.
     * They set errno to plausible values when they are called, but in
     * every other way are the same as the "real" ones.
     */
  extern int open();
#  ifdef FILE
   extern FILE *fopen();
   /* If we already included <stdio.h>, we have to declare the new fopen()
    */
#  endif

#endif

#endif /* !E_FATAL */
