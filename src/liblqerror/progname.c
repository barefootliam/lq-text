/* progname.c -- Copyright 1989 Liam R. Quin.  All Rights Reserved.
 * This code is NOT in the public domain.
 * See the file COPYRIGHT for full details.
 * This file simply declares progname.
 * This variable MUST be set by main().
 */

char *progname = (char *) 0;

/* $Id: progname.c,v 1.1 1994/02/26 14:53:22 lee Exp $
 *
 * $Log: progname.c,v $
 * Revision 1.1  94/02/26  14:53:22  lee
 * API change
 * 
 * Revision 1.2  90/10/06  00:12:19  lee
 * Prepared for first beta release.
 * 
 * Revision 1.1  90/03/24  17:07:22  lee
 * Initial revision
 * 
 *
 */
