/* cmdname.c -- Copyright 1989 Liam R. Quin.  All Rights Reserved.
 * This code is NOT in the public domain.
 * See the file COPYRIGHT for full details.
 */

char *cmdname = (char *) 0;

/* $Id: cmdname.c,v 1.1 1994/02/26 14:52:56 lee Exp $
 *
 * $Log: cmdname.c,v $
 * Revision 1.1  94/02/26  14:52:56  lee
 * API change
 * 
 * Revision 1.2  90/10/06  00:12:08  lee
 * Prepared for first beta release.
 * 
 * Revision 1.1  90/03/24  17:08:19  lee
 * Initial revision
 * 
 *
 */
