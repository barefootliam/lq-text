#include <stdio.h>
#include <glib.h>

main()
{
    char buffer[BUFSIZ];

    while (fgets(buffer, BUFSIZ, stdin)) {
	int len = strlen(buffer);

	gchar *normalized = g_utf8_normalize(buffer, -1, G_NORMALIZE_DEFAULT_COMPOSE);

	if (normalized) {
	    gchar *p = normalized;
	    gunichar u;

	    while (p && *p && (u = g_utf8_get_char(p)) != 0) {
		GUnicodeType gt;
		switch (u) {
		    case -1:
			printf(" [invalid] ");
			break;
		    case -2:
			printf(" [incomplete] ");
			break;
		    default:
			gt = g_unichar_type(u);
			if (gt != G_UNICODE_COMBINING_MARK &&
				gt != G_UNICODE_NON_SPACING_MARK) {
			    gchar mybuf[12] = { 0, };
			    int n = g_unichar_to_utf8(u, (char *) mybuf);

			    if (n > 0) {
				mybuf[n] = '\0';
				printf(" %s-%d ", mybuf, (int) gt);
			    }
			}
		}
		p = g_utf8_next_char(p);
	    } /* while p */
	    printf("\n");
	} /* if */
    } /* while fgets */
}
