/* XMLFilter.c -- Copyright 2008 Liam R. Quin.
 * All Rights Reserved.
 * This code is NOT in the public domain.
 * See the file COPYRIGHT for full details.
 *
 * $Id: XMLFilter.c,v 1.1 2019/04/21 06:03:39 lee Exp $
 *
 * Filter for Extensible Markup Language (XML) files.
 *
 */

#include "error.h"

#include <stdio.h>
#include <sys/types.h>
#include "globals.h"

#include <malloc.h>
#include <ctype.h>
#ifdef HAVE_STRING_H
# include <string.h>
#else
# include <strings.h>
#endif

#ifdef HAVE_STDLIB_H
# include <stdlib.h>
#endif

#ifdef HAVE_UNISTD_H
# include <unistd.h>
#endif

#include "wordrules.h"
#include "chartype.h"
#include "emalloc.h"
#include "lqutil.h"
#include "liblqtext.h"
#include "filter.h"

#ifndef START_ONLY_ONCE
# define START_ONLY_ONCE { static char _x = 0; if (!_x) { _x = 1;
# define END_ONLY_ONCE } }
#endif

#include <stdio.h>
/** Functions in this file that need to be declared **/
PRIVATE_INLINE int GetChar(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *db,
    FILE *fd
#endif
);

/** **/

static char InHeader = 0;
    /* InHeader is set if the file type is HTML and 
     * we have not seen BODY yet;
     * we don't index the head, by default.
     */
static int LastChar = 0;
static int InWord = 0;
static int LastInWord = 0;

PRIVATE void
Init()
{
    InHeader = 0;
    LastChar = 0;
    InWord = 0;
    LastInWord = 0;
}

PRIVATE_INLINE void
IgnoreChar(db, ch, OutputFile)
    t_LQTEXT_Database *db;
    int ch;
    FILE *OutputFile;
{
    if (!InWord) {
	if (isspace(ch) || LQT_ISPUNCT(db, ch)) {
	    putc(ch, OutputFile);
	} else if (LQT_ISDIGIT(db, ch)) {
	    putc(LQT_DIGIT_TO_IGNORE, OutputFile);
	} else {
	    putc(' ', OutputFile);
	}
    } else if (LQT_ISDIGIT(db, ch)) {
	putc(LQT_DIGIT_TO_IGNORE, OutputFile);
    } else {
	putc(LQT_CHAR_TO_IGNORE, OutputFile);
    }
}

PRIVATE_INLINE int
GetChar(db, fd)
    t_LQTEXT_Database *db;
    FILE *fd;
{
    if (LastChar) {
	int ch = LastChar;
	InWord = LastInWord;
	LastChar = 0;
	return ch;
    }

    if ((LastChar = getc(fd)) == EOF) return EOF;

    LastInWord = InWord;

    if (InWord == 0) {
	InWord = LQT_STARTS_WORD(db, LastChar);
    } else if (!LQT_WITHIN_OR_ENDS_WORD(db, LastChar)) {
	InWord = 0;
    }

    /* Only return a single quote if it is within a word:
     * can't --- OK
     * ...hello' he said --- rejected
     * '' --- rejected
     * 30's --- OK
     * 30'66" -- rejected
     */
    if (LastChar == '\'') {
	LastChar = getc(fd);
	if (InWord && !LQT_WITHIN_OR_ENDS_WORD(db, LastChar)) {
	    /* trailing ' sign */
	    LastInWord = InWord = 0;
	} else {
	    LastInWord = LQT_STARTS_WORD(db, LastChar);
	}
	return '\'';
    } else {
	int ch = LastChar;
	LastChar = 0;
	return ch;
    }
}

PRIVATE_INLINE void
UnGetChar(fd, c)
    FILE *fd;
    int c;
{
    if (LastChar) {
	(void) ungetc(LastChar, stdin);
    }
    LastChar = c;
    LastInWord = InWord;
}

#define isxmldelim(ch) (isspace(ch) || ch == ';' || ch == '<')

PRIVATE_INLINE void
SendIndexflag(Flag, Command, OutputFile)
    int Command;
    t_WordFlags Flag;
    FILE *OutputFile;
{
    unsigned int ch;
    putc(Command, OutputFile);
    ch = (Flag & 0377);
    putc(ch, OutputFile);
    ch = (Flag & (0377 << 8)) >> 8;
    putc(ch, OutputFile);
}

PRIVATE void
SetIndexflag(Flag, OutputFile)
    t_WordFlags Flag;
    FILE *OutputFile;
{
    SendIndexflag(Flag, LQT_CHAR_TO_SET_FLAG, OutputFile);
}

PRIVATE void
ClearIndexflag(Flag, OutputFile)
    t_WordFlags Flag;
    FILE *OutputFile;
{
    SendIndexflag(Flag, LQT_CHAR_TO_CLEAR_FLAG, OutputFile);
}


LIBRARY int
LQF_XML_Copy(db, InputFile, Name, OutputFile)
    t_LQTEXT_Database *db;
    FILE *InputFile;
    char *Name;
    FILE *OutputFile;
{
    int WithinATag = 0;
    int WithinAString = 0;
    int WithinSpecial = 0;
    char QuoteChar = 0;
    char Warned = 0;
    int ch;

#define MAXNMLEN 999
    unsigned char ElementNameBuffer[MAXNMLEN + 1];
    unsigned char *positionInElement = ElementNameBuffer;


    Init();

    InHeader = (db->IgnoreHTMLhead) ? 1 : 0;

    /* BUG: we should check putc()'s return value to see if 
     * the output file system has filled up.
     *
     * But I want to rewrite this so it doesn't need to copy
     * the file anyway.  I.e. the filter should be
     * calling LQT_AddWordPlaces and indeing each word.
     */
    while ((ch = GetChar(db, InputFile)) != EOF) {
	if (InHeader && WithinATag) {
	    if (isspace(ch) || ch == '>') {
		char *p;

		if (positionInElement > ElementNameBuffer) {
		    if (STREQ(ElementNameBuffer, db->IgnoreHTMLhead)) {
			InHeader = 0;
			InWord = 0;
			/* fprintf(stderr, "found tag %s\n", db->IgnoreHTMLhead); */
		    } else {
			/* fprintf(stderr, "! tag %s\n", ElementNameBuffer); */
		    }
		}

		InWord = 1;
		for (p = ElementNameBuffer; p < positionInElement; p++) {
		    IgnoreChar(db, *p, OutputFile);
		}
		InWord = 0;

		positionInElement = ElementNameBuffer;
		putc(ch, OutputFile);
		WithinATag = 0; /* don't care until InHeader == 0 */
	    } else {
		if (ElementNameBuffer - positionInElement < MAXNMLEN) {
		    *positionInElement++ = ch;
		    *positionInElement = '\0';
		}
	    }
	    continue;
	}

	/* <! starts comment or doctype declaration
	 * </ starts end tag
	 * <? starts processing instruction
	 * < followed by name char begins a start tag
	 */
	if (ch == '<' && !WithinATag) {
	    WithinATag = 1;
	    putc('<', OutputFile);

	    positionInElement = ElementNameBuffer;
	    *positionInElement = '\0';

	    switch (ch = GetChar(db, InputFile)) {
	    case EOF:
		fflush(stdout);
		Error(E_WARN, "%s: End of file within a tag", Name);
		return 0;
	    case '/': /* </endtag> */
		putc(ch, OutputFile);
		continue;
	    case '!': /* <! ..... > */
		WithinSpecial = 1;
		IgnoreChar(db, ch, OutputFile);
		continue;
	    case '\n': /* whitespace is illegal after < so not a tag */
	    case ' ':
	    case '\t':
	    case '\r':
		WithinATag = 0;
		putc(ch, OutputFile);
		continue;
	    case '<':
		if (!Warned) {
		    Error(E_WARN, "%s: Badly formed XML, found <<", Name);
		    Warned = 1;
		}
		WithinATag = 0;
		putc(ch, OutputFile);
		continue;
	    case '>':
		if (!Warned) {
		    Error(E_WARN, "%s: Badly formed XML, found <>", Name);
		    Warned = 1;
		}
		WithinATag = 0;
		putc(ch, OutputFile);
		continue;
	    default:
		if (isalpha(ch) && InHeader) {
		    if (ElementNameBuffer - positionInElement < MAXNMLEN) {
			*positionInElement++ = ch;
			*positionInElement = '\0';
		    } else {
			/* ignore absurdly long names -- you probably didn't
			 * want them anyway!
			 */
			char *p;
			for (p = ElementNameBuffer; p < positionInElement; p++){
			    IgnoreChar(db, *p, OutputFile);
			}
			positionInElement = ElementNameBuffer;
			break;
		    }
		} else {
		    /* IgnoreChar(db, ch, OutputFile); */
		    putc(ch, OutputFile);
		}
		continue;
	    }
	}

	if (WithinATag) {
	    /* special processing for attributes, ! and so forth */
	    if (WithinAString) {
		if (ch == QuoteChar) {
		    WithinAString = 0;
		    QuoteChar = 0;
		    InWord = 0;
		    putc(ch, OutputFile);
		    ClearIndexflag(WPF_XMLATTRIBUTE, OutputFile);
		} else {
		    putc(ch, OutputFile);
		}
		continue;
	    } else {
		/* within a tag but not a string */
		while (isspace(ch)) {
		    putc(ch, OutputFile);
		    if ((ch = GetChar(db, InputFile)) == EOF) {
			Error(E_WARN, "%s: end of file inside a tag!", Name);
			return 0;
		    }
		}
		switch (ch) {
		case '"': case '\'':
		    InWord = 0;
		    WithinAString = 1;
		    QuoteChar = ch;
		    SetIndexflag(WPF_XMLATTRIBUTE, OutputFile);
		    break;
		case '<': /* for DOCTYPE etc... */
		    if (WithinSpecial) {
			WithinATag++;
		    } else {
			if (!Warned) {
			    Error(E_WARN, "%s: < within a tag!", Name);
			    Warned = 1;
			}
		    }
		    break;
		case '>':
		    WithinATag--;
		    if (WithinSpecial && WithinATag == 0) {
			WithinSpecial = 0;
		    }
		    break;
		case '-': /* TODO NOTDONE FIXME do XML comments */
		default:
		    break;
		}
		/* IgnoreChar(db, ch, OutputFile); */
		putc(ch, OutputFile);
		continue;
	    }
	} else { /* not within a tag */
	    /* TODO: handle entities
	     * this is tricky to get right whilst mantaining the byte count...
	     * variable sized *input* blocks required for this.
	     */
	    if (ch == '<') {
		WithinATag++;
	    } else if (ch == '&') {
		SetIndexflag(WPF_XMLENTITYNAME, OutputFile);
		IgnoreChar(db, '&', OutputFile);
		while ((ch = GetChar(db, InputFile)) != EOF) {
		    if (isxmldelim(ch)) {
			/* although only ; ends an entity reference,
			 * we want to index files with errors, so
			 * we will also stop on a space or tag or quote.
			 * Errors often happen in URIs.
			 */
			break;
		    }
		    putc(ch, OutputFile);
		}
		if (ch == EOF) {
		    Error(E_WARN,
			"%s: end of file inside an entity!",
			Name
		    );
		    return 0;
		}
		if (ch == ';') {
		    putc(' ', OutputFile);
		} else {
		    UnGetChar(InputFile, ch);
		    continue;
		}
		ClearIndexflag(WPF_XMLENTITYNAME, OutputFile);
	    } else {
		if (InHeader) {
		    IgnoreChar(db, ch, OutputFile);
		} else {
		    putc(ch, OutputFile);
		}
	    }
	}
    }
    return 0;
}
