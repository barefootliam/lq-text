#! /usr/bin/perl -w
use strict;
use FileHandle;
use IPC::Open2;

# first argument is the element name
# production version uses -e element
my $e = shift @ARGV;

my $var = '${g.q} ${xml.contentbefore.' . $e . '->startbyte} ${NumberOfWordsInPhrase} ${BlockInFile} ${WordInBlock} ${FID} ${filename}\n';

my ($Writer, $Reader);
my $pid = open2($Reader, $Writer, "lqrank -g -r all -Q -F - | lqkwic -l4000 -r4000 -S '' -s '$var'") or die "could not open lqrank/lqkwic: $!";

my $resultSetCount = 0;
foreach (@ARGV) {
    print $Writer "$_\n";
    $resultSetCount++;
}
close($Writer) or die "can't close pipe: $!";


# want results only if every set is represented for a
# given startbyte, endbyte pair.  Actually we only need the startbyte

my ($startbyte, $endbyte, $fid) = (-1, -1, -1);
my %sets;
my $setCount = 0;
my @pending;


while (<$Reader>) {
    chomp;
    # g.q endbyte NumberOfWordsInPhrase BlockInFile WordInBlock FID filename
    # 0   1       2                     3           4           5   6
    my @fields = split;
    next if ($fields[1] eq "-"); # no such ancestor element
    if ($startbyte != $fields[1] || $fid != $fields[5]) {
	if ($setCount == $resultSetCount) {
	    foreach (@pending) {
		print join(" ", @{$_}), "\n";
	    }
	} else {
	    # print "# setCount $setCount sb was $startbyte now $fields[1]\n";
	}
	@pending = ();
	$setCount = 0;
	%sets = ();
	$startbyte = $fields[1];
	$fid = $fields[5];
    }
    if (!exists $sets{$fields[0]}) {
	$sets{$fields[0]} = 1;
	$setCount++;
	# print "# match for set $fields[0], setcount $setCount, sb $startbyte fid $fid, seen:" . join(", ", keys(%sets)) . "\n";
    }
    push @pending, [ @fields[2 .. 6] ];
}

if ($setCount == $resultSetCount) {
    foreach (@pending) {
	print join(" ", @{$_}), "\n";
    }
}

close($Reader) or die "can't close input: $!";

