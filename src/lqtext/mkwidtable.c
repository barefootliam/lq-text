/* mkwidtable.c -- Copyright 1992, 1994 Liam R. Quin.
 * All Rights Reserved.
 * This code is NOT in the public domain.
 * See the file COPYRIGHT for full details.
 *
 * Rebuild the database that lq-text uses to map words into numbers.
 * This is useful if you think it got corrupt.  It is also useful if you
 * are going to store a database, because you can remove the dbm files that
 * make up the wordlist.  WARNING: this requires the database to have
 * wordlist on
 * in the configuration ("README") file.
 *
 * $Id: mkwidtable.c,v 1.4 2019/04/21 06:08:14 lee Exp $
 */

#include "globals.h"
#include "error.h"

#include <stdio.h>

#include <sys/types.h>

#ifdef HAVE_UNISTD_H
# include <unistd.h> /* why is this needed? */
#endif

#ifdef HAVE_STDLIB_H
# include <stdlib.h>
#endif

#include "fileinfo.h"
#include "wordindex.h"
#include "wordinfo.h"
#include "smalldb.h"
#include "lqutil.h"
#include "liblqtext.h"
#include "numbers.h"
#include "lqtrace.h"

char *progname = "mkwidtable";

PRIVATE int
PutWordIntoIndex(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *,
    t_WordInfo *
#endif
);

PRIVATE void
MakeNewWordMap(db)
    t_LQTEXT_Database *db;
{
    long WordsSeen = 0;
    FILE *IndexFile;
    char Entry[WIDBLOCKSIZE];
    char Word[1024];

    LQT_ObtainWriteAccess(db);

    IndexFile = LQU_fEopen(E_FATAL,
	db->WidIndexFile,
	"fixed size record word index",
	"r"
    );

    /* for each word in the index */

    while (fread(Entry, sizeof Entry, 1, IndexFile) > 0) {
	t_WordInfo WordInfo;

	if (!WordsSeen) {
	    WordsSeen++;
	    continue; /* block 0 unused */
	}

	/* make a WordInfo structure */
	WordInfo.Length = (int) Entry[0];
	WordInfo.Word = Word;
	WordInfo.WID = WordsSeen;
	strncpy(Word, &Entry[1], WordInfo.Length); /* [1] coz of length byte */
	Word[WordInfo.Length] = '\0';

	LQT_Trace(LQTRACE_VERBOSE,
	    "%ld %s", WordsSeen,
	    Word
	);

	/* put the word into the index */
	if (PutWordIntoIndex(db, &WordInfo) < 0) {
	    Error(E_BUG, "failed to put \"%s\" into index\n", Word);
	}

	WordsSeen++;
    }
}

int
main(argc, argv)
    int argc;
    char *argv[];
{
    t_LQTEXT_Database *db;
    t_lqdbOptions *Options;

    progname = argv[0];

    Options = LQT_InitFromArgv(argc, argv);
    db = LQT_OpenDatabase(Options, O_RDWR, 0660);
    MakeNewWordMap(db);
    LQT_CloseDatabase(db);
    exit(0);
    return -1; /** this is here for lint and gcc */
}

PRIVATE int
PutWordIntoIndex(db, WordInfo)
    t_LQTEXT_Database *db;
    t_WordInfo *WordInfo;
{
    unsigned char NumBuf[sizeof(t_WID) + 1];
    unsigned char *q = NumBuf;
    datum key, data;

    /** First, write the WID itself, so we can go from Word to WID */

    (void) bzero(&key, sizeof key);
    (void) bzero(&data, sizeof data);

    key.dptr = WordInfo->Word;
    key.dsize = WordInfo->Length;

    (void) LQT_sWriteNumber(&q, WordInfo->WID, NumBuf, sizeof NumBuf);

    data.dptr = (char *) NumBuf;
    data.dsize = q - NumBuf;

    /* contact database server */
    if (!db->WordIndexDB) {
	db->WordIndexDB = LQT_OpenKeyValueDatabase(db, db->WordIndex);
	if (!db->WordIndexDB) {
	    Error(E_FATAL|E_SYS,
		    "PutWordIntoIndex could not open dbm word index %s",
		    db->WordIndex
	    );
	}
    }

    return dbm_store(db->WordIndexDB, key, data, DBM_REPLACE);
}
