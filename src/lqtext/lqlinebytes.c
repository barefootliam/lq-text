/* lqlinebytes.c -- Copyright 1995, 1996 Liam R. E. Quin.
 * All Rights Reserved.
 * This code is NOT in the public domain.
 * See the file COPYRIGHT for full details.
 *
 * $Id: lqlinebytes.c,v 1.1 1996/05/15 23:24:32 lee Exp $
 *
 * Turns line numbers into byte offsets
 */

#include "error.h"
#include "globals.h" /* defines and declarations for database filenames */
#include <stdio.h>
#include <ctype.h>
#include <sys/types.h>
#ifdef HAVE_STRING_H
# include <string.h>
#else
# include <strings.h>
#endif

#include "emalloc.h"
#include "lqutil.h"
#include "liblqtext.h"

#ifdef HAVE_UNISTD_H
# include <unistd.h>
#endif

#ifdef HAVE_STDLIB_H
# include <stdlib.h>
#endif

/** System calls and functions... **/
/** Unix system calls used in this file: **/
/** Unix Library Functions used: **/
/** lqtext library functions: **/
/** functions used before they're defined within this file: **/
static void ProcessFile(
#ifdef HAVE_PROTO
    CONST char *InputFile
#endif
);

/** **/

static char *Revision = "@(#) $$";

char *progname = "lqbyteline";

int
main(argc, argv)
    int argc;
    char *argv[];
{
    extern int optind, getopt();
    extern char *optarg;
    int ch;
    int ErrorFlag = 0;
    char *InputFile = 0;

    progname = argv[0];

    LQT_InitFromArgv(argc, argv);

    while ((ch = getopt(argc, argv, "Zz:af:hNpr:slxVv")) != EOF) {
	switch (ch) {
	case 'z':
	case 'Z':
	    break; /* done by LQT_InitFromArgv(); */
	case 'V':
	    fprintf(stderr, "%s version %s\n", progname, Revision);
	    break;
	case 'f':
	    InputFile = optarg;
	    break;
	case 'x':
	    ErrorFlag = (-1);
	    break;
	case '?':
	    ErrorFlag = 1;
	}
    }

    if (ErrorFlag) {
	fprintf(stderr, "Usage: %s [options] [file [...]]\n", progname);
	LQT_PrintDefaultUsage();
	exit( ErrorFlag > 0 ? 1 : 0); /* 0 means -x was used */
    }
    
    if (InputFile) {
	ProcessFile(InputFile);
    }

    if (optind >= argc) {
	ProcessFile("-");
    } else {
	while (optind < argc) {
	    ProcessFile(argv[optind++]);
	}
    }

    return 0;
}

static void
ProcessFile(InputFile)
    CONST char *InputFile;
{
    FILE *f;
    char NeedClose = 1;
    char *theLine;

    if (STREQ(InputFile, "-")) {
	f = stdin;
	NeedClose = 0;
	InputFile = "standard input";
    } else {
	f = LQU_fEopen(E_FATAL|E_SYS, InputFile, "input file to measure", "r");
    }

    {
	int c;
	long lineNumber = 1; /* we're already on line 1...*/
	long whereWeAre = 0L;
	int wantReport = 1;

	while ((c = getc(f)) != EOF) {
	    if (wantReport) {
		printf("%ld\t%ld\n", lineNumber, whereWeAre);
		wantReport = 0;
	    }
	    ++whereWeAre;
	    if (c == '\n') {
		++lineNumber;
		wantReport = 1;
	    }
	}

	if (wantReport) {
	    printf("%ld\t%ld\n", lineNumber, whereWeAre);
	}
    }

    if (NeedClose) {
	(void) close(f);
    }
}

