:
# FindCommon -- Copyright 1990 Liam R. Quin.  All Rights Reserved.
# This code is NOT in the public domain.
# See the file COPYRIGHT for full details.
#
# $Id: FindCommon.sh,v 1.3 1996/05/25 19:18:24 lee Exp $

# Find the most common words in the database.
# usage is % n, where n is the n most comon words to find

MAX=
if test ! -z "$1"
then
    MAX="| sed -e $1q"
fi

lqwordlist -n -u -g . | eval sort -0 +1n $MAX
