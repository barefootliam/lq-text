# Makefile for LQ-Text, a full text retrieval package by Liam R. Quin
# This Makefile belongs in the "src/lqtext" directory.
#
# Note that most of the actual configuration is done in ../Makefile and
# in ../h/global.h, and not here.

# Makefile -- Copyright 1990, 1995 Liam R. Quin.  All Rights Reserved.
# This code is NOT in the public domain.
# See the file ../COPYRIGHT for full details.
#
# $Id: Makefile,v 1.12 2019/04/21 06:08:14 lee Exp $


BINFILES =lqaddfile lqbyteline lqcat lqfile lqword lqphrase lqquery lqshow \
    lqkwic mkwidtable lqwordlist lqrename lqrank lqsed sortwids \
    lqsimilar lqunindex addbyline

SHELLFILES=lq lqclean lqsort lqmkindex
TARGETS = $(BINFILES) $(SHELLFILES)

# Following not yet shipped:
# scanfile lqmkfree lines mksignatures lqwordslike

# The following are for "make depend" and for sabre to load...
DEPENDFILES = lqaddfile.c lqbyteline lqfile.c lqword.c lqphrase.c lqquery.c \
	lqshow.c lqkwic.c mkwidtable.c lqwordlist.c lqrename.c lqrank.c  \
	sortwids.c lqsimilar.c wordtable.c addbyline.c

DESTDIR=/home/lee/lqtext/bin
MODE=755
RANLIB=echo

BININSTALL=install -s -c -m $(MODE)
SHINSTALL=install -c -m $(MODE)
# a copy of install is included in the utils directory.

EXTRA=-I../h

REGEX=-L../regex -lregex

all: $(TARGETS)

# DBMLIBS are set in the top level makefile.
DBMLIBS=-ldo_not_run_make_from_here
# DBMLIBS=ndbm.o bcopy.o

ERRORLIB=-llqerror
LQUTILLIB=-llqutil
FILTERLIB=../filters/liblqfilter.a
TEXTLIB=-L../lib -llqtext $(LQUTILLIB) $(FILTERLIB) $(ERRORLIB)
LIBDEPS=../liblqtext/liblqtext.a ../liblqerror/liblqerror.a ../liblqutil/liblqutil.a $(FILTERLIB)

# MALLFILES=/usr/lib/debug/malloc.o /usr/lib/debug/mallocmap.o
MALLFILES =

local: all
	-echo lqtext up to date.

install: all
	for i in $(BINFILES); do \
	    ${BININSTALL} "$$i" "$(DESTDIR)/bin/$$i" ; \
	done ; \
	for i in $(SHELLFILES); do \
	    ${SHINSTALL} "$$i" "$(DESTDIR)/bin/$$i" ; \
	done

lq: lq.sh
	cp lq.sh lq
	chmod +x lq

lqsort: $(LIBDEPS) lqsort.sh
	cp lqsort.sh lqsort
	chmod +x lqsort

lqclean: lqclean.sh
	cp lqclean.sh lqclean

lqcat: $(LIBDEPS) lqcat.o
	$(CC) $(CFLAGS) -o lqcat lqcat.o $(TEXTLIB) $(MALLOC) $(DBMLIBS)

lqquery: $(LIBDEPS) lqquery.o $(REGEX_DEP)
	$(CC) $(CFLAGS) -o lqquery lqquery.o $(TEXTLIB) $(MALLOC) $(DBMLIBS) $(REGEX)

lqrank: $(LIBDEPS) lqrank.o $(REGEX_DEP)
	$(CC) $(CFLAGS) -o lqrank lqrank.o $(TEXTLIB) $(MALLOC) $(DBMLIBS) $(REGEX)

lqsed: $(LIBDEPS) lqsed.o
	$(CC) $(CFLAGS) -o lqsed lqsed.o $(TEXTLIB) $(MALLOC) $(DBMLIBS)

lqshow: $(LIBDEPS) lqshow.o
	$(CC) $(CFLAGS) -o lqshow lqshow.o $(TEXTLIB) $(MALLOC) $(CURSES) $(DBMLIBS)

lines: $(LIBDEPS) lines.o
	$(CC) $(CFLAGS) -o lines lines.o $(MALLOC) $(TEXTLIB)
	
sortwids: $(LIBDEPS) sortwids.o
	$(CC) $(CFLAGS) -o sortwids sortwids.o \
			$(TEXTLIB) $(MALLOC) $(DBMLIBS) $(MALLFILES)

lqrename: $(LIBDEPS) lqrename.o
	$(CC) $(CFLAGS) -o lqrename lqrename.o \
			$(TEXTLIB) $(MALLOC) $(DBMLIBS) $(MALLFILES)

scanfile: $(LIBDEPS) scanfile.o
	$(CC) $(CFLAGS) -o scanfile scanfile.o \
			$(TEXTLIB) $(MALLOC) $(DBMLIBS) $(MALLFILES)

addbyline: $(LIBDEPS) addbyline.o wordtable.o
	$(CC) $(CFLAGS) -o addbyline addbyline.o wordtable.o \
			$(TEXTLIB) $(MALLOC) $(DBMLIBS) $(MALLFILES)

lqaddfile: $(LIBDEPS) lqaddfile.o wordtable.o
	$(CC) $(CFLAGS) -o lqaddfile lqaddfile.o wordtable.o \
			$(TEXTLIB) $(MALLOC) $(DBMLIBS) $(MALLFILES)

fastopen: $(LIBDEPS) fastopen.o
	$(CC) $(CFLAGS) -o fastopen fastopen.o $(TEXTLIB) $(MALLOC) $(DBMLIBS)

lqunindex: $(LIBDEPS) lqunindex.o
	$(CC) $(CFLAGS) -o lqunindex lqunindex.o $(TEXTLIB) $(MALLOC) $(DBMLIBS)

lqfile: $(LIBDEPS) lqfile.o
	$(CC) $(CFLAGS) -o lqfile lqfile.o $(TEXTLIB) $(MALLOC) $(DBMLIBS)
	
lqmkfree: $(LIBDEPS) lqmkfree.o
	$(CC) $(CFLAGS) -o lqmkfree lqmkfree.o $(TEXTLIB) $(DBMLIBS) $(MALLOC)

lqword: $(LIBDEPS) lqword.o
	$(CC) $(CFLAGS) -o lqword lqword.o $(TEXTLIB) $(MALLOC) $(DBMLIBS)

lqwordlist: $(LIBDEPS) lqwordlist.o $(REGEX_DEP)
	$(CC) $(CFLAGS) -o lqwordlist lqwordlist.o $(TEXTLIB) $(MALLOC) $(DBMLIBS) $(REGEX)

lqkwic: $(LIBDEPS) lqkwic.o
	$(CC) $(CFLAGS) -o lqkwic lqkwic.o $(TEXTLIB) $(MALLOC) $(DBMLIBS) $(TERMCAP)

lqsimilar: $(LIBDEPS) lqsimilar.o
	$(CC) $(CFLAGS) -o lqsimilar lqsimilar.o $(TEXTLIB) $(MALLOC) $(DBMLIBS)

lqphrase: $(LIBDEPS) lqphrase.o
	$(CC) $(CFLAGS) -o lqphrase lqphrase.o $(TEXTLIB) $(MALLOC) $(DBMLIBS)

mksignatures: $(LIBDEPS) mksignatures.o
	$(CC) $(CFLAGS) -o mksignatures mksignatures.o $(TEXTLIB) $(MALLOC) $(DBMLIBS)

lqwordslike: $(LIBDEPS) lqwordslike.o
	$(CC) $(CFLAGS) -o lqwordslike lqwordslike.o $(TEXTLIB) $(MALLOC) $(DBMLIBS)

mkwidtable: $(LIBDEPS) mkwidtable.o
	$(CC) $(CFLAGS) -o mkwidtable mkwidtable.o $(TEXTLIB) $(MALLOC) $(DBMLIBS)

lqbyteline: $(LIBDEPS) lqbyteline.o
	$(CC) $(CFLAGS) -o lqbyteline lqbyteline.o $(TEXTLIB) $(MALLOC) $(DBMLIBS)

lqlinebytes: $(LIBDEPS) lqlinebytes.o
	$(CC) $(CFLAGS) -o lqlinebytes lqlinebytes.o $(TEXTLIB) $(MALLOC) $(DBMLIBS)

lqmv: lqmv.o $(ERRORLIB)
	$(CC) $(CFLAGS) -o lqmv lqmv.o $(ERRORLIB) $(MALLOC)

lint: AddFile.Lint News.Lint FileInfo.Lint Phrase.Lint

tidy:
	/bin/rm -f *.o core

clean: tidy
	/bin/rm -f $(TARGETS) $(TEST)

depend:
	chmod +w Makefile
	mkdep $(CFLAGS) *.c

# DO NOT DELETE THIS LINE -- mkdep uses it.
# DO NOT PUT ANYTHING AFTER THIS LINE, IT WILL GO AWAY.

addbyline.o: addbyline.c ../h/globals.h ../h/port.h ../h/wordrules.h ../h/api.h
addbyline.o: ../h/port2.h ../h/error.h
addbyline.o: ../h/fileinfo.h
addbyline.o: ../h/wordinfo.h ../h/wordplace.h ../h/wordrules.h ../h/emalloc.h
addbyline.o: ../h/addfile.h ../h/lqutil.h ../h/range.h ../h/revision.h
addbyline.o: ../h/liblqtext.h 
addbyline.o: ../h/phrase.h ../h/lqconfig.h ../h/pblock.h ../h/chartype.h
addbyline.o: ../h/lqtrace.h ../h/filter.h
lqaddfile.o: lqaddfile.c ../h/globals.h ../h/port.h ../h/wordrules.h ../h/api.h
lqaddfile.o: ../h/port2.h ../h/error.h
lqaddfile.o: ../h/fileinfo.h ../h/wordinfo.h ../h/wordplace.h ../h/wordrules.h
lqaddfile.o: ../h/emalloc.h ../h/addfile.h ../h/lqutil.h ../h/range.h
lqaddfile.o: ../h/liblqtext.h 
lqaddfile.o: ../h/phrase.h ../h/lqconfig.h ../h/pblock.h ../h/chartype.h
lqaddfile.o: ../h/filter.h ../h/lqtrace.h ../h/revision.h
lqbyteline.o: lqbyteline.c ../h/error.h ../h/globals.h ../h/port.h
lqbyteline.o: ../h/wordrules.h ../h/api.h ../h/port2.h 
lqbyteline.o: ../h/emalloc.h ../h/lqutil.h
lqbyteline.o: ../h/range.h ../h/liblqtext.h ../h/wordinfo.h ../h/wordplace.h
lqbyteline.o: ../h/fileinfo.h 
lqbyteline.o: ../h/phrase.h ../h/lqconfig.h ../h/pblock.h ../h/chartype.h
lqfile.o: lqfile.c ../h/globals.h ../h/port.h ../h/wordrules.h ../h/api.h
lqfile.o: ../h/port2.h ../h/error.h 
lqfile.o: ../h/emalloc.h ../h/fileinfo.h ../h/lqutil.h
lqfile.o: ../h/range.h ../h/liblqtext.h ../h/wordinfo.h ../h/wordplace.h
lqfile.o: ../h/phrase.h
lqfile.o: ../h/lqconfig.h ../h/pblock.h ../h/chartype.h
lqkwic.o: lqkwic.c ../h/error.h 
lqkwic.o: ../h/globals.h ../h/port.h ../h/wordrules.h ../h/api.h ../h/port2.h
lqkwic.o: ../h/range.h
lqkwic.o: ../h/fileinfo.h ../h/wordinfo.h ../h/wordplace.h ../h/wordrules.h
lqkwic.o: ../h/emalloc.h ../h/lqutil.h ../h/liblqtext.h
lqkwic.o: ../h/phrase.h
lqkwic.o: ../h/lqconfig.h ../h/pblock.h ../h/chartype.h ../h/wmoffset.h
lqkwic.o: ../h/lqtrace.h ../h/namespace.h 
lqlinebytes.o: lqlinebytes.c ../h/error.h ../h/globals.h ../h/port.h
lqlinebytes.o: ../h/wordrules.h ../h/api.h ../h/port2.h 
lqlinebytes.o: ../h/emalloc.h ../h/lqutil.h
lqlinebytes.o: ../h/range.h ../h/liblqtext.h ../h/wordinfo.h ../h/wordplace.h
lqlinebytes.o: ../h/fileinfo.h 
lqlinebytes.o: ../h/phrase.h ../h/lqconfig.h
lqlinebytes.o: ../h/pblock.h ../h/chartype.h 
lqmkfree.o: lqmkfree.c ../h/globals.h ../h/port.h ../h/wordrules.h ../h/api.h
lqmkfree.o: ../h/port2.h ../h/error.h 
lqmkfree.o: ../h/emalloc.h
lqmkfree.o: ../h/fileinfo.h ../h/wordinfo.h ../h/wordplace.h ../h/pblock.h
lqmkfree.o: ../h/blkheader.h ../h/lqutil.h ../h/range.h
lqmv.o: lqmv.c ../h/globals.h ../h/port.h ../h/wordrules.h ../h/api.h
lqmv.o: ../h/port2.h 
lqmv.o: ../h/error.h 
lqmv.o: ../h/emalloc.h ../h/fileinfo.h ../h/lqutil.h ../h/range.h
lqmv.o: ../h/liblqtext.h ../h/wordinfo.h ../h/wordplace.h
lqmv.o: ../h/phrase.h ../h/lqconfig.h ../h/pblock.h
lqmv.o: ../h/chartype.h
lqphrase.o: lqphrase.c ../h/error.h ../h/globals.h ../h/port.h ../h/wordrules.h
lqphrase.o: ../h/api.h ../h/port2.h 
lqphrase.o: ../h/emalloc.h ../h/fileinfo.h
lqphrase.o: ../h/wordinfo.h ../h/wordplace.h ../h/pblock.h ../h/phrase.h
lqphrase.o: ../h/lqutil.h ../h/range.h ../h/liblqtext.h
lqphrase.o: ../h/lqconfig.h
lqphrase.o: ../h/chartype.h ../h/lqtrace.h
lqquery.o: lqquery.c ../h/error.h ../h/globals.h ../h/port.h ../h/wordrules.h
lqquery.o: ../h/api.h ../h/port2.h 
lqquery.o: ../h/emalloc.h ../h/fileinfo.h
lqquery.o: ../h/wordinfo.h ../h/wordplace.h ../h/pblock.h ../h/phrase.h
lqquery.o: ../h/lqutil.h ../h/range.h ../h/liblqtext.h 
lqquery.o: ../h/lqconfig.h ../h/chartype.h
lqquery.o: ../h/lqtrace.h
lqrank.o: lqrank.c ../h/globals.h ../h/port.h ../h/wordrules.h ../h/api.h
lqrank.o: ../h/port2.h ../h/error.h 
lqrank.o: ../h/emalloc.h ../h/fileinfo.h
lqrank.o: ../h/wordinfo.h ../h/wordplace.h ../h/phrase.h ../h/lqutil.h
lqrank.o: ../h/range.h ../h/liblqtext.h 
lqrank.o: ../h/lqconfig.h ../h/pblock.h ../h/chartype.h
lqrank.o: ../h/lqtrace.h
lqrename.o: lqrename.c ../h/globals.h ../h/port.h ../h/wordrules.h ../h/api.h
lqrename.o: ../h/port2.h ../h/error.h 
lqrename.o: ../h/emalloc.h
lqrename.o: ../h/fileinfo.h ../h/lqutil.h ../h/range.h ../h/liblqtext.h
lqrename.o: ../h/wordinfo.h ../h/wordplace.h 
lqrename.o: ../h/phrase.h ../h/lqconfig.h ../h/pblock.h
lqrename.o: ../h/chartype.h ../h/lqtrace.h
lqsed.o: lqsed.c ../h/globals.h ../h/port.h ../h/wordrules.h ../h/api.h
lqsed.o: ../h/port2.h ../h/error.h 
lqsed.o: ../h/range.h ../h/fileinfo.h ../h/wordinfo.h
lqsed.o: ../h/wordplace.h ../h/wordrules.h ../h/emalloc.h ../h/lqutil.h
lqsed.o: ../h/liblqtext.h 
lqsed.o: ../h/phrase.h ../h/lqconfig.h ../h/pblock.h ../h/chartype.h
lqsed.o: ../h/wmoffset.h ../h/lqtrace.h
lqshow.o: lqshow.c ../h/error.h ../h/globals.h ../h/port.h ../h/wordrules.h
lqshow.o: ../h/api.h ../h/port2.h 
lqshow.o: ../h/fileinfo.h ../h/wordinfo.h
lqshow.o: ../h/wordplace.h ../h/wordrules.h ../h/pblock.h ../h/emalloc.h
lqshow.o: ../h/lqutil.h ../h/range.h ../h/liblqtext.h 
lqshow.o: ../h/phrase.h ../h/lqconfig.h ../h/chartype.h
lqshow.o: ../h/wmoffset.h ../h/lqtrace.h
lqsimilar.o: lqsimilar.c ../h/error.h ../h/globals.h ../h/port.h
lqsimilar.o: ../h/wordrules.h ../h/api.h ../h/port2.h 
lqsimilar.o: ../h/emalloc.h
lqsimilar.o: ../h/fileinfo.h ../h/wordinfo.h ../h/wordplace.h ../h/pblock.h
lqsimilar.o: ../h/phrase.h ../h/lqutil.h ../h/range.h ../h/lqtrace.h
lqsimilar.o: ../h/liblqtext.h 
lqsimilar.o: ../h/lqconfig.h ../h/chartype.h
lqunindex.o: lqunindex.c ../h/globals.h ../h/port.h ../h/wordrules.h ../h/api.h
lqunindex.o: ../h/port2.h ../h/error.h
lqunindex.o: ../h/fileinfo.h ../h/wordinfo.h
lqunindex.o: ../h/wordplace.h ../h/wordrules.h ../h/wordplace.h ../h/emalloc.h
lqunindex.o: ../h/addfile.h ../h/lqutil.h ../h/range.h ../h/liblqtext.h
lqunindex.o: ../h/phrase.h
lqunindex.o: ../h/lqconfig.h ../h/pblock.h ../h/chartype.h ../h/lqtrace.h
lqunindex.o: ../h/filter.h ../h/revision.h
lqunoverlap.o: lqunoverlap.c ../h/error.h ../h/globals.h ../h/port.h
lqunoverlap.o: ../h/wordrules.h ../h/api.h ../h/port2.h 
lqunoverlap.o: ../h/emalloc.h
lqunoverlap.o: ../h/fileinfo.h ../h/wordinfo.h ../h/wordplace.h ../h/pblock.h
lqunoverlap.o: ../h/phrase.h ../h/lqutil.h ../h/range.h ../h/lqtrace.h
lqunoverlap.o: ../h/liblqtext.h 
lqunoverlap.o: ../h/lqconfig.h ../h/chartype.h
lqword.o: lqword.c ../h/globals.h ../h/port.h ../h/wordrules.h ../h/api.h
lqword.o: ../h/port2.h ../h/error.h 
lqword.o: ../h/fileinfo.h ../h/wordinfo.h
lqword.o: ../h/wordplace.h ../h/smalldb.h
lqword.o: ../h/liblqtext.h
lqword.o: ../h/phrase.h
lqword.o: ../h/lqconfig.h ../h/pblock.h ../h/chartype.h ../h/pblock.h
lqword.o: ../h/wordrules.h ../h/emalloc.h ../h/lqutil.h ../h/range.h
lqword.o: ../h/liblqtext.h
lqwordlist.o: lqwordlist.c ../h/globals.h ../h/port.h ../h/wordrules.h
lqwordlist.o: ../h/api.h ../h/port2.h ../h/error.h
lqwordlist.o: ../h/fileinfo.h ../h/wordinfo.h ../h/wordplace.h ../h/smalldb.h
lqwordlist.o: ../h/liblqtext.h
lqwordlist.o: ../h/phrase.h
lqwordlist.o: ../h/lqconfig.h ../h/pblock.h ../h/chartype.h ../h/pblock.h
lqwordlist.o: ../h/wordrules.h ../h/numbers.h ../h/emalloc.h ../h/lqutil.h
lqwordlist.o: ../h/range.h ../h/liblqtext.h ../h/hsregex.h
mkwidtable.o: mkwidtable.c ../h/globals.h ../h/port.h ../h/wordrules.h
mkwidtable.o: ../h/api.h ../h/port2.h ../h/error.h
mkwidtable.o: ../h/fileinfo.h
mkwidtable.o: ../h/wordindex.h ../h/wordinfo.h ../h/wordplace.h ../h/smalldb.h
mkwidtable.o: ../h/liblqtext.h 
mkwidtable.o: ../h/phrase.h ../h/lqconfig.h ../h/pblock.h ../h/chartype.h
mkwidtable.o: ../h/lqutil.h ../h/range.h ../h/liblqtext.h ../h/numbers.h
mkwidtable.o: ../h/lqtrace.h
sortwids.o: sortwids.c ../h/globals.h ../h/port.h ../h/wordrules.h ../h/api.h
sortwids.o: ../h/port2.h ../h/error.h
sortwids.o: ../h/fileinfo.h ../h/wordinfo.h
sortwids.o: ../h/wordplace.h ../h/smalldb.h
sortwids.o: ../h/liblqtext.h
sortwids.o: ../h/phrase.h
sortwids.o: ../h/lqconfig.h ../h/pblock.h ../h/chartype.h ../h/pblock.h
sortwids.o: ../h/wordrules.h ../h/numbers.h ../h/emalloc.h ../h/lqutil.h
sortwids.o: ../h/range.h ../h/liblqtext.h ../h/lqtrace.h
wordtable.o: wordtable.c ../h/globals.h ../h/port.h ../h/wordrules.h ../h/api.h
wordtable.o: ../h/port2.h ../h/error.h
wordtable.o: ../h/smalldb.h
wordtable.o: ../h/liblqtext.h
wordtable.o: ../h/wordinfo.h ../h/wordplace.h ../h/fileinfo.h
wordtable.o: ../h/phrase.h
wordtable.o: ../h/lqconfig.h ../h/pblock.h ../h/chartype.h ../h/fileinfo.h
wordtable.o: ../h/wordinfo.h ../h/wordplace.h ../h/pblock.h ../h/wordrules.h
wordtable.o: ../h/emalloc.h ../h/addfile.h ../h/lqutil.h ../h/range.h
wordtable.o: ../h/liblqtext.h ../h/lqtrace.h

# IF YOU PUT ANYTHING HERE IT WILL GO AWAY
