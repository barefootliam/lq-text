head	1.13;
access;
symbols;
locks;
comment	@ * @;


1.13
date	2019.04.21.06.08.14;	author lee;	state Exp;
branches;
next	1.12;

1.12
date	96.08.14.17.03.20;	author lee;	state Exp;
branches;
next	1.11;

1.11
date	96.07.04.21.42.06;	author lee;	state Exp;
branches;
next	1.10;

1.10
date	96.07.01.21.37.23;	author lee;	state Exp;
branches;
next	1.9;

1.9
date	96.06.11.20.53.10;	author lee;	state Exp;
branches;
next	1.8;

1.8
date	96.06.11.20.05.53;	author lee;	state Exp;
branches;
next	1.7;

1.7
date	96.06.11.16.10.47;	author lee;	state Exp;
branches;
next	1.6;

1.6
date	96.05.26.23.07.30;	author lee;	state Exp;
branches;
next	1.5;

1.5
date	96.05.26.18.19.45;	author lee;	state Exp;
branches;
next	1.4;

1.4
date	96.05.25.22.46.51;	author lee;	state Exp;
branches;
next	1.3;

1.3
date	96.05.17.06.09.31;	author lee;	state Exp;
branches;
next	1.2;

1.2
date	96.05.15.23.26.12;	author lee;	state Exp;
branches;
next	1.1;

1.1
date	94.07.09.00.09.53;	author lee;	state Exp;
branches;
next	;


desc
@LQU_fReadLine no longer allocates each line...
@


1.13
log
@for migration to CVS
@
text
@/* lqunindex.c -- Copyright 1994, 1996 Liam R. E. Quin.
 * All Rights Reserved.
 * This code is NOT in the public domain.
 * See the file COPYRIGHT for full details.
 */

/* lqunindex -- remove a document from the LQ-Text text retrieval index
 * Liam Quin, July 1994
 *
 * $Id: lqunindex.c,v 1.13 2001/05/31 03:50:13 liam Exp $ 
 */

static char *Version =
    "@@(#) $Id: lqunindex.c,v 1.13 2001/05/31 03:50:13 liam Exp $";

#include "globals.h" /* defines and declarations for database filenames */
#include "error.h"

#ifndef FILE
# include <stdio.h>
#endif

#include <ctype.h>

#include <sys/types.h>
#include <sys/stat.h>
#ifdef HAVE_FCNTL_H
# include <fcntl.h>
#endif

#ifdef HAVE_STRING_H
# include <string.h>
#else
# include <strings.h>
#endif

#ifdef HAVE_UNISTD_H
# include <unistd.h>
#endif

#ifdef HAVE_STDLIB_H
# include <stdlib.h>
#else
# include <malloc.h>
#endif


#include "fileinfo.h"
#include "wordinfo.h"
#include "wordrules.h"
#include "wordplace.h"
#include "emalloc.h"
#include "addfile.h"
#include "lqutil.h"
#include "liblqtext.h"
#include "lqtrace.h"
#include "filter.h"
#include "revision.h"

/** Arguments and actions for call-backs: **/
typedef enum {
    LQC_UNINDEX_COMPLETED_OK = 1,
    LQC_UNINDEX_FILE_NOT_INDEXED,
    LQC_UNINDEX_FILE_NOT_FOUND,
    LQC_UNINDEX_FILE_HAS_CHANGED,
    LQC_UNINDEX_FILE_INCOMPREHENSIBLE,
    LQC_UNINDEX_CONTAINED_NEW_WORDS,
    LQC_UNINDEX_OTHER_PROBLEM
} t_UnIndexResult;

typedef enum {
    LQC_UNINDEX_INTERNAL_ERROR = 0,
    LQC_UNINDEX_ACTION_ASSUME_OK = 100,
    LQC_UNINDEX_ACTION_RETRY,
    LQC_UNINDEX_ACTION_FAIL_AND_STOP,
    LQC_UNINDEX_ACTION_FAIL_START_NEXT_FILE
} t_UnIndexAction;

/** a callback function: **/
typedef t_UnIndexAction (* t_UnIndexCallBack)(
#ifdef HAVE_PROTO
    char *theName,
    t_UnIndexResult theResult
#endif
);

/** Data structure for hash table **/

typedef struct s_WordToDelete {
    struct s_WordToDelete *Next;	/* 4 bytes  */
    t_WID WID;				/* 4 bytes, total so far is  8 */
    t_FID *FIDArray;			/* 4 + (16 or more), total  28 */
	/* one for the first value, one for a trailing 0 */
} t_WordToDelete;

static t_WordToDelete ZeroEntry = {
    0,
};

#ifndef PENDING_DELETE_TABLE_SIZE
    /* Each cache entry is likely to use 16 bytes.
     * On a 64-bit machine, or an old MIPS/SGI system using 64-byte alignment, 
     * things are worse, but then you have lots and lot of memory.
     *
     * If there are 65536 entries (16 bits worth), 
     * WID could be a short, as it would only be necessary to store
     * the top 16 bits.  This uses about a megabyte, but in practice,
     * the short gets rounded out to a full word for byte alignment,
     * so it doesn't help.
     *
     * The trailing FIDArray is allocated as needed.  The last entry is
     * equal to 0.
     *
     * The default value for PENDING_DELETE_TABLE_SIZE is 128 * 1024 entries,
     * using 3.5 MBytes of memory, with an additional 64*1024 entries
     * adding up to another 1.75 MBytes.
     *
     */
# define PENDING_DELETE_TABLE_SIZE (128*1024)
#endif /* PENDING_DELETE_TABLE_SIZE */

t_WordToDelete *PendingDeletes = (t_WordToDelete *) 0;
unsigned long HashSize = PENDING_DELETE_TABLE_SIZE;

/* We are using open chaining, so MaxChainedWordsInHashTable represents
 * the maximum extra words that can be allocated before they are all
 * flushed.  We allow 50% overflow by default:
 */
unsigned long MaxChainedWordsInHashTable = PENDING_DELETE_TABLE_SIZE * 0.5;
unsigned long TotalWordsInHashTable = 0; /* cache starts off empty! */

/** Keep a queue of files to unindex.  This lets the filters get
 ** started, so we aren't waiting for an uncompress, for example.
 ** There can be several filters active at a time, each filling the
 ** first buffer in the input pipe.  If your Unix system has only a few
 ** file descriptors (between 20 and 30 per process, say), you should set
 ** LQC_MAX_FILES_WAITING to 2 or 3, or you'll sometimes see messages about
 ** running out of file descriptors.
 **
 ** Most modern Unix systems allow at least 256 file descriptors per process,
 ** so this isn't likely to be a problem.
 **/

typedef struct s_FileItem {
    t_FileInfo *FileInfo;
    t_UnIndexCallBack StatusCallback;
    int HaveStarted;
    struct s_FileItem *Next;
} t_FileItem;

static t_FileItem *FilesWaiting = 0;
static t_FileItem **NextFileItem = 0;
static int NumberOfFilesWaiting = 0;

#ifndef LQC_MAX_FILES_WAITING
# define LQC_MAX_FILES_WAITING 5
#endif

/** System calls and library routines used in this file: **/
/** System calls: **/

/** Library Functions: **/

/** liblqtext functions: **/

extern void lqSetSignals();

/** Functions within this file that need declaring: **/

PRIVATE t_UnIndexAction LQC_AddToFilesWaitingToBeDeleted(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *db,
    char *Name,
    t_UnIndexCallBack StatusCallback
#endif
);

PRIVATE void LQC_UnIndexOneWaitingFile(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *db
#endif
);

PRIVATE void LQC_EmptyHashTableOut(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *db
#endif
);

PRIVATE void LQC_InitialiseUnIndexing(/*empty*/);

PRIVATE void LQC_UnIndexStream(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *db,
    t_FileInfo *FileInfo
#endif
);

PRIVATE void LQC_UnIndexFrom(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *db,
    char *Name
#endif
);

/* Symbol Table Interface */
PRIVATE void LQC_UnIndexWord(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *db,
    t_FileInfo *FileInfo,
    t_WordInfo *WordInfo
#endif
);

/**/

char *progname = "@@(#) $Source: /usr/home/liam/src/lq-text1.17/src/lqtext/RCS/lqunindex.c,v $";

static int SignalFlag = 0;

PRIVATE int
SignalHandler()
{
    ++SignalFlag;
    if (SignalFlag > 3) {
	LQT_CloseDatabase((t_LQTEXT_Database *) 0);
	Error(E_FATAL,
	    "received %d signals to quit, exiting; db may be corrupt!.",
	    SignalFlag
	);
    }
    return 0;
}

int
main(argc, argv)
    int argc;
    char *argv[];
{
    extern int getopt();
    extern char *optarg;
    extern int optind;

    int c;
    int ErrorFlag = 0;
    int DoNothing = 0;
    char *InputFile = (char *) 0;

    t_lqdbOptions *Options;
    t_LQTEXT_Database *db = 0;

    progname = argv[0]; /* retain the full path at first */

    Options = LQT_InitFromArgv(argc, argv);

    while ((c = getopt(argc, argv, "f:H:w:xVuZz:")) != -1) {
	switch (c) {
	case 'f':
	    if (InputFile) {
		Error(E_USAGE|E_XHINT|E_FATAL,
		    "only one -f option allowed; use -xv for explanation"
		);
	    }
	    InputFile = optarg;
	    break;
	case 'H':
	    if (!LQU_cknatstr(optarg)) {
		Error(E_FATAL|E_USAGE|E_XHINT,
		    "-H must be given a hash table size >= 1, not \"%s\"",
		    optarg
		);
	    }
	    HashSize = atoi(optarg);
	    break;
	case 'w':
	    if (!LQU_cknatstr(optarg)) {
		Error(E_FATAL|E_USAGE|E_XHINT,
		    "-w must be given a number >= 0, not \"%s\"",
		    optarg
		);
	    }
	    MaxChainedWordsInHashTable = atoi(optarg);
	    break;
	case 'Z':
	case 'z':
	    break; /* work done in SetDefault() */
	case 'V':
	    fprintf(stderr, "%s: Release: %s\n", progname, LQTEXTREVISION);
	    fprintf(stderr, "%s: Revision: %s\n", progname, Version);
	    DoNothing = 1;
	    break;
	case 'x':
	    ErrorFlag = (-1);
	    break;
	default:
	case '?':
	    ErrorFlag = 1;
	}
    }

    if ((progname = strrchr(progname, '/')) != (char *) NULL) {
	++progname; /* step over the last / */
    } else {
	progname = argv[0];
    }

    if (ErrorFlag > 0) {
	fprintf(stderr, "use %s -x or %s -xv for an explanation.\n",
							progname, progname);
	exit(1);
    } else if (ErrorFlag < 0) { /* -x was used */
	fprintf(stderr,
	    "%s -- remove documents from an lq-text retrieval database\n",
	    progname
	);

	fputs("Options are:\n\
	-f file -- read the list of files to index from \"file\"\n\
	-w n	-- process the hash table when it contains more than n\n\
		   distinct words (each 20 bytes)\n\
	-H n    -- set the hash table size to n entries (each 20 bytes)\n\
	--	-- all following arguments are file names\n\
\n\
", stderr);

	LQT_PrintDefaultUsage(Options);

	if (LQT_TraceFlagsSet(LQTRACE_VERBOSE|LQTRACE_DEBUG)) {
	    /* used -v or -t1 */
	fprintf(stderr, "\n\
    Any remaining arguments are taken to be file names.  The current\n\
DOCPATH (%s) is searched for the files,\n\
which must be exactly as they were when they were added to the index.\n\
The index information about those files is removed from the database.\n\
The actual files themselves are completely untouched by the operation,\n\
except that they are scanned to determine which words they contain.\n\
If you use the -f option, you should not give filename\n\
arguments on the command line, although you can use \"-f -\" to read the\n\
list of files from standard input, one per line.  Setting (with -w) the\n\
size of the cache may dramatically improve performance.\n\
Systems with memory larger than the data can try -w0.\n\
See %s(1) for more information.\n",
		db->DocPath,
		progname
	    );
	}
    exit(0);

    }

    if (DoNothing) {
	if (optind < argc) {
	    Error(
		E_WARN|E_XHINT,
		    "%d extra argument%s ignored...",
		    argc - optind,
		    argc - optind == 1 ? "" : "%s"
		);
	}
	exit(0);
    }

    db = LQT_OpenDatabase(Options, O_RDONLY, 0644);
    if (!db || LQT_ObtainReadOnlyAccess(db) < 0) {
        Error(E_FATAL, "couldn't open lq-text database");
    }

    LQT_InitFilterTable(db);
    LQC_InitialiseUnIndexing();
    lqSetSignals(SignalHandler);
    LQT_ObtainWriteAccess(db);

    if (InputFile) {
	if (optind < argc) {
	    Error(E_FATAL|E_USAGE|E_XHINT,
		"cannot give filenames after -f %s",
		InputFile
	    );
	} else {
	    LQC_UnIndexFrom(db, InputFile);
	}
    } else for (; optind < argc; ++optind) {
	(void) LQC_AddToFilesWaitingToBeDeleted(
	    db,
	    argv[optind],
	    (t_UnIndexCallBack) 0
	);
	if (SignalFlag) {
	    Error(E_WARN,
		"Caught signal at level %d, dumping cache",
		SignalFlag
	    );

	    LQC_EmptyHashTableOut(db);
	    LQT_CloseDatabase(db);

	    Error(E_FATAL,
		"Caught signal at level %d, dumped cache, quitting",
		SignalFlag
	    );
	}
    }

    while (NumberOfFilesWaiting > 0) {
	LQC_UnIndexOneWaitingFile(db);
    }

    LQC_EmptyHashTableOut(db);
    LQT_CloseDatabase(db);
    return 0;
}

PRIVATE void
LQC_UnIndexFrom(db, Name)
    t_LQTEXT_Database *db;
    char *Name;
{
    FILE *fp;
    char *Line;

    if (Name[0] == '-' && Name[1] == '\0') {
	fp = stdin;
    } else {
	fp = LQU_fEopen(E_FATAL, Name, "list of files to add", "r");
    }

    while (LQU_fReadLine(fp, &Line, LQUF_NORMAL) != -1) {
	/* Note:
	 * LQU_fReadFile will silently swallow blank lines.
	 * If we use LQUF_NORMAL it will swallow lines that start with a #,
	 * but not delete # signs in the middle of a line.
	 */
	(void) LQC_AddToFilesWaitingToBeDeleted(
	    db,
	    Line,
	    (t_UnIndexCallBack) 0
	);

	if (SignalFlag) {
	    Error(E_WARN,
		"Caught signal at level %d -- dumping cache",
		SignalFlag
	    );

	    LQT_CloseDatabase(db);
	    exit(1);
	}
    }

    if (fp != stdin) {
	(void) fclose(fp);
    }
}

PRIVATE void
LQC_UnIndexOneWaitingFile(db)
    t_LQTEXT_Database *db;
{
    t_FileItem *Next;

    if (!FilesWaiting) {
	return;
    }

    LQC_UnIndexStream(db, FilesWaiting->FileInfo);
    if (LQT_RemoveFileInfoFromIndex(db, FilesWaiting->FileInfo) < 0) {
	Error(E_WARN,
	    "%s not removed from database index (may not have been there)",
	    FilesWaiting->FileInfo->Name
	);
    }

    LQT_Trace(LQTRACE_VERBOSE|LQTRACE_DEBUG,
	"%d: %s: removed from index.",
	FilesWaiting->FileInfo->FID,
	FilesWaiting->FileInfo->Name
    );

    LQT_DestroyFileInfo(db, FilesWaiting->FileInfo);

    Next = FilesWaiting->Next;
    (void) efree((char *) FilesWaiting);
    FilesWaiting = Next;
    --NumberOfFilesWaiting;
}

PRIVATE void
LQC_UnIndexStream(db, FileInfo)
    t_LQTEXT_Database *db;
    t_FileInfo *FileInfo;
{
    t_WordInfo *WordInfo;

    while (SignalFlag <= 1) {
	/* needs more than one signal to quit in the middle of a file */

	WordInfo = LQT_ReadWordFromFileInfo(
	    db,
	    FileInfo,
	    LQT_READWORD_IGNORE_COMMON
	);

	if (WordInfo == (t_WordInfo *) 0) {
	    break;
	} else {
	    LQC_UnIndexWord(db, FileInfo, WordInfo);
	    WordInfo->WID = (t_WID) 0;
	}
    }

    if (SignalFlag > 1) {
	Error(E_WARN, "Signal received during processing of %s",
		FileInfo->Name);
	Error(E_WARN, "That and other files may be incomplete...");
	return;
    }
}

PRIVATE t_UnIndexAction
LQC_AddToFilesWaitingToBeDeleted(db, FileName, StatusCallBack)
    t_LQTEXT_Database *db;
    char *FileName;
    t_UnIndexCallBack StatusCallBack;
{
    t_FileInfo *FileInfo;
    t_FID FID;

    if ((FID = LQT_NameToFID(db, FileName)) == (t_FID) 0) {
	if (StatusCallBack) {
	    switch ((* StatusCallBack)(FileName,LQC_UNINDEX_FILE_NOT_INDEXED)) {
	    case LQC_UNINDEX_ACTION_ASSUME_OK:
		return LQC_UNINDEX_ACTION_ASSUME_OK;
	    case LQC_UNINDEX_ACTION_RETRY:
		Error(E_INTERNAL|E_WARN,
"%s: %d: LQC_AddToFilesWaitingToBeDeleted: LQC_UNINDEX_ACTION_RETRY illegal callback return value for FIILE_NOT_INDEXED, file \"%s\"",
		    FileName
		);
		return LQC_UNINDEX_INTERNAL_ERROR;
	    case LQC_UNINDEX_ACTION_FAIL_START_NEXT_FILE:
		Error(E_WARN,
		    "%s was not indexed in the database, ignored",
		    FileName
		);
		return LQC_UNINDEX_ACTION_FAIL_START_NEXT_FILE;
	    case LQC_UNINDEX_ACTION_FAIL_AND_STOP:
		Error(E_FATAL,
		    "%s was not indexed in the database, quitting",
		    FileName
		);
	    default:
		return LQC_UNINDEX_INTERNAL_ERROR;
	    }
	} else {
	    Error(E_WARN,
		"%s was not indexed in the database, ignored",
		FileName
	    );
	    return LQC_UNINDEX_ACTION_FAIL_START_NEXT_FILE;
	}
    }

    if ((FileInfo = LQT_FIDToFileInfo(db, FID)) == (t_FileInfo *) 0) {
	Error(E_WARN|E_INTERNAL,
	    "Couldn't get File Info for document %ld, %s -- not unindexed",
	    FID,
	    FileName
	);
	return LQC_UNINDEX_ACTION_FAIL_START_NEXT_FILE;
    } else {
	/* Check to see if the file has changed since it was last
	 * indexed.  If it has, we should delete the old one from
	 * the database and give this one a new FID, but I have
	 * not done that yet -- that's /usr/local/lib/lqtextd or
	 * something, I suppose!  Deleting it involves going through the
	 * entire database looking for words that matched that FID and 
	 * making them not do so any more, e.g. by deleting them and rewriting
	 * the chain entirely.
	 * TODO:
	 *    Simply marking the FID as invalid would go a long way, though.
	 */

	struct stat StatBuf;
	char *theName = LQT_FindFile(db, FileInfo->Name);

	if (theName && theName != FileInfo->Name) {
	    (void) efree(FileInfo->Name);
	    FileInfo->Name = emalloc(theName, strlen(theName) + 1);
	    (void) strcpy(FileInfo->Name, theName);
	}
	theName = FileInfo->Name;

	if (stat(theName, &StatBuf) >= 0) {
	    if (FileInfo->Date < StatBuf.st_mtime &&
				FileInfo->FileSize != StatBuf.st_size) {
		if (LQT_TraceFlagsSet(LQTRACE_VERBOSE|LQTRACE_DEBUG)) {
		    Error(E_WARN,
			"%s changed since last run -- not indexed",
			FileName
		    );
		}
		LQT_DestroyFileInfo(db, FileInfo);
		return LQC_UNINDEX_ACTION_FAIL_START_NEXT_FILE;
	    }
	} else {
	    Error(E_WARN|E_SYS,
		"stat: couldn't get file system information for \"%s\"",
		FileName
	    );
	    LQT_DestroyFileInfo(db, FileInfo);
	    return LQC_UNINDEX_ACTION_FAIL_START_NEXT_FILE;
	}
    }

    if ((FileInfo->Stream = LQT_MakeInput(db, FileInfo)) == (FILE *) 0) {
	Error(E_WARN|E_SYS,
	    "couldn't open input filter for %s -- not unindexed",
	    FileInfo->Name
	);
	LQT_DestroyFileInfo(db, FileInfo);
	return LQC_UNINDEX_ACTION_FAIL_START_NEXT_FILE;
    }

    /* OK, so now we are ready to start! */

    if (!FilesWaiting) {
	NextFileItem = &FilesWaiting;
    }

    *NextFileItem = (t_FileItem *) emalloc("pending file", sizeof(t_FileItem));

    (*NextFileItem)->Next = (t_FileItem *) 0;
    (*NextFileItem)->FileInfo = FileInfo;
    (*NextFileItem)->StatusCallback = StatusCallBack;
    (*NextFileItem)->HaveStarted = 1;
    NextFileItem = &(*NextFileItem)->Next;

    ++NumberOfFilesWaiting;

    if (NumberOfFilesWaiting > LQC_MAX_FILES_WAITING) {
	LQC_UnIndexOneWaitingFile(db);
    }

    return LQC_UNINDEX_ACTION_ASSUME_OK;
}

/** Hash Code **/

static int BitsToShift = 0;

PRIVATE void
LQC_InitialiseUnIndexing()
{
    unsigned long RealHashSize = 1;

    if (PendingDeletes) {
	Error(E_FATAL|E_BUG,
	    "LQC_InitialiseUnIndexing() called more than once"
	);
    }

    while (RealHashSize <= HashSize) {
	RealHashSize <<= 1;
	++BitsToShift;
    }

    /* gone too far: */
    RealHashSize >>= 1;
    --BitsToShift;

    if (RealHashSize == HashSize) {
	LQT_Trace(LQTRACE_VERBOSE|LQTRACE_DEBUG,
	    "Using %ld hash table entries (%d bits), %ld bytes",
	    HashSize,
	    BitsToShift,
	    HashSize * sizeof(t_WordToDelete)
	);
    } else {
	/* HashSize had more than one bit set */
	RealHashSize <<= 1;
	++BitsToShift;
	LQT_Trace(LQTRACE_VERBOSE|LQTRACE_DEBUG,
	    "Hash size increased from %ld to %ld to be a power of two",
	    HashSize,
	    RealHashSize
	);
	LQT_Trace(LQTRACE_VERBOSE|LQTRACE_DEBUG,
	    "Using %ld hash table entries [%d bits], %ld bytes",
	    HashSize,
	    BitsToShift,
	    HashSize * sizeof(t_WordToDelete)
	);
	HashSize = RealHashSize;
    }

    PendingDeletes = (t_WordToDelete *) ecalloc(
	"Hash Table for pending words",
	HashSize,
	sizeof(t_WordToDelete)
    );

    TotalWordsInHashTable = 0;
}

PRIVATE void LQC_RemoveUnwantedPlacesFor(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *db,
    t_WordToDelete *WordToDelete
#endif
);

#ifdef ASCIITRACE
PRIVATE void
PrintHashTable()
{
    unsigned long theSlot;

    LQT_Trace(LQTRACE_DEBUG, "Hash table begins:");

    for (theSlot = 0; theSlot < HashSize; theSlot++) {
	register t_WordToDelete *theEntry;

	theEntry = &PendingDeletes[theSlot];

	if (theEntry->WID) {
	    do {
		LQT_Trace(LQTRACE_DEBUG,
		    "\t%d: WID %d", theSlot, theEntry->WID
		);
		theEntry = theEntry->Next;
	    } while (theEntry);
	}
    }

    LQT_Trace(LQTRACE_DEBUG, "Hash table ends.");

}
#endif

PRIVATE void
LQC_EmptyHashTableOut(db)
    t_LQTEXT_Database *db;
{
    unsigned long theSlot;

#ifdef ASCIITRACE
    if (LQT_TraceFlagsSet(LQTRACE_DEBUG)) {
	LQT_Trace(LQTRACE_DEBUG, "LQC_EmptyHashTableOut...");
	PrintHashTable();
    }
#endif

    for (theSlot = 0; theSlot < HashSize; theSlot++) {
	register t_WordToDelete *theEntry;

	theEntry = &PendingDeletes[theSlot];
	if (theEntry->WID) {
	    t_WordToDelete *StartingPlace = theEntry;

	    /* remove the entries from the database: */
	    do {
		LQC_RemoveUnwantedPlacesFor(db, theEntry);
		theEntry = theEntry->Next;
	    } while (theEntry);

	    /* reclaim storage: */
	    theEntry = StartingPlace;
	    do {
		t_WordToDelete *Next = theEntry->Next;

		if (theEntry->FIDArray) {
		    (void) efree((char *) theEntry->FIDArray);
		    theEntry->FIDArray = 0;
		}
		if (theEntry == StartingPlace) {
		    /* the first entry is part of an array,
		     * so we don't free it.
		     */

		    (*theEntry) = ZeroEntry; /* structure copy */
		} else {
		    (void) efree((char *) theEntry);
		}
		theEntry = Next;
	    } while (theEntry);
	}
    } /* for each slot */

    TotalWordsInHashTable = 0;
}

PRIVATE void
LQC_AddFileToSlot(theFileInfo, theWordInfo, Entry)
    t_FileInfo *theFileInfo;
    t_WordInfo *theWordInfo;
    register t_WordToDelete *Entry;
{
    long NewLength = 0;
    register unsigned long theSlot;

    /* When this function is called, we've already identified
     * that Entry is the right hash-table slot, and so we're going to
     * add the FID represented by theFileInfo to the list of files
     * whose wordplaces are to be removed for this word.
     *
     * If that FID is already here, we don't need to add it again --
     * we're going to delete all the matches of this word for that
     * file anyway.
     */
    if (Entry->FIDArray) {
	for (theSlot = 0; Entry->FIDArray[theSlot] != (t_FID) 0; theSlot++) {
	    if (Entry->FIDArray[theSlot] == theFileInfo->FID) {
		/* it's already there */
#ifdef ASCIITRACE
		LQT_Trace(LQTRACE_DEBUG,
		    "(%ld=%*.*s: FID %ld already there, slot %ld)",
		    theWordInfo->WID,
		    theWordInfo->Length,
		    theWordInfo->Length,
		    theWordInfo->Word,
		    theFileInfo->FID,
		    theSlot
		);
#endif
		return;
	    }
	}
    } else {
	theSlot = 0L;
    }

#ifdef ASCIITRACE
    LQT_Trace(LQTRACE_DEBUG,
	"<- %*.*s\tslot %ld\t%ld=%s",
	theWordInfo->Length,
	theWordInfo->Length,
	theWordInfo->Word,
	theSlot,
	theFileInfo->FID,
	theFileInfo->Name
    );
#endif

    switch (theSlot) {
    case 0:
	Entry->FIDArray = (t_FID *) emalloc(
	    "FIDArray",
	    sizeof(t_FID) * 2
	);
	break;
    case 1: /* second time round */
	NewLength = 4;
	break;
    case 3: /* third time round */
	NewLength = 8;
	break;
    case 7:
	NewLength = 12;
	break;
    case 11:
	NewLength = 16;
	break;
    default:
	if (((theSlot + 1) & 15) == 0) {
	    NewLength = theSlot + 16;
	}
	break;
    }

    if (NewLength) {
	Entry->FIDArray = (t_FID *) erealloc(
	    (char *) Entry->FIDArray,
	    NewLength * sizeof(t_FID)
	);
    }

    Entry->WID = theWordInfo->WID;
    Entry->FIDArray[theSlot++] = theFileInfo->FID;
    Entry->FIDArray[theSlot] = (t_FID) 0;
}

PRIVATE void
LQC_UnIndexWord(db, FileInfo, WordInfo)
    t_LQTEXT_Database *db;
    t_FileInfo *FileInfo;
    t_WordInfo *WordInfo;
{
    t_WordToDelete *theEntry;
    t_WordToDelete **Entryp;

    /* We assume that the following elements of WordInfo are OK,
     * but probably not any others:
     * WordInfo->Word -- the actual word (not necessarily nul-terminated)
     * WordInfo->Length -- the length of the word, excluding any trailing \0
     * WordInfo->WID -- if not 0, this is the WID of the word
     */
    
#ifdef ASCIITRACE
    if (LQT_TraceFlagsSet(LQTRACE_WORDINFO)) {
	LQT_Trace(LQTRACE_WORDINFO,
	    "unindex word: %*.*s",
	    WordInfo->Length,
	    WordInfo->Length,
	    WordInfo->Word
	);
    }
#endif

    if (WordInfo->WID) {
	WordInfo->WID = (t_WID) 0;
    }

    if (!WordInfo->WID) {
	static t_FID LastFIDWarnedAbout = (t_FID) 0;

	WordInfo->WID = LQT_WordToWID(db, WordInfo->Word, WordInfo->Length);
	if (!WordInfo->WID) {
	    static int ErrorCount = 0;

	    if (FileInfo->FID != LastFIDWarnedAbout || ErrorCount < 4) {
		if (FileInfo->FID == LastFIDWarnedAbout) {
		    ErrorCount++;
		} else {
		    LastFIDWarnedAbout = FileInfo->FID;
		    ErrorCount = 1;
		}

		Error((ErrorCount < 4) ? E_WARN : (E_WARN|E_MULTILINE),
		    "%s: document contains word not previously indexed: %*.*s",
		    FileInfo->Name,
		    WordInfo->Length,
		    WordInfo->Length,
		    WordInfo->Word
		);

		if (ErrorCount == 4) {
		    Error(E_WARN|E_MULTILINE|E_LASTLINE,
			"%s: (no more warnings for this document)",
			FileInfo->Name
		    );
		}
	    }
	    return;
	}
    }

    theEntry = &PendingDeletes[WordInfo->WID & (HashSize - 1)];

    if (theEntry->WID == (t_WID) 0) {
	LQC_AddFileToSlot(FileInfo, WordInfo, theEntry);
	return;
    }

    if (theEntry->WID != WordInfo->WID) {
	for (Entryp = &(theEntry->Next); *Entryp; Entryp = &(*Entryp)->Next) {
	    if ((*Entryp)->WID >= WordInfo->WID) {
		break;
	    }
	}

	if (!*Entryp || (*Entryp)->WID != WordInfo->WID) {
	    /* Not found, so make a new entry */

	    /* Actually, if there are already too many entries,
	     * We'll clear some out and try again
	     */
	    if (TotalWordsInHashTable + 1 > MaxChainedWordsInHashTable) {
		LQC_EmptyHashTableOut(db);
		LQC_UnIndexWord(db, FileInfo, WordInfo);
		return;
	    }

	    ++TotalWordsInHashTable;

	    theEntry = (t_WordToDelete *) ecalloc(
		"One Word To Delete",
		1,
		sizeof(t_WordToDelete)
	    );

	    LQC_AddFileToSlot(FileInfo, WordInfo, theEntry);

	    /* List insertion: */
	    theEntry->Next = (*Entryp);
	    *Entryp = theEntry;

	    /* done! */
	    return;
	} else {
	    /* Already there */
	    theEntry = (*Entryp);
	}
    }

    if (!theEntry) {
	Error(E_BUG|E_FATAL,
	    "%s: %c: theEntry is zero adding word \"%*.*s\" in file %s",
	    __FILE__, __LINE__,
	    WordInfo->Length,
	    WordInfo->Length,
	    WordInfo->Word,
	    FileInfo->Name
	);
    }

    if (theEntry->WID != WordInfo->WID) {
	Error(E_BUG|E_FATAL,
	    "%s: %c: theEntry->WID %ld != %d adding word \"%*.*s\" in file %s",
	    __FILE__, __LINE__,
	    theEntry->WID,
	    WordInfo->WID,
	    WordInfo->Length,
	    WordInfo->Length,
	    WordInfo->Word,
	    FileInfo->Name
	);
    }

    LQC_AddFileToSlot(FileInfo, WordInfo, theEntry);
}

static t_FID *FIDsToDelete = 0;
static int Last = 0;

PRIVATE void
LQC_PrepareEntryForExecution(theEntry)
    t_WordToDelete *theEntry;
{
    FIDsToDelete = theEntry->FIDArray;
    Last = 0;
}

PRIVATE int
LQC_FIDIsWanted(db, WID, WordPlace)
    t_LQTEXT_Database *db;
    t_WID WID;
    t_WordPlace *WordPlace;
{
    register t_FID queryFID;

    if (FIDsToDelete == (t_FID *) 0) {
	return 1;
    }

    queryFID = WordPlace->FID;

    while (FIDsToDelete[Last] != (t_FID) 0) {
	register t_FID ArrayFID = FIDsToDelete[Last];

	/* Reminder:
	 *   The FIDS in the array are sorted, and then
	 *   null terminated.
	 */

	if (ArrayFID == queryFID) {
	    return 0; /* not wanted */
	} else if (ArrayFID > queryFID) {
	    return 1;
	} else {
	    ++Last;
	}
    }

    return 1; /* not in the ToDelete list */
}

PRIVATE void
LQC_RemoveUnwantedPlacesFor(db, WordToDelete)
    t_LQTEXT_Database *db;
    t_WordToDelete *WordToDelete;
{
    t_pblock *pblock;
    t_WordInfo *WordInfo;

    /** get the old entry */

    if (!WordToDelete->WID ||
	!(WordInfo = LQT_WIDToWordInfo(db, WordToDelete->WID))
    ) {
	Error(E_BUG, "Word with WID %ld went away!", WordToDelete->WID);
	return;
    }

#ifdef ASCIITRACE
    if (LQT_TraceFlagsSet(LQTRACE_WORDINFO)) {
	LQT_fprintWordInfo(db, stderr, WordInfo, "LQC_RemoveUnwantedPlacesFor");
    }
#endif
    if (WordInfo->NumberOfWordPlaces == 0) {
	(void) LQT_DestroyWordInfo(db, WordInfo);
	return;
    }
    
    LQC_PrepareEntryForExecution(WordToDelete);
    pblock = LQT_GetpblockWhere(db, WordInfo, LQC_FIDIsWanted);

    if (pblock->NumberOfWordPlaces != WordInfo->NumberOfWordPlaces) {
	pblock->NumberOfWordPlaces = WordInfo->NumberOfWordPlaces;
    }

    /* delete the old entry from disk */
    if (WordInfo->Offset) {
	/* Remove the old information from disk.  */
	LQT_DeleteWordPlaces(db, WordInfo->Offset, WordInfo->WID);
    }

    WordInfo->Offset = pblock->ChainStart = 0L; /* it's invalid now... */

    /* First, let's make an index entry: */
    if (pblock->NumberOfWordPlaces <= MaxWordPlacesInAWordBlock(db)) {
	(void) LQT_MakeWordInfoBlock(db, WordInfo, pblock);
    }

    /** write out the new entry */
    if (WordInfo->WordPlacesInHere == pblock->NumberOfWordPlaces) {
	/* In this case it all fits into the WID index */
	pblock->ChainStart = 0L;
    } else {
	(void) LQT_Writepblock(db, WordInfo, pblock);
    }

    if (LQT_PutWordInfoIntoIndex(db, WordInfo, pblock->ChainStart) < 0) {
	Error(E_FATAL|E_SYS,
	    "UpdateEntry: Couldn't update \"%s\" in database at 0x%lx",
			    WordInfo->Word, pblock->ChainStart
	);
    }

    /** reclaim storage */
    (void) efree((char *)pblock);
    (void) LQT_DestroyWordInfo(db, WordInfo);
}

@


1.12
log
@beta 6
@
text
@d10 1
a10 1
 * $Id: lqunindex.c,v 1.11 96/07/04 21:42:06 lee Exp $ 
d14 1
a14 1
    "@@(#) $Id: lqunindex.c,v 1.11 96/07/04 21:42:06 lee Exp $";
a22 1
#include <malloc.h>
d43 2
d47 1
d217 1
a217 1
char *progname = "@@(#) $Source: /usr/src/cmd/lq-text/src/lqtext/RCS/lqunindex.c,v $";
@


1.11
log
@changed PrepareEntryforExecution to be void.
@
text
@d10 1
a10 1
 * $Id: lqunindex.c,v 1.10 96/07/01 21:37:23 lee Exp $ 
d14 1
a14 1
    "@@(#) $Id: lqunindex.c,v 1.10 96/07/01 21:37:23 lee Exp $";
d361 1
a361 1
    db = LQT_OpenDatabase(Options, O_RDONLY, 0);
d425 1
a425 1
    while (LQU_fReadLine(fp, &Line, LQUF_NORMAL) > 0) {
d433 1
a433 1
	    Name,
d670 1
a670 2
	    "Using %ld hash table entries (%d bits), %ld bytes\n",
	    progname,
@


1.10
log
@LQU_fReadLine no longer allocates each line...
@
text
@d10 1
a10 1
 * $Id: lqunindex.c,v 1.9 96/06/11 20:53:10 lee Exp $ 
d14 1
a14 1
    "@@(#) $Id: lqunindex.c,v 1.9 96/06/11 20:53:10 lee Exp $";
d1023 1
a1023 1
PRIVATE int
@


1.9
log
@Initialise filter table.
@
text
@d10 1
a10 1
 * $Id: lqunindex.c,v 1.8 96/06/11 20:05:53 lee Exp $ 
d14 1
a14 1
    "@@(#) $Id: lqunindex.c,v 1.8 96/06/11 20:05:53 lee Exp $";
a445 2

	(void) efree(Line);
@


1.8
log
@moved filter.h
@
text
@d10 1
a10 1
 * $Id: lqunindex.c,v 1.7 96/06/11 16:10:47 lee Exp $ 
d14 1
a14 1
    "@@(#) $Id: lqunindex.c,v 1.7 96/06/11 16:10:47 lee Exp $";
d366 1
@


1.7
log
@MakeInput went into the API.
@
text
@d10 1
a10 1
 * $Id: lqunindex.c,v 1.6 96/05/26 23:07:30 lee Exp $ 
d14 1
a14 1
    "@@(#) $Id: lqunindex.c,v 1.6 96/05/26 23:07:30 lee Exp $";
a49 1
#include "filter.h"
d55 1
@


1.6
log
@oops, a bad realloc!  Very, very bad, in fact...
@
text
@d10 1
a10 1
 * $Id: lqunindex.c,v 1.5 96/05/26 18:19:45 lee Exp $ 
d14 1
a14 1
    "@@(#) $Id: lqunindex.c,v 1.5 96/05/26 18:19:45 lee Exp $";
a453 79
PRIVATE FILE *
LQC_MakeInput(db, FileInfo)
    t_LQTEXT_Database *db;
    t_FileInfo *FileInfo;
{
    FILE *fp;
    char *Buffer;
    unsigned BufLen;
    extern FILE *fopen(), *popen();

#define FSTRING FilterTable[FileInfo->FilterType].String

    if (FileInfo->FilterType > MaxFilterType) {
	Error(E_WARN, "filter type %d for %s too high (max %d)",
		FileInfo->FilterType, FileInfo->Name, MaxFilterType);
	return (FILE *) 0;
    }

    if (FilterTable[FileInfo->FilterType].Type != FileInfo->FilterType) {
	Error(E_FATAL|E_INTERNAL,
	    "Filter table entry %d has type %d, expected %d",
	    FileInfo->FilterType,
	    FilterTable[FileInfo->FilterType].Type,
	    FileInfo->FilterType
	);
    }

    if (FSTRING == (char *) 0) {
	fp = fopen(FileInfo->Name, "r");

	if (fp == (FILE *) NULL) {
	    int fd;

	    if ((fd = LQT_UnpackAndOpen(db, FileInfo->Name)) < 0) {
		return (FILE *) NULL;
	    }

	    return fdopen(fd, "r");
	}

	return fp;
    }

    BufLen = strlen(FileInfo->Name) * 2 + 4 + strlen(FSTRING);
	/* The +4 is to allow for an embedded " < " plus a \0;
	 * we append "< Name", but also expand %s to be the Name, hence
	 * the strlen * 2; +2 is for quotes.
	 */
    Buffer = emalloc("MakeInput command buffer", BufLen);

    (void) sprintf(Buffer, FSTRING, FileInfo->Name);
    (void) strcat(Buffer, " < ");
    {
	register char *p, *q;

	for (p = Buffer; *p; p++) {
	    /*NULLBODY*/;
	}
	*p++ = '"';
	for (q = FileInfo->Name; *q; q++) {
	    if (*q == '"' || *q == '$' || *q == '\\' || *q == '`') {
		*p++ = '\\';
	    }
	    *p++ = *q;
	}
	*p++ = '"';
	*p = '\0';
    }

    fp = popen(Buffer, "r");
    if (!fp) {
	Error(E_WARN|E_SYS, "%s: Couldn't run input command [%s]",
	    FileInfo->Name, Buffer
	);
    }
    (void) efree(Buffer);
    return fp;
}

d613 1
a613 1
    if ((FileInfo->Stream = LQC_MakeInput(db, FileInfo)) == (FILE *) 0) {
@


1.5
log
@fix the linked list up after wrecking it.
@
text
@d10 1
a10 1
 * $Id: lqunindex.c,v 1.4 96/05/25 22:46:51 lee Exp Locker: lee $ 
d14 1
a14 1
    "@@(#) $Id: lqunindex.c,v 1.4 96/05/25 22:46:51 lee Exp Locker: lee $";
d559 1
a559 1
    Next = FilesWaiting;
d663 1
a663 3
	if (!theName) {
	    theName = FileInfo->Name;
	} else {
d668 1
d851 1
d949 3
a951 3
	Entry->FIDArray = (t_FID *) realloc(
	    Entry->FIDArray,
	    NewLength
@


1.4
log
@Empty out the hash table on exit.
@
text
@d10 1
a10 1
 * $Id: lqunindex.c,v 1.3 96/05/17 06:09:31 lee Exp $ 
d14 1
a14 1
    "@@(#) $Id: lqunindex.c,v 1.3 96/05/17 06:09:31 lee Exp $";
d1211 1
@


1.3
log
@added casts to efree().
@
text
@d10 1
a10 1
 * $Id: lqunindex.c,v 1.2 1996/05/15 23:26:12 lee Exp $ 
d14 1
a14 1
    "@@(#) $Id: lqunindex.c,v 1.2 1996/05/15 23:26:12 lee Exp $";
d405 1
d560 1
a560 1
    (void) efree(FilesWaiting);
d791 1
d793 27
d825 7
d854 4
d879 9
d894 1
a894 1
		    "(%ld=%*.*s@@%ld already there, slot %ld)",
d1112 2
a1113 1
LQC_FIDIsWanted(WID, WordPlace)
@


1.2
log
@API, headers...
@
text
@d10 1
a10 1
 * $Id: lqunindex.c,v 1.1 94/07/09 00:09:53 lee Exp Locker: lee $ 
d14 1
a14 1
    "@@(#) $Id: lqunindex.c,v 1.1 94/07/09 00:09:53 lee Exp Locker: lee $";
d815 1
a815 1
		    (void) efree(theEntry->FIDArray);
d820 1
a820 1
		    (void) efree(theEntry);
@


1.1
log
@Initial revision
@
text
@d1 1
a1 1
/* lqunindex.c -- Copyright 1994 Liam R. Quin.
d10 1
a10 1
 * $Id$ 
d14 1
a14 1
    "@@(#) $Id$";
d19 4
a22 1
#include <stdio.h>
d25 1
d28 4
a36 1
#include <stdlib.h>
d38 8
d49 1
a52 2
#include "Revision.h"
#include "readfile.h"
d55 2
a57 1

d170 1
d176 12
a187 3
PRIVATE void LQC_UnIndexOneWaitingFile(/*empty*/);
PRIVATE void LQC_EmptyHashTableOut(/*empty*/);
PRIVATE void LQC_EmptyHashTableOut(/*empty*/);
d192 1
d199 1
d205 1
a205 1
extern void LQC_UnIndexWord(
d207 1
d215 1
a215 1
char *progname = "@@(#) $Source$";
a216 2
extern int AsciiTrace; /* provide increasingly verbose info if not zero */

d224 1
a224 5
	LQT_FlushBlockCache(0);
	LQT_WriteCurrentMaxWID();
	LQT_SyncAndCloseAllKeyValueDatabases();
	LQTpFlushWIDCache();
	LQTp_FlushLastBlockCache();
d247 3
d252 1
a252 1
    LQT_InitFromArgv(argc, argv);
d324 1
a324 1
	LQT_PrintDefaultUsage();
d326 1
a326 1
	if (AsciiTrace == 1) {
d337 3
a339 3
list of files from standard input, one per line.\n\
Setting (with -w) the size of the cache may dramatically\n\
improve performance.  Systems with memory larger than the data can try -w0.\n\
d341 3
a343 1
DocPath, progname);
d361 5
d368 1
a368 1
    LQT_ObtainWriteAccess();
d377 1
a377 1
	    LQC_UnIndexFrom(InputFile);
d381 1
d391 2
a392 5
	    LQC_EmptyHashTableOut();
	    LQT_WriteCurrentMaxWID();
	    LQT_SyncAndCloseAllKeyValueDatabases();
	    LQTpFlushWIDCache();
	    LQTp_FlushLastBlockCache();
d402 1
a402 1
	LQC_UnIndexOneWaitingFile();
d405 1
a405 6
    LQC_EmptyHashTableOut();
    LQT_WriteCurrentMaxWID();
    LQT_SyncAndCloseAllKeyValueDatabases();
    LQTpFlushWIDCache();
    LQTp_FlushLastBlockCache();

d410 2
a411 1
LQC_UnIndexFrom(Name)
d420 1
a420 1
	fp = fEopen(E_FATAL, Name, "list of files to add", "r");
d423 1
a423 1
    while (LQU_fReadLine(fp, &Line, UF_NORMAL) > 0) {
d426 1
a426 1
	 * If we use UF_NORMAL it will swallow lines that start with a #,
d430 1
d441 1
a441 6
	    LQC_EmptyHashTableOut();
	    LQT_WriteCurrentMaxWID();
	    LQT_SyncAndCloseAllKeyValueDatabases();
	    LQTpFlushWIDCache();
	    LQTp_FlushLastBlockCache();

d454 2
a455 1
LQC_MakeInput(FileInfo)
d486 1
a486 1
	    if ((fd = LQT_UnpackAndOpen(FileInfo->Name)) < 0) {
d533 2
a534 1
LQC_UnIndexOneWaitingFile()
d542 7
a548 2
    LQC_UnIndexStream(FilesWaiting->FileInfo);
    LQT_DestroyFileInfo(FilesWaiting->FileInfo);
d550 8
d565 2
a566 1
LQC_UnIndexStream(FileInfo)
d574 5
a578 1
	WordInfo = LQT_ReadWordFromFileInfo(FileInfo);
d583 1
a583 1
	    LQC_UnIndexWord(FileInfo, WordInfo);
a593 8

    if (AsciiTrace) {
	fprintf(stderr, "%s: %d: %s: unindexed.\n",
	    progname,
	    FileInfo->FID,
	    FileInfo->Name
	);
    }
d597 2
a598 1
LQC_AddToFilesWaitingToBeDeleted(FileName, StatusCallBack)
d605 1
a605 1
    if ((FID = LQT_NameToFID(FileName)) == (t_FID) 0) {
d639 1
a639 1
    if ((FileInfo = LQT_FIDToFileInfo(FID)) == (t_FileInfo *) 0) {
d660 1
a660 1
	char *theName = LQT_FindFile(FileInfo->Name);
d673 1
a673 1
		if (AsciiTrace) {
d679 1
a679 1
		LQT_DestroyFileInfo(FileInfo);
d687 1
a687 1
	    LQT_DestroyFileInfo(FileInfo);
d692 1
a692 1
    if ((FileInfo->Stream = LQC_MakeInput(FileInfo)) == (FILE *) 0) {
d697 1
a697 1
	LQT_DestroyFileInfo(FileInfo);
d718 1
a718 1
	LQC_UnIndexOneWaitingFile();
d749 7
a755 9
	if (AsciiTrace) {
	    fprintf(stderr,
		"%s: Using %ld hash table entries (%d bits), %ld bytes\n",
		progname,
		HashSize,
		BitsToShift,
		HashSize * sizeof(t_WordToDelete)
	    );
	}
d760 12
a771 14
	if (AsciiTrace) {
	    Error(E_WARN|E_MULTILINE,
		"Hash size increased from %ld to %ld to be a power of two",
		HashSize,
		RealHashSize
	    );
	    Error(E_WARN|E_LASTLINE,
		"Using %ld hash table entries [%d bits], %ld bytes",
		HashSize,
		BitsToShift,
		HashSize * sizeof(t_WordToDelete)
	    );
	    HashSize = RealHashSize;
	}
d785 1
d791 2
a792 1
LQC_EmptyHashTableOut()
a801 1
fprintf(stderr, "...%d[%ld]", theSlot, theEntry->WID);
d805 1
a805 1
		LQC_RemoveUnwantedPlacesFor(theEntry);
d844 9
a852 10
		if (AsciiTrace > 5) {
		    fprintf(stderr, "(%ld=%*.*s@@%ld already there, slot %ld) ",
			theWordInfo->WID,
			theWordInfo->Length,
			theWordInfo->Length,
			theWordInfo->Word,
			theFileInfo->FID,
			theSlot
		    );
		}
d862 9
a870 10
    if (AsciiTrace > 5) {
	fprintf(stderr, "<- %*.*s\tslot %ld\t%ld=%s\n",
	    theWordInfo->Length,
	    theWordInfo->Length,
	    theWordInfo->Word,
	    theSlot,
	    theFileInfo->FID,
	    theFileInfo->Name
	);
    }
d912 2
a913 1
LQC_UnIndexWord(FileInfo, WordInfo)
d928 3
a930 2
    if (AsciiTrace >= 4) {
	fprintf(stderr, "{%*.*s} ",
d945 1
a945 1
	WordInfo->WID = LQT_WordToWID(WordInfo->Word, WordInfo->Length);
a975 1
fprintf(stderr, "WID %ld hashes to Slot %ld\n", WordInfo->WID,WordInfo->WID & (HashSize - 1));
a978 1
fprintf(stderr, "it was zero.\n");
a983 2
fprintf(stderr, "theEntry->WID %ld != WID %ld\n", theEntry->WID, WordInfo->WID);

a985 1
fprintf(stderr, "got to %ld >= %ld\n", (*Entryp)->WID, WordInfo->WID);
d997 2
a998 2
		LQC_EmptyHashTableOut();
		LQC_UnIndexWord(FileInfo, WordInfo);
a1003 2
fprintf(stderr, "allocting entry for WID %ld\n", WordInfo->WID);

a1047 1
fprintf(stderr, "A new one!\n");
d1096 2
a1097 1
LQC_RemoveUnwantedPlacesFor(WordToDelete)
d1106 1
a1106 1
	!(WordInfo = LQT_WIDToWordInfo(WordToDelete->WID))
d1113 2
a1114 12
    if (AsciiTrace >= 4) {
	fprintf(stderr, "LQC_RemoveUnwantedPlacesFor: WID %d: Word %s\n",
	    WordInfo->WID,
	    WordInfo->Word
	);
	if (AsciiTrace & 32) {
	    LQT_fprintWordInfo(stderr, WordInfo, "LQC_RemoveUnwantedPlacesFor");
	}
	if (WordInfo->NumberOfWordPlaces == 0) {
	    (void) LQT_DestroyWordInfo(WordInfo);
	    return;
	}
d1117 4
d1123 1
a1123 1
    pblock = LQT_GetpblockWhere(WordInfo, LQC_FIDIsWanted);
d1132 1
a1132 1
	LQT_DeleteWordPlaces(WordInfo->Offset, WordInfo->WID);
d1138 2
a1139 2
    if (pblock->NumberOfWordPlaces <= MaxWordPlacesInAWordBlock) {
	(void) LQT_MakeWordInfoBlock(WordInfo, pblock);
d1147 1
a1147 1
	(void) LQT_Writepblock(WordInfo, pblock);
d1150 1
a1150 1
    if (LQT_PutWordInfoIntoIndex(WordInfo, pblock->ChainStart) < 0) {
d1159 1
a1159 1
    (void) LQT_DestroyWordInfo(WordInfo);
@
