head	1.12;
access;
symbols;
locks; strict;
comment	@ * @;


1.12
date	2019.04.21.06.08.14;	author lee;	state Exp;
branches;
next	1.11;

1.11
date	96.07.01.21.37.10;	author lee;	state Exp;
branches;
next	1.10;

1.10
date	95.09.05.00.32.05;	author lee;	state Exp;
branches;
next	1.9;

1.9
date	95.08.20.20.19.08;	author lee;	state Exp;
branches;
next	1.8;

1.8
date	95.08.20.20.17.39;	author lee;	state Exp;
branches;
next	1.7;

1.7
date	94.02.26.14.55.08;	author lee;	state Exp;
branches;
next	1.6;

1.6
date	93.10.24.14.30.42;	author lee;	state Exp;
branches;
next	1.5;

1.5
date	93.09.03.16.05.07;	author lee;	state Exp;
branches;
next	1.4;

1.4
date	90.10.06.00.50.56;	author lee;	state Rel1-10;
branches;
next	1.3;

1.3
date	90.08.29.21.45.29;	author lee;	state Exp;
branches;
next	1.2;

1.2
date	90.08.09.19.17.16;	author lee;	state Exp;
branches;
next	1.1;

1.1
date	90.03.24.20.22.49;	author lee;	state Exp;
branches;
next	;


desc
@LQU_fReadLine no longer allocates each line...
@


1.12
log
@for migration to CVS
@
text
@/* lqphrase.c -- Copyright 1989, 1990, 1994, 1995 Liam R. E. Quin.
 * All Rights Reserved.
 * This code is NOT in the public domain.
 * See the file COPYRIGHT for full details.
 *
 * $Id: lqphrase.c,v 1.11 1996/07/01 21:37:10 lee Exp lee $
 *
 * lqphrase, part of Liam Quin's text retrieval package...
 *
 * lqphrase is intended to be an example of one way to use the programming
 * interface to lq-text.
 *
 * The idea is quite simple:
 * Simply take a phrase p, held in a string (char *p), and call
 *	t_Phrase *Phrase = LQT_StringToPhrase(p);
 * The result, if not null, contains only one interesting thing at this
 * point:
 *	Phrase->ModifiedString
 * is the canonical version of p -- with common and short words removed.
 * for example,
 *	p = "The boy sat down in His Boat and playd with his toes.";
 * might result in Phrase->ModifiedString containing
 *	"[*the] boy sat down [in] [*his] boat [*and] [?playd] with [*his] toe"
 * Common words are marked with a *, and unknown words with ?.
 * [NOTE: modifiedPhrase is no longer be supported]
 *
 * An attempt may have been made to reduce plurals.
 *
 * Since this phrase contains a word not in the database (playd), it will
 * never match anything.  As a result, it is a good idea to print this string
 * (possibly massaging it first) so users can see what is going on.  If you
 * have it, the curses-based "lqtext" does this.
 *
 * If we change "playd" to "played", the above string is equivalent to
 *	"[*the] boy sat down [xx] [*the] boat [*the] played with [*the] toe"
 * In other words, all common words are equivalent.  The package remembers
 * that one or more common words were skipped, and also that one or more
 * lumps of letters too small to make up a word were skipped.
 * Compare also h/wordrules.h for more info, and liblqtext/Phrase.c for
 * the code that actually does the matching.
 *
 * Now, having sorted that out, we have our canonical string (and lots of
 * other things) in Phrase, so we can now call
 *	LQT_MakeMatches(Phrase);
 * This will return the number of matches (*NOT* the number of files) for
 * the given ModifiedPhrase in the database.
 * This can take several seconds, so again, it can be worth printing out
 * the modified string as soon as it is available, so the user is looking at
 * that whilst LQT_MakeMatches is working!
 *
 * Now we have done LQT_MakeMatches, we can march along the linked list of
 * pointers to linked lists of arrays of matches.  See MatchOnePhrase() below.
 *
 * As an optimisation, LQT_MakeMatchesWhere() lets us call a function to
 * print (in out casr) each match as it is found.  This is slower overall,
 * because of the extra function calls, but not much slower, and the results
 * start to appear much sooner, so it feels much faster.
 *
 * Now, each match currently gives us
 * t_FID FID; Files are numbered from 1 in the database
 * unsigned long BlockInFile; -- the block in the file
 * unsigned char WordInBlock; -- the word in the block
 * unsigned char StuffBefore; -- the amount of leading garbage
 * unsigned char Flags, including (see wordrules.h):
 *
 * WPF_WASPLURAL		The word...  ended in s
 * WPF_UPPERCASE		...Started with a capital letter
 * WPF_POSSESSIVE		...ended in 's
 * WPF_ENDEDINING		...ended in ing
 * WPF_LASTWASCOMMON	the previous word was common
 * WPF_LASTHADLETTERS	we skipped some letters to get here
 * WPF_LASTINBLOCK	I'm the last word in this block
 *
 */

#include "error.h"
#include "globals.h" /* defines and declarations for database filenames */

#include <stdio.h> /* stderr, also for fileinfo.h */
#include <sys/types.h>
#ifdef HAVE_SYSV_FCNTL_H
# include <sys/file.h>
#endif
#ifdef HAVE_FCNTL_H
#include <fcntl.h>
#endif
#include <malloc.h>
#ifdef HAVE_STRING_H
# include <string.h>
#else
# include <strings.h>
#endif

#include "emalloc.h" /* for efree() */
#include "fileinfo.h" /* for wordinfo.h */
#include "wordinfo.h"
#include "pblock.h"
#include "phrase.h"
#include "lqutil.h"
#include "liblqtext.h"
#include "lqtrace.h"

/** System calls and functions... **/
/** Unix system calls used in this file: **/
extern void exit();

/** Unix Library Functions used: **/
/** lqtext library functions: **/

/** functions used before they're defined within this file: **/

PRIVATE void MatchOnePhrase(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *db,
    char *Phrase
#endif
);

/** **/

static char *Revision = "@@(#) $Id: lqphrase.c,v 1.11 1996/07/01 21:37:10 lee Exp lee $";

char *progname = "lqphrase";

int SilentMode = 0; /* don't print matches if set to one */

int
main(argc, argv)
    int argc;
    char *argv[];
{
    extern int optind, getopt();
    extern char *optarg;
    int ch;
    int ErrorFlag = 0;
    char *InputFile = 0;
    t_lqdbOptions *Options;
    t_LQTEXT_Database *db = 0;


    progname = argv[0];

    Options = LQT_InitFromArgv(argc, argv);

    while ((ch = getopt(argc, argv, "Zz:af:hNpr:slxVv")) != EOF) {
	switch (ch) {
	case 'z':
	case 'Z':
	    break; /* done by LQT_InitFromArgv(); */
	case 'V':
	    fprintf(stderr, "%s version %s\n", progname, Revision);
	    break;
	case 'f':
	    InputFile = optarg;
	    break;
	case 'l':
	    break; /* list mode is the default */
	case 'r':
	    /* ignored for compat. with lqrank*/
	    break;
	case 's':
	    SilentMode = 1;
	    break;
	case 'x':
	    ErrorFlag = (-1);
	    break;
	case '?':
	    ErrorFlag = 1;
	}
    }

    if (ErrorFlag) {
	fprintf(stderr, "Usage: %s [options] \"phrase\" [...]\n", progname);
	fprintf(stderr, "%s: options are:\n", progname);
	fputs("\
	-l	-- list mode, suitable for lqshow (the default)\n\
	-s	-- silent mode; exit status indicates success of matching\n\
\n", stderr);

	LQT_PrintDefaultUsage(Options);
	exit( ErrorFlag > 0 ? 1 : 0); /* 0 means -x was used */
    }
    
    db = LQT_OpenDatabase(Options, O_RDONLY, 0);
    if (!db || LQT_ObtainReadOnlyAccess(db) < 0) {
	Error(E_FATAL, "couldn't open lq-text database");
    }

    if (InputFile) {
	FILE *f;
	char NeedClose = 1;
	char *theLine;

	if (STREQ(InputFile, "-")) {
	    f = stdin;
	    NeedClose = 0;
	} else {
	    f = LQU_fEopen(E_FATAL|E_SYS, InputFile, "List of phrases", "r");
	}

	while (LQU_fReadLine(f, &theLine, LQUF_NORMAL) >= 0) {
	    if (theLine && *theLine) {
		MatchOnePhrase(db, theLine);
	    }
	}

	if (NeedClose) {
	    (void) fclose(f);
	}
    }

    while (optind < argc) {
	MatchOnePhrase(db, argv[optind++]);
    }

    if (SilentMode) {
	/* if we got to here we didn't find anything */
	exit(1);
    }
    return 0;
}

static int gotOne = 0;

static int
ExitSilently(db, Phrase, Match)
    t_LQTEXT_Database *db;
    t_Phrase *Phrase;
    t_Match *Match;
{
    ++gotOne;
    return LQMATCH_QUIT;
}

typedef struct s_Rank {
    t_FID FID;
    unsigned long Sum;
} t_Rank;

PRIVATE void
MatchOnePhrase(db, Phrase)
    t_LQTEXT_Database *db;
    char *Phrase;
{
    t_Phrase *P;

    if (!Phrase || !*Phrase) {
	/* ignore an empty phrase */
	return;
    }

    if ((P = LQT_StringToPhrase(db, Phrase)) == (t_Phrase *) 0) {
#ifdef LQTRACE_VERBOSE
	LQT_Trace(LQTRACE_VERBOSE,
	    "Query \"%s\" contained no recognised words - ignored",
	    Phrase
	);
#endif
	return;
    }

    if (SilentMode) {
	(void) LQT_MakeMatchesWhere(db, P, ExitSilently);
	if (gotOne) {
	    exit(0);
	}
    } else {
	(void) LQT_MakeMatchesWhere(db, P, LQT_PrintAndRejectOneMatch);
    }
    LQT_DestroyPhrase(db, P);
}
@


1.11
log
@LQU_fReadLine no longer allocates each line...
@
text
@d6 1
a6 1
 * $Id: lqphrase.c,v 1.10 95/09/05 00:32:05 lee Exp $
d121 1
a121 1
static char *Revision = "@@(#) $Id: lqphrase.c,v 1.10 95/09/05 00:32:05 lee Exp $";
d252 9
a260 1
    if ((P = LQT_StringToPhrase(db, Phrase)) == (t_Phrase *) 0) return;
@


1.10
log
@renamed fEopen to add lQU_ prefic.
@
text
@d1 1
a1 1
/* lqphrase.c -- Copyright 1989, 1990, 1994 Liam R. E. Quin.
d6 1
a6 1
 * $Id: lqphrase.c,v 1.9 95/08/20 20:19:08 lee Exp Locker: lee $
a98 1
#include "readfile.h"
a102 2
extern t_PhraseCaseMatch PhraseMatchLevel;

d114 1
d121 1
a121 1
static char *Revision = "@@(#) $Id: lqphrase.c,v 1.9 95/08/20 20:19:08 lee Exp Locker: lee $";
a125 1
int PrintNumberOfWordsInMatchedPhrases = 1;
d137 2
d140 1
d143 1
a143 1
    LQT_InitFromArgv(argc, argv);
a157 3
	case 'N':
	    PrintNumberOfWordsInMatchedPhrases = 0;
	    break;
d180 1
a180 1
	LQT_PrintDefaultUsage();
d184 3
a186 5
    if (LQT_ObtainReadOnlyAccess() < 0) {
	Error(E_FATAL,
	    "couldn't open lq-text database in directory \"%s\"",
	    DatabaseDir
	);
a188 18
    if (LQT_TraceFlagsSet(LQTRACE_VERBOSE|LQTRACE_DEBUG)) {
	switch (PhraseMatchLevel) {
	case PCM_HalfCase:
	    fprintf(stderr, "%s: Matching phrases heuristically.\n", progname);
	    break;
	case PCM_SameCase:
	    fprintf(stderr, "%s: Matching phrases precisely.\n", progname);
	    break;
	case PCM_AnyCase:
	    fprintf(stderr, "%s: Matching phrases approximately.\n", progname);
	    break;
	default:
	    fprintf(stderr, "%s: internall error, case matching is %d\n",
						progname, PhraseMatchLevel);
	    exit(2);
	}
    }

d201 1
a201 1
	while (LQU_fReadLine(f, &theLine, UF_NORMAL) >= 0) {
d203 1
a203 1
		MatchOnePhrase(theLine);
a204 3
	    if (theLine) {
		(void) efree(theLine);
	    }
d213 1
a213 1
	MatchOnePhrase(argv[optind++]);
d223 1
a223 1
static int NumberOfWords = 1;
d226 2
a227 1
PrintAndAcceptOneMatch(Phrase, Match)
d231 2
a232 53
    static t_FID LastFID = (t_FID) 0;
    static t_FileInfo *FileInfo = 0;

    if (Match != (t_Match *) 0) {
	if (Match->Where->FID != LastFID) {
	    t_FID FID = Match->Where->FID;
	    /*TODO: use DestroyFileInfo instead of efree:... */

	    if (FileInfo) {
		efree((char *) FileInfo);
	    }

	    if ((FileInfo = LQT_FIDToFileInfo(FID)) == (t_FileInfo *) 0) {
		return 0; /* don't want this match! */
	    }
	    LastFID = FID;
	}

	/* Now that we know that we have something to print... */
	if (SilentMode) {
	    exit(0); /* OK, found something */
	}
	if (LQT_TraceFlagsSet(LQTRACE_VERBOSE|LQTRACE_DEBUG)) {
	    printf(
		"NW %-3d BIF %-7lu WIB %-7u Sep %-3d F %-4o FID %-4d %s\n",
		NumberOfWords,
		Match->Where->BlockInFile,
		(unsigned) Match->Where->WordInBlock,
		(unsigned) Match->Where->StuffBefore,
		(unsigned) Match->Where->Flags,
		FileInfo->FID,
		FileInfo->Name
	    );
	} else {
	    if (PrintNumberOfWordsInMatchedPhrases) {
		printf("%d %lu %u %lu %s\n",
		    NumberOfWords,
		    Match->Where->BlockInFile,
		    Match->Where->WordInBlock,
		    FileInfo->FID,
		    FileInfo->Name
		);
	    } else {
		printf("%lu %u %lu %s\n",
		    Match->Where->BlockInFile,
		    Match->Where->WordInBlock,
		    FileInfo->FID,
		    FileInfo->Name
		);
	    }
	}
    }
    return 0;
d241 2
a242 1
MatchOnePhrase(Phrase)
d252 1
a252 1
    if ((P = LQT_StringToPhrase(Phrase)) == (t_Phrase *) 0) return;
d254 7
a260 8
    /* Count words in the phrase: */
    if (PrintNumberOfWordsInMatchedPhrases ||
	LQT_TraceFlagsSet(LQTRACE_VERBOSE|LQTRACE_DEBUG)
    ) {
	/* NumberOfWords is global to this file, so that the
	 * callback PrintAndAcceptOneMatch() can use it:
	 */
	NumberOfWords = LQT_NumberOfWordsInPhrase(P);
d262 1
a262 4

    if (LQT_MakeMatchesWhere(P, PrintAndAcceptOneMatch) <= 0L) {
	return;
    }
@


1.9
log
@Changing the tracing had introduced a bug.
@
text
@d6 1
a6 1
 * $Id: lqphrase.c,v 1.8 95/08/20 20:17:39 lee Exp Locker: lee $
d123 1
a123 1
static char *Revision = "@@(#) $Id: lqphrase.c,v 1.8 95/08/20 20:17:39 lee Exp Locker: lee $";
d221 1
a221 1
	    f = fEopen(E_FATAL|E_SYS, InputFile, "List of phrases", "r");
@


1.8
log
@New trace flags
@
text
@d6 1
a6 1
 * $Id: lqphrase.c,v 1.7 94/02/26 14:55:08 lee Exp $
d123 1
a123 1
static char *Revision = "@@(#) $Id: lqphrase.c,v 1.7 94/02/26 14:55:08 lee Exp $";
d330 1
a330 1
    if (PrintNumberOfWordsInMatchedPhrases &&
@


1.7
log
@API change
@
text
@d1 2
a2 1
/* lqphrase.c -- Copyright 1989, 1990 Liam R. Quin.  All Rights Reserved.
d6 1
a6 4
 * $Id: lqphrase.c,v 1.6 93/10/24 14:30:42 lee Exp $
 */

/* lqphrase, part of Liam Quin's text retrieval package...
d8 2
d49 1
a49 4
 * that whilst LQT_MakeMatches is working!  I have experimented with faster
 * versions of LQT_MakeMatches involving binary search, but the extra complexity
 * slowed things down on smaller databases.  I don't have enough disk space
 * here to make a large enough database to do real timings, sorry.
d52 1
a52 2
 * pointers to linked lists of arrays of matches.  Clear?  No?  Well,
 * that's why there's an example.  See Match() below.
d54 5
d80 5
d86 1
a86 1
#include <sys/types.h>
d88 7
a94 1
#include "emalloc.h"
d101 2
a103 5
#ifndef STREQ
# define STREQ(boy,girl) ((*(boy) == *(girl)) && (!strcmp((boy),(girl))))
#endif

extern int AsciiTrace;
a111 2
extern void LQT_InitFromArgv();
extern void LQT_PrintDefaultUsage();
d114 7
a120 1
void Match();
d123 1
a123 1
static char *Revision = "@@(#) $Id: lqphrase.c,v 1.6 93/10/24 14:30:42 lee Exp $";
a152 3
	case 'v': /* same as -t 1 */
	    AsciiTrace++;
	    break;
d187 8
a194 1
    if (AsciiTrace > 1) {
a220 2
	    extern FILE *fEopen();

d226 1
a226 1
		Match(theLine);
d234 1
a234 1
	    (void) close(f);
d239 1
a239 1
	Match(argv[optind++]);
a255 1
    extern t_FileInfo *LQT_FIDToFileInfo();
d278 1
a278 1
	if (AsciiTrace) {
d280 1
a280 1
		"NW %-3d BIF %-7lu WIB %-7u Sep %-3d F %-04o FID %-4d %s\n",
d311 7
a317 2
void
Match(Phrase)
a319 3
    extern t_Phrase *LQT_StringToPhrase();
    extern long LQT_MakeMatchesWhere();

a320 1
    t_MatchList *Matches;
d322 5
a326 1
    if (!Phrase || !*Phrase) return;
d330 3
a332 1
    if (PrintNumberOfWordsInMatchedPhrases || AsciiTrace) {
d339 2
a340 8
    if (LQT_MakeMatchesWhere(P, PrintAndAcceptOneMatch) <= 0L) return;

#if 0
    if (P) {
	for (Matches = P->Matches; Matches != (t_MatchList *) 0;
						Matches = Matches->Next) {
	    (void) PrintAndAcceptOneMatch(P, Matches->Match);
	}
a341 1
#endif
@


1.6
log
@new match format includes FID. 
@
text
@d5 1
a5 1
 * $Id: lqphrase.c,v 1.5 93/09/03 16:05:07 lee Exp Locker: lee $
d15 1
a15 1
 *	t_Phrase *Phrase = String2Phrase(p);
d44 1
a44 1
 *	MakeMatches(Phrase);
d49 2
a50 2
 * that whilst MakeMatches is working!  I have experimented with faster
 * versions of MakeMatches involving binary search, but the extra complexity
d54 1
a54 1
 * Now we have done MakeMatches, we can march along the linked list of
d88 1
d103 2
a104 2
extern void SetDefaults();
extern void DefaultUsage();
d110 1
a110 1
static char *Revision = "@@(#) $Id: lqphrase.c,v 1.5 93/09/03 16:05:07 lee Exp Locker: lee $";
d126 1
a126 1
    char *InputFile;
d130 1
a130 1
    SetDefaults(argc, argv);
d136 1
a136 1
	    break; /* done by SetDefaults(); */
d173 1
a173 1
	DefaultUsage();
d209 1
a209 1
	while (fReadLine(f, &theLine, UF_NORMAL) >= 0) {
d234 63
d301 2
a302 3
    extern t_Phrase *String2Phrase();
    extern t_FileInfo *GetFileInfo();
    extern long MakeMatches();
a305 3
    t_FID LastFID = (t_FID) 0;
    t_FileInfo *FileInfo = 0;
    int NumberOfWords;
d308 1
a308 1
    if ((P = String2Phrase(Phrase)) == (t_Phrase *) 0) return;
d310 1
a310 3
    if (MakeMatches(P) <= 0L) return;

    /* Count words in the match: */
d312 4
a315 1
	NumberOfWords = NumberOfWordsInPhrase(P);
d318 3
d324 1
a324 45
	    if (Matches->Match != (t_Match *) 0) {
		if (Matches->Match->Where->FID != LastFID) {
		    t_FID FID = Matches->Match->Where->FID;
		    /*TODO: use DestroyFileInfo instead of efree:... */
		    if (FileInfo) efree((char *) FileInfo);
		    if ((FileInfo = GetFileInfo(FID)) == (t_FileInfo *) 0) {
			continue;
		    }
		    LastFID = FID;
		}

		/* Now that we know that we have something to print... */
		if (SilentMode) {
		    exit(0); /* OK, found something */
		}
		if (AsciiTrace) {
		    printf(
			"NW %-3d BIF %-7lu WIB %-7u Sep %-3d F %-04o FID %-4d %s\n",
			NumberOfWords,
			Matches->Match->Where->BlockInFile,
			(unsigned) Matches->Match->Where->WordInBlock,
			(unsigned) Matches->Match->Where->StuffBefore,
			(unsigned) Matches->Match->Where->Flags,
			FileInfo->FID,
			FileInfo->Name
		    );
		} else {
		    if (PrintNumberOfWordsInMatchedPhrases) {
			printf("%d %lu %u %lu %s\n",
			    NumberOfWords,
			    Matches->Match->Where->BlockInFile,
			    Matches->Match->Where->WordInBlock,
			    FileInfo->FID,
			    FileInfo->Name
			);
		    } else {
			printf("%lu %u %lu %s\n",
			    Matches->Match->Where->BlockInFile,
			    Matches->Match->Where->WordInBlock,
			    FileInfo->FID,
			    FileInfo->Name
			);
		    }
		}
	    }
d327 1
@


1.5
log
@Add NmnerOfWordsInphrase to match
@
text
@d5 1
a5 1
 * $Id: lqphrase.c,v 1.4 90/10/06 00:50:56 lee Rel1-10 Locker: lee $
d25 2
d28 1
d39 2
a40 10
 * The following are equivalent:
 * L.R.E. Quin    L. Quin	L.R.Quin	X.X.Quin
 * in a QUIN	a QuIn
 * and the following are not the same as those:
 * Quin (no preceding garbage)
 * L.R.E. quin (first letter of `Quin' is not upper case (the rest is ignored)
 * [*the] Quin (common words are not the same as skipped letters)
 * L. Quin's (the presence of the posessive ('s) is significant)
 * L. Quins (plural (two Quins) not the same as singular)
 * L. Quinn (spelt incorrectly!)
d54 1
a54 1
 * Now we have done MakeMatches, we can marck along the linked list of
d56 1
a56 1
 * that's why there's en axample.  See Match() below.
d109 1
a109 1
static char *Revision = "@@(#) $Id: lqphrase.c,v 1.4 90/10/06 00:50:56 lee Rel1-10 Locker: lee $";
d114 1
a114 1
int PrintNumberOfWordsInMatchedPhrases = 0;
d148 1
a148 1
	    PrintNumberOfWordsInMatchedPhrases = 1;
d276 2
a277 1
		    printf("%-3d %-7lu %-7u %-3d %-3d %-4d %s\n",
d288 1
a288 1
			printf("%d %u %u %s\n",
d292 1
d296 1
a296 1
			printf("%u %u %s\n",
d299 1
a307 17

/*
 * $Log:	lqphrase.c,v $
 * Revision 1.4  90/10/06  00:50:56  lee
 * Prepared for first beta release.
 * 
 * Revision 1.3  90/08/29  21:45:29  lee
 * Alpha release
 * 
 * Revision 1.2  90/08/09  19:17:16  lee
 * *** empty log message ***
 * 
 * Revision 1.1  90/03/24  20:22:49  lee
 * Initial revision
 * 
 */

@


1.4
log
@Prepared for first beta release.
@
text
@d5 1
a5 1
 * $Id$
d80 1
d92 1
d114 1
a114 1
static char *Revision = "@@(#) $Id$";
d116 1
a116 1
char *progname = "tryphrase";
d119 1
d127 1
a127 1
    /** extern char *optarg; (unused at present) **/
d130 1
d136 1
a136 1
    while ((ch = getopt(argc, argv, "Zz:ahpslxVv")) != EOF) {
d145 1
a145 1
	    AsciiTrace = 1;
d147 3
d152 6
a168 3
    /* Normally put call to lrqError here to give a helpful message,
     * but not yet ready to ship the error handling package, sorry
     */
d199 28
d250 1
d257 5
d281 9
a289 6
		    printf("%-7lu %-7u %-3d %-3d %s\n",
				Matches->Match->Where->BlockInFile,
				(unsigned) Matches->Match->Where->WordInBlock,
				(unsigned) Matches->Match->Where->StuffBefore,
				(unsigned) Matches->Match->Where->Flags,
				FileInfo->Name);
d291 14
a304 4
		    printf("%-7.7lu %-7.7u %s\n",
				Matches->Match->Where->BlockInFile,
				Matches->Match->Where->WordInBlock,
				FileInfo->Name);
d313 3
@


1.3
log
@Alpha release
@
text
@d1 1
a1 1
/* lqphrase.c -- Copyright 1989 Liam R. Quin.  All Rights Reserved.
d5 1
a5 1
 * $Header: /home/lee/lq-text/src/lqtext/RCS/lqphrase.c,v 1.2 90/08/09 19:17:16 lee Exp Locker: lee $
a79 11
/*
 * $Log:	lqphrase.c,v $
 * Revision 1.2  90/08/09  19:17:16  lee
 * *** empty log message ***
 * 
 * Revision 1.1  90/03/24  20:22:49  lee
 * Initial revision
 * 
 *
 */

d112 1
a112 1
static char *Revision = "@@(#) $Header: /home/lee/lq-text/src/lqtext/RCS/lqphrase.c,v 1.2 90/08/09 19:17:16 lee Exp Locker: lee $";
d253 14
@


1.2
log
@*** empty log message ***
@
text
@d5 1
a5 1
 * $Header: lqphrase.c,v 1.1 90/03/24 20:22:49 lee Locked $
d82 3
d123 1
a123 1
static char *Revision = "@@(#) $Header: lqphrase.c,v 1.1 90/03/24 20:22:49 lee Locked $";
d236 1
a236 1
		    if (FileInfo) efree((char *) FileInfo, "Match:207");
@


1.1
log
@Initial revision
@
text
@d5 1
a5 1
 * $Header$
d81 4
a84 1
 * $Log$
d117 1
d120 1
a120 1
static char *Revision = "@@(#) $Header$";
d132 1
a132 1
    extern char *optarg;
@
