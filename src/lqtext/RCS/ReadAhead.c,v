head     1.2;
branch   ;
access   ;
symbols  ;
locks    ;
comment  @ * @;


1.2
date     90.10.06.00.50.47;  author lee;  state Rel;
branches ;
next     1.1;

1.1
date     90.08.09.19.17.09;  author lee;  state Exp;
branches ;
next     ;


desc
@@


1.2
log
@Prepared for first beta release.
@
text
@/* ReadAhead.c -- Copyright 1990 Liam R. Quin.  All Rights Reserved.
 * This code is NOT in the public domain.
 * See the file COPYRIGHT for full details.
 *
 * $Id$
 *
 * $Log:	ReadAhead.c,v $
 * Revision 1.1  90/08/09  19:17:09  lee
 * Initial revision
 * 
 *
 */

#include "globals.h" /* defines and declarations for database filenames */

#include <stdio.h>
#include <fcntl.h>

#include "Message.h"

/* ReadAhead -- simple readahead daemon for text database.
 *
 * The idea is that whoever requests a block of data that is not already
 * in the Unix buffer cache gets put to sleep, and we don't want that process
 * to be the lqtext front end.
 * So this program goes and requests the data slightly before lqtext needs
 * it, ensuring that it is in the buffer cache when needed.
 * Clearly we have to be able to recieve a message in a fraction of the
 * time it would take for Unix to fetch the block, although with a little
 * care we can send some of the requests to ReadAhead when the user thinks
 * we're waiting for him/her to type something.
 *
 * Addfile uses the Read File request to get the first few blocks of a file.
 * There's no point going too far, since the blocks will then get flushed
 * before lqaddfile reads them on a busy system.
 * The big win there is likely to be that the open() will force the i-node
 * into the Unix cache, so lqaddfile doesn't have to wait for it.  On many
 * busy systems an open() can take a noticeable fraction of a second if
 * we have to go to sleep and the i-node is fetched from disk...
 *
 */

static char *FiFo;
static FILE *FiFoFp;


int
main(argc, argv)
    int argc;
    char *argv[];
{
    t_lqMessage *M;

    SetDefaults(argc, argv);

    StartMessages(getpid());

    while ((M = GetMessage()) != LQM_QUIT) {
	DoMessage(M);
    }
    Quit(0);
}

StartMessages(Number)
    int Number;
{
    /* make a FIFO */
    FiFo = malloc(30);
    (void) sprintf(FiFo, "/tmp/.lqtd%06d", Number);
    if (mknod(FiFo, S_IFIFO|0666, 0) < 0) {
	perror(FiFo);
	exit(1);
    }
    if ((FiFoFp = fopen(FiFo, "r")) == (FILE *) 0) {
	perror(FiFoFp);
	Quit(1);
    }
}

SendMessage(M)
    t_Message *M;
{
    static int MyPid = getpid();

    if (!FiFoFp) {
	/* initialise */
	exit(23);
    }

    (void) fprintf(FiFoFp, "%c %ld %ld\n", N->Instruction, M->WID, MyPid);
}

t_Message *
GetMessage()
{
    static t_Message M;
    char *Buf[512];
    int n;

    if (!FiFoFp) return LQM_QUIT;

    do {
	if (fgets(FiFoFp, Buf, sizeof Buf) == (char *) 0) {
	    (void) fclose(FiFoFp);
	    FiFoFp = (FILE *) 0;
	    return LQM_QUIT;
	}
    } while (Buf[0] == '#' || Buf[0] == '\n' || !Buf[0]);

    if (sscanf(Buf, "%c %ld%ld\n", &M.Instruction, &M.WID, &M.Sender) != 3) {
	fprintf(stderr, "%s: bad message [%s]\n", progname, Buf);
	Quit(1);
    }

    return &M;
}

void
Quit(n)
    int n;
{
    (void) unlink(FiFo);
    exit(n);
}

DoMessage(M)
    t_lqMessage M;
{
    switch (M->Type) {
    case LQM_READCHAIN:
	ReadChain(M);
	break;
    case LQM_READFILE:
	ReadFile(M);
	break;
    case LQM_QUIT:
	exit(0);
	/*NOTREACHED*/
	break;
    }
}

void
ReadFile(M)
    t_lqMessage *M;
{
    int fd;
    int BlockCount;
    char Buffer[1024];

    if (M->String[0] == '\0') {
	return;
    }

    if ((fd = open(M->String, O_RDONLY, 0)) < 0) {
	return;
    }

    for (BlockCount = 0; BlockCount < 10; BlockCount++) {
	if (read(fd, Buffer, sizeof Buffer) < sizeof Buffer) {
	    break;
	}
    }
    (void) close(fd);
}

static int DataFile = -1;
int AsciiTrace = 0;

/* The purpose of this routine is simply to put data into the buffer cache.
 * It should really know about the size of the cache, I suppose, to avoid
 * creating lots of extra work!
 */
ReadChain(WID)
    t_lqMessage *M;
{
    t_WordInfo *WordInfo;
    t_BlockHeader *H;
    char Block[BLOCKSIZE];
    unsigned long NextOffset;

    /* Read the index info */
    if ((WordInfo = WID2WordInfo(M->WID)) == (t_WordInfo *) 0) {
	return;
    }

    if (WordInfo->Offset == 0L || WordInfo->NumberOfWordPlaces == 0) {
	return; /* nothing to read! */
    }

    NextOffset = WordInfo->Offset;

    /* Silent about errors, this is only a performance fellow */
    if (DataFile < 0) {
	if ((DataFile = open(DataBase, O_RDONLY, 0)) < 0) {
	    exit(11); /* arbitrary number */
	}
    }

    do {
	if (lseek(DataFile, NextOffset, 0) < 0) {
	    exit(12);
	}

	if (read(DataFile, Block, BLOCKSIZE) != BLOCKSIZE) {
	    exit(13);
	}

	/*NOSTRICT*/
	H = (t_BlockHeader *) Block;

	NextOffset = H->NextOffset;
    } while (NextOffset != 0L);
}

@


1.1
log
@Initial revision
@
text
@d5 1
a5 1
 * $Header$
d7 4
a10 1
 * $Log$
@
