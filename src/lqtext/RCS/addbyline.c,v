head	1.8;
access;
symbols;
locks; strict;
comment	@ * @;


1.8
date	2019.04.21.06.08.14;	author lee;	state Exp;
branches;
next	1.7;

1.7
date	96.08.14.17.02.06;	author lee;	state Exp;
branches;
next	1.6;

1.6
date	96.07.04.21.43.34;	author lee;	state Exp;
branches;
next	1.5;

1.5
date	96.06.28.22.40.47;	author lee;	state Exp;
branches;
next	1.4;

1.4
date	96.06.20.14.22.08;	author lee;	state Exp;
branches;
next	1.3;

1.3
date	96.05.17.22.35.55;	author lee;	state Exp;
branches;
next	1.2;

1.2
date	96.05.17.02.50.17;	author lee;	state Exp;
branches;
next	1.1;

1.1
date	96.05.15.23.18.25;	author lee;	state Exp;
branches;
next	;


desc
@add files with line numbers instead of blocks.
@


1.8
log
@for migration to CVS
@
text
@/* addbyline.c -- Copyright 1995, 1996 Liam R. E. Quin.
 * All Rights Reserved.
 * This code is NOT in the public domain.
 * See the file COPYRIGHT for full details.
 */

/* addbyline -- add a file to the LQ-Text text retrieval index
 * Liam Quin, August 1989 and later...
 * Modified to prevent phrase matches over line boundaries.
 *
 * $Id: addbyline.c,v 1.7 1996/08/14 17:02:06 lee Exp lee $ 
 */

static char *Version =
    "@@(#) $Id: addbyline.c,v 1.7 1996/08/14 17:02:06 lee Exp lee $";

#include "globals.h" /* defines and declarations for database filenames */
#include "error.h"

#include <stdio.h>
#include <malloc.h>
#include <ctype.h>
#include <sys/types.h>

#ifdef HAVE_FCNTL_H
# ifdef HAVE_SYSV_FCNTL_H
#  include <sys/stat.h>
# endif
# include <fcntl.h>
#endif

#ifdef HAVE_STRING_H
# include <string.h>
#else
# include <strings.h>
#endif

#ifdef HAVE_STDLIB_H
# include <stdlib.h>
#endif

#ifdef HAVE_UNISTD_H
# include <unistd.h>
#endif

#include "fileinfo.h"
#include "wordinfo.h"
#include "wordrules.h"
#include "emalloc.h"
#include "addfile.h"
#include "lqutil.h"
#include "revision.h"
#include "liblqtext.h"
#include "lqtrace.h"
#include "filter.h"

/** Functions within this file that need declaring: **/
PRIVATE void AddStream(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *db,
    t_FileInfo *FileInfo
#endif
);

PRIVATE void AddFrom(
#ifdef HAVE_PROTO
    char *Name
#endif
);

/* Symbol Table Interface */
extern void AddWord(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *db,
    t_WordInfo *WordInfo
#endif
);

extern void DumpCache(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *db,
    int CallFree
#endif
);

extern void SetDumpThresh(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *db,
    int Thresh
#endif
);

PRIVATE int AddFile(
#ifdef HAVE_PROTO
    char *Name
#endif
);

extern int SetHashSize(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *db,
    int theNewSize
#endif
);


/**/

char *progname = "@@(#) $Id: addbyline.c,v 1.7 1996/08/14 17:02:06 lee Exp lee $";

#ifdef USE_LINENUMBERS
static int UseLineNumbers = 0;
#endif

static int SignalFlag = 0;
t_LQTEXT_Database *db = 0;

PRIVATE int
SignalHandler()
{
    ++SignalFlag;
    if (SignalFlag > 3) {
	LQT_CloseDatabase(db);
	Error(E_FATAL,
	    "received %d signals to quit, exiting; db may be corrupt!.",
	    SignalFlag
	);
    }
    return 0;
}

int
main(argc, argv)
    int argc;
    char *argv[];
{
    extern int getopt();
    extern char *optarg;
    extern int optind;
    extern int MaxWordsInCache; /* see wordtable.c */

    t_lqdbOptions *Options;
    int c;
    int ErrorFlag = 0;
    int DoNothing = 0;
    char *InputFile = (char *) 0;

#ifdef MALLOCTRACE
    malloc_debug(2);
#endif

    progname = argv[0]; /* retain the full path at first */

#ifdef M_MXFAST
    (void) mallopt(M_MXFAST, 6); /* i.e. typical word length with \0 */
    /* may need to comment mallopt() out entirely for BSD -- use ifndef.
     * seems to work under SunOS, though.
     * It says "Allocate 100 or so chunks of this size at a time, and whenever
     * I ask for this much or less, give me one of the chunks".
     */
#endif
    Options = LQT_InitFromArgv(argc, argv);

    while ((c = getopt(argc, argv, "w:f:H:M:xVZz:")) != -1) {
	switch (c) {
	case 'M':
	    if (!LQU_cknatstr(optarg)) {
		Error(E_FATAL|E_USAGE|E_XHINT,
		    "-M must be given a number >= 0, not \"%s\"",
		    optarg
		);
	    }
	    SetDumpThresh(db, atoi(optarg));
	    break;
	case 'H':
	    if (!LQU_cknatstr(optarg)) {
		Error(E_FATAL|E_USAGE|E_XHINT,
		    "-H must be given a hash table size >= 1, not \"%s\"",
		    optarg
		);
	    }
	    SetHashSize(db, atoi(optarg));
	    break;
	case 'w':
	    if (!LQU_cknatstr(optarg)) {
		Error(E_FATAL|E_USAGE|E_XHINT,
		    "-w must be given a number >= 0, not \"%s\"",
		    optarg
		);
	    }
	    MaxWordsInCache = atoi(optarg);
	    break;
	case 'Z':
	case 'z':
	    break; /* work done in SetDefault() */
	case 'V':
	    fprintf(stderr, "%s: Release: %s\n", progname, LQTEXTREVISION);
	    fprintf(stderr, "%s: Revision: %s\n", progname, Version);
	    DoNothing = 1;
	    break;
	case 'f':
	    if (InputFile) {
		Error(E_USAGE|E_XHINT|E_FATAL,
		    "only one -f option allowed; use -xv for explanation"
		);
	    }
	    InputFile = optarg;
	    break;
	case 'x':
	    ErrorFlag = (-1);
	    break;
	default:
	case '?':
	    ErrorFlag = 1;
	}
    }

    if ((progname = strrchr(progname, '/')) != (char *) NULL) {
	++progname; /* step over the last / */
    } else {
	progname = argv[0];
    }

    if (ErrorFlag > 0) {
	fprintf(stderr, "use %s -x or %s -xv for an explanation.\n",
							progname, progname);
	exit(1);
    } else if (ErrorFlag < 0) { /* -x was used */
	fprintf(stderr, "%s -- add files to an lq-text retrieval database\n",
								    progname);

	fputs("Options are:\n\
	-f file -- read the list of files to index from \"file\"\n\
	-M n	-- try not to flush cache entries with n or more entries\n\
	-w n	-- dump the word-cache every n words\n\
\n\
", stderr);

	LQT_PrintDefaultUsage(Options);

	if (LQT_TraceFlagsSet(LQTRACE_VERBOSE)) {
	    /* used -v or -t1 */
	    fprintf(stderr,
		"\n\
    Any remaining arguments are taken to be file names.  The current\n\
DOCPATH (%s) is searched for the files,\n\
and they are read and added to the index.\n\
If you use the -f option, you should not give filename\n\
arguments on the command line, although you can use \"-f -\" to read the\n\
list of files from standard input, one per line.\n\
Setting (with -w) the size of the cache may dramatically\n\
improve performance.  Systems with memory larger than the data can try -w0.\n\
See %s(1) for more information.\n",
		Options->filesearchpath.Value,
		progname
	    );
	}
	exit(0);
    }

#ifdef WIDINBLOCK
# ifdef ASCIITRACE
    /* remind people to recompile... */
    Error(E_WARN, "**** Compiled with -DWIDINBLOCK for debugging ****");
    Error(E_WARN, "**** Compiled with -DWIDINBLOCK for debugging ****");
    Error(E_WARN, "**** Compiled with -DWIDINBLOCK for debugging ****");
    /* remind them a lot... */
    Error(E_WARN, "**** Compiled with -DWIDINBLOCK for debugging ****");
    Error(E_WARN, "**** Compiled with -DWIDINBLOCK for debugging ****");
# else
    /* don't allow -DWIDINBLOCK without -DASCIITRACE */
    Error(E_BUG, "Compiled with -DWIDINBLOCK but not -DASCIITRACE!");
    syntax error; this prevents compilation here;
# endif /* ASCIITRACE */
#endif

    if (DoNothing) {
	if (optind < argc) {
	    Error(E_WARN|E_XHINT,
		"%d extra argument%s ignored...",
		argc - optind,
		argc - optind == 1 ? "" : "%s"
	    );
	}
	exit(0);
    }

    if (!(db = LQT_OpenDatabase(Options, O_RDWR|O_CREAT, 0664))) {
	Error(E_FATAL, "couldn't write to database.");
    }
    LQT_ObtainWriteAccess(db);
    LQT_InitFilterTable(db);
    lqSetSignals(SignalHandler);

    if (InputFile) {
	if (optind < argc) {
	    Error(E_FATAL|E_USAGE|E_XHINT,
		"cannot give filenames after -f %s",
		InputFile
	    );
	}
	AddFrom(InputFile);
    } else for (; optind < argc; ++optind) {
	if (
	    AddFile(argv[optind]) < 0 &&
	    LQT_TraceFlagsSet(LQTRACE_VERBOSE|LQTRACE_DEBUG)
	) {
	    if (SignalFlag) {
		Error(E_WARN,
		    "Caught signal at level %d, dumping cache",
		    SignalFlag
		);
		DumpCache(db, DUMP_SYNC);
		LQT_CloseDatabase(db);
		exit(1);
	    } else {
		Error(E_WARN, "%s not added to index", argv[optind]);
	    }
	}

    }

#ifndef MALLOCTRACE
    /* don't bother recaiming storage if we're about to exit, unless we
     * want to check for memory leaks afterwards.
     */
    DumpCache(db, DUMP_SYNC|DUMP_NOFREE);
#else
    DumpCache(db, DUMP_SYNC);
#endif

    LQT_CloseDatabase(db);

#ifdef MALLOCTRACE
    (void) fprintf(stderr, "%s: Malloctrace: checking...\n", progname);
    malloc_verify();
    (void) fprintf(stderr, "%s: Malloc Map\n", progname);
    mallocmap();
#endif

#ifdef WIDINBLOCK
# ifdef ASCIITRACE
    /* remind people again to recompile... */
    Error(E_WARN, "Reminder: Compiled with -DWIDINBLOCK for debugging ****");
    Error(E_WARN, "Reminder: Compiled with -DWIDINBLOCK for debugging ****");
    Error(E_WARN, "Reminder: Compiled with -DWIDINBLOCK for debugging ****");
# else
    /* don't allow -DWIDINBLOCK without -DASCIITRACE */
    Error(E_BUG, "Compiled with -DWIDINBLOCK but not -DASCIITRACE!");
    syntax error; this prevents compilation here;
# endif /* ASCIITRACE */
#endif

    return 0;
}

static void
AddFrom(Name)
    char *Name;
{
    FILE *fp;
    char *Line;

    if (Name[0] == '-' && Name[1] == '\0') {
	fp = stdin;
    } else {
	fp = LQU_fEopen(E_FATAL, Name, "list of files to add", "r");
    }

    while (LQU_fReadLine(fp, &Line, LQUF_NORMAL) > 0) {
	/* Note:
	 * LQU_fReadFile will silently swallow blank lines.
	 * if we use LQUF_NORMAL it will swallow lines that start with a #,
	 * but we don't want that here!
	 */
	if (AddFile(Line) < 0) {
	    if (SignalFlag) {
		Error(E_WARN,
		    "Caught signal at level %d -- dumping cache",
		    SignalFlag
		);
		DumpCache(db, DUMP_SYNC);
		LQT_CloseDatabase(db);
		exit(1);
	    } else {
		if (LQT_TraceFlagsSet(LQTRACE_VERBOSE|LQTRACE_DEBUG)) {
		    /* AddFile should already have printed a message... */
		    Error(E_WARN, "-f %s: \"%s\" not added to index",
			Name,
			Line
		    );
		}
	    }
	}
    }

    if (fp != stdin) {
	(void) fclose(fp);
    }
}

PRIVATE int
AddFile(Name)
    char *Name;
{
    t_FileInfo *theFileInfo;

    if (!Name || !*Name) {
	return -1;
    }

    if ((theFileInfo = LQT_MakeFileInfo(db, Name)) == (t_FileInfo *) 0) {
	return -1;
    }

    AddStream(db, theFileInfo);
    LQT_SaveFileInfo(db, theFileInfo);
    LQT_DestroyFileInfo(db, theFileInfo);

    if (SignalFlag) {
	return -1;
    }

    return 0;
}

PRIVATE void
AddStream(db, FileInfo)
    t_LQTEXT_Database *db;
    t_FileInfo *FileInfo;
{
    /* I have to mark the last word in the block.
     * I do that by marking the previous word if it was in a different block
     * than the current one.
     */
    char *Line;
    long lineNumber;
    int Len;

    lineNumber = 0;

    /* Read lines one at a time into Line and then read the words from
     * those lines.
     * Note: LQU_fReadFile() uses a private (but growable) buffer.
     */
    while ((Len = LQU_fReadLine(FileInfo->Stream, &Line, 0)) != -1) {
	char *Start;
	t_WordInfo *WordInfo;
	t_WordInfo *LastWord = 0;

	++lineNumber;

	if (!Line || !*Line || Len < 0) {
	    continue;
	}
    
	if (db->FileBlockSize <= Len) {
	    db->FileBlockSize = Len + 1;
	}
	/* reset the word-reading routine */
	(void) LQT_ReadWordFromStringPointer(
	    db,
	    (char **) NULL,
	    (char **) NULL,
	    (char *) NULL,
	    0
	);

	/* add the words in this line, one at a time.
	 * We are always one word behind, because when ReadWord
	 * finds punctuation after a word, it sets the flag in the
	 * previous word's WordPlace... so we have to leave it in place
	 * to get set!
	 */

	Start = Line;
	LastWord = (t_WordInfo *) 0;

	while (SignalFlag <= 1) {
	    /* needs more than one signal to quit in the middle of a file */

	    WordInfo = LQT_ReadWordFromStringPointer(
		db,
		&Start,
		(char **) NULL,
		&Line[Len],
		LQT_READWORD_IGNORE_COMMON
	    );

	    if (WordInfo == (t_WordInfo *) NULL) {
		break;
	    } else {
		WordInfo->WordPlace.BlockInFile = lineNumber;
		WordInfo->WordPlace.FID = FileInfo->FID;

		if (LastWord) {
		    AddWord(db, LastWord);
		}

		LastWord = WordInfo;
	    }
	}

	if (LastWord) {
	    /* ensure that the WPF_LASTINBLOCK flag is not set */
	    LastWord->WordPlace.Flags &= ~WPF_LASTINBLOCK;
	    LastWord->WordPlace.BlockInFile = lineNumber;
	    LastWord->WordPlace.FID = FileInfo->FID;
	    AddWord(db, LastWord);
	    LastWord = (t_WordInfo *) 0;
	}

	if (SignalFlag > 1) {
	    break;
	}
    }

    if (SignalFlag > 1) {
	Error(E_WARN, "Signal received during processing of %s",
		FileInfo->Name);
	Error(E_WARN, "That and other files may be incomplete...");
	return;
    }


    if (SignalFlag <= 1 && LQT_TraceFlagsSet(LQTRACE_VERBOSE|LQTRACE_DEBUG)) {
	LQT_Trace(LQTRACE_VERBOSE|LQTRACE_DEBUG,
	    "%d: %s: indexed.\n",
	    FileInfo->FID,
	    FileInfo->Name
	);
    }
}
@


1.7
log
@beta 6.
@
text
@d11 1
a11 1
 * $Id: addbyline.c,v 1.6 96/07/04 21:43:34 lee Exp $ 
d15 1
a15 1
    "@@(#) $Id: addbyline.c,v 1.6 96/07/04 21:43:34 lee Exp $";
d109 1
a109 1
char *progname = "@@(#) $Id: addbyline.c,v 1.6 96/07/04 21:43:34 lee Exp $";
d433 1
a433 1
     * I do that by marking the previous word if it was in a differant block
@


1.6
log
@gcc's warnings...
@
text
@d11 1
a11 1
 * $Id: addbyline.c,v 1.5 96/06/28 22:40:47 lee Exp $ 
d15 1
a15 1
    "@@(#) $Id: addbyline.c,v 1.5 96/06/28 22:40:47 lee Exp $";
d109 1
a109 1
char *progname = "@@(#) $Id: addbyline.c,v 1.5 96/06/28 22:40:47 lee Exp $";
d438 1
d446 1
a446 1
    while (LQU_fReadLine(FileInfo->Stream, &Line, 0) > 0) {
d453 1
a453 1
	if (!Line || !*Line) {
d457 3
d465 1
d486 1
@


1.5
log
@oops, don't free line....
@
text
@d11 1
a11 1
 * $Id: addbyline.c,v 1.4 96/06/20 14:22:08 lee Exp $ 
d15 1
a15 1
    "@@(#) $Id: addbyline.c,v 1.4 96/06/20 14:22:08 lee Exp $";
d93 1
a93 1
int AddFile(
d109 1
a109 1
char *progname = "@@(#) $Id: addbyline.c,v 1.4 96/06/20 14:22:08 lee Exp $";
d116 1
a116 1
t_LQTEXT_Database *db;
@


1.4
log
@put filter.h in the right place.
Need to initialise the fiter table before indexing files (oops, blush).
@
text
@d11 1
a11 1
 * $Id: addbyline.c,v 1.3 1996/05/17 22:35:55 lee Exp $ 
d15 1
a15 1
    "@@(#) $Id: addbyline.c,v 1.3 1996/05/17 22:35:55 lee Exp $";
d88 1
d101 1
d109 1
a109 1
char *progname = "@@(#) $Id: addbyline.c,v 1.3 1996/05/17 22:35:55 lee Exp $";
d173 1
a173 1
	    SetDumpThresh(atoi(optarg));
d182 1
a182 1
	    SetHashSize(atoi(optarg));
a394 1
	(void) efree(Line);
@


1.3
log
@fixed phrase matching -- more complex code, though.
@
text
@d11 1
a11 1
 * $Id: addbyline.c,v 1.2 1996/05/17 02:50:17 lee Exp $ 
d15 1
a15 1
    "@@(#) $Id: addbyline.c,v 1.2 1996/05/17 02:50:17 lee Exp $";
a48 1
#include "filter.h"
d55 1
d107 1
a107 1
char *progname = "@@(#) $Id: addbyline.c,v 1.2 1996/05/17 02:50:17 lee Exp $";
d290 1
@


1.2
log
@API updates.
@
text
@d11 1
a11 1
 * $Id: addbyline.c,v 1.1 1996/05/15 23:18:25 lee Exp $ 
d15 1
a15 1
    "@@(#) $Id: addbyline.c,v 1.1 1996/05/15 23:18:25 lee Exp $";
d107 1
a107 1
char *progname = "@@(#) $Id: addbyline.c,v 1.1 1996/05/15 23:18:25 lee Exp $";
a433 1
    t_WordInfo *WordInfo;
d438 5
d445 2
d449 2
a450 1
	if (!Line) {
a451 3
	} else if (!*Line) {
	    efree(Line);
	    continue;
d462 6
a467 1
	/* add the words in this line, one at a time. */
d470 1
d472 3
a474 1
	for (;;) {
d484 9
d494 1
d496 7
a502 3
	    WordInfo->WordPlace.BlockInFile = lineNumber;
	    WordInfo->WordPlace.FID = FileInfo->FID;
	    AddWord(db, WordInfo);
a503 3

	/* done */
	efree(Line);
@


1.1
log
@Initial revision
@
text
@d1 1
a1 1
/* addbyline.c -- Copyright 1995 Liam R. Quin.
d11 1
a11 1
 * $Id: lqaddfile.c,v 1.33 95/06/03 22:34:55 lee Exp Locker: lee $ 
d15 1
a15 1
    "@@(#) $Id: lqaddfile.c,v 1.33 95/06/03 22:34:55 lee Exp Locker: lee $";
d24 8
a31 1
#include <sys/stat.h>
d37 1
d42 4
d53 1
d60 1
d74 1
d81 1
d98 5
d104 1
d107 1
a107 1
char *progname = "@@(#) $Id: lqaddfile.c,v 1.33 95/06/03 22:34:55 lee Exp Locker: lee $";
d114 1
d116 1
a116 1
int
d121 1
a121 1
	LQT_CloseDatabase((t_LQTEXT_Database *) 0);
a129 8
extern int SetHashSize(
#ifdef HAVE_PROTO
    int theNewSize
#endif
);

t_LQTEXT_Database *db;

d237 1
a237 1
	LQT_PrintDefaultUsage();
d252 1
a252 1
		DocPath,
d286 1
a286 1
    if (!(db = LQT_OpenDatabase(Options)) || LQT_ObtainWriteAccess() < 0) {
d289 1
d310 2
a311 2
		DumpCache(DUMP_SYNC);
		LQT_CloseDatabase((t_LQTEXT_Database *) 0);
d324 1
a324 1
    DumpCache(DUMP_SYNC|DUMP_NOFREE);
d326 1
a326 1
    DumpCache(DUMP_SYNC);
d329 1
a329 1
    LQT_CloseDatabase((t_LQTEXT_Database *) 0);
d379 2
a380 2
		DumpCache(DUMP_SYNC);
		LQT_CloseDatabase((t_LQTEXT_Database *) 0);
d410 1
a410 1
    if ((theFileInfo = LQT_MakeFileInfo(Name)) == (t_FileInfo *) 0) {
d476 1
a476 1
	    AddWord(WordInfo);
@
