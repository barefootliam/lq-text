#! /bin/sh

# $Id: lqclean.sh,v 1.7 2019/04/21 06:08:14 lee Exp $

CMDNAME="`basename $0`"

if test x"$1" = x"-d"
then
    case "$2" in
    -*|'')
	echo "usage: $CMDNAME [-d directory] [-i|-f]" 1>&2
	exit 1;
	;;
    *)
	LQTEXTDIR="$2"
	;;
    esac
    shift # the -d
    shift # the directory
else
    if test -d ./lQTEXTDIR/.
    then
	LQTEXTDIR="`pwd`/LQTEXTDIR"
    else
	LQTEXTDIR=${LQTEXTDIR-"$HOME/LQTEXTDIR"}
    fi
fi

cd "$LQTEXTDIR" || exit 1

FILES="FIDFile fidfile FileList* filelist* WIDIndex widindex chainend data freelist titles wordlist* widfile.dbg lastblks"

if test ! -f config.txt -a ! -f README -a ! -f readme
then
    echo "$CMDNAME: $LQTEXTDIR is not an lq-text directory." 1>&2
    exit 1
fi

OK=no
for i in $FILES
do
    if test -f "$i"
    then
	OK=yes
	break;
    fi
done

if test $OK = no
then
    echo "$CMDNAME: $LQTEXTDIR is not in use by lq-text." 1>&2
    exit 0
fi

case "$1" in
-i) /bin/rm -i $FILES
    exit $?
    ;;
-f) /bin/rm -f $FILES
    exit $?
    ;;
*)
    N=; C=;
    if test "hello" = "`echo -n hello`"; then N='-n'; else C='\c'; fi
    echo "$CMDNAME: database $LQTEXTDIR"
    echo "$CMDNAME: currently using `du | awk '{print $1}'` blocks"
    echo $N "$CMDNAME: remove database in `pwd`? $C"
    read ans
    case "$ans" in
    [yY]*)
	/bin/rm -f $FILES
	echo "$CMDNAME: database removed."
	exit 0
	;;
    *)
	echo "$CMDNAME: database retained."
	exit 1
	;;
    esac
esac

# /*NOTREACHED*/
exit 1
