/* lqcat.c -- Copyright 2000 Liam R. E. Quin.
 * All Rights Reserved.
 * This code is NOT in the public domain.
 * See the file COPYRIGHT for full details.
 *
 * Print the contents of a file based on its FID,
 * including a DOCPATH search.
 */

static char *Revision = "@(#) $Id: lqcat.c,v 1.1 2019/04/21 06:08:14 lee Exp $";

#include "globals.h" /* defines and declarations for database filenames */
#include "error.h"
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>

#ifdef HAVE_FCNTL_H
# include <fcntl.h> /* for O_RDONLY etc */
#endif

#ifdef HAVE_STDLIB_H
# include <stdlib.h>
#endif

#ifdef HAVE_STDLIB_H
# include <stdlib.h>
#else
# include <malloc.h>
#endif

#include "emalloc.h"
#include "fileinfo.h"
#include "lqutil.h"
#include "liblqtext.h"

char *progname;

/** System calls and library functions used in this file: **/

/** Unix System calls: **/
/** System Library Functions: **/

/** Functions defined within this file: **/

PRIVATE void PrintInfo(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *db,
    char *Name
#endif
);

PRIVATE void PrintFIDInfo(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *db,
    t_FID FID
#endif
);

PRIVATE void DisplayFile(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *db,
    t_FileInfo *FileInfo
#endif
);

int
main(argc, argv)
    int argc;
    char *argv[];
{
    extern int optind, getopt();
    /** extern char *optarg; (unused at the moment) **/
    int ch;
    int ErrorFlag = 0;
    int FIDMode = 0;
    t_lqdbOptions *Options;
    t_LQTEXT_Database *db;

    progname = argv[0];

    Options = LQT_InitFromArgv(argc, argv);

    /* All programs take Zz:Vv */
    while ((ch = getopt(argc, argv, "Zz:VvAaFlMx")) != EOF) {
	switch (ch) {
	case 'z':
	case 'Z':
	    break; /* done by LQT_InitFromArgv(); */
	case 'V':
	    fprintf(stderr, "%s version %s\n", progname, Revision);
	    break;
	case 'F':
	    FIDMode = 1;
	    break;
	case 'x':
	    ErrorFlag = (-1);
	    break;
	case '?':
	    ErrorFlag = 1;
	    break;
	}
    }

    if (ErrorFlag) {
	fprintf(stderr, "%s: usage: %s [options] [files]\n",progname,progname);
	fprintf(stderr, "%s: options are:\n", progname);
	LQT_PrintDefaultUsage(Options);
	fputs("\
	    -F      -- treat file names as numeric FIDs instead of paths\n\
	    -t N	-- set trace level to N [default: 0]\n\
	    -x	-- print this explanation\n",
	    stderr
	);
	exit((ErrorFlag > 0) ? 1 : 0);
    }

    db = LQT_OpenDatabase(Options, O_RDONLY, 0);
    if (!db || LQT_ObtainReadOnlyAccess(db) < 0) {
	Error(E_FATAL,
	    "couldn't open lq-text database in directory \"%s\"",
	    db->DatabaseDirectory
	);
    }

    if (optind >= argc) {
	exit(0); /* files are optional */
    }

    while (optind < argc) {
	if (FIDMode) {
	    t_FID FID;

	    if (!LQU_cknatstr(argv[optind]) ||
			    ((FID = atol(argv[optind])) == (t_FID) 0)) {
		Error(E_WARN,
		    "Invalid FID %s; FIDs must be numeric, > 0L",
		    argv[optind]
		);
	    } else {
		PrintFIDInfo(db, FID);
	    }
	} else {
	    PrintInfo(db, argv[optind]);
	}
	++optind;
    }
    exit(0);
}

PRIVATE void
PrintInfo(db, Name)
    t_LQTEXT_Database *db;
    char *Name;
{
    t_FID FID;

    if ((FID = LQT_NameToFID(db, Name)) == (t_FID) 0) {
	Error(E_WARN,
	    "No FID available for filename: %s",
	    Name
	);
    } else {
	PrintFIDInfo(db, FID);
    }
}

PRIVATE void
PrintFIDInfo(db, FID)
    t_LQTEXT_Database *db;
    t_FID FID;
{
    t_FileInfo *FileInfo;

    /* get info from the list */
    if ((FileInfo = LQT_FIDToFileInfo(db, FID)) == (t_FileInfo *) 0) {
	Error(E_WARN,
	    "No Index information for FID: %ld",
	    FID
	);
    } else {
	DisplayFile(db, FileInfo);
    }
}

PRIVATE void
DisplayFile(db, theFileInfo)
    t_LQTEXT_Database *db;
    t_FileInfo *theFileInfo;
{
    int fd;
    int c;
    FILE *f;

    char *filename = LQT_FindFile(db, theFileInfo->Name);

    if (!filename) {
	filename = theFileInfo->Name;
    }

    if ((fd = LQT_UnpackAndOpen(db, filename)) < 0) {
	Error(E_WARN|E_SYS,
	    "Could not open file %ld, \"%s\"",
	    theFileInfo->FID,
	    filename
	);
	return;
    }

    f = fdopen(fd, "r");
    if (!f) {
	Error(E_WARN|E_SYS|E_INTERNAL,
	    "freopen(%d) failed for \"%s\"",
	    fd, filename
	);
	return;
    }

    while ((c = getc(f)) != EOF) {
	putchar(c);
    }

    fclose(f);
}
