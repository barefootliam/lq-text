:
# intersect word-one word-two
#
# intersect -- Copyright 1990 Liam R. Quin.  All Rights Reserved.
# This code is NOT in the public domain.
# See the file ../COPYRIGHT for full details.
#
# $Id: intersect.sh,v 1.5 2019/04/21 06:08:14 lee Exp $
#


FileNumber=0
FileList=
Program=lqphrase
ProgOpts=
All=/tmp/iAll$$
export All

trap '/bin/rm -f $All $tmp $First $FileList; exit' 0 1 2 3 15

if [ x"$1" = x"" ]
then
    echo "$0: Usage: `basename $0` {-w word} | {-p phrase} ..." 1>&2
    exit 1
fi


for i
do
    if [ x"$i" = x"-p" ]
    then
	Program=lqphrase
	ProgOpts=
    elif [ x"$i" = x"-w" ]
    then
	Program=lqword
	ProgOpts=-l
    else
	tmp=/tmp/inter.$FileNumber
	$Program $ProgOpts "$i" | tee -a $All | awk '{ print $3 }' | sort -u > $tmp
	if [ x"$First" = x"" ]
	then
	    First="$tmp"
	else
	    FileList="$FileList $tmp"
	fi
	FileNumber=`expr $FileNumber + 1`
    fi
done

# Find matches...
tmp=/tmp/inter.tmp$$

for i in $FileList
do
    fgrep -x -f $First $i | sort -u > $tmp
    mv $tmp $First
done

mv $First $tmp
sed 's/^/ /' $tmp > $First

fgrep -f $First $All
exit 0

#
#
# $Log: intersect.sh,v $
# Revision 1.5  2019/04/21 06:08:14  lee
# for migration to CVS
#
# Revision 1.4  91/08/08  21:20:51  lee
# fixed typo (ALL vs. All).
# 
# Revision 1.3  91/03/03  00:18:59  lee
# brought up to date a little...
# 
# Revision 1.2  90/10/06  00:50:52  lee
# Prepared for first beta release.
# 
# Revision 1.1  90/08/29  21:45:01  lee
# Initial revision
# 
#
#
