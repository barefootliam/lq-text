/** sgml stuff */

typedef struct s_Entity {
    char *Name;
    char *Value;
    struct s_Entity *Next;
} t_Entity;

static t_Entity *EntityList = NULL;

static void
InsertEntity(e)
    t_Entity *e;
{
    t_Entity **Epp = &EntityList;

    /* Locate: */
    for (Epp = &EntityList; *Epp; Epp = &(*Epp)->Next) {
	if (strcmp(e->Name, (*Epp)->Name) >= 0) {
	    break;
	}
    }

    /* Insert: */
    e->Next = (*Epp);
    *Epp = e;
    LQT_Trace(LQTRACE_DEBUG,
	"Insert Entity &%s; as \"%s\"\n", e->Name, e->Value
    );
}

PRIVATE char *
GetNextString(Source, pp)
    char *Source; /* e.g. the file name */
    char **pp;
{
    char quotes = 0;
    char *Start;
    char *String;
    char *result;

    if (!pp || !*pp || !**pp) return 0;

    String = (*pp); /* for error reporting */

    while (isspace(**pp)) ++*pp;

    switch (**pp) {
    case '\0':
	return (char *) NULL;
    case '"': case '\'':
	quotes = (**pp);
	++*pp;
	break;
    case '`':
	quotes = '\''; /* `...' */
	++*pp;
	break;
    default:
	break;
    }

    Start = (*pp);

    /* find the end... */
    while (**pp) {
	if ((quotes && **pp == quotes) || (!quotes && isspace(**pp))) {
	    break;
	}
	++*pp;
    }

    if (quotes) {
	if (**pp != quotes) {
	    Error(E_FATAL,
		"%s: unmatched quote in %s, expected %c",
		Source,
		String,
		quotes
	    );
	}

    }
    result = emalloc("lqkwic::GetNextString.result", *pp - Start + 2);
    (void) strncpy(result, Start, *pp - Start + 1);
    result[*pp - Start] = '\0';


    if (**pp) {
	++*pp;
    }

    return result;
}

PRIVATE char *
StartsEntity(String)
    char *String;
{
    register char *p;

    if (*String != '&') {
	return 0;
    }

    ++String; /* skip over the initial & */

    for (p = String; *p; p++) {
	if (isspace(*p)) return 0;
	if (*p == ';') {
	    char *Name = emalloc(
		"lqkwic:SGML Entity Name",
		(unsigned int)(p - String + 1)
	    );
	    (void) strncpy(Name, String, p - String);
	    Name[p - String] = '\0';
	    return Name;
	}
    }
    return 0;
}

static char *
EndsEntity(String, Buffer)
    char *String;
    char *Buffer;
{
    register char *p;

    if (*String != ';') {
	return 0;
    }

    for (p = String; p > Buffer; --p) {
	if (isspace(*p)) return 0;
	if (*p == '&') {
	    char *Name;
	    ++p; /* skip over the & */
	    Name = emalloc("lqkwic:EndsEntity.Name", String - p + 1);
	    (void) strncpy(Name, p, String - p);
	    Name[String - p] = '\0';
	    return Name;
	}
    }
    return 0;
}

static t_Entity *
NewEntity(String, FileName)
    char *String;
    char *FileName;
{
    t_Entity *e;

    e = (t_Entity *) emalloc("lqkwic:NewEntity.Name", sizeof (t_Entity));
    e->Next = 0;
    e->Name = GetNextString(FileName, &String);
    e->Value = GetNextString(FileName, &String);
    return e;
}

PRIVATE void
ReadEntityFile(FileName)
    char *FileName;
{
    char **Lines = 0;
    int count;
    int i;
    
    count = LQU_ReadFile(
	E_FATAL,
	FileName,
	"SGML Entity Replacement File",
	&Lines, /* yes, a (char ***) */
	UF_IGNBLANKS|UF_IGNSPACES|UF_IGNHASH|UF_ESCAPEOK
    );

    for (i = 0; i < count; i++) {
	t_Entity *oneEntity = NewEntity(Lines[i], FileName);

	if (oneEntity) {
	    InsertEntity(oneEntity);
	}
	(void) efree(Lines[i]);
    }

    (void) efree((char *)Lines);
}

PRIVATE char *
EntityValue(Name)
    char *Name;
{
    register t_Entity *Ep;
    int i = 1; /* i.e. non-zero in case loop is never reached */

    for (Ep = EntityList; Ep; Ep = Ep->Next) {
	if ((i = strcmp(Name, Ep->Name)) >= 0) {
	    break;
	}
    }

    if (i == 0 && Ep) {
	LQT_Trace(LQTRACE_DEBUG,
	    "SGML Entity &%s; has value \"%s\"\n", Name, Ep->Value
	);
	return Ep->Value;
    } else {
	LQT_Trace(LQTRACE_DEBUG,
	    "Entity &%s; has no declared value\n", Name
	);
	return 0;
    }
}

/* end sgml stuff */
