/* lqrename.c -- Copyright 1992, 1994 Liam R. Quin.  All Rights Reserved.
 * This code is NOT in the public domain.
 * See the file COPYRIGHT for full details.
 */

/* Tell the lq-text database that files have moved.
 *
 * $Id: lqrename.c,v 1.5 2019/04/21 06:08:14 lee Exp $
 */

#include "globals.h" /* defines and declarations for database filenames */

#include "error.h"

#include <stdio.h>
#include <sys/types.h>

#ifdef HAVE_STDLIB_H
# include <stdlib.h>
#else
# include <malloc.h>
#endif

#ifdef HAVE_UNISTD_H
# include <unistd.h>
#endif

#ifndef O_RDWR
# ifdef HAVE_FCNTL_H
#  ifdef HAVE_SYS_V_FCNTL_H
#   include <sys/stat.h>
#  endif
#  include <fcntl.h>
# endif
#endif

#include "emalloc.h"
#include "fileinfo.h"
#include "lqutil.h"
#include "liblqtext.h"
#include "lqtrace.h"

#define FIRST_ARCHVE_CHAR '('
#define LAST_ARCHIVE_CHAR ')'

static char *Revision = "$Id: lqrename.c,v 1.5 2019/04/21 06:08:14 lee Exp $";

/* The position of the \n in the 26-char string returned by ctime(3): */
#define DATENEWLINE 24

char *progname;

/** System calls and library functions used in this file: **/
/** Unix System calls: **/
/** System Library Functions: **/

/** external lqtext functions: **/
/** Functions defined within this file: **/
PRIVATE void Rename(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *,
    char *OldName,
    char *NewName
#endif
);

int
main(argc, argv)
    int argc;
    char *argv[];
{
    extern int optind, getopt();
    extern char *optarg;
    int ch;
    int ErrorFlag = 0;
    char *FileNameContainingChanges = 0;
    t_LQTEXT_Database *db;
    t_lqdbOptions *Options;

    progname = argv[0];

    Options = LQT_InitFromArgv(argc, argv);

    /* All programs take Zz:Vv */
    while ((ch = getopt(argc, argv, "Zz:VvAax")) != EOF) {
	switch (ch) {
	case 'z':
	case 'Z':
	    break; /* done by LQT_InitFromArgv(); */
	case 'V':
	    fprintf(stderr, "%s version %s\n", progname, Revision);
	    break;
	case 'x':
	    ErrorFlag = (-1);
	    break;
	case 'f':
	    if (FileNameContainingChanges) {
		fprintf(stderr, "%s: you must only use -f once; see -xv\n",
			progname);
		exit(1);
	    }
	    FileNameContainingChanges = optarg;
	case '?':
	    ErrorFlag = 1;
	    break;
	}
    }

    /* Normally put call to lrqError here to give a helpful message,
     * but not yet ready to ship the error handling package, sorry
     */
    if (ErrorFlag) {
	fprintf(stderr, "%s: usage: %s [options] [files]\n",progname,progname);
	fprintf(stderr, "%s: options are:\n", progname);
	fprintf(stderr, "\
	-f file -- file contains old-file new-file pairs\n\
	-V      -- print version information\n\
	-x      -- print usage explanation\n\
	-xv      -- print longer usage explanation\n\
\n\
Remaining options are paired: oldfilename newfilename\n"
	);
	if (LQT_TraceFlagsSet(LQTRACE_VERBOSE|LQTRACE_DEBUG)) {
	    fprintf(stderr, "\
Archives are described as ArchiveName%cMember%c\n\
Files ending in .Z are assumed to be compressed.\n",
		FIRST_ARCHVE_CHAR, 
		LAST_ARCHIVE_CHAR ? LAST_ARCHIVE_CHAR : ' '
	    );
	}
	LQT_PrintDefaultUsage(Options);
	exit((ErrorFlag > 0) ? 1 : 0);
    }

    if (optind >= argc && !FileNameContainingChanges) {
	fprintf(stderr,
	"%s: You must either give the -f option or specify file name pairs.\n",
		progname);
	fprintf(stderr, "\tuse %s -xv for further explanation.\n", progname);
	exit(1);
    } else if ((argc - optind) & 01) {
	fprintf(stderr, "%s: you must rename files in pairs.\n", progname);
	exit(1);
    }

    db = LQT_OpenDatabase(Options, O_RDWR, 0660);
    if (!db || LQT_ObtainWriteAccess(db) < 0) {
	Error(E_FATAL, "couldn't open database");
    }

    /* Rename files */
    while (optind < argc) {
	Rename(db, argv[optind], argv[optind + 1]);
	optind += 2;
    }

    if (FileNameContainingChanges) {
	int lineLength;
	char *Line;
	long lineNumber = 0;
	int thereWereErrors = 0;
	FILE *f = LQU_fEopen(E_FATAL,
	    FileNameContainingChanges,
	    "list of old-name <tab> new-name pairs",
	    "r"
	);

	while ((lineLength = LQU_fReadLine(f, &Line, LQUF_NORMAL)) >= 0) {
	    ++lineNumber;
	    if (lineLength > 0) {
		char *old = 0;
		register char *new;

		for (new = Line; *new; new++) {
		    if (*new == '\t') {
			*new = '\0';
			old = Line;
		    }
		}
		if (old) {
		    Rename(db, old, new);
		} else {
		    Error(E_WARN,
			"%s: %d: line does not contain a tab character",
			FileNameContainingChanges,
			lineNumber
		    );
		    thereWereErrors = 1;
		}
	    }
	}

	if (thereWereErrors) {
	    Error(E_WARN|E_MULTILINE,
		"The input file must contain two filenames on each line,"
	    );
	    Error(E_WARN|E_MULTILINE,
		"with a single tab character between them."
	    );
	    Error(E_WARN|E_MULTILINE,
		"The first must name an existing document in the index;"
	    );
	    Error(E_WARN|E_MULTILINE|E_LASTLINE,
		"The second is the new filename it's stored under."
	    );
	}

	LQU_fEclose(E_WARN, f,
	    FileNameContainingChanges, 
	    "list of old-name <tab> new-name pairs"
	);
    }

    LQT_CloseDatabase(db);
    exit(0);
    /*NOTREACHED*/
    return 1; /* for lint and gcc... */
}

PRIVATE void
Rename(db, OldName, NewName)
    t_LQTEXT_Database *db;
    char *OldName;
    char *NewName;
{
    long FID;

    if ((FID = LQT_NameToFID(db, OldName)) == (t_FID) 0) {
	Error(E_WARN,
	    "%s: not found in database",
	    OldName
	);
	return;
    }

    LQT_RenameFileInIndex(db, OldName, NewName);
}

