/* wordinfo.h -- Copyright 1989, 1994, 1996 Liam R. Quin.
 * All Rights Reserved.
 * This code is NOT in the public domain.
 * See the file COPYRIGHT for full details.
 *
 * $Id: wordinfo.h,v 1.10 96/06/11 13:43:01 lee Exp $
 */

#ifndef LQ_WORDINFO_H
# define LQ_WORDINFO_H 1

#ifndef LQ_WORDPLACE_H
# include "wordplace.h"
#endif

#ifndef LQ_WORDRULES_H
# include "wordrules.h"
#endif


#ifndef WIDBLOCKSIZE
you must include globals.h before this file;
#endif

typedef unsigned long t_WID;

/* this is a hack for speed: */
#define LQT_GetMaxOrAllocateWID LQTp_GetMaxOrAllocateWID

/** A t_WordInfo describes a single word, in terms of
 ** where it came from
 ** how to find its database entries
 ** how to find the in-core database entries (a copy of the above)
 **/

typedef struct s_WordInfo {
    char *Word;
    t_WID WID; /* My Word Identifier */
    unsigned long NumberOfWordPlaces; /* total */
    t_FID FID; /* where we got it from */
    unsigned long Offset; /* word entry position in the data base */
    unsigned char *DataBlock; /* for writing me out to the index */
    unsigned char *WordPlaceStart;
    t_WordPlace *WordPlaces; /* first few pairs */
    t_WordPlace WordPlace; /* For addfile -- this is due to go!!!! */
    unsigned long WordPlacesInHere;
    /* shorts are at the end to minimise alignment padding... */
    t_WordFlags Flags;
    unsigned char Length; /* Word length; reduce the need for strlen */
} t_WordInfo;


#endif /* LQ_WORDINFO_H */
