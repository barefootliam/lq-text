/* filter.h -- Copyright 1989, 1996 Liam R. E. Quin.  All Rights Reserved.
 * This code is NOT in the public domain.
 * See the file COPYRIGHT for full details.
 */

/* filter.h -- define filter table for lq-text Liam Quin's text retrieval
 * program.
 * There should be an external table, but there isn't yet...
 *
 * $Id: filter.h,v 1.19 2019/04/21 06:04:30 lee Exp $
 *
 */

#ifndef LQ_FILTER_H
# define LQ_FILTER_H 1

#ifndef FILE
# include <stdio.h>
#endif

/* File types
 *
 * If you add to these, you must keep
 * LQT_MaxFilterType up to date.
 * Changing the numbers would potentially
 * invalidate existing indexes, so would need a
 * change to the MAJOR version number of the
 * index format... you need a good reason to do this!
 */
#define FTYPE_NEWS  1
#define FTYPE_MAIL  2
#define FTYPE_DEFAULT 3
#define FTYPE_SGML 4
#define FTYPE_TROFF 5
#define FTYPE_XML 6

#define LQT_MaxFilterType(db) FTYPE_XML

/* the actual filters */
LIBRARY int LQF_XML_Copy(
# ifdef HAVE_PROTO
    t_LQTEXT_Database *db,
    FILE *InputFile,
    char *Name,
    FILE *OutputFile
# endif
);

LIBRARY int LQF_SGML_Copy(
# ifdef HAVE_PROTO
    t_LQTEXT_Database *db,
    FILE *InputFile,
    char *Name,
    FILE *OutputFile
# endif
);

LIBRARY int LQF_NetNews_Copy(
# ifdef HAVE_PROTO
    t_LQTEXT_Database *db,
    FILE *InputFile,
    char *Name,
    FILE *OutputFile
# endif
);

LIBRARY int LQF_RFC822_Copy(
# ifdef HAVE_PROTO
    t_LQTEXT_Database *db,
    FILE *InputFile,
    char *Name,
    FILE *OutputFile
# endif
);

LIBRARY int LQF_Troff_Copy(
# ifdef HAVE_PROTO
    t_LQTEXT_Database *db,
    FILE *InputFile,
    char *Name,
    FILE *OutputFile
# endif
);


/* The Type field in each array entry is so that I can do some very simple
 * checking...
 */
typedef struct s_FilterTable {
    int Type;
    char *Name; /* no more than 10 chars if possible, please */
    int (* copyFile)(); /* input filter */
    int (* findMatchEnds)(); /* unused for now */
    int (* findFile)(); /* unused for now */
    int (* closeFile)(); /* how to close the darned stream */
} t_FilterTable;

LIBRARY t_FilterTable *LQTpFilterTable;

#endif /* LQ_FILTER_H */
