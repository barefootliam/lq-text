/* wordplace.h -- Copyright 1989, 1994, 1996 Liam R. Quin.
 * All Rights Reserved.
 * This code is NOT in the public domain.
 * See the file COPYRIGHT for full details.
 *
 * WordPlace -- a structure for a single occurrence of a single word.
 * Used mainly in pblock.c and lqsort.c, and also in lqaddfile, for which
 * it was designed.  The larger version, t_MatchStart, is for I/O of
 * matches, and for a complete representation of a single match.
 *
 * $Id: wordplace.h,v 1.10 1996/06/11 14:14:10 lee Exp $
 *
 */
#ifndef WORDPLACE_H
# define WORDPLACE_H 1

# ifndef LQ_FILEINFO_H
#  include "fileinfo.h"
# endif

# ifndef LQ_WORDRULES_H
# include "wordrules.h"
# endif

typedef struct {
    t_FID FID;
    unsigned long BlockInFile;
    unsigned long WordInBlock;
    t_WordFlags Flags;
    unsigned char StuffBefore; /* preceding ignored garbage */
} t_WordPlace;

typedef struct {
    t_FID FID;
    unsigned long BlockInFile;
    unsigned long WordInBlock;
    long NumberOfWordsMatched;
    char *FileName;
    t_WordFlags Flags;
    unsigned char StuffBefore; /* preceding ignored garbage */
} t_MatchStart;

# ifndef MaxWordPlacesInAWordBlock
#  define MaxWordPlacesInAWordBlock(db) ((WIDBLOCKSIZE-((db)->MinWordLength+2)/3))
# endif

/** The next two structures are currently (May 1996) unused;
 ** they are intended to save memory in a future version of lq-text,
 ** and are here for forwards compatibility.
 **/

/* a smaller wordplace that doesn't include the FID */
typedef struct {
    unsigned long BlockInFile;
    unsigned long WordInBlock;
    t_WordFlags Flags;
    unsigned char StuffBefore;
} t_MatchWithinFile;

/* structure to hold all the wordplaces for a given FID */
typedef struct {
    t_FID FID;
    unsigned long Count;
    t_MatchWithinFile Matches[1];
} t_WordsForAGIvenFID;

#endif
