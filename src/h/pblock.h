/* pblock.h -- Copyright 1989, 1993, 1994, 1996 Liam R. E. Quin.
 * All Rights Reserved.
 * This code is NOT in the public domain.
 * See the file COPYRIGHT for full details.
 */

#ifndef LQ_PBLOCK_H /* the matching endif is at the end of the file... */
# define LQ_PBLOCK_H 1

/* The physical Word Database...
 *
 * First, there is the WID (from 1 to 4 bytes)
 *
 * Then, there is a NEXT pointer (or 0).
 *
 * Then, there is a list of (FID, OFFSET) pairs.
 *
 * $Id: pblock.h,v 1.8 96/05/15 21:56:10 lee Exp $
 *
 */

#ifndef WORDPLACE_H
# include "wordplace.h"
#endif

#define SET_BLOCK_AS_FREE 37 /* unlikely numbers to be passed by mistake */
#define SET_BLOCK_AS_USED 119

/* Warning: One cannot use structure copy for a pblock! */

/* This does *NOT* correspond to the physical disk layout -- see pblock.c */
typedef struct {
    t_WID WID; /* for checking; */
    unsigned long ChainStart;
    unsigned long NumberOfWordPlaces;
    t_WordPlace WordPlaces[1]; /* made by joining lots of disk blocks... */
} t_pblock;

#endif /* LQ_PBLOCK_H */
