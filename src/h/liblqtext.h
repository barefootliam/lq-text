/* liblqtext.h -- Copyright 1994, 1995, 1996 Liam R. E. Quin.
 * All Rights Reserved.
 * This code is NOT in the public domain.
 * See the file COPYRIGHT for full details.
 *
 * Include this header file in any C or C++ file using -llqtext.
 *
 */

/* $Id: liblqtext.h,v 1.18 2019/04/21 06:04:30 lee Exp $
 *
 */

#ifndef LQ_LIBLQTEXT_H

#ifndef API
# include "api.h"
#endif

#ifndef LQ_WORDINFO_H
# include "wordinfo.h"
#endif

#include <sys/types.h>
#include <sys/stat.h>

#ifndef LQ_FILEINFO_H
# include "fileinfo.h"
#endif

#ifndef LQ_PHRASE_H
# include "phrase.h"
#endif

#ifndef LQ_CONFIG_H
# include "lqconfig.h"
#endif

#ifndef LQ_PBLOCK_H
# include "pblock.h"
#endif

#ifndef LQT_C_ISUPPER
# include "chartype.h"
#endif

#ifndef SMALLDB_H
# include "smalldb.h"
#endif

/** A database:
 ** This is intended to be an opaque data structure: you should
 ** never do anything with it except pass it as an argument to
 ** a liblqtext function.
 **
 ** The next time I edit this, the members of this struct will 
 ** all be hidden from view... use the functions to get at them instead.
 **/
typedef struct {
    char *DatabaseDirectory;		/** PRIVATE **/
    char *ConfigurationFile;		/** PRIVATE **/
    unsigned char *UsedInternally;	/** PRIVATE **/
    t_WID LQTp__LastNextWIDVal;		/** PRIVATE **/
    t_FID LQTp__LastNextFIDVal;		/** PRIVATE **/
    char *CommonWordFile; 		/** PRIVATE **/
    char *FileIndex;			/** PRIVATE **/
    char *WordIndex;			/** PRIVATE **/
    char *DataBase;			/** PRIVATE **/
    char *WidIndexFile;			/** PRIVATE **/
    char *DocPath; 			/** PRIVATE **/
    char *FreeFileName;			/** PRIVATE **/
    char *LastBlockFile;		/** PRIVATE **/
    char *Locale;			/** PRIVATE **/
    int MinWordLength;			/** PRIVATE **/
    int MaxWordLength;			/** PRIVATE **/
    int FileBlockSize;			/** PRIVATE **/
    t_PhraseCaseMatch PhraseMatchLevel;	/** PRIVATE **/
    t_lqdbOptions Options;		/** PRIVATE **/
    int FileFlags;			/** PRIVATE **/
    int FileModes;			/** PRIVATE **/
    t_WordFlags WordFlags;		/** PRIVATE **/
    char *CommonWordsLow;		/** PRIVATE **/
    char *CommonWordsHigh;		/** PRIVATE **/
    char ctypeTable[256];		/** PRIVATE **/
    char upperTable[256];		/** PRIVATE **/
    char lowerTable[256];		/** PRIVATE **/
    unsigned char WordsInWordIndex : 2;	/** PRIVATE **/
    unsigned char IndexNumbers : 2;	/** PRIVATE **/
    unsigned char ConvertNumbers : 2;	/** PRIVATE **/
    unsigned char *IgnoreHTMLhead;	/** PRIVATE **/
    DBM *WordIndexDB;			/** PRIVATE **/
    DBM *FileIndexDB;			/** PRIVATE **/
} t_LQTEXT_Database;

API t_LQTEXT_Database *LQT_OpenDatabase(
#ifdef HAVE_PROTO
    /* int Severity, */
    /* char *DatabaseDirectory,*/ /* 0 = default */
    /*char *Description,*/ /* e.g. "Flowers in Northern Japan" */
    t_lqdbOptions *Options,
    int Mode, /* as for open(2) */
    int Flags /* as for open(2), but most always be provided */
#endif
);

LIBRARY t_LQTEXT_Database *LQTpCreateLQTEXTDatabase(
#ifdef HAVE_PROTO
    t_lqdbOptions *Options,
    int flags,
    int modes
#endif
);

API int LQT_CloseDatabase(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *Database
#endif
);

API int LQT_SyncDatabase(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *Database
#endif
);

#define LQT_ON_CLOSE	01
#define LQT_ON_SYNC	02

API void LQT_AddActionOnClose(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *db,
    char *Description,
    int (* Action)(t_LQTEXT_Database *),
    unsigned int Flags
#endif
);

API int LQT_WordIsInStopList(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *db,
    t_WordInfo *WordInfo
#endif
);

API int LQT_ReadStopList(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *db,
    CONST char *CommonFile
#endif
);

API void LQT_InsertCommonWord(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *db,
    char *Root
#endif
);

API void *LQT_GetOption(
#ifdef HAVE_PROTO
    t_lqdbOptions *Options,
    char *Name
#endif
);

LIBRARY void LQTpReadDefaultFile(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *db
#endif
);

API t_lqdbOptions *LQT_InitFromArgv(
#ifdef HAVE_PROTO
    int argc,
    char **argv
#endif
);

API void LQT_PrintDefaultUsage(
#ifdef HAVE_PROTO
    t_lqdbOptions *Options
#endif
);

API FILE *LQT_MakeInput(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *db,
    t_FileInfo *
#endif
);

API t_FileInfo *LQT_MakeFileInfo(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *,
    char * /* the filename */
#endif
);

API int LQT_SaveFileInfo(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *db,
    t_FileInfo *FileInfo
#endif
);

API int LQT_RemoveFileInfoFromIndex(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *db,
    t_FileInfo *FileInfo
#endif
);

API int LQT_RenameFileInIndex(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *db,
    char *OldName,
    char *NewName
#endif
);

API t_FID LQT_NameToFID(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *db,
    char *Name
#endif
);

API t_FileInfo *LQT_FIDToFileInfo(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *db,
    t_FID FID
#endif
);

API void LQT_DestroyFileInfo(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *db,
    t_FileInfo *FileInfo
#endif
);

API t_FID LQT_GetMaxFID(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *db
#endif
);

API t_WID LQT_GetMaxWID(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *db
#endif
);

#ifndef LQ_PHRASE_H
# include "phrase.h"
#endif

API char *LQT_PhraseToString(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *db,
    t_Phrase *Phrase
#endif
);

API char *
LQT_StringToMatch(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *db,
    int Severity,
    char *theString,
    t_MatchStart **theMatchpp
#endif
);

API void LQT_DestroyPhrase(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *db,
    t_Phrase *Phrase
#endif
);

API t_Phrase *LQT_StringToPhrase(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *db,
    char *String
#endif
);

API void LQT_ResetPhraseMatch(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *db,
    t_Phrase *thePhrase
#endif
);

API int LQT_NumberOfWordsInPhrase(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *db,
    t_Phrase *Phrase
#endif
);

API int LQT_PrintAndAcceptOneMatch(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *db,
    t_Phrase *Phrase,
    t_Match *Match
#endif
);

API int LQT_PrintAndRejectOneMatch(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *db,
    t_Phrase *Phrase,
    t_Match *Match
#endif
);

API void LQT_fPrintOneMatch(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *db,
    FILE *theFile,
    int FirstNumber,
    t_FileInfo *FileInfo,
    t_WordPlace *WordPlace
#endif
);

/* a query is like a phrase but has embedded syntax */
typedef struct {
    char *Query; /* the original query */
    unsigned char *internalData; /* opaque handle */
} t_LQT_Query;

/* an expanded query list is a linked list of terms to use in a query */
typedef struct s_ExpandedQueryList {
    t_WordInfo *WordInfo;
    unsigned long NumberOfOccurrences;
    struct s_ExpandedQueryList *Next;
} t_ExpandedQueryList;

typedef struct s_QueryTerm {
    t_WordInfo *WordInfo;
    t_ExpandedQueryList *Expansion;
    struct s_QueryTerm *Next;
    unsigned long totalMatches;
    unsigned char flags;
} t_QueryItem;

API t_ExpandedQueryList *LQT_ExpandQueryItem(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *db,
    t_QueryItem *Item
#endif
);

API t_LQT_Query *LQT_ParseQuery(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *db,
    char *theString
#endif
);

/* LQT_QueryToPhraseKludge converts an expanded query into a phrase that
 * can be used with LQT_MakeMatchesWhere -- this is very inefficient.
 */
API t_Phrase *LQT_QueryToPhraseKludge(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *db,
    t_LQT_Query *Query
#endif
);

API long LQT_MakeMatchesWhere(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *db,
    t_Phrase *Phrase,
    int (*AcceptFunction)(
	t_LQTEXT_Database *,
	t_Phrase *,
	t_Match *
    )
#endif
);

API long LQT_MakeMatches(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *db,
    t_Phrase *Phrase
#endif
);

API t_PhraseElement *
LQT_AllPhrasesOfLengthNOrMore(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *db,
    int N,
    char *theQuery,
    long *Countp
#endif
);

LIBRARY int LQTp_CheckFlags(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *db,
    t_WordInfo *QueryWord,
    t_WordPlace *New,
    t_Position Position
#endif
);

API char *LQT_ReduceWordToRoot(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *db,
    t_WordInfo *WordInfo
#endif
);

API char *LQT_GenerateWordFromRoot(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *db,
    t_WordInfo *WordInfo,
    unsigned int Flags
#endif
);

LIBRARY t_FID LQT_GetMaxOrAllocateFID(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *db,
    int WriteCurrent
#endif
);

API void LQT_WriteCurrentMaxFID(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *db
#endif
);

LIBRARY t_WID LQTp_GetMaxOrAllocateWID(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *db,
    int WriteCurrent
#endif
);

API int LQT_WriteCurrentMaxWID(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *db
#endif
);

#define LQT_WIDMATCH_FAILED (-65599)
    /* must be bigger than the max difference between any 2 characters */

API t_WID LQT_FindFirstWIDMatchingWildCard(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *db,
    char *Prefix,
    int PrefixLength,
    int WildCard
#endif
);

API t_WID LQT_FindNextWIDMatchingWildCard(
# ifdef HAVE_PROTO
    t_LQTEXT_Database *db,
    t_WID WID,
    char *Prefix,
    int PrefixLength
# endif
);

API t_WID LQT_FindFirstWIDMatchingPrefix(
# ifdef HAVE_PROTO
    t_LQTEXT_Database *db,
    char *Prefix,
    int PrefixLength
# endif
);


API t_WID LQT_FindFirstWIDMatchingPattern(
# ifdef HAVE_PROTO
    t_LQTEXT_Database *db,
    unsigned char *Pattern,
    int PatternLength,
    int PrefixLength,
    int (* Matcher)(
	/* prefix with `the' to work aroung old gcc bug */
	t_LQTEXT_Database *thedb,
	unsigned char *theString,
	int theStringLength,
	unsigned char *thePattern,
	int thePatternLength,
	int thePrefixLength,
	unsigned char *theArgument
    ),
    unsigned char *Argument
# endif
);

API t_WID LQT_FindNextWIDMatchingPattern(
# ifdef HAVE_PROTO
    t_LQTEXT_Database *db,
    t_WID WID,
    unsigned char *Pattern,
    int PatternLength,
    int PrefixLength,
    int (* Matcher)(
	/* prefix with `the' to work aroung old gcc bug */
	t_LQTEXT_Database *thedb,
	unsigned char *theString,
	int theStringLength,
	unsigned char *thePattern,
	int thePatternLength,
	int thePrefixLength,
	unsigned char *theArgument
    ),
    unsigned char *Argument
# endif
);

API unsigned char *LQT_PrepareWildCardForMatching(
# ifdef HAVE_PROTO
    t_LQTEXT_Database *db,
    unsigned char *WildCard,
    int Length,
    int *PrefixLengthp
# endif
);

API void LQT_FinishWildCardAfterMatching(
# ifdef HAVE_PROTO
    t_LQTEXT_Database *db,
    unsigned char *Argument
# endif
);

API int LQT_MatcherForWildCards(
# ifdef HAVE_PROTO
    t_LQTEXT_Database *db,
    unsigned char *String,
    int StringLength,
    unsigned char *Pattern,
    int PatternLength,
    int PrefixLength,
    unsigned char *Argument
# endif
);

API t_WordInfo *LQT_WIDToWordInfo(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *db,
    t_WID WID
#endif
);

LIBRARY void LQT_MakeWordInfoBlockHeader(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *db,
    t_WordInfo *WordInfo,
    t_pblock *pblock
#endif
);

LIBRARY unsigned long LQT_MakeWordInfoBlock(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *db,
    t_WordInfo *WordInfo,
    t_pblock *pblock
#endif
);

API void LQT_UpdateWIDMatchCount(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *db,
    t_WID WID,
    unsigned long AddedThese
#endif
);

LIBRARY int LQTpFlushWIDCache(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *db
#endif
);

API t_WID LQT_WordToWID(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *db,
    char *Word,
    unsigned int Length
#endif
);

API char *LQT_WIDToWord(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *db,
    t_WID WID
#endif
);

API t_WID LQT_WriteWordAndWID(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *db,
    char *Word,
    int Length,
    t_WID WID
#endif
);

API int LQT_PutWordInfoIntoIndex(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *db,
    t_WordInfo *theWordInfo,
    unsigned long Offset
#endif
);

API int LQT_DeleteWordFromIndex(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *db,
    char *Word
#endif
);

API t_WordInfo *LQT_MakeWordInfo(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *db,
    t_WID WID,
    int Length,
    unsigned char *Word /* the word, which might not be nul-terminated */
#endif
);

API void LQT_DestroyWordInfo(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *db,
    t_WordInfo *WP
#endif
);

API void LQT_fprintWordInfo(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *db,
    FILE *stream,
    t_WordInfo *W,
    char *Caller
#endif
);

API char *LQT_FIDToDocumentTitle(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *db,
    t_FID FID,
    char *Name
#endif
);

API unsigned char *LQT_LastBlockInChain(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *db,
    t_WID WID,
    unsigned long *Offsetp,	/* in: first offset; Out: last offset */
    unsigned char **FirstUnusedBytepp, /* out only */ 
    unsigned int *BlockLengthp
#endif
);

API void LQT_SetLastBlockInChain(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *db,
    t_WID WID,
    unsigned long *Offsetp,
    unsigned char *FirstUnusedBytepp,
    unsigned char *theBlock
#endif
);

LIBRARY int LQTp_FlushLastBlockCache(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *db
#endif
);

API void LQT_CheckDatabaseVersion(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *db
#endif
);

API void LQT_WriteVersionToDatabase(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *db
#endif
);

API int LQT_FlushOneBlockFromCache(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *db
#endif
);

API int LQT_FlushBlockCache(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *db
#endif
);


#ifndef ASCIITRACE
    /* do the normal (not ASCIITRACE) case first so that
     * ctags indexed this one, for convenience in editing.
     */
API void LQT_WriteBlock(
# ifdef HAVE_PROTO
    t_LQTEXT_Database *db,
    unsigned long Block,
    unsigned char *Data,
    int Length,
    t_WID theWID
# endif
);

#else /* !ASCIITRACE */

# define LQT_WriteBlock(db, Block,Data,Length,WID) \
	LQTp_WriteBlock(db, __FILE__, __LINE__, Block, Data, Length, WID)

API void LQTp_WriteBlock(
# ifdef HAVE_PROTO
    t_LQTEXT_Database *db,
    char *theFile,
    int theLine,
    unsigned long Block,
    unsigned char *Data,
    int Length,
    t_WID theWID
# endif
);

#endif /* ASCIITRACE */

API unsigned long LQT_FindFreeBlock(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *db,
    t_WID WID,
    unsigned int *BlockLengthp,
    unsigned long BytesWanted
#endif
);

API void LQT_ExtendBlock(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *db,
    unsigned long Offset,
    unsigned int *BlockCountp,
    unsigned long BytesWanted
#endif
);

API int LQT_BlockIsFree(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *db,
    unsigned long Offset
#endif
);

API void LQT_SetBlockStatus(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *db,
    unsigned long Offset,
    int Status
#endif
);

API int LQT_BlockIsCached(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *db,
    unsigned long Block
#endif
);

#ifdef ASCIITRACE
    /* These two functions are defined only if
     * ASCIITRACE is set, and they are to be used only for
     * pretty serious debugging...
     */
LIBRARY t_WID LQTpGetWIDofBlock(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *db,
    unsigned long Offset
#endif
);

LIBRARY void LQTpSetWIDofBlock(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *db,
    unsigned long Offset,
    t_WID WID
#endif
);

#endif

API unsigned char *LQT_ReadBlock(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *db,
    unsigned long Offset,
    t_WID WID
#endif
);

API void LQT_DeleteWordPlaces(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *db,
    unsigned long FirstBlock,
    t_WID WID
#endif
);

API void LQT_Deletepblock(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *db,
    t_pblock *pblock
#endif
);

LIBRARY unsigned char LQTp__GetByte(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *db,
    t_WID WID,
    unsigned char **sp,
    unsigned char **Blockp,
    unsigned int *BlockLength,
    unsigned long *NextBlock
#endif
);

LIBRARY unsigned long LQTp__GetLong(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *db,
    t_WID WID,
    unsigned char **sp,
    unsigned char **Blockp,
    unsigned int *BlockLength,
    unsigned long *NextBlock
#endif
);

#define LQT_READWORD_IGNORE_COMMON	001
#define LQT_READWORD_WILDCARDS		002

API t_WordInfo *LQT_ReadWordFromStringPointer(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *db,
    char **Stringpp,
    char **Startp,
    CONST char *Endp,
    unsigned int Flags
#endif
);

API t_WordInfo *LQT_ReadWordFromFileInfo(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *db,
    t_FileInfo *FileInfo,
    unsigned int Flags
#endif
);

API t_pblock *LQT_GetpblockWhere(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *db,
    t_WordInfo *WordInfo,
    int (* AcceptFunc)(
	t_LQTEXT_Database *,
	t_WID,
	t_WordPlace *
    )
#endif
);

LIBRARY void LQT_PrintBlock(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *db,
    t_WID theWID,
    unsigned char **currentPos,
    unsigned char **dataStart,
    int *blockLength,
    long *nextOffset
#endif
);

API t_pblock *LQT_Getpblock(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *db,
    t_WordInfo *WordInfo
#endif
);

API t_WordPlace *LQT_GetWordPlacesWhere(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *db,
    t_WID WID,
    unsigned char *Block,
    unsigned int BlockLength,
    unsigned long NextOffset,
    unsigned long *NumberExpected,
    int (* AcceptFunc)(
	t_LQTEXT_Database *,
	t_WID,
	t_WordPlace *
    )
#endif
);

API t_WordPlace *LQT_GetWordPlaces(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *db,
    t_WID WID,
    unsigned char *Block,
    unsigned int BlockLength,
    unsigned long NextOffset,
    unsigned long *NumberExpected
#endif
);

API int LQT_ObtainWriteAccess(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *db
#endif
);

API int LQT_ObtainReadOnlyAccess(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *db
#endif
);

API int LQT_CurrentlyHaveWriteAccess(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *db
#endif
);

API void LQT_GetFileModes(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *db,
    int *Flagsp,
    int *Modesp
#endif
);

API int LQT_SyncAndCloseAllKeyValueDatabases(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *db
#endif
);

API void LQU_CursesSafeystem(
#ifdef HAVE_PROTO
    char *string,
    int *statusp
#endif
);

API int LQT_UnpackAndOpen(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *,
    char *
#endif
);

API void LQT_FlushBlock(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *db,
    unsigned char *Block,
    int ByteCount,
    unsigned long *NextOffset,
    unsigned long  *LastStart,
    t_WID WID
#endif
);

LIBRARY int LQTp__PutByte(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *db,
    unsigned int Byte,
    t_WID WID,
    unsigned char **sp,
    unsigned char **Blockp,
    unsigned int *BlockLength,
    unsigned long *NextBlock,
    unsigned long *NextLength,
    unsigned long *LastStart /* for writing the linked list */
#endif
);

LIBRARY int LQTp__PutLong(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *db,
    unsigned long Long,
    t_WID WID,
    unsigned char **sp,
    unsigned char **Blockp,
    unsigned int *BlockLength,
    unsigned long *NextBlock,
    unsigned long *NextLength,
    unsigned long *LastStart
#endif
);

API unsigned long LQT_Writepblock(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *db,
    t_WordInfo *WordInfo,
    t_pblock *pblock
#endif
);

API unsigned long LQT_WriteWordPlaces(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *db,
    t_WordPlace *WordPlaces,
    t_WID WID,
    unsigned long LastStart,
    unsigned char *Block,
    unsigned char *DataStart,
    unsigned int BlockLength,
    unsigned long NextOffset,
    unsigned long NextSize,
    unsigned long NumberToWrite
#endif
);

API unsigned long LQT_AddWordPlaces(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *db,
    t_WordPlace *WordPlaces,
    t_WID WID,
    unsigned long Offset,
    unsigned long NumberToWrite
#endif
);


API char *LQT_FindFile(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *db,
    char *Name
#endif
);

LIBRARY int LQTp_MakeDocPath(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *db,
    char *Path
#endif
);

LIBRARY int LQT_InitFilterTable(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *db
#endif
);

LIBRARY void LQTpInitReadOnlyPartOfFilterTable(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *db
#endif
);

LIBRARY int LQT_GetFilterType(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *db,
    t_FileInfo *FileInfo,
    struct stat *StatBuf
#endif
);

LIBRARY char *LQT_GetFilterName(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *db,
    t_FileInfo *FileInfo
#endif
);

LIBRARY char *LQTp_CreateEmptyKeyValueDatabase(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *db,
    char *root,
    char *prefix
#endif
);

API void LQT_SortWordPlaces(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *db,
    unsigned long NumberOfWordPlaces,
    t_WordPlace *WordPlaces
#endif
);

API char *LQT_WordFlagsToString(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *db,
    t_WordFlags Flags
#endif
);

API char *LQT_FlagsToString(
#ifdef HAVE_PROTO
    unsigned long Flags,
    t_FlagNamePair *WordFlagNamePairArray,
    char *Separator
#endif
);

API char *LQT_StringToFlags(
#ifdef HAVE_PROTO
    char *String,
    unsigned long *Flagp,
    t_FlagNamePair *WordFlagNamePairArray,
    char *Separator
#endif
);

API char *LQT_StringToWordFlags(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *db,
    char *String,
    t_WordFlags *Flagp
#endif
);

API int
LQT_ForEachWordFlag(
#ifdef HAVE_PROTO
    void (* CallMe)(
	char *Name,
	unsigned int Value
    );
#endif
);

DBM *LQT_OpenKeyValueDatabase(
# ifdef HAVE_PROTO
    t_LQTEXT_Database *db,
    char *theName
# endif
);

#ifndef LQT_CloseKeyValueDatabase
API void LQT_CloseKeyValueDatabase(
# ifdef HAVE_PROTO
  DBM *db
# endif
);
#endif /* !LQT_CloseKeyValueDatabase */

# define LQ_LIBLQTEXT_H Included
#endif
