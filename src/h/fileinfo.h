/* fileinfo.h -- Copyright 1989, 1993-1995, 1996 Liam R. E. Quin.
 * All Rights Reserved.
 * This code is NOT in the public domain.
 * See the file COPYRIGHT for full details.
 */

/* Internal structure used by lq-Text to represent a file
 * Needs sys/types.h
 *
 * $Id: fileinfo.h,v 1.9 1996/08/14 16:50:41 lee Exp $
 */

#ifndef LQ_FILEINFO_H
# define LQ_FILEINFO_H 1

typedef unsigned long t_FID;

typedef struct {
    char *Name;
    t_FID FID; /* File Identifier */
    int FilterType;
	/* Command to ASCIIify, 0 unknown, 1 none;
	 * See h/filters.h and liblqtext/filters.c for more information
	 * about these and the list of possible values.
	 */
    time_t Date;
	/* When the file was last indexed?  This is used so that if a
	 * file hasn't changed, we don't need to index it again.
	 */
    unsigned long FileSize;
	/* size of the file in bytes.  This limits us to just under
	 * 4 GBytes (since 4G would be -1, used as an error value!).
	 */
    FILE *Stream;
	/* used to index the file.  This isn't stored in the database! */
    unsigned char Flags;
	/* Currently stored but unused by lq-text.  You can bitwise-or in
	 * your own flags if you like in this release.
	 */
} t_FileInfo;

# define LQT_FILE_VARBLOCKS	0001 /* uses variable sized blocks */

#endif /* LQ_FILEINFO_H */
