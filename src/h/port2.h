/* If you make changes here, copy this file to
 * ../../config.dir/sparc/sunos4.1.1/port2.h
 * so that the changes are saved.
!!cp -i %  ../config.dir/sparc/sunos4.1.1/port2.h
 *
 */

/* For SunOS 4.1.x, this file declares some routines that are used
 * by stdio but that are not in any header file.  Some of them are internal
 * to stdio, and some of them are documented and external (e.g. fprintf)
 * but still not in any header file.
 *
 * Doing this satisfies compilers that warn about using functions
 * that you haven't declared, and also provides a small amount of extra
 * error checking.
 */

#ifndef FILE
# include <stdio.h>
#endif

extern int _filbuf(
#ifdef HAVE_PROTO
    FILE *p
#endif
);

extern int fprintf(
#ifdef HAVE_PROTO
    FILE *f,
    char *fmt,
    ...
#endif
);

extern int fclose(
#ifdef HAVE_PROTO
    FILE *p
#endif
);

extern int fputs(
#ifdef HAVE_PROTO
    char *s,
    FILE *p
#endif
);

extern int _flsbuf(
#ifdef HAVE_PROTO
    unsigned char,
    FILE *
#endif
);
