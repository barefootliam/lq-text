/* block0.h -- Copyright 1989, 1993-1994, 1996 Liam R. E. Quin.
 * All Rights Reserved.
 * This code is NOT in the public domain.
 * See the file COPYRIGHT for full details.
 * 
 * $Id: block0.h,v 1.3 1996/05/15 19:29:03 lee Exp $
 *
 * This file describes the layout of block zero of the "data" file.
 * This is used to store various pieces of information about a database.
 */

#ifndef BLOCK_ZERO_BLOCKLENGTH
# define BLOCK_ZERO_BLOCKLENGTH		 0	/* length of block */

#define BLOCK_ZERO_MAXWID0		 9	/* Max wid */
#define BLOCK_ZERO_MAXWID1		10
#define BLOCK_ZERO_MAXWID2		11
#define BLOCK_ZERO_MAXWID3		12

#define BLOCK_ZERO_MAXFID0		13	/* Max wid */
#define BLOCK_ZERO_MAXFID1		14
#define BLOCK_ZERO_MAXFID2		15
#define BLOCK_ZERO_MAXFID3		16

#define BLOCK_ZERO_COMPILEFLAGBYTE	17	/* length of block */

#define BLOCK_ZERO_MAJORVERSION0	18	/* lq-text index version */
#define BLOCK_ZERO_MAJORVERSION1	19	/* lq-text index version */
#define BLOCK_ZERO_MINORVERSION		20	/* minor version */

#define BLOCK_ZERO_WRITER_PID0		21	/* process ID of db locker */
#define BLOCK_ZERO_WRITER_PID1		22 	/* We allow 4 byte, even */
#define BLOCK_ZERO_WRITER_PID2		23	/* thought it's only 16bits */
#define BLOCK_ZERO_WRITER_PID3		24	/* on most sytems. */

#define BLOCK_ZERO_WRITER_HOST0		25	/* Internet address of */
#define BLOCK_ZERO_WRITER_HOST1		26	/* host on which the locking */
#define BLOCK_ZERO_WRITER_HOST2		27	/* process is running */
#define BLOCK_ZERO_WRITER_HOST3		28

/* We reserve this many bytes for the above information,
 * and a few extra for future expansion:
 */
#define BLOCK_ZERO_RESERVEDBYTES	32

#endif /* BLOCK_ZERO_BLOCKLENGTH */
