head	1.7;
access;
symbols;
locks;
comment	@ * @;


1.7
date	2019.04.21.06.04.30;	author lee;	state Exp;
branches;
next	1.6;

1.6
date	2003.05.10.15.52.03;	author lee;	state Exp;
branches;
next	1.5;

1.5
date	96.08.14.16.50.59;	author lee;	state Exp;
branches;
next	1.4;

1.4
date	96.07.01.21.11.50;	author lee;	state Exp;
branches;
next	1.3;

1.3
date	96.05.15.21.52.26;	author lee;	state Exp;
branches;
next	1.2;

1.2
date	95.09.13.23.05.17;	author lee;	state Exp;
branches;
next	1.1;

1.1
date	94.02.26.14.52.37;	author lee;	state Exp;
branches;
next	;


desc
@Header for liblqutil
@


1.7
log
@for migration to CVS
@
text
@#ifndef LQ_UTIL_H
# define LQ_UTIL_H 1

/* $Id: lqutil.h,v 1.6 2003/05/10 15:52:03 lee Exp $
 */

# ifndef API
#  include "api.h"
# endif

#ifndef LQ_RANGE_H
# include "range.h"
#endif


/**** [1] String Manipulation Functions ****/


/** LQU_cknatstr(s) returns 1 if s points to a string representing
 ** a natural number (i.e. an integer).
 ** The string must match the regular expression
 **	/^[ 	]*-?[0-9]+[ 	]*$/
 ** (where [ 	] represents any whitespace)
 **/

API int LQU_cknatstr(
# ifdef HAVE_PROTO
    CONST char *theString
# endif /*HAVE_PROTO*/
);

/** LQU_DownCase(s) returns a malloc()'d copy of s in which any upper-case
 ** characters have been converted to their lower-case equivalents.
 ** LQU_DownCase() uses isascii() and isupper() to do this.  However,
 ** it uses isalnum() rather than isascii() before checking whether a
 ** character is upper-case or not.
 ** This will work if set_locale() has been called.
 **/
API char *LQU_DownCase(
# ifdef HAVE_PROTO
    CONST char *theString
# endif /*HAVE_PROTO*/
);

API char *LQU_joinstr2(
# ifdef HAVE_PROTO
    CONST char *s1,
    CONST char *s2
# endif /*HAVE_PROTO*/
);

API char *LQU_joinstr3(
# ifdef HAVE_PROTO
    CONST char *s1,
    CONST char *s2,
    CONST char *s3
# endif /*HAVE_PROTO*/
);

API int LQU_StringContainedIn(
# ifdef HAVE_PROTO
    CONST char *ShortString,
    CONST char *LongString
# endif /*HAVE_PROTO*/
);

API int LQU_LargerThanRangeTop(
# ifdef HAVE_PROTO
    CONST int n,
    CONST t_Range *Range
# endif /*HAVE_PROTO*/
);

API int LQU_NumberWithinRange(
# ifdef HAVE_PROTO
    CONST int n,
    CONST t_Range *Range
# endif /*HAVE_PROTO*/
);

API t_Range *LQU_StringToRange(
# ifdef HAVE_PROTO
    CONST char *String
# endif /*HAVE_PROTO*/
);

API char *LQU_ReverseString(
# ifdef HAVE_PROTO
    char *start,
    char *end,
    int type
# endif /*HAVE_PROTO*/
);

/**** [2] File Manipulation Routines ****/


/** LQU_Eopen() calls open(2) to open a file.  If the open fails, LQU_Eopen()
 ** calls Error() with the given Severity, or'd with E_SYS, creating
 ** an error message including "What", so that
 ** int fd = LQU_Eopen(E_FATAL, "foo", "output listing", O_WRONLY|O_CREAT, 0744);
 ** might print
 ** progname: fatal error: can't open foo (output listing):
 ** progname: fatal error: can't create files in the current directory (E_PERM)
 ** and exit.
 **/
API int LQU_Eopen(
# ifdef HAVE_PROTO
    int Severity,
    CONST char *Name,
    CONST char *What,
    int Flags,
    int Modes
# endif /*HAVE_PROTO*/
);

/** LQU_fEopen() is the stdio equivalent of LQU_Eopen().
 **/
API FILE *LQU_fEopen(
# ifdef HAVE_PROTO
    int Severity,
    CONST char *Name,
    CONST char *What,
    CONST char *Mode
# endif /*HAVE_PROTO*/
);

API void LQU_fEclose(
# ifdef HAVE_PROTO
    int Severity,
    FILE *fp,
    CONST char *Name,
    CONST char *What
# endif /*HAVE_PROTO*/
);

API int LQU_Eread(
# ifdef HAVE_PROTO
    int Severity,
    CONST char *Name,
    CONST char *What,
    int fd,
    char *Buffer,
    int ByteCount
# endif /*HAVE_PROTO*/
);


/** LQU_IsDir() returns non-zero if Dir points to the name of a directory.
 ** Returns 0 otherwise.
 ** NOTE: in the future, this routine may return -1 on error.
 **/
API int LQU_IsDir(
# ifdef HAVE_PROTO
    CONST char *Dir
# endif /*HAVE_PROTO*/
);

/** LQU_IsFile() returns non-zero if Path points to the name of a regular file.
 ** Returns 0 otherwise.
 ** NOTE: in the future, this routine may return -1 on error.
 ** A special character device or a broken symbolic link do not count
 ** as regular files; see <sys/stat.h> for IF_REG.
 **/
API int LQU_IsFile(
# ifdef HAVE_PROTO
    CONST char *Path
# endif /*HAVE_PROTO*/
);

/** LQU_IsNonEmptyFile() is like LQU_IsFile, but only returns 1 if the
 ** named file contains data.  It returns 0 otherwise.
 ** NOTE: in the future, this routine may return -1 on error.
 ** A special character device or a broken symbolic link do not count
 ** as regular files; see <sys/stat.h> for IF_REG.
 **/
API int LQU_IsNonEmptyFile(
# ifdef HAVE_PROTO
    CONST char *Path
# endif /*HAVE_PROTO*/
);

API off_t LQU_Elseek(
# ifdef HAVE_PROTO
    int Severity,
    CONST char *Name,
    CONST char *What,
    int fd,
    off_t Position,
    int Whence
# endif /*HAVE_PROTO*/
);

API char *LQU_GetLoginDirectory(
# ifdef HAVE_PROTO
    FUNCTION_WITHOUT_ARGUMENTS
# endif /*HAVE_PROTO*/
);

/* Flags for LQU_fReadFile */

#define LQUF_IGNBLANKS	  001 /* throw away blank lines */
#define LQUF_IGNSPACES	  002 /* discard leading and trailing spaces */
#define LQUF_IGNHASH  	  004 /* discard LEADING comments (# at line start) */
#define LQUF_ESCAPEOK 	  010 /* accept \# and \\ as # and \ */
#define LQUF_IGNALLHASH   020 /* discard ALL comments (# anywhere) */
#define LQUF_IGNNEWLINE   040 /* discard trailing newline */
#define LQUF_NORMAL \
    (LQUF_IGNBLANKS|LQUF_IGNSPACES|LQUF_IGNHASH|LQUF_ESCAPEOK|LQUF_IGNNEWLINE)


/** File and Line Reading Routines:
 **
 **/

API long LQU_ReadFile(
#ifdef HAVE_PROTO
    int Severity,
    CONST char *Name,
    CONST char *What,
    char ***Lines,
    int Flags
#endif
);

API long LQU_fReadFile(
#ifdef HAVE_PROTO
    int S,
    FILE *f,
    CONST char *Name,
    CONST char *What,
    char ***Lines,
    int Flags
#endif
);

API int LQU_fReadLine(
#ifdef HAVE_PROTO
    FILE *f,
    char **Linep,
    int Flags
#endif
);

API char * LQU_StealReadLineBuffer(
#ifdef HAVE_PROTO
    FUNCTION_WITHOUT_ARGUMENTS
#endif
);

/** Character set and string stuff
 **
 **/
API unsigned char *
LQU_UTF8FirstNonPartialOctet(
#ifdef HAVE_PROTO
    unsigned char *Start
#endif
);

API unsigned long
LQU_UTF8CharLength(
#ifdef HAVE_PROTO
    CONST unsigned char *String
#endif
);

/** User preference and login directory:
 **
 **/
#endif /* LQ_UTIL_H */

@


1.6
log
@change Position to off_t; change definition of LQUF_NORMAL
@
text
@d4 1
a4 1
/* $Id: lqutil.h,v 1.5 96/08/14 16:50:59 lee Exp Locker: lee $
d248 17
@


1.5
log
@for beta 6.
@
text
@d4 1
a4 1
/* $Id: lqutil.h,v 1.4 96/07/01 21:11:50 lee Exp $
d189 1
a189 1
    long Position,
d202 8
a209 6
#define LQUF_IGNBLANKS	001 /* throw away blank lines */
#define LQUF_IGNSPACES	002 /* discard leading and trailing spaces */
#define LQUF_IGNHASH  	004 /* discard LEADING comments (# with a hash-sign) */
#define LQUF_ESCAPEOK 	010 /* accept \# and \\ as # and \ */
#define LQUF_IGNALLHASH   020 /* discard ALL comments (# with a hash-sign) */
#define LQUF_NORMAL (LQUF_IGNBLANKS|LQUF_IGNSPACES|LQUF_IGNHASH|LQUF_ESCAPEOK)
@


1.4
log
@Added ability to steal ReadLine buffer.
@
text
@d4 1
a4 1
/* $Id: lqutil.h,v 1.3 1996/05/15 21:52:26 lee Exp $
d194 1
a194 2
API char *
LQU_GetLoginDirectory(
d196 1
a196 1
    /* empty */
d214 1
a214 2
API long
LQU_ReadFile(
d224 1
a224 2
API long
LQU_fReadFile(
d235 1
a235 2
API int
LQU_fReadLine(
d243 1
a243 2
API char *
LQU_StealReadLineBuffer(
d245 1
a245 1
    /* EMPTY */
d249 3
@


1.3
log
@new API...
@
text
@d4 1
a4 1
/* $Id: lqutil.h,v 1.1 94/02/26 14:52:37 lee Exp Locker: lee $
d244 7
@


1.2
log
@added CONST,  fix to fEopen.
@
text
@d132 2
a133 2
    char *Name,
    char *What
a147 24
/** LQU_ReadFile() eads lines from the named file (Name) and puts them in an
 ** array that it allocates with malloc().  It returns a pointer to the
 ** start of the array of pointers to lines (in *Lines), and
 ** the number of lines read.  If the file can't be opened, or it runs
 ** out of memory, LQU_ReadFile() calls Error() with the given Severity,
 ** and with an error message constructed out of What, which should
 ** be a short (e.g. 3-word) description of the purpose of the file.
 ** The Flags can currently include
 ** 	UF_IGNBLANKS to throw away blank lines,
 ** 	UF_IGNSPACES to discard leading and trailing spaces,
 ** 	UF_IGNHASH   to discard leading comments (# with a hash-sign)
 ** 	UF_IGNALLHASH   to discard comments (# with a hash-sign)
 ** 	UF_ESCAPEOK  to accept \# and \\ as # and \
 ** this file also defines UF_NORMAL, see above.
 **/
API long LQU_ReadFile(
# ifdef HAVE_PROTO
    int Severity,	/* to pass to Error() if necessary */
    CONST char *Name,	/* the filename to read */
    CONST char *What,
    char ***Lines,
    int Flags
# endif /*HAVE_PROTO*/
);
a148 33
/** LQU_fReadFile() is identical to LQU_ReadFile() except that it takes an
 ** already-opened FILE pointer and
 ** reads up to EOF:
 **/
API long LQU_fReadFile(
# ifdef HAVE_PROTO
    int Severity,	/* to pass to Error() if necessary */
    FILE *f,
    CONST char *Name,	/* the filename to read */
    CONST char *What,	/* e.g. "input Pascal source file" */
    char ***Lines,	/* return value pointer */
    int Flags		/* see readfile.h */
# endif /*HAVE_PROTO*/
);

/** LQU_fReadLine reads a single line into memory, making room with malloc()
 ** and setting *Linep to point to the new storage.  It returns -1
 ** when it detects the end of input (EOF).
 **
 ** If malloc() or realloc returns 0 (indicating that no more memory
 ** is available), *Linep is set to 0 and the number of characters
 ** read is returned.
 **
 ** The Flags argument is the same as for LQU_ReadFile.
 **/
API int LQU_fReadLine(
# ifdef HAVE_PROTO
    FILE *f,		/* The already open FILE * from which to read */
    char **Linep,	/* Result: set to point to one input line */
    int Flags		/* See readfile.h */
# endif /*HAVE_PROTO*/
);

d199 46
@


1.1
log
@API change
@
text
@d4 1
a4 1
/* $Id$
d45 1
a45 1
API char * LQU_joinstr3(
d48 7
d81 1
a81 1
API t_Range * LQU_StringToRange(
d87 7
d98 1
a98 1
/** Eopen() calls open(2) to open a file.  If the open fails, Eopen()
d101 1
a101 1
 ** int fd = Eopen(E_FATAL, "foo", "output listing", O_WRONLY|O_CREAT, 0744);
d107 1
a107 1
API int Eopen(
d117 1
a117 1
/** fEopen() is the stdio equivalent of Eopen().
d119 1
a119 1
API FILE *fEopen(
d128 20
d237 18
@
