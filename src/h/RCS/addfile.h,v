head     1.7;
branch   ;
access   ;
symbols  ;
locks    ;
comment  @ * @;


1.7
date     96.08.14.16.50.11;  author lee;  state Exp;
branches ;
next     1.6;

1.6
date     96.05.15.19.23.46;  author lee;  state Exp;
branches ;
next     1.5;

1.5
date     94.02.26.14.52.05;  author lee;  state Exp;
branches ;
next     1.4;

1.4
date     92.07.21.19.01.25;  author lee;  state Exp;
branches ;
next     1.3;

1.3
date     92.07.21.18.59.01;  author lee;  state Exp;
branches ;
next     1.2;

1.2
date     92.07.21.17.43.49;  author lee;  state Exp;
branches ;
next     1.1;

1.1
date     92.07.21.17.42.17;  author lee;  state Exp;
branches ;
next     ;


desc
@parameters for DumpCache().
@


1.7
log
@for beta 6.
@
text
@/* addfile.h -- Copyright 1989, 1994, 1996 Liam R. Quin.
 * All Rights Reserved.
 * This code is NOT in the public domain.
 * See the file COPYRIGHT for full details.
 */

/* $Id: addfile.h,v 1.6 1996/05/15 19:23:46 lee Exp $
 *
 * This file defines the parameter to DumpCache.
 *
 */

#define MAXPROBES	15
    /* If we don't find an empty cache slot after this many tries,
     * we decide the hash table is full and clear it out, then try again.
     */

#define DUMP_CACHE	00	/* normal usage */
#define DUMP_SYNC	01	/* write all cached entries to disk */
#define DUMP_NOFREE	02	/* don't bother calling free() */
#define DUMP_FAST	04	/* only do a fast dump */

/* when the word-cache fills up, we first try and dump all of
 * the words that occur DUMP_FAST_THRESH or fewer times.  This generally
 * clears up a lot of slots in the hash table.  If that hasn't cleared
 * enough memory, we then dump the single most frequent word, before
 * increasing the dump threshhold and trying again.
 * Now that updating the index is possible, I am not sure that this is
 * the best way to continue, but DUMP_FAST_THRESH is certainly an effective
 * way to clear slots in the cache.
 * MUST_DUMP is used to start the dump process.
 */

#ifndef DUMP_FAST_THRESH
# define DUMP_FAST_THRESH	5
#endif

/* MUST_FREE_SLOTS is true when we need more free slots in the symbol table
 * used to cache words being indexed.
 *
 * We require 80% of the slots to be free, so that the hash function doen't
 * spend ages finding a free slot.
 */
#ifndef MUST_FREE_SLOTS
# define MUST_FREE_SLOTS(InCacheCount,MaxInCache,SlotsUsed,TotalSlots) \
	(SlotsUsed * 10 > TotalSlots * 8)
#endif

/* MUST_FREE_MORE_SLOTS is true when we have already started to dump the
 * cache to disk and we are checking to see when to stop.  This value is set
 * so that we don't immediately dump the cache again -- it must take a while
 * to fill up, no?
 * We require at least 80% of the slots to be empty, so that finding a slot
 * for a new word doesn't take too long.
 *
 * Usually there are plenty of slots to go round, but if you index something
 * with a huge vocabulary (/usr/dict/words, for example!) where there are
 * lots of words each occurring infrequently, you may run low on slots, and
 * with that sort of input, you really do need lots of slots free.
 *
 * So dumping all but 20% is only a performance hit when there are lots of
 * infrequent words, and that's when we need to do it most anyway.
 */
#ifndef MUST_FREE_MORE_SLOTS
# define MUST_FREE_MORE_SLOTS(InCacheCount,MaxInCache,SlotsUsed,TotalSlots) \
	(SlotsUsed * 5 > TotalSlots)
#endif

/* MUST_DUMP evaluates to non-zero when lqaddfile must dump its cache;;
 * MUST_DUMP_MORE is used to see if it's OK to stop dumping.
 */
#ifndef MUST_DUMP
# define MUST_DUMP(InCacheCount,MaxInCache,SlotsUsed,TotalSlots) \
	( (InCacheCount + 1 >= MaxInCache) || \
	    MUST_FREE_SLOTS(InCacheCount,MaxInCache,SlotsUsed,TotalSlots))
#endif

/* MUST_DUMP_MORE returns 0 when the cache is suffiently dumped to
 * enable more indexing to proceed.
 */
#ifndef MUST_DUMP_MORE
# define MUST_DUMP_MORE(InCacheCount,MaxInCache,SlotsUsed,TotalSlots) \
	( (InCacheCount * 100 >= MaxInCache * 60) /* allow 60% full */ || \
	    MUST_FREE_MORE_SLOTS(InCacheCount,MaxInCache,SlotsUsed,TotalSlots))
#endif

/* FILE_DUMP returns 1 if we should dump the cache between files:
 */
#ifndef MUST_FILE_DUMP
# define MUST_FILE_DUMP(InCacheCount,MaxInCache,SlotsUsed,TotalSlots) \
	( (InCacheCount * 100 >= MaxInCache * 98)  || \
	  (SlotsUsed * 100 > TotalSlots * 75)) /* allow 75% full */
#endif

/* FILE_DUMP_MORE returns 1 if we haven't finished dumping between files:
 */
#ifndef MUST_FILE_DUMP_MORE
# define MUST_FILE_DUMP_MORE(InCacheCount,MaxInCache,SlotsUsed,TotalSlots) \
	( (InCacheCount * 100 >= MaxInCache * 90) /* allow 90% full */ || \
	    MUST_FREE_MORE_SLOTS(InCacheCount,MaxInCache,SlotsUsed,TotalSlots))
#endif

@


1.6
log
@added MUST_FREE_MORE_SLOTS.
@
text
@d7 1
a7 1
/* $Id: addfile.h,v 1.5 94/02/26 14:52:05 lee Exp Locker: lee $
d13 1
a13 1
#define MAXPROBES	35
d40 3
d46 1
a46 1
	(SlotsUsed * 10 > TotalSlots * 9)
d53 10
d66 1
a66 1
	(SlotsUsed * 10 > TotalSlots * 3)
d83 1
a83 1
	( (InCacheCount * 100 >= MaxInCache * 80) /* allow 80% full */ || \
@


1.5
log
@API change
@
text
@d1 2
a2 1
/* addfile.h -- Copyright 1989 Liam R. Quin.  All Rights Reserved.
d7 1
a7 1
/* $Id: addfile.h,v 1.4 92/07/21 19:01:25 lee Exp $
d38 18
d62 1
a62 1
	  (SlotsUsed * 10 > TotalSlots * 9)) /* allow 90% full */
d71 1
a71 1
	  (SlotsUsed * 10 > TotalSlots * 3)) /* allow 30% full */
d78 1
a78 1
	( (InCacheCount * 100 >= MaxInCache * 99)  || \
d87 1
a87 1
	  (SlotsUsed * 100 > TotalSlots * 60)) /* allow 60% full */
@


1.4
log
@changed the name of the low water test.
@
text
@d6 1
a6 1
/* $Id: addfile.h,v 1.3 92/07/21 18:59:01 lee Exp $
d12 5
d22 11
d34 1
a34 1
# define DUMP_FAST_THRESH	20
d37 7
a43 3
#ifndef DUMP_CACHE_LOW_WATER
# define DUMP_CACHE_BELOW_LOW_WATER(InCacheCount,MaxInCache) \
		((InCacheCount) * 4 <= (MaxInCache) * 3)
d45 26
@


1.3
log
@added CACHE_LOW_WATR and DUMP-THRESH
@
text
@d6 1
a6 1
/* $Id: addfile.h,v 1.2 92/07/21 17:43:49 lee Exp $
d21 3
a23 3
#ifndef CACHE_LOW_WATER
# define CACHE_LOW_WATER(InCacheCount,MaxInCache) \
		((InCacheCount) * 4 >= (MaxInCache) * 3)
@


1.2
log
@added DUMP_cACHE
@
text
@d6 1
a6 1
/* $Id: addfile.h,v 1.1 92/07/21 17:42:17 lee Exp Locker: lee $
d16 9
@


1.1
log
@Initial revision
@
text
@d6 1
a6 1
/* $Id$
d12 1
@
