/* API.h -- Copyright 1994 Liam R. Quin.  All Rights Reserved.
 * This code is NOT in the public domain.
 * See the file COPYRIGHT for full details.
 *
 * $Id: api.h,v 1.2 1996/07/04 22:17:34 lee Exp $
 */

#ifndef API
# ifdef __cplusplus
#  define API extern "C"
#  define LIBRARY extern "C"
# else /* not C++ */
#  define API extern
#  define LIBRARY extern
# endif
# define PRIVATE static
# define INTERNAL API	/* secret functions that are actually global */
#endif /*!API*/


#ifndef HAVE_PROTO
# if defined(__STDC__)||defined(__GNUC__)
#  define HAVE_PROTO /* use function prototypes */
# endif
#endif /*!HAVE_PROTO*/

#ifndef FUNCTION_WITHOUT_ARGUMENTS
# ifdef HAVE_PROTO
#  define FUNCTION_WITHOUT_ARGUMENTS void
# else
#  define FUNCTION_WITHOUT_ARGUMENTS /* empty */
# endif
#endif
