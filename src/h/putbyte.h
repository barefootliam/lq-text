/* putbyte.h -- Copyright 1989, 1993, 1996 Liam R. E. Quin.
 * All Rights Reserved.
 * This code is NOT in the public domain.
 * See the file COPYRIGHT for full details.
 *
 * $Id: putbyte.h,v 1.4 1996/05/15 21:57:25 lee Exp $
 *
 */

#ifndef LQ_PUTBYTE_H
# define LQ_PUTBYTE_H 1

/* LQTp_PutByte gets called literally millions of times, so it is a macro.
 * The idea is that if we reach the end of the block, LQTp_PutByte calls
 * LQTp__PutByte which fetches a new block and sets offset pointers,
 * blocklength, etc., and resets the char * pointers to point into the
 * right buffer.
 */
#define LQTp_PutByte(db,Byte,WID,sp,Blockp,BlockLength,LastStart,NextBlock,NextSize) \
    ( (*(sp) - (unsigned char *) *(Blockp) >= *(BlockLength)) ? \
       LQTp__PutByte(db,Byte,WID,sp,Blockp,BlockLength,LastStart,NextBlock,NextSize) : \
      ((*((*(sp))++) = (Byte)), 0))

#define LQTp_PutLong(db,L,WID,sp,Blockp,BlkLength,LastStart,NextBlk,NextSz) \
    (((L) < 128) ? \
	LQTp_PutByte(db,(L),WID,sp,Blockp,BlkLength,LastStart,NextBlk,NextSz) \
		: \
	LQTp__PutLong(db,(L),WID,sp,Blockp,BlkLength,LastStart,NextBlk,NextSz) \
    )

#endif /* LQ_PUTBYTE_H */
