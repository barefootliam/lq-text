/* range.h -- Copyright 1993, 1994 Liam R. Quin.  All Rights Reserved.
 * This code is NOT in the public domain.
 * See the file COPYRIGHT for full details.
 */

/* $Id: range.h,v 1.3 1994/02/26 14:52:49 lee Exp $ */

#ifndef LQ_RANGE_H
# define LQ_RANGE_H 1

typedef struct s_Range {
    unsigned long First;
    unsigned long Last;
    unsigned int AndAllBefore : 1; /* only on the first of the list */
    unsigned int AndAllAfter : 1; /* only if no Next field */
    struct s_Range *Next;
} t_Range;

#define DEFAULT_RANGE_END ((unsigned long) 65535L)

#endif /*!LQ_RANGE_H*/
