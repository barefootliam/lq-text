/* chartype.h -- Copyright 1996 Liam R. E. Quin.
 * All Rights Reserved.
 * This code is NOT in the public domain.
 * See the file COPYRIGHT for full details.
 *
 * character types
 *
 * $Id: chartype.h,v 1.3 2019/04/21 06:04:30 lee Exp $
 *
 */

#ifndef LQT_C_ISUPPER

# ifndef WPF_UPPERCASE
#  include "wordrules.h"
# endif

# if WPF_UPPERCASE != 002
this is required by readword -- please fix wordrules.h
LQT_TOUPPER must return either 0 (not upper) or WPF_UPPERCASE as appropriate.
# endif

# define LQT_C_ISLOWER		0001
# define LQT_C_ISUPPER		0002
# define LQT_C_ISDIGIT		0004
# define LQT_C_ISPUNCT		0010
# define LQT_C_ISALPHA		0020
# define LQT_C_STARTS_WORD	0040
# define LQT_C_ONLY_WITHIN_WORD	0100
# define LQT_C_ENDS_WORD	0200 /* the largest that fits in 1 byte */

# define LQT_ISLOWER(db, c) ((db)->ctypeTable[(unsigned int)(c)] & LQT_C_ISLOWER)
# define LQT_TOLOWER(db, c) ((db)->lowerTable[(unsigned int)(c)])

# define LQT_ISUPPER(db, c) ((db)->ctypeTable[(unsigned int)(c)] & LQT_C_ISUPPER)
# define LQT_TOUPPER(db, c) ((db)->upperTable[(unsigned int)(c)])

# define LQT_ISALPHA(db, c) ((db)->ctypeTable[(unsigned int)(c)] & LQT_C_ISALPHA)
# define LQT_ISDIGIT(db, c) ((db)->ctypeTable[(unsigned int)(c)] & LQT_C_ISDIGIT)
# define LQT_ISPUNCT(db, c) ((db)->ctypeTable[(unsigned int)(c)] & LQT_C_ISPUNCT)
# define LQT_ISDIGIT_OR_PUNCT(db, c) \
	((db)->ctypeTable[(unsigned int)(c)] & (LQT_C_ISPUNCT | LQT_C_ISDIGIT))

# define LQT_STARTS_WORD(db, c) \
	((db)->ctypeTable[(unsigned int)(c)] & LQT_C_STARTS_WORD)

# define LQT_ONLY_WITHIN_WORD(db, c) \
	((db)->ctypeTable[(unsigned int)(c)] & LQT_C_ONLY_WITHIN_WORD)

# define LQT_ENDS_WORD(db, c) \
	((db)->ctypeTable[(unsigned int)(c)] & LQT_C_ENDS_WORD)

# define LQT_WITHIN_OR_ENDS_WORD(db, c) \
	((db)->ctypeTable[(unsigned int)(c)] & (LQT_C_ONLY_WITHIN_WORD|LQT_C_ENDS_WORD))

#endif /* !LQT_C_ISUPPER */
