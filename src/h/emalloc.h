/* emalloc.h -- Copyright 1989, 1994, 1995, 1996 Liam R.E. Quin.
 * All Rights Reserved.
 * This code is NOT in the public domain.
 * See the file COPYRIGHT for full details.
 *
 * emalloc.h -- header file for emalloc.c, Liam Quin's malloc() wrapper
 *
 * $Id: emalloc.h,v 1.8 1996/05/15 19:33:39 lee Exp $
 */

#ifndef LQ_EMALLOC_H
# define LQ_EMALLOC_H 1

extern char *emalloc(
#ifdef HAVE_PROTO
    CONST char *what,
    unsigned int amount
#endif
);

extern char *erealloc(
#ifdef HAVE_PROTO
    char *lump,
    unsigned int amount
#endif
);

extern char *ecalloc(
#ifdef HAVE_PROTO
    CONST char *what,
    unsigned int number_of_lumps,
    unsigned int lump_size
#endif
);

extern void efree(
#ifdef HAVE_PROTO
    char *lump
#endif
);

#ifdef MALLOCTRACE
#define emalloc(what, u) LQU_TraceMalloc(what, u, __FILE__, __LINE__)
#define erealloc(s, u) LQU_TraceRealloc(s, u, __FILE__, __LINE__)
#define ecalloc(what, n, siz) LQU_TraceCalloc(what, n, siz, __FILE__, __LINE__)
#define efree(s) LQU_TraceFree(s, __FILE__, __LINE__)
#endif

#endif /* LQ_EMALLOC_H */
