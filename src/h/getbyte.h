/* getbyte.h -- Copyright 1989, 1993, 1996 Liam R. Quin.
 * All Rights Reserved.
 * This code is NOT in the public domain.
 * See the file COPYRIGHT for full details.
 */

/* $Id: getbyte.h,v 1.3 1996/05/15 21:46:59 lee Exp $
 *
 */

#ifndef LQ_GETBYTE_H
# define LQ_GETBYTE_H 1

#define LQTp_GetByte(db, WID, sp, Blockp, BlockLength, NextBlock) \
    ( \
	(*(sp) - (unsigned char *) *(Blockp) >= *(BlockLength)) \
    ? \
       LQTp__GetByte(db, WID, sp, Blockp, BlockLength, NextBlock) \
    : \
	*((*(sp))++) \
    )

#define LQTp_GetLong(db, WID, sp, Blockp, BlockLength, NextBlock) \
    ( \
	(*(sp) - (unsigned char *) *(Blockp) >= *(BlockLength) || \
	 ((**(sp)) & 0200) ) /* i.e. the long needs more than one byte */ \
    ? \
	LQTp__GetLong(db, WID, sp, Blockp, BlockLength, NextBlock) \
    : \
	*((*(sp))++) \
    )

#endif /* LQ_GETBYTE_H */
