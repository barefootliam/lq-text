/* we can use strerror() instead of errno */
#define HAVE_STRERROR 1

/* declare strlen(), strcpy(), etc.: */
#define HAVE_STRING_H 1

/* declare unix system calls: */
#define HAVE_UNISTD_H 1

/* declare standard C library functions: */
#define HAVE_STDLIB_H 1

/* max unsigned int, etc: */
#define HAVE_LIMITS_H 1

/* symbolic constants for arguments to open(): */
#define HAVE_FCNTL_H

/* symbolic constants for whence argument to lseek: */
#define HAVE_SEEK_SET

/* the system bcopy() handles overlapping areas: */
#define HAVE_BCOPY 1

/* How to declare signal(): */
#define SIGTYPEDEF typedef void (* SIGTYPE)();

/* sys_siglist[] is available: */
#undef HAVE_SIGLIST

/* Default file viewer: */
#define DEFAULT_PAGER "less"

