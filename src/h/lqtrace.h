#ifndef LQTRACE_ALL
/* trace.h -- Copyright 1994, 1996 Liam R. E. Quin.
 * All Rights Reserved.
 * This code is NOT in the public domain.
 * See the file COPYRIGHT for full details.
 *
 * $Id: lqtrace.h,v 1.7 2019/04/21 06:04:30 lee Exp $
 *
 * Trace flags for lq-text
 */

#define LQTRACE_ALL		000000000000L

#define LQTRACE_VERBOSE		000000000001L /* progname -v */
#define LQTRACE_DEBUG		000000000002L /* progname -vv, or -t2 */

#define LQTRACE_READBLOCK	000000000004L
#define LQTRACE_WRITEBLOCK	000000000010L
#define LQTRACE_PUTPLACES	000000000020L
#define LQTRACE_GETPLACES	000000000040L

#define LQTRACE_WORDROOT	000000000100L
#define LQTRACE_READWORD	000000000200L
#define LQTRACE_ADDWORD		000000000400L

#define LQTRACE_ABORT_ON_ERROR	000000001000L
#define LQTRACE_CLOSE_ON_ERROR	000000002000L /* should this be default? */
#define LQTRACE_BLOCKDUMP	000000004000L

#define LQTRACE_FINDFILE	000000010000L
#define LQTRACE_WORDINFO	000000020000L
#define LQTRACE_READAFTERWRITE	000000040000L

#define LQTRACE_BLOCKCACHE	000000100000L
#define LQTRACE_WIDCACHE	000000200000L
#define LQTRACE_FREEBLOCKS	000000400000L

#define LQTRACE_FID_ALLOC	000001000000L

#define LQTRACE_FINDMATCH	000002000000L
#define LQTRACE_MAKE_PHRASE	000004000000L
#define LQTRACE_MATCH_PHRASE	000010000000L

#define LQTRACE_LASTBLOCK	000020000000L
#define LQTRACE_TERM_EXPANSION	000040000000L

#define LQTRACE_FILTER_DATA	000100000000L

typedef unsigned long t_TraceFlag;

extern t_TraceFlag LQTp_AsciiTraceLevel;

/* This should really be an inline function, but it is used so much that
 * the extra optimisation saves nearly 9% of runtime.
 */
#define LQT_TraceFlagsSet(flags) \
    ( \
	((flags) & LQTp_AsciiTraceLevel) ? ( \
	     ((flags) & LQTp_AsciiTraceLevel) == (flags) \
	) : 0 \
    ) \


/* LQT_TraceFlagsSet(flags) (((flags) & LQTp_AsciiTraceLevel) == (flags)) */

#ifndef LIBRARY
# include "api.h"
#endif

LIBRARY int LQTp_TraceFlagsSet(
#ifdef HAVE_PROTO
    t_TraceFlag flags
#endif
);

API int LQT_ForEachTraceFlag(
#ifdef HAVE_PROTO
    void CallMe(
	char *Name,
	unsigned int Value,
	int isSet
    )
#endif
);

API char *LQT_GetTraceFlagsAsString(
#ifdef HAVE_PROTO
    /* empty */
#endif
);

API char *LQT_GetGivenTraceFlagsAsString(
#ifdef HAVE_PROTO
    t_TraceFlag Flags
#endif
);

API t_TraceFlag LQT_GetTraceFlags(
#ifdef HAVE_PROTO
    /* empty */
#endif
);

API t_TraceFlag LQT_SetTraceFlag(
#ifdef HAVE_PROTO
    t_TraceFlag theFlag
#endif
);

API int LQT_UnSetTraceFlag(
#ifdef HAVE_PROTO
    t_TraceFlag theFlag
#endif
);

API char *LQT_SetTraceFlagsFromString(
#ifdef HAVE_PROTO
    char *theString
#endif
);

API void LQT_Trace(
#ifdef HAVE_PROTO
    t_TraceFlag Flags,
    char *Format,
    ...
#endif
);

#endif /*!LQTRACE_ALL*/
