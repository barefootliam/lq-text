/* globals.h -- Copyright 1989, 1994, 1996, 2005 Liam R. E. Quin.
 * All Rights Reserved.
 * This code is NOT in the public domain.
 * See the file COPYRIGHT for full details.
 *
 * $Id: globals.h,v 1.19 2019/04/21 06:04:30 lee Exp $
 *
 */

/* globals.h -- declarations of globally accessible variables, and also
 * of configurable parameters.
 *
 * Some of the configuation options might be given in ../Makefile, so
 * you must check in there too.
 *
 * Everything that includes this file must be linked with Defaults.c
 */

#ifndef API
# include "api.h"
#endif

#include "port.h"

/* 
 * DOCPATH gives the list of directories in which to search in order
 * to find files to retrieve and to index.  The default can be wired
 * in here, or can be simply "." (in which case relative pathnames will
 * be from wherever one invokes the commands, and absolute pathnames
 * will be absolute.  For example,
 * #define DFLTDOCPATH "/usr/man:."
 * In any case, it can be overridden by a DOCPATH line in the configuration
 * file for a given database (README in the database directory), and also
 * by an environment variable DOCPATH (the latter taking precedence over
 * the former).
 *
 * Use ((char *) 0) to disable the default -- in this case, you always have
 * to give one, either with the $DOCPATH variable or in the database file.
 *
 */
#ifndef DFLTDOCPATH
# define DFLTDOCPATH ((char *) 0)
#endif

/* LQTEXTDIR: if the programs can't find the directory to use -- i.e.,
 * there was no -d option and $(LQTEXTDIR) is unset, we either
 * look in UNDERHOME (if that was defined here) or in wherever LQTEXTDIR
 * is defined to point.
 */
#ifndef LQTEXTDIR
# define LQTEXTDIR "/var/spool/lqtextdir"
#endif

/* If UNDERHOME is set, look there for a directory -- e.g. 
 * #define UNDERHOME "sockdrawer"
 * would make lqtext programs look for a directory something like
 * /users/liam/sockdrawer
 * (where /users/liam is my login directory)
 */
#ifndef UNDERHOME
# define UNDERHOME "LQTEXTDIR"
#endif


#ifndef LQ_WORDRULES_H
  /* Note: h/wordrules.h defines the definition of a word; you may want
   * to look at it.
   * Currently, there are quite a few things in there that can only be
   * changed by recompiling everything; this may be changed in the future.
   */
# include "wordrules.h"
#endif

#ifndef LQC_DEFAULT_WORD_FLAGS
/* These flags are described in wordrules.h, and are declared in
 * liblqtext/wflags.c in the form used here.
 *
 * For each word in the database, lq-text will record all the information
 * requested here.  It used to record all of this in all cases, but not
 * everyone wanted it all, and you get a smaller database without it.
 * However, the fewer flags are stored, the less precise matching you get.
 *
 * If you omit LastInBlock, you won't be able to match phrases that cross
 * DATABLOCKSIZE byte block boundaries.  This is normally 64 bytes, so you
 * certainly want this, unless you know what you're doing!
 *
 * You can choose any combination of
 *   Plural,UpperCase,Possessive,
 *   LastHadPunct,LastWasCommon,LastHadLetters,
 *   NextHasPunct,NextIsCommon"
 *   HasStuffBefore,
 *   LastInBlock
 *   Check in liblqtext/wflags.c for others.
 *
 *   "All" gets you everything.
 *
 * For XML documents you also need
 *   XMLAttributeValue and XMLEntityReference
 */
# define LQC_DEFAULT_WORD_FLAGS "All"
#endif

/***
 *** If you want to change anything beyond here...
 ***
 *** well, you can.
 *** After all, it's your copy.
 ***
 *** But don't come running back to me if it doesn't work!
 *** At least not until you have tried
 ***	+  understanding what the problem is;
 ***	+  looking at the source to see why;
 ***	+  fixing the problem;
 ***	+  taking off your shoes and socks and grinning for a while.
 ***
 *** Liam.
 ***
 ***/

/* WIDBLOCKSIZE and BLOCKSIZE can be defined in the database itself
 * (set them in the config.txt/README file) so these are just defaults.
 */

#ifndef WIDBLOCKSIZE
# define WIDBLOCKSIZE	64
/* WIDBLOCKSIZE absolutely must be large enough to fit at least one byte
 * of actual data, or all hell will break loose.
 * (actually that could be fixed...).
 * In any case, it has to contain (apart from the >= 1 byte of data):
 * + the length count (1 byte) and the word itself (no null on the end)
 * + the block number in the database (1..5 bytes)
 * + the number of matches (1..5 bytes)
 *
 * It helps efficiency slightly if these are a power of two
 * bytes, as then they never cross Unix block boundaries.
 *
 */
#endif

#ifndef BLOCKSIZE
#define BLOCKSIZE	 64
/* BLOCKSIZE is the size of blocks in the data file.  There are several
 * tradeoffs:
 * + there is a 4-bytes-per-block overhead for list pointers, so it's
 *   a good idea to make them large
 * + there's a bit of work involved in fetching the blocks, so things go
 *   faster if they're larger...
 * + many blocks are not full, so it's a good idea to make them small.
 *   On average, a little over (BLOCKSIZE - 4) / 2 bytes are wasted for
 *   every word chain.
 * + since many of the blocks are not full, it's a good idea to make them
 *   small, minimising the amount of extra data that gets copied around by
 *   the Unix kernel.  If the blocks are smaller it'll go faster...
 *
 * It helps efficiency very, very slightly if these are a power of two
 * bytes, as then they never cross Unix block boundaries.
 * Good values are 16, 24, 32, 48, 64.  Larger than 64 will waste a lot
 * of space, and things will slow down.  The lq-text library actually
 * allocates these blocks in chunks of 256 of them when it can, so the
 * I/O is always on 1K or 8K boundaries, which helps a lot.
 *
 */
#endif

/**** Some useful macros */

/* STREQ(a,b) is much faster than strcmp() in the (common) case that the
 * first character of the strings differ.
 * It is due (as far as I know) to Henry Spencer, at the University of
 * Toronto Zoology Dept., utzoo!henry <henry@zoo.utoronto.ca>
 * Actually it's no longer faster in most cases if you have a C compiler
 * that optimises out the call to the function.  E.g. gcc will do this
 * on at least Intel x86 systems in most cases.
 */
#ifdef OLD_AND_STUPID
#ifndef STREQ
# define STREQ(henry,utzoo) (*(henry) == *(utzoo) && !strcmp(henry, utzoo))
#endif

#ifndef STRNCMP
    /* note: this macro considers (strncmp(x, y, 0) to succeed for all
     * values of x and y, whether or not they are plausible.
     * STRNCMP returns 0 if the strings are the same.
     */
#define STRNCMP(henry, utzoo, n) \
    ( \
	((n) > 0) ? ( \
	    (*(henry) == *(utzoo)) ? \
		strncmp(henry,utzoo,n) : \
		(((int)(unsigned char) *(henry)) - ((int)(unsigned char) *(utzoo))) \
	    ) : 0 \
    )

#endif
#ifndef STRCMP
# define STRCMP(henry, utzoo) \
     ( (*(henry) == *(utzoo)) ? strcmp(henry,utzoo) : \
       ( ( (int)(unsigned char) *(henry)) - ((int)(unsigned char) *(utzoo)) ))
    /* Note: the double casts are in case there's an 8-bit value, and chars
     * are signed on the local machine, both of which do happen in practice.
     * Thanks to msb@sq.com (Mark Brader) for pointing this out.
     */
#endif
#else
#define STREQ(a, b) (!strcmp(a, b))
#define STRCMP strcmp
#define STRNCMP strncmp 

#endif

 
/* Inline functions are functions that get expanded inline during
 * compilation -- sort of like macros with real local arguments.
 * Not all compilers support them.
 *
 * This is mostly for numbers.c and numbers.h, which contain two functions
 * that may be called (literally) billions of times.  They never seem to
 * get tired, though.
 *
 * TODO: make `configure' test for this.
 */
#ifdef __GNUC__
# define HAVE_INLINE
# define INLINE static inline
# define PRIVATE_INLINE static inline
#else
# define INLINE /* not supported */
# define PRIVATE_INLINE static
#endif

#if defined(__GNUC__)||defined(__cplusplus) || defined(__STDC__)
# define CONST const
#else
# define CONST /* const not supported */
#endif

#ifdef DefineThem
# define DECL(name, type, value)   type name = value
# define EXTERN /* just define them please */
#else
# define EXTERN extern /* declare but do not define */
# define DECL(name, type, value)   EXTERN type name
#endif

/****/

/* Now declare (or define) things: */

extern char *progname; /* from progname.c, for error messages */

