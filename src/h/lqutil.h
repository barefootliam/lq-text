#ifndef LQ_UTIL_H
# define LQ_UTIL_H 1

/* $Id: lqutil.h,v 1.7 2019/04/21 06:04:30 lee Exp $
 */

# ifndef API
#  include "api.h"
# endif

#ifndef LQ_RANGE_H
# include "range.h"
#endif


/**** [1] String Manipulation Functions ****/


/** LQU_cknatstr(s) returns 1 if s points to a string representing
 ** a natural number (i.e. an integer).
 ** The string must match the regular expression
 **	/^[ 	]*-?[0-9]+[ 	]*$/
 ** (where [ 	] represents any whitespace)
 **/

API int LQU_cknatstr(
# ifdef HAVE_PROTO
    CONST char *theString
# endif /*HAVE_PROTO*/
);

/** LQU_DownCase(s) returns a malloc()'d copy of s in which any upper-case
 ** characters have been converted to their lower-case equivalents.
 ** LQU_DownCase() uses isascii() and isupper() to do this.  However,
 ** it uses isalnum() rather than isascii() before checking whether a
 ** character is upper-case or not.
 ** This will work if set_locale() has been called.
 **/
API char *LQU_DownCase(
# ifdef HAVE_PROTO
    CONST char *theString
# endif /*HAVE_PROTO*/
);

API char *LQU_joinstr2(
# ifdef HAVE_PROTO
    CONST char *s1,
    CONST char *s2
# endif /*HAVE_PROTO*/
);

API char *LQU_joinstr3(
# ifdef HAVE_PROTO
    CONST char *s1,
    CONST char *s2,
    CONST char *s3
# endif /*HAVE_PROTO*/
);

API int LQU_StringContainedIn(
# ifdef HAVE_PROTO
    CONST char *ShortString,
    CONST char *LongString
# endif /*HAVE_PROTO*/
);

API int LQU_LargerThanRangeTop(
# ifdef HAVE_PROTO
    CONST int n,
    CONST t_Range *Range
# endif /*HAVE_PROTO*/
);

API int LQU_NumberWithinRange(
# ifdef HAVE_PROTO
    CONST int n,
    CONST t_Range *Range
# endif /*HAVE_PROTO*/
);

API t_Range *LQU_StringToRange(
# ifdef HAVE_PROTO
    CONST char *String
# endif /*HAVE_PROTO*/
);

API char *LQU_ReverseString(
# ifdef HAVE_PROTO
    char *start,
    char *end,
    int type
# endif /*HAVE_PROTO*/
);

/**** [2] File Manipulation Routines ****/


/** LQU_Eopen() calls open(2) to open a file.  If the open fails, LQU_Eopen()
 ** calls Error() with the given Severity, or'd with E_SYS, creating
 ** an error message including "What", so that
 ** int fd = LQU_Eopen(E_FATAL, "foo", "output listing", O_WRONLY|O_CREAT, 0744);
 ** might print
 ** progname: fatal error: can't open foo (output listing):
 ** progname: fatal error: can't create files in the current directory (E_PERM)
 ** and exit.
 **/
API int LQU_Eopen(
# ifdef HAVE_PROTO
    int Severity,
    CONST char *Name,
    CONST char *What,
    int Flags,
    int Modes
# endif /*HAVE_PROTO*/
);

/** LQU_fEopen() is the stdio equivalent of LQU_Eopen().
 **/
API FILE *LQU_fEopen(
# ifdef HAVE_PROTO
    int Severity,
    CONST char *Name,
    CONST char *What,
    CONST char *Mode
# endif /*HAVE_PROTO*/
);

API void LQU_fEclose(
# ifdef HAVE_PROTO
    int Severity,
    FILE *fp,
    CONST char *Name,
    CONST char *What
# endif /*HAVE_PROTO*/
);

API int LQU_Eread(
# ifdef HAVE_PROTO
    int Severity,
    CONST char *Name,
    CONST char *What,
    int fd,
    char *Buffer,
    int ByteCount
# endif /*HAVE_PROTO*/
);


/** LQU_IsDir() returns non-zero if Dir points to the name of a directory.
 ** Returns 0 otherwise.
 ** NOTE: in the future, this routine may return -1 on error.
 **/
API int LQU_IsDir(
# ifdef HAVE_PROTO
    CONST char *Dir
# endif /*HAVE_PROTO*/
);

/** LQU_IsFile() returns non-zero if Path points to the name of a regular file.
 ** Returns 0 otherwise.
 ** NOTE: in the future, this routine may return -1 on error.
 ** A special character device or a broken symbolic link do not count
 ** as regular files; see <sys/stat.h> for IF_REG.
 **/
API int LQU_IsFile(
# ifdef HAVE_PROTO
    CONST char *Path
# endif /*HAVE_PROTO*/
);

/** LQU_IsNonEmptyFile() is like LQU_IsFile, but only returns 1 if the
 ** named file contains data.  It returns 0 otherwise.
 ** NOTE: in the future, this routine may return -1 on error.
 ** A special character device or a broken symbolic link do not count
 ** as regular files; see <sys/stat.h> for IF_REG.
 **/
API int LQU_IsNonEmptyFile(
# ifdef HAVE_PROTO
    CONST char *Path
# endif /*HAVE_PROTO*/
);

API off_t LQU_Elseek(
# ifdef HAVE_PROTO
    int Severity,
    CONST char *Name,
    CONST char *What,
    int fd,
    off_t Position,
    int Whence
# endif /*HAVE_PROTO*/
);

API char *LQU_GetLoginDirectory(
# ifdef HAVE_PROTO
    FUNCTION_WITHOUT_ARGUMENTS
# endif /*HAVE_PROTO*/
);

/* Flags for LQU_fReadFile */

#define LQUF_IGNBLANKS	  001 /* throw away blank lines */
#define LQUF_IGNSPACES	  002 /* discard leading and trailing spaces */
#define LQUF_IGNHASH  	  004 /* discard LEADING comments (# at line start) */
#define LQUF_ESCAPEOK 	  010 /* accept \# and \\ as # and \ */
#define LQUF_IGNALLHASH   020 /* discard ALL comments (# anywhere) */
#define LQUF_IGNNEWLINE   040 /* discard trailing newline */
#define LQUF_NORMAL \
    (LQUF_IGNBLANKS|LQUF_IGNSPACES|LQUF_IGNHASH|LQUF_ESCAPEOK|LQUF_IGNNEWLINE)


/** File and Line Reading Routines:
 **
 **/

API long LQU_ReadFile(
#ifdef HAVE_PROTO
    int Severity,
    CONST char *Name,
    CONST char *What,
    char ***Lines,
    int Flags
#endif
);

API long LQU_fReadFile(
#ifdef HAVE_PROTO
    int S,
    FILE *f,
    CONST char *Name,
    CONST char *What,
    char ***Lines,
    int Flags
#endif
);

API int LQU_fReadLine(
#ifdef HAVE_PROTO
    FILE *f,
    char **Linep,
    int Flags
#endif
);

API char * LQU_StealReadLineBuffer(
#ifdef HAVE_PROTO
    FUNCTION_WITHOUT_ARGUMENTS
#endif
);

/** Character set and string stuff
 **
 **/
API unsigned char *
LQU_UTF8FirstNonPartialOctet(
#ifdef HAVE_PROTO
    unsigned char *Start
#endif
);

API unsigned long
LQU_UTF8CharLength(
#ifdef HAVE_PROTO
    CONST unsigned char *String
#endif
);

/** User preference and login directory:
 **
 **/
#endif /* LQ_UTIL_H */

