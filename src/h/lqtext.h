/* lqtext.h Copyright Liam R. E. Quin 1995, 1996
 * All Rigts Reserved.
 * This code is NOT in the public domain.
 * See the file COPYRIGHT for full details.
 *
 * $Id: lqtext.h,v 1.3 1996/05/15 21:51:45 lee Exp $
 *
 */

/* You can use this header file instead of including exactly those header
 * files that you need.
 * It makes the source easier to read at the cost of slightly slower
 * compilation times.
 */

#include "error.h"
#include "globals.h"
#ifndef FILE
# include <stdio.h> /* stderr, also for fileinfo.h */
#endif
#include <sys/types.h>
#ifdef HAVE_SYSV_FCNTL_H
# include <sys/file.h>
# include <sys/stat.h>
#endif
#ifdef HAVE_FCNTL_H
#include <fcntl.h>
#endif
#include <malloc.h>
#ifdef HAVE_STRING_H
# include <string.h>
#else
# include <strings.h>
#endif
#ifdef HAVE_STDLIB_H
# include <stdlib.h>
#endif
#ifdef HAVE_UNISTD_H
# include <unistd.h>
#endif

#include "emalloc.h" /* for efree() */
#include "fileinfo.h" /* for wordinfo.h */
#include "wordinfo.h"
#include "pblock.h"
#include "phrase.h"
#include "lqutil.h"
#include "liblqtext.h"
#include "lqtrace.h"

