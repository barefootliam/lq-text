/* This header file gets updated with every distributed change to any source
 * file anywhere in the lq-text package.
 *
 * Probably it should be renamed patchlevel.h these days.
 *
 */

#define LQTEXTREVISION "Release 1.19"

/* the index version: */

#define LQ_MAJOR_VERSION 2 /* incopmpatible with earlier numbers */
#define LQ_MINOR_VERSION 1

#ifdef WIDINBLOCK
# define LQ_FLAG_VALUE 3
#else
# define LQ_FLAG_VALUE 1
#endif

/* $Id: revision.h,v 1.21 2019/04/21 06:04:30 lee Exp $
 * The version of this file isn't the same as the version of
 * the package as a whole, of course.
 */
