/* smalldb.h -- Copyright 1989, 1995, 1995 Liam R. E. Quin.
 * All Rights Reserved.
 * This code is NOT in the public domain.
 * See the file COPYRIGHT for full details.
 */

/* $Id: smalldb.h,v 1.10 2019/04/21 06:04:30 lee Exp $
 */

#ifndef LQ_SMALLDB_H
# define LQ_SMALLDB_H 1

#ifndef O_RDWR
    /* You should include fcntl.h before this file. */
# include <fcntl.h>
#endif

#ifdef sdbm
# include "sdbm.h"
# define FoundDbmOK
#endif

#ifdef bsddb
# include "ndbm.h"
# define FoundDbmOK
#endif

#ifdef dbnative
# include <sys/types.h>
# include <limits.h>

# ifdef USE_DB_185_H
#  include <db_185.h>
#  define DB_VERSION_MAJOR 1
# else
#  ifdef USE_DB_4_H
#   include <db4/db.h>
#   define DB_VERSION_MAJOR 4
#  else
#   include <db53/db.h>
#   define DB_VERSION_MAJOR 5
#  endif
# endif

# define FoundDbmOK
# undef DBM_REPLACE
# define DBM_REPLACE  0 /* REPLACE is default behaviour for db */

#if 0
typedef struct {
    unsigned char *dptr;
    size_t dsize;
} datum;
#endif

/* DBM is the type returned by the various dbm hash packages */
# undef DBM
# define DBM DB

# undef dbm_fetch
# define dbm_fetch(db,key) dbm_dbfetch(db, &(key))

#if DB_VERSION_MAJOR>3
# define dbm_delete(db,key) ((db)->del(db, (DB_TXN *)0, &(key), (u_int32_t) 0))
#else
# define dbm_delete(db,key) ((db)->del(db, &(key), (u_int32_t) 0))
#endif

# define dbm_store(db,key,value,flg) lqdbnativestore(db,&(key),&(value),flg)
# define dbm_close(db) (db)->close(db, 0)

LIBRARY DBT dbm_dbfetch(const DB *db, DBT *key);
LIBRARY void lqnativestore(DB *, DBT *, DBT *, u_int32_t);
LIBRARY int lqdbnativestore(
# ifdef HAVE_PROTO
    DB *db,
    DBT *key,
    DBT *value,
    u_int32_t flags
# endif
);
LIBRARY int kvpdbg(char *,DBT);

LIBRARY char *LQT_dberror(
# ifdef HAVE_PROTO
    int s_val
#endif
);

# define datum DBT
# define dptr data
# define dsize size
#endif

#ifndef FoundDbmOK
you have to define one of the dbm files;
#endif

#ifndef O_RDWR
# include <fcntl.h>
#endif

#if 0
/* #ifdef HAVE_STRING_H
 * # include <string.h>
 * #else
 * # include <strings.h>
 * #endif
 */
#endif

#endif /* LQ_SMALLDB_H */
