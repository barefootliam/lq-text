/* phrase.h -- Copyright 1989, 1994, 1996 Liam R. E. Quin.
 * All Rights Reserved.
 * This code is NOT in the public domain.
 * See the file COPYRIGHT for full details.
 */

/* LQ-Text -- Liam's Text Retrieval Package
 * Liam R. Quin, September 1989, and later...
 *
 * phrase.h -- data structures for handling entire phrases
 *
 * $Id: phrase.h,v 1.4 1996/05/15 21:56:52 lee Exp $
 *
 * Represent a Phrase as a linked list of WordInfo pointers, plus a list
 * of matches.
 */

#ifndef LQ_PHRASE_H
# define LQ_PHRASE_H

typedef struct s_PhraseItem {
    t_WordInfo *Word;
    struct s_PhraseItem *Next;
    unsigned long SearchIndex; /* For phrase-matching */
    char *WordStart; /* pointer into original phrase */
} t_PhraseItem;

typedef enum {
    PCM_AnyCase, /* Ignore case entirely */
    PCM_HalfCase, /* Upper only matches upper; lower matches either */
    PCM_SameCase, /* Exact matching */
} t_PhraseCaseMatch;

typedef struct s_Match {
    t_WID WID;
    t_WordPlace *Where;
    struct s_Match *Next;
} t_Match;

typedef struct s_MatchList {
    t_Match *Match;
    struct s_MatchList *Next;
} t_MatchList;


typedef struct s_Phrase {
    t_PhraseItem *Words; /* list of words and pblocks */
    char *OriginalString; /* as supplied by the user */
    char *ModifiedString; /* after deleting short/unindexed words */
    unsigned long NumberOfMatches;
    t_MatchList *Matches;
    struct s_Phrase *Next; /* for use when we're in a list of phrases... */
    unsigned int HasUnknownWords;
} t_Phrase;

typedef enum {
    e_First, e_Middle, e_Last
} t_Position;

#define LQMATCH_ACCEPT		001
#define LQMATCH_REJECT		002
#define LQMATCH_QUIT		004
#define LQMATCH_NEXT_FILE	010

/* for document similarity: */
typedef struct {
    char *PhraseStart;
    char *PhraseEnd;
    t_Phrase *Phrase;
} t_PhraseElement;

#endif
