/* wmoffset.h -- Copyright 1993, 1994, 1996 Liam R. E. Quin.
 * All Rights Reserved.
 * This code is NOT in the public domain.
 * See the file COPYRIGHT for full details.
 *
 * $Id: wmoffset.h,v 1.4 1996/06/11 15:49:02 lee Exp $
 */
 
#ifndef LQ_WMOFFSET_H
# define LQ_WMOFFSET_H 1

typedef struct {
    char *Start;
    char *End;
} t_OffsetPair;

API t_OffsetPair *LQT_FindMatchEnds(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *db,
    char *Buffer,
    unsigned int Length,
    char *StartBlock,
    unsigned long BIF,
    unsigned long WIB,
    int NumberOfWords
#endif
);

#endif /* LQ_WMOFFSET_H */
