/* lqconfig.h -- Copyright 1995, 1996 Liam R. Quin.
 * All Rights Reserved.
 * This code is NOT in the public domain.
 * See the file COPYRIGHT for full details.
 *
 * $Id: lqconfig.h,v 1.4 2019/04/21 06:04:30 lee Exp $
 *
 */

#ifndef LQ_CONFIG_H
# define LQ_CONFIG_H 1

/* Interface to runtime configuration;
 * currently, this uses hard wired command-line options and
 * a hand-parsed "readme" file in $LQTEXTDIR.
 * this will change in the next release.
 *
 * Please don't change this file without asking lee@sq.com first, as
 * he might already have done it, and if you ask him, perhaps he'll send
 * you the latest version!
 */

typedef enum {
    LQCF_Cmdline,
    LQCF_Envvar,
    LQCF_Default, /* use the default */
    LQCF_File, /* from the config file */
    LQCF_LoginDir, /* from your home directory */
    LQCF_CurrentDir, /* found here */
    LQCF_None /* don't use any at all */
} t_FromWhere;

typedef struct {
    char *Value;
    t_FromWhere HowItWasSet;
    char *Name;
} t_lqdbStringOption;

typedef struct {
    int Value;
    t_FromWhere HowItWasSet;
    char *Name;
} t_lqdbIntegerOption;

#define LQCF_SetOptionDefault(option, value, name) \
    (option)->HowItWasSet = LQCF_Default; \
    (option)->Value = (value); \
    (option)->Name = (name) 

typedef struct {
    t_lqdbStringOption directory;
    t_lqdbStringOption stoplist;
    t_lqdbStringOption filesearchpath;
    t_lqdbIntegerOption phrasematchlevel; /* actually a t_PhraseMatch! */
    t_lqdbStringOption title;
    t_lqdbStringOption administrator_email;
    t_lqdbStringOption data_owner_email;
    t_lqdbStringOption cgi_url;
    t_lqdbStringOption wordflags; /* or should this be a flags object?? */
    t_lqdbStringOption locale;
} t_lqdbOptions;

#endif /* LQ_CONFIG_H */
