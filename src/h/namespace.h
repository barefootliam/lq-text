/* namespace.h -- Copyright 1995, 1996 Liam R. Quin.  All Rights Reserved.
 * This code is NOT in the public domain.
 * See the file COPYRIGHT for full details.
 */

/* $Id: namespace.h,v 1.5 1996/08/14 16:51:01 lee Exp $ */

typedef enum {
    LQU_NameType_Integer,
    LQU_NameType_Long,
    LQU_NameType_String,
    LQU_NameType_Character,
    LQU_NameType_NameRef,
    LQU_NameType_NameSpace
} t_NameType;

struct s_Name;
typedef struct s_Name **t_NameRef;

typedef struct s_Name {
    char *Name;
    void *Variable;
    t_NameType Type;
    void *Argument;
    t_NameRef MyRef;
    char *Description;
    unsigned char VariableAllocatedByLibrary : 1;
    unsigned char VariablePointsToFunction : 1;
    unsigned char FunctionTakesArgument : 1;
} t_Name;

/* Documentation for these macros appears in liblqutil/namespace.c,
 * where the macros are also defined as functions.
 */

/* If you change this macro, also change the corresponding function defined
 * in liblqutil/namespace.c
 */
#define LQU_NameRefIsValid(NameSpace, NameRef) \
    ((NameSpace) && (NameRef) && (*NameRef))

/* If you change this macro, also change the corresponding function defined
 * in liblqutil/namespace.c
 */
#define LQU_GetNameFromNameRef(NameRef) \
    ((*(NameRef))->Name)

/* If you change this macro, also change the corresponding function defined
 * in liblqutil/namespace.c
 */
#define LQU_GetTypeFromNameRef(NameRef) \
    ((*(NameRef))->Type)

/* If you change this macro, also change the corresponding function defined
 * in liblqutil/namespace.c
 */
#define LQU_GetVariableFromNameRef(NameRef) \
    ((*(NameRef))->Variable)

/* If you change this macro, also change the corresponding function defined
 * in liblqutil/namespace.c
 */
#define LQU_GetDescriptionFromNameRef(NameRef) \
    ((*(NameRef))->Description)

/* If you change this macro, also change the corresponding function defined
 * in liblqutil/namespace.c
 */
#define LQU_NameRefVariableAllocatedByLibrary(NameRef) \
    ((*(NameRef))->VariableAllocatedByLibrary == 1)

/* If you change this macro, also change the corresponding function defined
 * in liblqutil/namespace.c
 */
#define LQU_SetNameRefVariableAllocatedByLibrary(NameRef) \
    ((*(NameRef))->VariableAllocatedByLibrary = 1)

/* If you change this macro, also change the corresponding function defined
 * in liblqutil/namespace.c
 */
#define LQU_NameRefVariablePointsToFunction(NameRef) \
    ((*(NameRef))->VariablePointsToFunction == 1)

/* If you change this macro, also change the corresponding function defined
 * in liblqutil/namespace.c
 */
#define LQU_SetNameRefVariablePointsToFunction(NameRef) \
    ((*(NameRef))->VariablePointsToFunction = 1)

/* If you change this macro, also change the corresponding function defined
 * in liblqutil/namespace.c
 */
#define LQU_NameRefFunctionTakesArgument(NameRef) \
    ((*(NameRef))->FunctionTakesArgument == 1)

/* If you change this macro, also change the corresponding function defined
 * in liblqutil/namespace.c
 */
#define LQU_SetNameRefFunctionTakesArgument(NameRef) \
    ((*(NameRef))->FunctionTakesArgument = 1)


typedef struct s_NameSpace {
    /* Do not do anything that depends on the size or layout of
     * a t_NameSpace, because it is certain to change.
     * Use the API instead, which is more likely to be stable -- and
     * even if the API is unstable, at least you will have been using a
     * documented interface, so changing should be easy.
     */

     struct s_NameSpace *MoreNames; /* first for efficiency */

     /* in the future, allow for a stack of name spaces: */
     /* t_NameSpace *Up, *LocalStack  */

     char *Name; /* name of this NameSpace */
     char *Description;

     unsigned char NamesAreCaseSensitive : 1;
     unsigned char FollowNestedReferences : 1;

     t_Name *Names[30]; /* pointers to the names */
} t_NameSpace;

/* If you change this macro, also change the corresponding function defined
 * in liblqutil/namespace.c
 */
#define LQU_GetDescriptionFromNameSpace(NameSpace) \
    ((NameSpace)->Description)

typedef struct {
    char *Name;
    t_NameType Type;
    void *Variable;
    unsigned char Flags; /* for if it's a function */
    void *Argument;
} t_NameSpaceTableEntry;

#define LQU_NS_Function			01
#define LQU_NS_FunctionNoArguments	01 /* i.e. the same */
#define LQU_NS_FunctionOneArgument	03 /* i.e. same but add 2 */

typedef t_NameSpaceTableEntry *t_NameSpaceTable;

API t_NameRef LQU_StringToNameRef(
#ifdef HAVE_PROTO
    t_NameSpace *NameSpace,
    char *theName
#endif
);

API t_NameSpace *LQU_NameSpaceTableToNameSpace(
#ifdef HAVE_PROTO
    char *Name,
    t_NameSpaceTable theTable
#endif
);

API t_NameRef LQU_SetNameVariable(
#ifdef HAVE_PROTO
    t_NameRef NameRef,
    void *Variable
#endif
);

API t_NameRef LQU_SetNameValue(
#ifdef HAVE_PROTO
    t_NameRef NameRef,
    void *Value
#endif
);

API void *LQU_GetValueFromNameRef(
#ifdef HAVE_PROTO
    t_NameRef NameRef
#endif
);


API t_NameRef LQU_SetNameTypeAndVariable(
#ifdef HAVE_PROTO
    t_NameRef NameRef,
    t_NameType NameType,
    void *Variable
#endif
);

API t_NameRef LQU_FirstNameRef(
#ifdef HAVE_PROTO
    t_NameSpace *NameSpace
#endif
);

API t_NameRef LQU_NextNameRef(
#ifdef HAVE_PROTO
    t_NameSpace *NameSpace,
    t_NameRef NameRef
#endif
);

API char *LQU_NameRefToString(
#ifdef HAVE_PROTO
    t_NameRef NameRef
#endif
);

API char *LQU_NameRefValueToString(
#ifdef HAVE_PROTO
    t_NameRef NameRef
#endif
);

API char *LQU_NameRefTypeToString(
#ifdef HAVE_PROTO
    t_NameType NameType
#endif
);
