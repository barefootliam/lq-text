/* blkheader.h -- Copyright 1989, 1992-1994, 1996 Liam R. E. Quin.
 * All Rights Reserved.
 * This code is NOT in the public domain.
 * See the file COPYRIGHT for full details.
 *
 * (was called blockheader.h, but this was too long on SysV for RCS)
 *
 * $Id: blkheader.h,v 1.6 1996/05/15 19:25:04 lee Exp $
 */

#ifndef LQ_BLKHEADER_H
# define LQ_BLKHEADER_H

typedef struct {
    unsigned long NextOffset; /* a byte offset */
#ifdef WIDINBLOCK
    unsigned long WID;
#endif
    unsigned char NumberOfBlocks;
    unsigned char Data[1];
} t_BlockHeader;

#endif
