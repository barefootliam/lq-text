/* Header file for Ultrix only
 * $Id: ultrixhack.h,v 1.2 1992/03/30 21:38:05 lee Exp $
 */

#define ACS_BSSS '+'
#define ACS_SBSS '+'
#define ACS_SSBS '+'
#define ACS_SSSB '+'
#define ACS_HLINE '='
#define ACS_LARROW '>'
#define ACS_LLCORNER '+'
#define ACS_LRCORNER '+'
#define ACS_RARROW '<'
#define ACS_VLINE '|'
#define KEY_HELP 999 /* an unlikely key to be pressed */
