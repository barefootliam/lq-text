/* internal.h -- header file for internal use.
 *
 * $Header: /home/lee/src/lq-text-cvs/src/menu/RCS/internal.h,v 1.2 2019/04/21 06:09:02 lee Exp $
 *
 * $Log: internal.h,v $
 * Revision 1.2  2019/04/21 06:09:02  lee
 * for migration to CVS
 *
 * Revision 1.2  2001/05/31 03:50:13  liam
 * for release 1.17
 *
 * Revision 1.1  90/08/29  21:50:36  lee
 * Initial revision
 * 
 * Revision 2.1  89/08/07  13:49:27  lee
 * First fully working (V.3.2 only) release;
 * this is the baseline for future development.
 * 
 * Revision 1.1  89/07/27  11:39:40  lee
 * Initial revision
 * 
 *
 */

/* Thanks to Henry Spencer (utzoo!henry) for this: */
#ifndef STREQ
#define STREQ(boy,girl) ((*(boy)== *(girl))&&!strcmp((boy),(girl)))
#endif


/* SAVEMENULINE is set if you want to the menus to hang down from
 * the bar when selected.  (this is default)
 * Otherwise, define MENUTEXTOVERBAR and recompile...
 * the top line of the menu goes on screen row MENUTOP.
 * If MENUTOP is >= 2, the top line of the menu is boxed.  Otherwise,
 * the first item in the menu goes on the menu line, overwriting it.
 * Only use this if you have (ROWS - 1) items on each menu...
 */

#ifndef MENUTEXTOERBAR
# define SAVEMENULINE
#endif
#define SAVEMENULINE
#ifdef SAVEMENULINE
  /* MENUTOP is the line on which pull-down menus start. */
# ifndef MENUTOP
#  define MENUTOP 2
# endif /*!MENUTOP*/
# if MENUTOP>1
#  define MENUBOXONTOPLINE
# endif
#else
# define MENUTOP 1
#  undef MENUBOXONTOPLINE
#endif

#define new(type)	((type *) malloc(sizeof(type)))
