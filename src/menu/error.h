/* Codes that can be passed to error()
 *
 * $Header: /home/lee/src/lq-text-cvs/src/menu/RCS/error.h,v 1.2 2019/04/21 06:09:02 lee Exp $
 *
 * $Log: error.h,v $
 * Revision 1.2  2019/04/21 06:09:02  lee
 * for migration to CVS
 *
 * Revision 1.2  2001/05/31 03:50:13  liam
 * for release 1.17
 *
 * Revision 1.1  90/08/29  21:49:53  lee
 * Initial revision
 * 
 * Revision 2.1  89/08/07  13:48:38  lee
 * First fully working (V.3.2 only) release;
 * this is the baseline for future development.
 * 
 * Revision 1.1  89/07/28  19:11:06  lee
 * Initial revision
 * 
 *
 */

extern void error();

#define ERR_INTERNAL	01
#define ERR_MEMORY	02
#define ERR_FATAL	04
