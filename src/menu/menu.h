/* menu.h -- user programs should include this after curses.h 
 *
 * $Header: /home/lee/src/lq-text-cvs/src/menu/RCS/menu.h,v 1.2 2019/04/21 06:09:02 lee Exp $
 *
 * $Log: menu.h,v $
 * Revision 1.2  2019/04/21 06:09:02  lee
 * for migration to CVS
 *
 * Revision 1.2  2001/05/31 03:50:13  liam
 * for release 1.17
 *
 * Revision 1.1  90/08/29  21:50:50  lee
 * Initial revision
 * 
 * Revision 2.1  89/08/07  13:49:31  lee
 * First fully working (V.3.2 only) release;
 * this is the baseline for future development.
 * 
 * Revision 1.2  89/08/04  17:59:07  lee
 * Fully working with Basic Functionality.
 * Scrolling menubar, scrolling menus, moveable Info windows.
 * 
 * Revision 1.1  89/07/27  11:39:20  lee
 * Initial revision
 * 
 *
 */

/* for errors... -- set this whenever initscr() is in effect */
extern int InCurses;

/* A menu bar from a data structures (how quaint!) viewpoint:
 *
 *   File  Edit  *Implement*  Duration
 * -------------+---------------+------------------------------------
 *		| Tongs		|
 *		| *Whip******** |
 *		| Branding Iron |
 *		| Thumbscrew	|
 *		| Dead Fish	|
 *		+---------------+
 *
 * MenuBar ---> +-----------------------------------------------------
 *		|HowManyMenus = 4
 *		|SelectedMenu = 2
 *		|MenuBarId [internal] = 34 (say)
 *		|ScrollOffset = 0 // horizontal scrolling of bar
 *		|Menus--->an array of 4 menu structures:
 *		|	 
 *
 * MenuBar->Menus[2] looks like this:
 *		+-----------------------------------------------------
 *		|Name = "Implement"
 *		|HowManyItems = 5
 *		|NeedToBox = 1
 *		|IsSelected = 1 // it's highlighted in the MenuBar
 *		|TopLineOnScreen = 0 // for vertical scrolling
 *		|PositionInBar = 13 // characters from the left
 *		|Width = 14 // length of widest item
 *		|SelectedLine = 1 // they're numbered from 0
 *		|MenuId [private] = 4097 (say)
 *		|Description = "Choose a torture implement..."
 *		|Items--->an array of 5 items:
 *		|
 *
 * MenuBar->Menus[2]->Items[1] looks like this:
 *		+-----------------------------------------------------
 *		|Function-->points to a function to be called when
 *		|Name = "Whip"
 *		|NameLength = 4 // strlen(Name), for speed
 *		|Description = "Cat O' Nine Tails"
 *		|LongText = "Ouch" // passed to Function().
 */

typedef struct {
    int (*Function)();
    /* Function is called when the menu is selected.  If it is
     * zero, you just get a beep(); this is intended for testing.
     * The function is passed two arguments: a pointer to the menu
     * which contained this item, and the item number.  In the
     * "Whip" example above, it would be a pointer to the Implement
     * menu, and the number "1".
     * The LongText argumenht is intended for use by this function,
     * although what it does with it is undefined.  For example,
     * it could contain a shell command, or it could be a pointer
     * into a table of statements to be interpreted, or....
     */
    char *Name;
    /* Name is the string displayed in the menu itself.
     */
    unsigned short NameLength;
    /* NameLength must either be 0 or strlen(Name).  In the former case,
     * it will automatically be updated.  It is not clear that this is
     * a big saving, so it may well go way soon.
     */
    char *Description;
    /* pop-up box for help/explanation; when the item is highlighted,
     * the user can press `?' or `x' and get this short explanation in
     * a pop-up box.
     */
    char *LongText;
    /* for use only by Function -- see the comment there.
     */
} t_MenuItem;

/* For MenuStyle, see below */
#ifndef MENUSTYLE_DEFAULT
# define MENUSTYLE_DEFAULT 0
#endif

/* Each menu structure holds (a pointer to) a number of Items...*/
typedef struct {
    int HowManyItems;
    /* This can of course be less than the real number, if you want to
     * shorten a menu for some reason.  If there are more items than
     * will fit on the screen, the menu can scroll vertically, so there
     * is no limit imposed on this by the menu routines.  On the other
     * hand, it would be so irritating to scroll through 1000 items that
     * the list should preferably be under 20.
     */
    t_MenuItem *Items;
    /* This is a pointer to a block of menus.
     * A linked list might be better.
     */
    unsigned int MenuStyle;
    /* currently always zero, as there is only one style.
     * you can use MENUSTYLE_DEFAULT for this.
     * A future style might have no box, just a line on the left.
     */
    unsigned int IsSelected : 1;
    /* This is in case you are using interrupts to change menus.  You
     * wouldn't want to delete or hide a selected menu!
     * The software updates this, but otherwise ignores it.
     */
    int TopLineOnScreen;
    /* This is for vertical scrolling;  if it's 13 (say), then the
     * first 13 lines of the menu are not displayed -- If it was "1" in
     * the example, "Whip" would be where "Tongs" are, and "Tongs"
     * would not be displayed.
     */
    int PositionInBar;
    /* Horizontal position of the left edge of the box.  This is set
     * for each menu by ShowMenuBar() at each call.
     */
    int Width;
    /* The same as the length of the longest item.
     * If you change the items, set this to zero to force ShowMenuBar()
     * or ShowMenu() to recalculate it from the Item->[*].Width entries,
     * or using strlen() if necessary.
     */
    char *Name;
    int NameLength;
    /* This is the text displayed on the menu bar.
     * NameLength is computed by ShowMenuBar(); the user should set it
     * to zero to indicate that this is necessary, when the menu is
     * created and whenever the name changes.
     */
    char *Description;
    /* This should be a short (one line) description of the purpose
     * of this menu.
     */
    int SelectedLine;
    /* This is the currently highlighted line.  If you set it yourself,
     * ShowMenu() will make this the highlighted item when it first
     * displays the menu.  Subsequently, of course, the user will move
     * it up or down at will...
     * The default is zero, the first item.
     */
    /* private */ int MenuId;
    /* Set this to zero. */
} t_Menu;

#define MENUMAXINBAR 30

/* A Menu Bar... */
typedef struct {
    int HowManyMenus;
    /* The number of menus in Menus[].  Only the first MENUMAXINBAR will
     * be used, however.  Also, not all will be displayed on the screen...
     * depending on how many fit.
     */
    int SelectedMenu;
    /* This is the Menu whose name is highlighted.
     * Default is zero; set it to something else if you want...
     */
    t_Menu *Menus[MENUMAXINBAR];
    /* Pointers to the menu structures */
    int ScrollOffset;
    /* This is for horizontal scrolling.
     * The default is zero, meaning that the first menu is as near to the
     * left of the screen as it will go (there's a 2-char gap to make
     * room for the left edge of the box).
     */
    /* private */ int MenuBarId;
    /* Set this to zero. */
} t_MenuBar;

/* Some functions that you can arrange to have called by menu selection.
 * The functions in this table must all take two arguments, a t_Menu *
 * and an integer, which represents the (menu, item) combination that
 * was selected.
 * They should return either the integer or -1.
 * If they return -3 or -4, it's the same as if the user pressed the
 * left or right arrow keys instead of selecting.
 * If they return -5, it's as if they typed 'q' at the prompt.
 */

/*PRINTFLIKE1*/
extern void error();

/* A TextBox has the following structure:
 *
 * +------------------------------------------
 * | Name (optional, only displayed if != 0)
 * | Flags
 * | 	BoldName
 * |	BoldText
 * |	CanEdit
 * |	IfChanged (set if the text has been edited)
 * | Window (0 if not allocated)
 * | tlx, tly -- where it is
 * | Height, Width -- how big it is (including the surrounding box)
 * | char *Text -- the text itself
 * | HowManyLines
 * | short *LineLengths --> pointer to array of line lengths
 * | HScrollPos, VScrollPos
 */
typedef struct s_TextBox {
    char *Name;
    unsigned long flags;
    WINDOW *Window;
    short tlx, tly;
    short Height, Width;
    int HScrollPos, VScrollPos;
    char *String;
    int HowManyLines;
    short *LineLengths;
    int StringWidth;
} t_StringBox;

#ifndef ACS_BSSS
# define ACS_BSSS '+'
# define ACS_SBSS '+'
# define ACS_SSBS '+'
# define ACS_SSSB '+'
# define ACS_HLINE '='
# define ACS_LARROW '>'
# define ACS_LLCORNER '+'
# define ACS_LRCORNER '+'
# define ACS_RARROW '<'
# define ACS_VLINE '|'
# define KEY_HELP 999 /* an unlikely key to be pressed */
#endif
