/* Error.c -- print an error message and exit.
 * If we're in curses mode, do an endwin() first.
 *
 * $Id: error.c,v 1.4 2019/04/21 06:09:02 lee Exp $
 *
 * $Log: error.c,v $
 * Revision 1.4  2019/04/21 06:09:02  lee
 * for migration to CVS
 *
 * Revision 1.4  2001/05/31 03:50:13  liam
 * for release 1.17
 *
 * Revision 1.3  94/02/26  14:56:07  lee
 * API change
 * 
 * Revision 1.2  92/02/15  05:54:20  lee
 * Changed Header to Id for version control.
 * 
 * Revision 1.1  90/08/29  21:49:49  lee
 * Initial revision
 * 
 * Revision 2.1  89/08/07  13:49:36  lee
 * First fully working (V.3.2 only) release;
 * this is the baseline for future development.
 * 
 * Revision 1.2  89/08/04  17:59:14  lee
 * Fully working with Basic Functionality.
 * Scrolling menubar, scrolling menus, moveable Info windows.
 * 
 *
 */

#include <stdio.h>
#include "globals.h"
#ifdef ultrix
# include <cursesX.h>
#else
# include <curses.h>
#endif

#ifdef HAVE_STDLIB_H
# include <stdlib.h>
#else
# include <malloc.h>
#endif

#include "internal.h"
#include "error.h"

int InCurses = 0;
extern char *cmdname, *progname;

/*PRINTFLIKE1*/
/*VARARGS1*/
void
error(Type, fmt, a1, a2, a3, a4)
    int Type;
    char *fmt;
{
    extern char *strchr();
    char *p;

    if (InCurses) {
	if (!(Type & (ERR_INTERNAL | ERR_MEMORY))) {
	    if ((p = malloc(500)) == (char *) 0) {
		Type |= ERR_MEMORY;
	    } else {
		(void) sprintf(p, fmt, a1, a2, a3, a4);
		(void) ShowInfo(p, (WINDOW *) 0, COLS / 2 - 20, LINES / 2 - 4);
		if (Type & ERR_FATAL) {
		    endwin();
		    exit(1);
		} else {
		    return;
		}
	    }
	}
	if (Type & ERR_FATAL) {
	    endwin();
	    exit(1);
	}
    }
    if (InCurses) {
	if (Type & ERR_FATAL) {
	    endwin();
	    InCurses = 0;
	}
    }
    if (InCurses && !(Type & ERR_INTERNAL)) {
	wmove(stdscr, 5, 0);
	clrtoeol();
	refresh();
    }
    if (cmdname) {
	(void) fprintf(stderr, "%s: ", cmdname);
    }
    if (progname) {
	(void) fprintf(stderr, "%s: ", progname);
    }
    (void) fprintf(stderr, fmt, a1, a2, a3, a4);
    if (strchr(fmt, '\n') == (char *) 0) {
	(void) fputc('\n', stderr);
    }
    if (InCurses) fputc('\r', stderr);
    if (Type & ERR_FATAL) exit(-1);
}
