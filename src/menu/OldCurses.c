/* Compatibility routines for older versions of curses...
 * $Id: OldCurses.c,v 1.3 2019/04/21 06:09:02 lee Exp $
 *
 */

#include <curses.h>
#include <ctype.h>

#ifndef A_STANDOUT
#include "oldcurses.h"

#undef CONTROL
#define CONTROL(c) (c ^ 64)

#undef wgetch

chtype
Lqwgetch(w)
    WINDOW *w;
{
    int ch = wgetch(w);

    if (isprint(ch)) return ch;

    switch (ch) {
    case CONTROL('^'): return KEY_HOME;
    case CONTROL('P'): return KEY_UP;
    case CONTROL('N'): return KEY_DOWN;
    case CONTROL('B'): return KEY_LEFT;
    case CONTROL('F'): return KEY_RIGHT;
    case CONTROL('X'): return KEY_HELP; /* Xplain.... (groan) */
    case '\033': /* Escape */
	(void) fprintf(stderr, "ESC\007");
	(void) fflush(stderr);

	switch (ch = wgetch(w)) {
	case 0:		return KEY_F0;
	case 1:		return KEY_F(1);
	case 2:		return KEY_F(2);
	case 3:		return KEY_F(3);
	case 4:		return KEY_F(4);
	case 5:		return KEY_F(5);
	case 6:		return KEY_F(6);
	case 7:		return KEY_F(7);
	case 8:		return KEY_F(8);
	case 9:		return KEY_F(9);
	case 'a': case 'A':	return KEY_F(10);
	case 'b': case 'B':	return KEY_F(11);
	case 'c': case 'C':	return KEY_F(12);
	case 'd': case 'D':	return KEY_F(13);
	case 'e': case 'E':	return KEY_F(14);
	case 'f': case 'F':	return KEY_F(15);
	case 'h':	return KEY_HELP;
	}
	break;
    }
    return ch;
}

void
beep()
{
    (void) putc('\b', stderr);
    (void) fflush(stderr);
}

void
box(win, vert, hor)
    WINDOW *win;
    int vert;
    int hor;
{
#undef box
    if (hor == 0) hor = ACS_HLINE;
    if (vert == 0) vert = ACS_VLINE;
    box(win, vert, hor);
}

void
Lqattrset(win, attr)
    WINDOW *win;
    int attr;
{
    if (attr) {
	wstandout(win);
    } else {
	wstandend(win);
    }
}

wnoutrefresh(win)
    WINDOW *win;
{
    touchwin(win);
}
#endif

/* $Log: OldCurses.c,v $
 * Revision 1.3  2019/04/21 06:09:02  lee
 * for migration to CVS
 *
 * Revision 1.2  90/10/04  16:27:58  lee
 * SysV compat improved.
 * 
 * Revision 1.1  90/10/03  21:54:04  lee
 * Initial revision
 * 
 *
 */
