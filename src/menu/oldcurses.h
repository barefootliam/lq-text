/* oldcurses.h -- compatibility with pre-System V.3 curses...
 * $Id: oldcurses.h,v 1.3 1994/02/26 14:56:20 lee Exp $
 */

#ifndef CURSESX
typedef int chtype;

#define ACS_LARROW	'>'
#define ACS_RARROW	'<'
#define ACS_HLINE	'='
#define ACS_VLINE	'|'
#define ACS_LRCORNER	'+'
#define ACS_LLCORNER	'+'

/* Line drawing: */
#define ACS_BSSS	'+'	/* T-piece */
#define ACS_SBSS	'+'	/* -| */
#define ACS_SSBS	'+'	/* inverted T-piece */
#define ACS_SSSB	'+'	/* |- */
#define ACS_BBSS	'+'	/* top right corner */
#define ACS_BSSB	'+'	/* bottom left corner */

#define KEY_DOWN	257
#define KEY_UP  	258
#define KEY_LEFT	259
#define KEY_RIGHT	260
#define KEY_HELP	261
#define KEY_HOME	262
#define KEY_F0		300
#define KEY_F(n)	(KEY_F0+n)

#undef getch
#define getch()	Lqwgetch(stdscr)
#define wgetch	Lqwgetch

#undef box
#define box LqBox

#define A_STANDOUT 1
#undef standout
#undef standend
#define wattrset Lqattrset
#define attrset(a) Lqattrset(stdscr, a)
#define keypad(win, bool)	1 /* ignore this one please */

/* $Log: oldcurses.h,v $
 * Revision 1.3  94/02/26  14:56:20  lee
 * API change
 * 
 * Revision 1.2  90/10/04  16:28:31  lee
 * SysV compat improved.
 * 
 * Revision 1.1  90/10/03  21:56:32  lee
 * Initial revision
 * 
 *
 */
#endif
