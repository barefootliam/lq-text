/* emalloc.h -- Copyright 1989 Liam R. Quin.  All Rights Reserved.
 * This code is NOT in the public domain.
 * See the file COPYRIGHT for full details.
 */

/* emalloc.h -- header file for emalloc.c, Liam Quin's malloc() wrapper
 *
 * $Id: endwin.h,v 1.1 1994/02/26 14:56:00 lee Exp $
 */

extern char *_emalloc(), *_erealloc(), *_ecalloc();
extern void _efree();

#ifdef MALLOCTRACE
#define emalloc(what, u) _emalloc(what, u, __FILE__, __LINE__)
#define erealloc(s, u) _erealloc(s, u, __FILE__, __LINE__)
#define ecalloc(n, siz) _ecalloc(n, siz, __FILE__, __LINE__)
#define efree(s) _efree(s, __FILE__, __LINE__)
#else
#define emalloc(what, u) _emalloc(what, u)
#define erealloc(s, u) _erealloc(s, u)
#define ecalloc(n, siz) _ecalloc(n, siz)
#define efree(s) _efree(s)
#endif
