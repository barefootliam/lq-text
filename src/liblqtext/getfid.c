/* GetFID.c -- Copyright 1989, 1993, 1994, 1995, 1996 Liam R.E. Quin.
 * All Rights Reserved.
 * This code is NOT in the public domain.
 * See the file COPYRIGHT for full details.
 *
 * LQT_GetMaxFID -- get the largest current file number
 * 
 * $Id: getfid.c,v 1.6 1996/05/14 23:58:30 lee Exp $
 */

#include "globals.h" /* defines and declarations for database filenames */
#include "error.h"

#include <stdio.h>
#include <sys/types.h>
#ifdef HAVE_FCNTL_H
# ifdef HAVE_SYSV_FCNTL_H
#  include <sys/stat.h>
# endif
# include <fcntl.h>
#endif

#include "fileinfo.h"
#include "wordinfo.h"
#include "block0.h"

#include "liblqtext.h"
#include "lqtrace.h"

/** declarations: **/
/** Unix system calls that need to be declared: **/
/** Unix Library Calls that need to be declared: **/
/** lq-text calls that need to be declared: **/

/** **/

/* <Function>
 *   <Name>LQT_GetMaxFID
 *   <Class>Database/Documents
 *   <Purpose>
 *      Returns the largest allocated FID.
 *   <Returns>
 *      <LIST>
 *        <LI>the largest FID already allocated.
 *        <LI>1 if no FIDS have been allocated.
 *	</LIST>
 *   <Errors>
 *      as for LQT_ReadBlock, LQT_OpenDataBase.
 * </Function>
 */
API t_FID
LQT_GetMaxFID(db)
    t_LQTEXT_Database *db;
{
    unsigned char *p;
    t_FID Result;

    p = LQT_ReadBlock(db, (unsigned long) 0L, (t_WID) 0);

    Result = (t_FID) p[BLOCK_ZERO_MAXFID0]; /* most significant byte */
    Result <<= 8; Result |= (t_FID) p[BLOCK_ZERO_MAXFID1];
    Result <<=8; Result |= (t_FID) p[BLOCK_ZERO_MAXFID2];
    Result <<=8; Result |= (t_FID) p[BLOCK_ZERO_MAXFID3];

    if (!Result) {
	Result = 1;
    }

    if (Result > db->LQTp__LastNextFIDVal) {
	db->LQTp__LastNextFIDVal = Result;
    } else if (db->LQTp__LastNextFIDVal > Result) {
	Result = LQT_GetMaxOrAllocateFID(db, 1);
	if (Result > db->LQTp__LastNextFIDVal) {
	    db->LQTp__LastNextFIDVal = Result;
	} else if (Result != db->LQTp__LastNextFIDVal) {
	    Error(E_BUG,
		"LQT_GetMaxFID confused between %ld and %ld",
		Result, db->LQTp__LastNextFIDVal
	    );
	}
    }
#ifdef ASCIITRACE
    LQT_Trace(LQTRACE_FID_ALLOC, "LQT_GetMaxFID --> %d", Result);
#endif
    return Result;
}

