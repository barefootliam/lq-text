/* GetWID.c -- Copyright 1989, 1993, 1994, 1996 Liam R. E. Quin.
 * All Rights Reserved.
 * This code is NOT in the public domain.
 * See the file COPYRIGHT for full details.
 */

/* LQT_GetMaxWID -- get the largest current word number
 * 
 * $Id: getwid.c,v 1.9 1996/05/14 23:59:15 lee Exp $
 */

#include "globals.h" /* defines and declarations for database filenames */
#include "error.h"

#include <stdio.h>
#include <sys/types.h>
#ifdef HAVE_FCNTL_H
# ifdef HAVE_SYSV_FCNTL_H
#  include <sys/stat.h>
# endif
# include <fcntl.h>
#endif

#include "fileinfo.h"
#include "wordinfo.h"
#include "block0.h"
#include "liblqtext.h"
#include "lqtrace.h"

/** declarations: **/

/** Unix system calls that need to be declared: **/
/** Unix Library Calls that need to be declared: **/
/** lq-text calls that need to be declared: **/

/** **/

/* <Function>
 *   <Name>LQT_GetMaxWID
 *   <Class>Database/Words
 *   <Purpose>
 *      Returns the largest currently allocated WID.
 * </Function>
 */
API t_WID
LQT_GetMaxWID(db)
    t_LQTEXT_Database *db;
{
    unsigned char *p;
    t_WID Result;

    p = LQT_ReadBlock(db, 0L, (t_WID) 0);

    Result = (t_WID) p[BLOCK_ZERO_MAXWID0]; /* most significant byte */
    Result <<= 8; Result |= (t_WID) p[BLOCK_ZERO_MAXWID1];
    Result <<=8; Result |= (t_WID) p[BLOCK_ZERO_MAXWID2];
    Result <<=8; Result |= (t_WID) p[BLOCK_ZERO_MAXWID3];

    if (Result > db->LQTp__LastNextWIDVal) {
	db->LQTp__LastNextWIDVal = Result;
    } else if (db->LQTp__LastNextWIDVal > Result) {
	Result = LQT_GetMaxOrAllocateWID(db, 1);
	if (Result > db->LQTp__LastNextWIDVal) {
	    db->LQTp__LastNextWIDVal = Result;
	} else if (Result != db->LQTp__LastNextWIDVal) {
	    Error(E_BUG,
		"LQT_GetMaxWID confused between %ld and %ld",
		Result, db->LQTp__LastNextWIDVal
	    );
	}
    }
#ifdef ASCIITRACE
    if (LQT_TraceFlagsSet(LQTRACE_WORDINFO)) {
	LQT_Trace(LQTRACE_WORDINFO, "LQT_GetMaxWID --> %d", Result);
    }
#endif
    return Result;
}

