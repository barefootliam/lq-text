/* trace.c -- Copyright 1994, 1995, 1996 Liam R. E. Quin.
 * All Rights Reserved.
 * This code is NOT in the public domain.
 * See the file COPYRIGHT for full details.
 *
 * $Id: trace.c,v 1.10 2019/04/21 06:06:40 lee Exp $
 *
 * Tracing for lq-text
 */

#include "globals.h" /* defines and declarations for database filenames */
#include "error.h"

#include <sys/types.h> /* for fileinfo.h */
#ifndef FILE
# include <stdio.h>
#endif
#ifdef HAVE_STRING_H
# include <string.h>
#else
# include <strings.h>
#endif

#ifdef HAVE_STDLIB_H
# include <stdlib.h>
#else
# include <malloc.h>
#endif

#include <stdarg.h>

#include "fileinfo.h"
#include "wordinfo.h"
#include "wordrules.h"
#include "emalloc.h"
#include "lqutil.h"
#include "liblqtext.h"

#include "lqtrace.h"

t_TraceFlag LQTp_AsciiTraceLevel = 0;

/** Unix system calls that need declaring: **/
/** Unix/C Library Functions that need declaring: **/
/** lqtext library functions that need declaring: **/
/** Functions within this file that are used before being defined: **/
/** **/

static t_FlagNamePair LQTpTraceFlagArray[] = {
    { LQTRACE_VERBOSE, "Verbose" },
    { LQTRACE_DEBUG, "Debug" },
    { LQTRACE_READBLOCK, "ReadBlock" },
    { LQTRACE_WRITEBLOCK, "WriteBlock" },
    { LQTRACE_PUTPLACES, "PutPlaces" },
    { LQTRACE_GETPLACES, "GetPlaces" },
    { LQTRACE_WORDROOT, "WordRoot" },
    { LQTRACE_READWORD, "ReadWord" },
    { LQTRACE_ADDWORD, "AddWord" },
    { LQTRACE_ABORT_ON_ERROR, "AbortOnError" },
    { LQTRACE_CLOSE_ON_ERROR, "CloseOnError" },
    { LQTRACE_BLOCKDUMP, "BlockDump" },
    { LQTRACE_FINDFILE, "FindFile" },
    { LQTRACE_WORDINFO, "WordInfo" },
    { LQTRACE_READAFTERWRITE, "ReadAfterWrite" },
    { LQTRACE_BLOCKCACHE, "BlockCache" },
    { LQTRACE_WIDCACHE, "WidCache" },
    { LQTRACE_FREEBLOCKS, "FreeBlocks" },
    { LQTRACE_FID_ALLOC, "FIDAlloc" },
    { LQTRACE_FINDMATCH, "FindMatch" },
    { LQTRACE_MAKE_PHRASE, "MakePhrase" },
    { LQTRACE_MATCH_PHRASE, "MatchPhrase" },
    { LQTRACE_LASTBLOCK, "LastBlock" },
    { LQTRACE_TERM_EXPANSION, "TermExpansion" },
    { LQTRACE_FILTER_DATA, "FilterData" },
    { 0L, "none" },
    { 0L, (char *) 0 }
};


/* <Function>
 *   <Name>LQT_TraceFlagsSet
 *   <Class>Tracing
 *   <Purpose>
 *	Determines whether any of a particular group of trace flags
 *	are set; if so, a non-zero value is returned, otherwise zero.
 *	The flags may have been or'd together, and are defined in
 *	the <h>lqtrace.h</h> header file.  For each such flag, all of
 *	the bits set in the flag have been be set in the argument to
 *	LQT_TraceFlags in order for it to be considered as being set.
 *   <Returns>
 *	non-zero if one or more flags satisfies the constraints
 *   <Notes>
 *	This may be implemented as a macro; the prototype shown may in
 *	that case have a different name.
 *   <SeeAlso>
 *	LQT_Trace
 *	LQT_SetTraceFlag
 *	LQT_UnSetTraceFlag
 *	LQT_GetTraceFlags
 * </Function>
 */ 
LIBRARY int
LQTp_TraceFlagsSet(QueryFlags)
    t_TraceFlag QueryFlags;
{
    t_FlagNamePair *wp;
    int i;

    for (i = 0; (wp = &LQTpTraceFlagArray[i])->Name; i++) {
	if ((QueryFlags & wp->Value) == wp->Value) {
	    if (wp->Value) {
		return i + 1;
	    }
	}
    }
    return 0;
}

/* <Function>
 *   <Name>LQT_ForEachTraceFlag
 *   <Class>Tracing
 *   <Purpose>
 *	<P>Calls the given function for each available trace flag.
 *	The integer argument IsSet passed to the function is non-zero for
 *	those flags that are set in the current trace flags, and zero
 *	for the others.</P>
 *	<P>The flags are defined in the <h>lqtrace.h</h> header file.</P>
 *   <Returns>
 *	zero.
 *   <SeeAlso>
 *	LQT_Trace
 *	LQT_SetTraceFlag
 * </Function>
 */ 
API int
LQT_ForEachTraceFlag(CallMe)
    void (* CallMe)(
#ifdef HAVE_PROTO
	char *Name,
	unsigned int Value,
	int isSet
#endif
    );
{
    int i;

    for (i = 0; LQTpTraceFlagArray[i].Name; i++) {
	int amSet = (
	    LQTpTraceFlagArray[i].Value && (
		LQT_TraceFlagsSet(LQTpTraceFlagArray[i].Value) != 0
	    )
	);

	(* CallMe) (
	    LQTpTraceFlagArray[i].Name,
	    LQTpTraceFlagArray[i].Value,
	    amSet
	);
    }
    return 0;
}

/* <Function>
 *   <Name>LQT_GetTraceFlagsAsString
 *   <Class>Tracing
 *   <Purpose>
 *	<P>Returns a static pointer to a string representation of the
 *	current lq-text trace flags.
 *	This is suitable for printing in error messages, and can also be
 *	used with LQT_SetTraceFlagsFromString to save and restore flags in
 *	a machine-independent way.</P>
 *	<P>The caller should not attempt to write into, or free,
 *	the result string.</P>
 *	<P>The flags are defined in the <h>lqtrace.h</h> header file.</P>
 *   <Returns>
 *	a pointer to a private string.
 *   <SeeAlso>
 *	LQT_Trace
 *	LQT_SetTraceFlag
 *	LQT_UnSetTraceFlag
 *	LQT_GetGivenTraceFlagsAsString
 *	LQT_SetTraceFlagsFromString
 * </Function>
 */ 
API char *
LQT_GetTraceFlagsAsString()
{
    return LQT_FlagsToString(LQTp_AsciiTraceLevel, LQTpTraceFlagArray, "|");
}

/* <Function>
 *   <Name>LQT_GetGivenTraceFlagsAsString
 *   <Class>Tracing
 *   <Purpose>
 *	<P>This function works like LQT_GetTraceFlagsAsString, except that it
 *	uses the given flags instead of the current value of the lq-text
 *	trace flags.</P>
 *	<P>The caller should not attempt to write into, or free,
 *	the result string.</P>
 *	<P>The flags are defined in the <h>lqtrace.h</h> header file.</P>
 *   <Returns>
 *	non-zero if one or more flags satisfies the constraints
 *   <Notes>
 *	You can get a rather long line giving all possible flags using
 *	the C expression (t_TraceFlag) ~(unsigned long) 0, which provides a
 *	number with all bits set, as an argument to
 *	LQT_GetGivenTraceFlagsAsString.
 *   <SeeAlso>
 *	LQT_Trace
 *	LQT_SetTraceFlag
 *	LQT_GetTraceFlags
 *	LQT_GetTraceFlagsAsString
 * </Function>
 */ 
API char *
LQT_GetGivenTraceFlagsAsString(Flags)
    t_TraceFlag Flags;
{
    return LQT_FlagsToString((unsigned long) Flags, LQTpTraceFlagArray, "|");
}

/* <Function>
 *   <Name>LQT_GetTraceFlags
 *   <Class>Tracing
 *   <Purpose>
 *	<P>Returns the current value of the lq-text trace flags.
 *	The various flag values are defined in the <h>lqtrace.h</h> header
 *	file, and may be combined (using bitwise or) in any combination.</P>
 *	<P>The value returned by LQT_GetTraceFlags should not normally be used
 *	by itself in diagnostic or error messages.
 *	Instead, use LQT_GetTraceFlagsAsString, which provides a more
 *	readable value for humans.</P>
 *   <Returns>
 *	the current lq-text trace flags, or'd together
 *   <SeeAlso>
 *	LQT_Trace
 *	LQT_SetTraceFlag
 *	LQT_UnSetTraceFlag
 *	LQT_SetTraceFlagsFromString
 * </Function>
 */ 
API t_TraceFlag
LQT_GetTraceFlags()
{
    return LQTp_AsciiTraceLevel;
}

/* <Function>
 *   <Name>LQT_SetTraceFlag
 *   <Class>Tracing
 *   <Purpose>
 *	<P>Adds the given argument to the current lq-text trace flags.
 *	You can add several flags at a time by combining them with
 *	bitwise or.
 *	If you do, the return value may be hard to decipher, although
 *	since the return value is primarily of interest to internal
 *	liblqtext routines, this probably doesn't matter.
 *   <Returns>
 *	non-zero if any of the the given flags were set.
 *   <SeeAlso>
 *	LQT_Trace
 *	LQT_UnSetTraceFlag
 *	LQT_GetTraceFlags
 * </Function>
 */ 
API t_TraceFlag
LQT_SetTraceFlag(theFlag)
    t_TraceFlag theFlag;
{
    t_TraceFlag FlagWasSet = LQT_TraceFlagsSet(theFlag);

    LQTp_AsciiTraceLevel |= theFlag;
    return FlagWasSet;
}

/* <Function>
 *   <Name>LQT_UnSetTraceFlag
 *   <Class>Tracing
 *   <Purpose>
 *	<P>The given flag is removed from the current lq-text trace flags.
 *	You can combine multiple flag values using bitwise or.</P>
 *	<P>This routine can be used in conjunction with LQT_GetTraceFlags to
 *	unset all of the current flags.</P>
 *   <Returns>
 *	1 if any of the given flags were set, and 0 otherwise.
 *   <Errors>
 *	An attempt to unset flags that were not set produces an error
 *	of type E_WARN|E_INTERNAL.
 *   <SeeAlso>
 *	LQT_Trace
 *	LQT_GetTraceFlags
 *	LQT_SetTraceFlagsFromString
 * </Function>
 */ 
API int
LQT_UnSetTraceFlag(theFlag)
    t_TraceFlag theFlag;
{
    int FirstFlagSet;
    
    if (!theFlag) {
	Error(E_WARN|E_INTERNAL, "LQT_UnSetTraceFlag(0) makes no sense");
	return 0;
    }

    FirstFlagSet = LQT_TraceFlagsSet(theFlag);

    if (!FirstFlagSet) {
	Error(E_WARN|E_INTERNAL,
	    "LQT_UnSetTraceFlag(%u: %s), flags not set",
	    theFlag, LQT_GetTraceFlagsAsString()
	);
	LQTp_AsciiTraceLevel &= ~theFlag; /* just in case... */
	return 0;
    } else {
	LQTp_AsciiTraceLevel &= ~theFlag;
	return 1;
    }
}

/* <Function>
 *   <Name>LQT_SetTraceFlagsFromString
 *   <Class>Tracing
 *   <Purpose>
 *	<P>Attempts to set the lq-text trace flags by reading a string
 *	representation of them.
 *	The string must be in the format produced by LQT_GetTraceFlagsAsString;
 *	in other words, a sequence of words separated by the vertical bar.
 *	The various flag values are defined in the <h>lqtrace.h</h> header
 *	file, and may be combined (using bitwise or) in any combination</P>
 *	<P>If the return value points to a NUL byte, the end of the string
 *	was reached without error; otherwise, it is up to the caller to
 *	determine whether the extra unconverted text was expected.
 *   <Returns>
 *	a pointer to the first unconverted character in the given string
 *   <SeeAlso>
 *	LQT_Trace
 *	LQT_SetTraceFlag
 *	LQT_GetTraceFlagsAsString
 *	LQT_StringToFlags
 * </Function>
 */ 
API char *
LQT_SetTraceFlagsFromString(theString)
    char *theString;
{
    return LQT_StringToFlags(
	theString,
	 &LQTp_AsciiTraceLevel,
	LQTpTraceFlagArray,
	"|"
    );
}

static FILE *TraceFile;

/* <Function>
 *   <Name>LQT_SetTraceFile
 *   <Class>Tracing
 *   <Purpose>
 *	<P>After this call, all lq-text tracing output produced with LQT_Trace
 *	will be sent to the given file.
 *	It is the caller's responsibility to ensure that the given FILE * is
 *	valid and points to a file that is open for writing.</P>
 *	<P>The default file used before LQT_SetTraceFile has been called is
 *	stderr.
 *	An argument of (FILE *) NULL will reset the file to the default value,
 *	but will not close the given stream.
 *	The file is also not when a database is closed;
 *	see LQT_AddActionOnClose for a way of changing this behaviour.</P>
 *   <Returns>
 *	the previous file pointer
 *   <SeeAlso>
 *	LQT_Trace
 *	LQT_SetTraceFlag
 *	LQT_AddActionOnClose
 * </Function>
 */ 
API FILE *
LQT_SetTraceFile(newFile)
    FILE *newFile;
{
    FILE *oldFile = TraceFile;

    if (newFile) {
	TraceFile = newFile;
    } else {
	TraceFile = stderr;
    }
    return oldFile;
}

/* <Function>
 *   <Name>LQT_Trace
 *   <Class>Tracing
 *   <Purpose>
 *	<P>Prints diagnostic messages.
 *	The Flags argument must be one or more flags
 *	taken from <h>lqtrace.h</h> and combined with bitwise or.
 *	If one of more of the given Flags is set in the current lq-text
 *	trace flags, the remainder of the arguments are passed to fprintf</P>
 *	<P>For efficiency, it may be best to use LQT_TraceFlagsSet first
 *	to determine whether to call LQT_Trace, as the former is likely
 *	to be implemented as a short macro in <h>lqtrace.h</h>, but
 *	currently LQT_Trace cannot be so implemented.</P>
 *	<P>Each line of trace output is preceded by the current program name,
 *	the word `trace', and a string representation of 
 *	one or more of those flags in the Flags argument to LQT_Trace which
 *	are set in the current lqtext trace flags.</P>
 *   <SeeAlso>
 *	LQT_SetTraceFlag
 *	LQT_UnSetTraceFlag
 *	LQT_SetTraceFlagsFromString
 * </Function>
 */ 
API void
LQT_Trace(t_TraceFlag Flags, char *Format, ...)
{
    va_list ap;
    va_list acopy;
    int FirstFlagSet;

    va_start(ap, Format);
    va_copy(acopy, ap); /* so we can call end later */

    FirstFlagSet = LQT_TraceFlagsSet(Flags);
    if (FirstFlagSet > 0) {
	register char *p;

	for (p = Format; *p; p++)  ;
	if (p > Format && p[-1] == '\n') {
	    --p; /* remember we do not need to print a newline */
	}

	/* Show only first trace flag that matched: */
	if (LQTpTraceFlagArray[FirstFlagSet - 1].Value == LQTRACE_VERBOSE) {
	    (void) fprintf(stderr,
		"%s: ",
		progname
	    );
	} else {
	    (void) fprintf(stderr,
		"%s: <%s>: ",
		progname,
		LQTpTraceFlagArray[FirstFlagSet - 1].Name
	    );
	}

	vfprintf(stderr, Format, ap);

	if (*p != '\n') {
	    fputc('\n', stderr);
	}

	(void) fflush(stderr);
    }
    va_end(acopy);
}

