/* filtertb.c -- Copyright 1995 Liam R. E. Quin.
 * All Rights Reserved.
 * This code is NOT in the public domain.
 * See the file COPYRIGHT for full details.
 *
 * $Id: filtertb.c,v 1.2 1996/07/01 21:36:05 lee Exp $
 *
 * handle a filter table
 */
this code is not yet used or distributed.

#include "error.h"
#include "globals.h" /* defines and declarations for database filenames */

#include <stdio.h> /* stderr, also for fileinfo.h */
#include <sys/types.h>

#ifdef HAVE_FCNTL_H
# ifdef HAVE_SYSV_FCNTL_H
#  include <sys/stat.h>
# endif
# include <fcntl.h>
#endif

#include <malloc.h>
#include <ctype.h>

#ifdef HAVE_STRING_H
# include <string.h>
#else
# include <strings.h>
#endif

#include "fileinfo.h" /* for wordinfo.h */
#include "wordinfo.h"
#include "pblock.h"
#include "phrase.h"
#include "wordrules.h"
#include "emalloc.h"
#include "lqutil.h"
#include "liblqtext.h"
#include "lqtrace.h"

/** Unix system calls that need to be declared: **/
/** Unix/C Library Functions: **/

/** lqtext functions: **/
/** functions within this file that need forward declarations **/
/** **/

PRIVATE char *GetNextFilterToken(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *db,
    char **Linep,
    int LineNumber
#endif
);

typedef struct s_FilterTableEntry {
    char *Name;
    int ID;
    char *IdentifyingString; /* like /etc/magic */
    char *ImportFilter;
    char *DisplayFilter;
    int (* CloseFunction)(); /* normally either fclose or pclose */
    struct s_FilterTableEntry *Next;
} t_FilterTableEntry;

static t_FilterTableEntry *Filters;
static int FilterCount = 0;

/* <Function>
 *   <Name>LQT_ReadFilterTable
 *   <Class>Database/Files, Database/Defaults
 *   <Purpose>
 *      <P>Reads the filter table into memory if one was specified.
 *	The filter table lists the file types that can be indexed, and
 *	gives an index filter for each of them.</P>
 *	<P>If there is no `filtertable' entry in the database configuration
 *	file, a built-in list of defaults is used.</P>
 *   <Returns>
 *      zero on success.
 *   <SeeAlso>
 *      LQT_GetFilterType
 * </Function>
 */
API int
LQT_ReadFilterTable(db)
    t_LQTEXT_Database *db;
{
    FILE *FilterFP;
    t_FilterTableEntry **Itempp;
    char *Line;
    int LineNumber = 0;

    Itempp = &Filters;

    if (db->filtertable) {
	FilterFP = LQU_fEopen(E_FATAL,
	    db->filtertable,
	    "list of conversion filters",
	    O_RDONLY,
	    0
	);
	while (LQU_fReadLine(f, &Line, LQUF_NORMAL) >= 0) {
	    char *SaveLine;
	    char *Token;
	    t_FilterTableEntry NewEntry;

	    ++LineNumber;
	    if (!Line || !*Line) {
		continue; /* it was blank */
	    }
	    SaveLine = Line;

	    /* Format of the line:
	     *
	     * formatname identify-string import-filter display-filter
	     */
	    Token = GetNextFilterToken(db, &Line, LineNumber);
	    if (!Token) { /* e.g. blank except for a comment */
		continue;
	    }
	    NewEntry.Name = StealFilterToken(Token);

	    Token = GetNextFilterToken(db, &Line, LineNumber);
	    if (!Token) { /* e.g. blank except for a comment */
		Error(E_FATAL,
		    "%s %d: expected magic-string and filter after \"%s\"",
		    db->filtertable,
		    LineNumber,
		    NewEntry.Name
		);
	    }
	    NewEntry.
	}

	(void) fclose(FilterFP);
    } else {
	return LQTpMakeDefaultFilterList(db);
    }
}

static char *Buffer = 0;
unsigned int BufferLength = 0;

PRIVATE char *
StealFilterToken()
{
    char *Result = Buffer;
    BufferLength = 0;
    Buffer = (char *) 0;
    return Result;
}

PRIVATE char *
GetNextFilterToken(db, Linep, LineNumber)
    t_LQTEXT_Database *db;
    char **Linep;
    int LineNumber;
{
    int delim = 0;
    char *Start;
    register char *p;

    if (!Linep || !*Linep || !**Linep) {
	if (BufferLength) { /* reclaim some memory */
	    BufferLength = 0;
	    efree(Buffer);
	}
	return (char *) 0;
    }

    /* skip leading whitespace */
    for (p = (*Linep); *p; p++) {
	if (!isspace(*p)) {
	    break;
	}
    }

    if (!*p) {
	return (char *) 0;
    }

    if (*p == '"' || *p == '\'') {
	delim = *p;
	++p; /* skip over the quote */

	for (*Linep = Start = p; *p; p++) {
	    if (p == delim) {
		--p; /* don't want to include the delimiter */
		break;
	}

	/* we got to the end of the string and it wasn't there! */
	Error(E_FATAL,
	    "%s:%d: missing closing %s (%c)",
	    db->filtertable,
	    delim == '"' ? "double quote" : "single quote",
	    delim
	);
    } else if (*p == '#' || *p == ';') {
	/* comment */
	return (char *) 0;
    } else {
	for (*Linep = Start = p; *p; p++) {
	    if (isspace(*p)) {
		break;
	    }
	}
	--p; /* don't want to include the space or NUL */
    }

    /* Assert:
     *  Start points to the beginning of the token
     *  *Linep == Start
     *  p >= Start
     */

    if (BufferLength <= p - Start) {
	if (Buffer) {
	    efree(Buffer);
	}
	if (BufferLength < 15) {
	    /* avoid lots of tiny mallocs */
	    BufferLength += 20;
	}
	Buffer = emalloc(Start, BufferLength);
    }
    (void) strncpy(Buffer, Start, p - Start);
    Buffer[p - Start] = '\0';
    return Buffer;
}
