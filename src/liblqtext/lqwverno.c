/* lqwversion.c -- Copyright 1993, 1994, 1996 Liam R. E. Quin.
 * All Rights Reserved.
 * This code is NOT in the public domain.
 * See the file COPYRIGHT for full details.
 */

#ifndef LINT
static char *RcsId = "@(#) $Id: lqwverno.c,v 1.4 1996/05/15 23:00:14 lee Exp $";
#endif

#include "error.h"

#include <stdio.h> /* for ANSI C */
#include <sys/types.h>
#include "globals.h" /* defines and declarations for database filenames */

#include "revision.h"
#include "blkheader.h"
#include "block0.h"
#include "liblqtext.h"

/* <Function>
 *   <Name>LQT_WriteVersionToDatabase
 *   <Class>Database/Files
 *   <Purpose>
 *      Writes the liblqtext library version to the database so that
 *	LQT_CheckDatabaseVersion will accept it.
 *	This routine is called automatically when a new lq-text database
 *	is created.
 *   <SeeAlso>
 *	LQT_CheckDatabaseVersion
 * </Function>
 */
API void
LQT_WriteVersionToDatabase(db)
    t_LQTEXT_Database *db;
{
    unsigned char *p;
    t_BlockHeader *BH;

    /* OpenDatabase depends on this function doing a LQT_ReadBlock(0) */
    p = LQT_ReadBlock(db, 0L, (t_WID) 0);
    BH = (t_BlockHeader *) p;

    p[BLOCK_ZERO_COMPILEFLAGBYTE] = LQ_FLAG_VALUE;
    p[BLOCK_ZERO_MAJORVERSION0] = (LQ_MAJOR_VERSION / 256);
    p[BLOCK_ZERO_MAJORVERSION1] = (LQ_MAJOR_VERSION & 255);
    p[BLOCK_ZERO_MINORVERSION] = LQ_MINOR_VERSION;

    BH->NumberOfBlocks = 1;
    LQT_WriteBlock(db, 0L, p, 1, (t_WID) 0);
}

