/* smalldb.c -- Copyright 1989, 1992, 1994, 1996 Liam R. E. Quin.
 * All Rights Reserved.
 * This code is NOT in the public domain.
 * See the file COPYRIGHT for full details.
 */

/* Simple interface to start and end dbm.
 * You may also need to supply dbm_store() and dbm_fetch(), but these
 * should certainly be macros.
 *
 * $Id: smalldb.c,v 1.27 2019/04/21 06:06:40 lee Exp $
 */

#include "globals.h"
#include "error.h"

/* Actually we don't need stdio.h on most systems, but ANSI C requires it */
#include <stdio.h>

#ifdef HAVE_FCNTL_H
# ifdef HAVE_SYSV_FCNTL_H
#  include <sys/stat.h>
# endif
# include <fcntl.h>
#endif

#ifdef HAVE_UNISTD_H
# include <unistd.h> /* for open() */
#endif

#ifdef HAVE_STDLIB_H
# include <stdlib.h>
#endif

#ifdef HAVE_STRING_H
# include <string.h>
#else
# include <strings.h>
#endif

#include "emalloc.h"
#include "smalldb.h"
#include "lqutil.h"
#include "liblqtext.h"

/* The physical database for the list of words, and for the list
 * of files, uses ndbm.
 * The advantage of this is that it takes only two file system accesses
 * to retrieve any data item (honest!).
 * It's also reasonably fast at insertion.
 * One disadvantage is that it doesn't cope if too many words have the
 * same (32-bit) hash function, although some of the publicly available
 * replacements such as the 4.4 BSD db package fix this.
 *
 * Earlier versions oflq-text than 1.19 used a cache of dbm files sorted
 * by name.
 *
 */

#undef LQT_OpenKeyValueDatabase

#ifdef dbnative
LIBRARY DBT dbm_dbfetch(db, keyp)
    const DB *db;
    DBT *keyp;
{
    DBT tmp;

#if DB_VERSION_MAJOR<4
#error you need db4 or later
    die ## DB_VERSION_MAJOR die die;
#endif

    (void) bzero(&tmp, sizeof(tmp));
    if ((db)->get(
		db,
#if DB_VERSION_MAJOR>3
		0,
#endif
		keyp,
		&tmp
#if DB_VERSION_MAJOR>3
		, (u_int32_t)0 /* flags */
#endif
		) != 0) {
	tmp.size = 0;
	tmp.data = 0;
    }
    return tmp;
}

#endif

/* <Function>
 *   <Name>LQT_ObtainWriteAccess
 *   <Class>Database/Database, Database/Files
 *   <Purpose>
 *      Grants write access to the current database.
 *      This is called automatically by LQT_OpenDatabase if appropriate.
 *   <Returns>
 *      <LIST>
 *        <LI>zero on success
 *        <LI>-1 on error or failure
 *	</LIST>
 *   <Notes>
 *	Write access may on some systems be exclusive, so that no other
 *	process can open the database, neither for reading nor for writing.
 *	You should not rely on this, however; on some systems, multiple
 *	clients may succeed in writing, and will corrupt the database.
 *   <Errors>
 *      A corrupt database may cause a fatal or E_BUG error.
 *   <SeeAlso>
 *	LQT_ObtainReadOnlyAccess
 *	LQT_OpenDatabase
 * </Function>
 */
API int
LQT_ObtainWriteAccess(db)
    t_LQTEXT_Database *db;
{
    int fd;

    db->FileFlags |= (O_RDWR|O_CREAT);
    db->FileModes |= 0600; /* allow self to read and write */

    fd = open(db->DataBase, db->FileFlags, db->FileModes);
    if (fd >= 0) {
	(void) close(fd);
	return 0; /* seems to be working, at least for that file. */
    } else {
	/* permission problem */
	Error(E_WARN|E_SYS,
	    "Couldn't obtain write access for lq-text database %s",
	    db->DatabaseDirectory
	);
	return -1;
    }
}

/* <Function>
 *   <Name>LQT_ObtainReadOnlyAccess
 *   <Class>Database/Database, Database/Files
 *   <Purpose>
 *      Obtains read-only access to the current database.
 *      This is called automatically by LQT_OpenDatabase if appropriate.
 *	If the database was previously open for writing, it should be
 *	closed first with LQT_CloseDatabase or LQT_SyncDatabase.
 *   <Returns>
 *      <LIST>
 *        <LI>zero on success</LI>
 *        <LI>-1 on failure or error</LI>
 *	</LIST>
 *   <Errors>
 *      A corrupt database may cause a fatal or E_BUG error.
 *   <SeeAlso>
 *	LQT_OpenDatabase
 *	LQT_ObtainWriteAccess
 *	LQT_OpenKeyValueDatabase
 *	LQT_CloseDatabase
 * </Function>
 */
API int
LQT_ObtainReadOnlyAccess(db)
    t_LQTEXT_Database *db;
{
    int fd;

    db->FileFlags = O_RDONLY;

    fd = open(db->DataBase, db->FileFlags, db->FileModes);

    if (fd >= 0) {
	(void) close(fd);
	return 0; /* seems to be working, at least for that file. */
    } else {
	return -1;
    }
}

/* <Function>
 *   <Name>LQT_CurrentlyHaveWriteAccess
 *   <Class>Database/Database, Database/Files
 *   <Purpose>
 *      Returns non-zero if and only if the given database is open
 *	with write access.
 *   <Notes>
 *	Write access may on some systems be exclusive, so that no other
 *	process can open the database, neither for reading nor for writing.
 *	You should not rely on this, however.
 *   <SeeAlso>
 *      LQT_ObtainWriteAccess
 *	LQT_OpenDatabase
 * </Function>
 */
API int
LQT_CurrentlyHaveWriteAccess(db)
    t_LQTEXT_Database *db;
{
    return ((db->FileFlags & O_RDWR) == O_RDWR);
}

/* <Function>
 *   <Name>LQT_GetFileModes
 *   <Class>Database/Retrieval, Database/Physical
 *   <Purpose>
 *	Returns the current file modes, as determined by 
 *	LQT_ObtainReadOnlyAccess or LQT_ObtainWriteAccess, in Flagsp and
 *	Modesp.
 *	The returned values are suitable for passing to open(2).
 *   <Errors>
 *      Passing null pointers causes a fatal (E_BUG) error.
 *   <SeeAlso>
 *	LQT_OpenDatabase
 *	LQT_ObtainWriteAccess
 *	LQU_Eopen
 * </Function>
 */
API void
LQT_GetFileModes(db, Flagsp, Modesp)
    t_LQTEXT_Database *db;
    int *Flagsp;
    int *Modesp;
{
    if (!Flagsp || !Modesp) {
	Error(E_BUG, "LQT_GetFileModes(Flagsp=0x%x, Modesp=0x%x): %s zero",
	    Flagsp, Modesp,
	    (Flagsp) ? "Flagsp" : "Modesp"
	);
    }
    *Flagsp = db->FileFlags;
    *Modesp = db->FileModes;
}

/* <Function>
 *   <Name>LQT_OpenKeyValueDatabase
 *   <Class>Database/Dynamic Hashing, Database/Files
 *   <Purpose>
 *      <P>Opens an ndbm-style database of the given name, creating it if
 *	the current database modes allow it.</P>
 *	<P>The result is stored by the caller in either db->WordInfoDB
 *	or db->FileInfoDB and will be closed only when the entire
 *	database is closed.</P>
 *   <Returns>
 *      A handle (usually a DBM * pointer) to the named Key Value Database.
 *   <Errors>
 *	If the underlying ndbm-style database couldn't be opened, a fatal
 *	error is produced (E_FATAL|E_SYS) indicating the problem.
 *	One possible cause of this is that $HOME/LQTEXTDIR isn't a directory,
 *	or doesn't exist, and $LQTEXTDIR isn't set to point to a suitable
 *	alternate directory.
 *	Another possible problem is that a previous run of lqaddfile
 *	failed, and left the Key Value Databases locked for writing; the
 *	best thing to do in this case is to run the lqclean program and
 *	start again.
 *   <SeeAlso>
 *	LQT_CloseKeyValueDatabase
 *      LQT_OpenDatabase
 *	LQT_AddActionOnClose
 *	LQT_SyncDatabase
 * </Function>
 */
API DBM *
LQT_OpenKeyValueDatabase(db, FilePrefix)
    t_LQTEXT_Database *db;
    char *FilePrefix;
{
    DBM *result = 0;

#ifdef dbnative
    {
#ifdef USE_DB_1_85_H
	HASHINFO *H =  (HASHINFO *) emalloc("db parameters", sizeof(HASHINFO));

	H->bsize = 4096;
	H->ffactor = 1000;
	H->nelem = 33000;
	H->cachesize = 1 * 1024 * 1024; /* 1 MB cache */
	H->hash = 0; /* default */
	H->lorder = 0; /* default */

	result = dbopen(
	    FilePrefix,
	    db->FileFlags | O_CREAT,
	    db->FileModes,
	    DB_HASH,
	    H
	);

	/* Should H be freed now?  I assume not */
	/* since we only ever open two of these, best to be safe */
#else
	/* more recent db than 1.85 */

	DB *dbp;
	int ret;

	if ((ret = db_create(&dbp, (DB_ENV *) NULL, 0)) != 0 || !dbp) {
	    Error(E_FATAL|E_INTERNAL,
		"couldn't create internal DB structure [%s]",
		FilePrefix,
		db_strerror(ret)
	    );
	}
	dbp->set_cachesize(dbp, 
	    0L, /* gigabytes */
	    1 * 1024 * 1024, /* + bytes */
	    1 /* unused for a cache this small, see db3 api docs */
	);
	dbp->set_pagesize(dbp, 4096);
	dbp->set_h_ffactor(dbp, 1000);
	dbp->set_h_nelem(dbp, 33000);
	ret = dbp->open(
	    dbp,
#if DB_VERSION_MAJOR>3
	    (DB_TXN *) NULL,
#endif
	    FilePrefix,
	    NULL,
	    DB_HASH,
	    DB_CREATE,
	    db->FileModes
	);
	if (ret != 0) {
	    Error(E_FATAL,
		"couldn't open DB database \"%s\"; is $LQTEXTDIR set? [%s]",
		FilePrefix,
		db_strerror(ret)
	    );
	}
	result = dbp;

#endif
    }
#else
    /* not using db native API */
    result = dbm_open(
	FilePrefix,
	db->FileFlags,
	db->FileModes
    );
#endif

    if (result) {
	return result;
    } else {
	/* error handling */

	char *p = getenv("LQTEXTDIR");

	if (!p) {
	    Error(E_FATAL|E_SYS,
		"couldn't open dbm database \"%s\"; set $LQTEXTDIR",
		FilePrefix
	    );
	} else {
	    Error(E_FATAL|E_SYS,
		"couldn't open dbm database \"%s\" [$LQTEXTDIR is \"%s\"]",
		FilePrefix,
		p
	    );
	}
	return (DBM *) 0;
    }
}

#ifndef LQT_CloseKeyValueDatabase

/* <Function>
 *   <Name>LQT_CloseKeyValueDatabase
 *   <Class>Database/Dynamic Hashing, Database/Files
 *   <Purpose>
 *      This currently does nothing, since the Key Value Databases are
 *	kept open.  If the library is compiled with dbm instead of ndbm,
 *	or with the cache disabled, LQT_CloseKeyDatabase becomes active,
 *	so it should be paired with every call to LQT_OpenKeyValueDatabase
 *   <SeeAlso>
 *	LQT_SyncAndCloseAllKeyValueDatabases
 * </Function>
 */
/*ARGSUSED*/
API void
LQT_CloseKeyValueDatabase(db)
    DBM *db;
{
    /* should check and print an error but not all versions of
     * dbm return something useful here.
     */
    fprintf(stderr, "*** LQT_CloseKeyValueDatabase(%x)\n", db);
    (void) dbm_close(db);
}
#endif

/* <Function>
 *   <Name>LQT_SyncAndCloseAllKeyValueDatabases
 *   <Class>Database/Dynamic Hashing, Database/Files
 *   <Purpose>
 *      <P>Closes all Key Value Databases that have been opened, after
 *	writing any pending data to disk.</P>
 *	<P>This function is registered automatically as an action to be
 *	performed when a database is closed or on a call to LQT_Sync, and
 *	should not normally need to be called directly.
 *	The return value and argument are for compatibility with
 *	LQT_AddActionOnClose.
 *	The argument must be a null pointer, for future compatibility.</P>
 *   <SeeAlso>
 *	LQT_OpenKeyValueDatabase
 *	LQT_AddActionOnClose
 *	LQT_CloseDatabase
 * </Function>
 */
API int
LQT_SyncAndCloseAllKeyValueDatabases(db)
    t_LQTEXT_Database *db;
{
    if (db->WordIndexDB) {
#ifdef dbnative
	int failure = db->WordIndexDB->close(db->WordIndexDB, 0);

	if (failure != 0) {
	    Error(E_WARN,
		"could not close BSD key/value database %s: %s",
		db->WordIndex, db_strerror(failure)
	    );
	}
#else
	(void) dbm_close(db->WordIndexDB);
#endif
	db->WordIndexDB = NULL;
    }

    if (db->FileIndexDB) {
#ifdef dbnative
	int failure = db->FileIndexDB->close(db->FileIndexDB, 0);

	if (failure != 0) {
	    Error(E_WARN,
		"could not close BSD key/value database %s: %s",
		db->FileIndex, db_strerror(failure)
	    );
	}
#else
	(void) dbm_close(db->FileIndexDB);
#endif
	db->FileIndexDB = NULL;
    }

    return 0;
}

LIBRARY char *
/* <Function>
 *   <Name>LQT_dberror
 *   <Class>Database/Dynamic Hashing, Database/Files
 *   <Purpose>
 *      <P>Provides an error message about a value returned by
 *      dbm_store.  Works only with dbnative; otherwise you
 *      will get "unknown dbm hash error".
 *   <SeeAlso>
 *	LQT_OpenKeyValueDatabase
 * </Function>
 */
LQT_dberror(s_val)
    int s_val;
{
#ifdef dbnative
    return db_strerror(s_val);
#else
    return "[unknown error]";
#endif
}

/* <Function>
 *   <Name>LQTp_CreateEmptyKeyValueDatabase
 *   <Class>Database/Dynamic Hashing, Database/Files
 *   <Purpose>
 *      <P>Some versions of dbm or ndbm provided with various Unix systems
 *	do not automatically create a new DBM file, even when asked to; it
 *	is necessary to create the file with the open(2) or creat(2) system
 *	calls.  The original Unix dbm library was like this.</P>
 *	<P>This function creates the necessary files, in the given
 *	Directory; the files will have names beginning with the given
 *	Prefix, and depending on the version of ndbm in use, may have a
 *	suffix such as .db; BSD db uses a single file, but most other
 *	implementations use two, one called Prefix.dir and one called
 *	Prefix.pag.</P>
 *	<P>This routine is called automatically by LQT_OpenKeyValueDatabase
 *	when necessary, but is made available for general use
 *	for convenience.</P>
 *   <Bugs>
 *	LQTp_CreateEmptyKeyValueDatabase should be in liblqutil instead.
 *   <SeeAlso>
 *	LQT_OpenKeyValueDatabase
 * </Function>
 */
LIBRARY char *
LQTp_CreateEmptyKeyValueDatabase(db, Directory, prefix)
    t_LQTEXT_Database *db;
    char *Directory;
    char *prefix;
{

    char *p = LQU_joinstr3(Directory, "/", prefix);

    return p; /* the prefix for dbm, not the whole path */
}

int
lqdbnativestore(db, key, value, flags)
    DB *db;
    DBT *key;
    DBT *value;
    u_int32_t flags;
{
    int ret = (db)->put(db,
#if DB_VERSION_MAJOR>3
	    (DB_TXN *)NULL,
#endif
	    key, value, flags);

    if (ret != 0) {
	/* TODO use Error here instead of LQT_kvpdbg */
	LQT_kvpdbg("error storing pair; key: ", *key);
	LQT_kvpdbg(" value ", *value);
	fprintf(stderr, "\n");
	Error(E_FATAL,
	    "could not save pair to BSD key/value database 0x%x: %s [flags %ld]",
	    db,
	    db_strerror(ret),
	    flags
	);
    }
}


/* key-value pair debugging */
LIBRARY int
LQT_kvpdbg(char *prefix, DBT t)
{
    unsigned char *p;

    fprintf(stderr, prefix);
    putc('[', stderr);
    for (p = t.dptr; p - (unsigned char *) t.dptr < t.dsize; p++) {
	if (*p >= ' ' && *p <= '~') {
	    putc(*p, stderr);
	} else {
	    fprintf(stderr, "\\%03o", *p);
	}
    }
    putc(']', stderr);
    return 37; /* so I can use it in a comma expression */
}
