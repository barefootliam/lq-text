/* apmatch.c -- Copyright 1995 Liam R. E. Quin.
 * All Rights Reserved.
 * This code is NOT in the public domain.
 * See the file COPYRIGHT for full details.
 *
 * $Id: apmatch.c,v 1.4 2019/04/21 06:06:40 lee Exp $
 *
 * print a match to stdout and accept it so it gets remembered in memory
 */

#include "error.h"
#include "globals.h" /* defines and declarations for database filenames */

#include <stdio.h> /* stderr, also for fileinfo.h */
#include <sys/types.h>

#ifdef HAVE_FCNTL_H
# ifdef HAVE_SYSV_FCNTL_H
#  include <sys/stat.h>
# endif
# include <fcntl.h>
#endif

#ifdef HAVE_STDLIB_H
# include <stdlib.h>
#else
# include <malloc.h>
#endif

#include <ctype.h>

#ifdef HAVE_STRING_H
# include <string.h>
#else
# include <strings.h>
#endif

#include "fileinfo.h" /* for wordinfo.h */
#include "wordinfo.h"
#include "pblock.h"
#include "phrase.h"
#include "wordrules.h"
#include "emalloc.h"
#include "lqutil.h"
#include "liblqtext.h"
#include "lqtrace.h"

/** Unix system calls that need to be declared: **/
/** Unix/C Library Functions: **/
/** lqtext functions: **/
/** functions within this file that need forward declarations **/

/** **/

/* <Function>
 *   <Name>LQT_PrintAndAcceptOneMatch
 *   <Class>Retrieval/Matching, Retrieval/Phrases
 *   <Purpose>
 *      <P>This is intended for use as a callback function to be passed
 *	as an argument to LQT_MakeMatchesWhere.
 *	It prints each match to stdout as it is read from disk, and
 *	also accepts it so that it is retained in the Phrase data structure.
 *   <Returns>
 *      LQM_ACCEPT
 *   <SeeAlso>
 *      LQT_StringToPhrase
 *      LQT_PrintAndRejectOneMatch
 * </Function>
 */
API int
LQT_PrintAndAcceptOneMatch(db, Phrase, Match)
    t_LQTEXT_Database *db;
    t_Phrase *Phrase;
    t_Match *Match;
{
    static t_FID LastFID = (t_FID) 0;
    static t_FileInfo *FileInfo = 0;

    if (Match != (t_Match *) 0) {
	if (Match->Where->FID != LastFID) {
	    t_FID FID = Match->Where->FID;
	    /*TODO: use DestroyFileInfo instead of efree:... */

	    if (FileInfo) {
		LQT_DestroyFileInfo(db, FileInfo);
	    }

	    if ((FileInfo = LQT_FIDToFileInfo(db, FID)) == (t_FileInfo *) 0) {
		return 0; /* don't want this match! */
	    }
	    LastFID = FID;
	}

	if (LQT_TraceFlagsSet(LQTRACE_VERBOSE|LQTRACE_DEBUG)) {
	    printf("NW %-3d BIF %-7lu WIB %-7u Sep %-3d F %-4o FID %-4ld %s\n",
		LQT_NumberOfWordsInPhrase(db, Phrase),
		Match->Where->BlockInFile,
		(unsigned) Match->Where->WordInBlock,
		(unsigned) Match->Where->StuffBefore,
		(unsigned) Match->Where->Flags,
		FileInfo->FID,
		FileInfo->Name
	    );
	} else {
	    printf("%d %lu %lu %lu %s\n",
		LQT_NumberOfWordsInPhrase(db, Phrase),
		Match->Where->BlockInFile,
		Match->Where->WordInBlock,
		FileInfo->FID,
		FileInfo->Name
	    );
	}
    }
    return LQMATCH_ACCEPT;
}

