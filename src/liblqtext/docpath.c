/* docpath.c -- Copyright 1989, 1993, 1994 Liam R. E. Quin.
 * All Rights Reserved.
 * This code is NOT in the public domain.
 * See the file COPYRIGHT for full details.
 *
 * $Id: docpath.c,v 1.17 2019/04/21 06:06:40 lee Exp $
 *
 */

#include "globals.h" /* defines and declarations for database filenames */
#include "error.h"

#include <stdio.h> /* for ANSI C */

#include <sys/types.h>
#ifdef HAVE_STRING_H
# include <string.h>
#else
# include <strings.h>
#endif

#ifdef HAVE_LIMITS_H
# include <limits.h> /* for PATH_MAX */
#else
# include <sys/param.h>
#endif

#ifndef PATH_MAX
# ifdef MAXPATHLEN
#  define PATH_MAX MAXPATHLEN
# else
#  define PATH_MAX 4096 /* safe amount.... */
# endif
#endif

#ifdef HAVE_STDLIB_H
# include <stdlib.h>
#endif

#ifdef HAVE_UNISTD_H
# include <unistd.h>
#endif

#include "emalloc.h"
#include "fileinfo.h"
#include "lqutil.h"
#include "liblqtext.h"
#include "lqtrace.h"

/** **/

typedef struct s_DocPath {
    char *DirName;
    struct s_DocPath *Next;
} t_DocPath;

static t_DocPath *XDocPath = 0;

#ifndef PATH_MAX
# define PATH_MAX 2048
#endif

/* <Function>
 *   <Name>LQT_FindFile
 *   <Class>Database/Documents
 *   <Purpose>
 *	<P>Returns a pointer to a full pathname, given a filename as stored
 *	in the lq-text File index.
 *	The current database DocPath is searched, and if that fails, an
 *	attempt is made to find the file with .bz2 appended, then with .gz,
 *      then with .Z appended.</P>
 *	<P>The returned string points to a static buffer, and should not
 *	be freed.  The buffer is overwritten on successive calls to
 *	LQT_FindFile(). This is not thread-safe.</P>
 *   <Bugs>
 *	Does not understand the archive name notation, archive(filename).
 *	Not thread safe.
 * </Function>
 */
API char *
LQT_FindFile(db, Name)
    t_LQTEXT_Database *db;
    char *Name;
{
    t_DocPath *p;
    static char Buffer[PATH_MAX + 5]; /* +5 for ".bz2\0" */

    if (!Name || !*Name) {
	Error(E_FATAL|E_INTERNAL,
	    "LQT_FindFile called to find file with %s name",
	    Name ? "empty" : "null"
	);
    }

    /* check for absolute and explicit relative names first */
    if (*Name == '/' ||
	(*Name == '.' &&
	    (Name[1] == '/' ||
		(Name[1] == '.' && Name[2] == '/')))
    ) {
	if (LQU_IsFile(Name)) {
# ifdef ASCIITRACE
	    LQT_Trace(LQTRACE_FINDFILE,
		"FindFile(%s) --> %s\n", Name, Buffer
	    );
# endif
	    return Name;
	}
    }

    if (!XDocPath) {
	if (LQU_IsFile(Name)) {
#ifdef ASCIITRACE
	    LQT_Trace(LQTRACE_FINDFILE, "FindFile(%s) --> %s", Name);
#endif
	    return Name;
	}

	(void) sprintf(Buffer, "%s.bz2", Name);

	if (LQU_IsFile(Buffer)) {
#ifdef ASCIITRACE
	    LQT_Trace(LQTRACE_FINDFILE, "FindFile(%s) --> %s", Name);
#endif
	    return Buffer;
	}

	(void) sprintf(Buffer, "%s.gz", Name);

	if (LQU_IsFile(Buffer)) {
#ifdef ASCIITRACE
	    LQT_Trace(LQTRACE_FINDFILE, "FindFile(%s) --> %s", Name);
#endif
	    return Buffer;
	}

	(void) sprintf(Buffer, "%s.Z", Name);

	if (LQU_IsFile(Buffer)) {
#ifdef ASCIITRACE
	    LQT_Trace(LQTRACE_FINDFILE, "FindFile(%s) --> %s", Name);
#endif
	    return Buffer;
	}

#ifdef ASCIITRACE
	LQT_Trace(LQTRACE_FINDFILE, "FindFile(%s) --> 0 (FAIL)");
#endif
	return (char *) 0;
    }

    for (p = XDocPath; p; p = p->Next) {
	(void) sprintf(Buffer, "%s/%s", p->DirName, Name);
	if (LQU_IsFile(Buffer)) {
#ifdef ASCIITRACE
	    LQT_Trace(LQTRACE_FINDFILE, "FindFile(%s) --> %s", Name, Buffer);
#endif
	    return Buffer;
	}

	(void) sprintf(Buffer, "%s/%s.bz2", p->DirName, Name);
	if (LQU_IsFile(Buffer)) {
	    (void) sprintf(Buffer, "%s/%s", p->DirName, Name);
#ifdef ASCIITRACE
	    LQT_Trace(LQTRACE_FINDFILE, "FindFile(%s) --> %s", Name, Buffer);
#endif
	    return Buffer;
	}

	(void) sprintf(Buffer, "%s/%s.gz", p->DirName, Name);
	if (LQU_IsFile(Buffer)) {
	    (void) sprintf(Buffer, "%s/%s", p->DirName, Name);
#ifdef ASCIITRACE
	    LQT_Trace(LQTRACE_FINDFILE, "FindFile(%s) --> %s", Name, Buffer);
#endif
	    return Buffer;
	}

	(void) sprintf(Buffer, "%s/%s.Z", p->DirName, Name);
	if (LQU_IsFile(Buffer)) {

	    (void) sprintf(Buffer, "%s/%s", p->DirName, Name);
#ifdef ASCIITRACE
	    LQT_Trace(LQTRACE_FINDFILE, "FindFile(%s) --> %s", Name, Buffer);
#endif
	    return Buffer;
	}

#ifdef ASCIITRACE
	LQT_Trace(LQTRACE_FINDFILE, "FindFile(%s) looked in % for %s",
		Name, p->DirName, Buffer);
#endif
    }

#ifdef ASCIITRACE
    LQT_Trace(LQTRACE_FINDFILE, "FindFile(%s) --> 0 (FAIL)");
#endif

    return (char*) 0;
}

LIBRARY int
LQTp_MakeDocPath(db, Path)
    t_LQTEXT_Database *db;
    char *Path;
{
    char *Start, *End;
    t_DocPath **dpp;

    if (XDocPath == (t_DocPath *) 0) {
	dpp = &XDocPath;
	*dpp = (t_DocPath *) 0;

	/* For each element in DocPath, */
	for (Start = Path; Start && *Start; Start = End) {
	    char SaveEnd;

	    /* find the end of this bit of the path */
	    for (End = Start; *End && *End != ':'; End++)
		;
	    
	    if (End == Start) break;

	    SaveEnd = (*End);
	    *End = '\0';

	    /* if not a directory, delete from path */
	    if (!LQU_IsDir(Start)) {
		*End = SaveEnd;
		continue;
	    }

	    /* add to the linked list */
	    *dpp = (t_DocPath *) emalloc("DocPath entry", sizeof(t_DocPath));
	    (*dpp)->DirName = emalloc("LQTp_MakeDocPath.DirName",strlen(Start) + 1);
	    (void) strcpy((*dpp)->DirName, Start);
	    dpp = &(*dpp)->Next;
	    (*dpp) = (t_DocPath *) 0;
	    if ((*End = SaveEnd) != '\0') {
		End++;
	    }
	}
    }
    return 0;
}

