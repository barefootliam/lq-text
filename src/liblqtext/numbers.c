/* numbers.c -- Copyright 1989, 1992, 1994, 1996 Liam R. E. Quin.
 * All Rights Reserved.
 * This code is NOT in the public domain.
 * See the file COPYRIGHT for full details.
 *
 * $Id: numbers.c,v 1.16 1996/05/15 23:01:03 lee Exp $
 */

#include "error.h"
#include <stdio.h>
#include <sys/types.h>
#include "globals.h"

#include "liblqtext.h"

#ifndef HAVE_INLINE

  /* Define the functions that are in the header file: */
# define HAVE_INLINE
# include "numbers.h"
# undef HAVE_INLINE

#endif /* HAVE_INLINE */
