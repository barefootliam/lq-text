/* filtertype.c -- Copyright 1989, 1992, 1994, 1995, 1996 Liam R. E. Quin.
 * All Rights Reserved.
 * This code is NOT in the public domain.
 * See the file COPYRIGHT for full details.
 */

/* FilterType -- determine how to deal with a given file.
 * Part of Liam Quin's LQ-Text text retrieval package.
 *
 * $Id: rwfilters.c,v 1.3 2019/04/21 06:06:40 lee Exp $
 *
 */

#include "error.h"
#include "globals.h"

#include <stdio.h>
#include <errno.h>
#include <ctype.h>

#include <sys/types.h>
#include <sys/stat.h>

#ifdef HAVE_FCNTL_H
#include <fcntl.h>
#endif

#ifdef HAVE_UNISTD_H
# include <unistd.h>
#endif

#ifdef HAVE_STDLIB_H
# include <stdlib.h>
#else
# include <malloc.h>
#endif

#include "emalloc.h"

#ifdef HAVE_STRING_H
# include <string.h>
#else
# include <strings.h>
#endif

#include "fileinfo.h"
#include "lqutil.h"
#include "liblqtext.h"
#define FILTERDEF /* see filter.h */
#include "filter.h"

/* The current filter types are:
 * FTYPE_NEWS  1
 * FTYPE_MAIL  2
 * FTYPE_MOSTLYASCII 4
 * FTYPE_C_SOURCE 5 -- unimplemented for now
 * FTYPE_SGML 6
 */

/* LQT_InitFilterTable might one day be called from Defaults.c....
 * At which point, it will read an ascii file that describes the
 * various filters, I suppose.
 */

LIBRARY int
LQT_InitFilterTable(db)
    t_LQTEXT_Database *db;
{
    /* This function just initialises the functions.
     * In this way, programs that don't use the filters don't
     * get linked in with them.
     */
    
    /* do the base initialisation: */
    LQTpInitReadOnlyPartOfFilterTable(db);

    /* now bring in all the filters */
    LQTpFilterTable[FTYPE_DEFAULT].copyFile = 0;
    LQTpFilterTable[FTYPE_SGML].copyFile = LQF_SGML_Copy;
    LQTpFilterTable[FTYPE_NEWS].copyFile = LQF_NetNews_Copy;
    LQTpFilterTable[FTYPE_MAIL].copyFile = LQF_RFC822_Copy;
    LQTpFilterTable[FTYPE_TROFF].copyFile = LQF_Troff_Copy;
    LQTpFilterTable[FTYPE_XML].copyFile = LQF_XML_Copy;
    LQTpFilterTable[FTYPE_XML + 1].copyFile = 0;

    return 0;
}
