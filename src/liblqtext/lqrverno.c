/* lqversion.c -- Copyright 1993, 1994, 1996 Liam R. E. Quin.
 * All Rights Reserved.
 * This code is NOT in the public domain.
 * See the file COPYRIGHT for full details.
 */

#ifndef LINT
static char *RcsId = "@(#) $Id: lqrverno.c,v 1.3 1996/05/15 00:01:21 lee Exp $";
#endif

#include "globals.h" /* defines and declarations for database filenames */
#include "error.h"
#include <stdio.h>
#include <sys/types.h>
#include "revision.h"
#include "blkheader.h"
#include "block0.h"
#include "liblqtext.h"

PRIVATE char *
lqFlagString(byte)
    unsigned char byte;
{
    if ((byte & 01) == 0) {
	return "[unknown, or flags corrupt]";
    }
    if (byte & 02) {
	return "-DWIDINBLOCK";
    } else {
	return "-UWIDIINBLOCK";
    }
}

/* <Function>
 *   <Name>LQT_CheckDatabaseVersion
 *   <Class>Database/Database
 *   <Purpose>
 *      Checks that the current database is compatible with this version of
 *	the library.  Some versions of liblqtext may have a backwards
 *	compatibility mode, which this function will enable.
 *	This routine is called automatically whenever an lq-text database
 *	is opened.
 *   <Notes>
 *	The liblqtext library is capable of maintaining backward
 *	compatibility with earlier versions; for example, Release 1.13 
 *	could read a database created with Release 1.12; this feature is
 *	not presently included, however.  In practice, it's almost always
 *	possible to index the data again rather than using backwards
 *	compatibility modes, and performance is usually then better.
 *   <Errors>
 *      Fatal error if the database is incompatible with the current version
 *	of the lqtext library.
 * </Function>
 */
API void
LQT_CheckDatabaseVersion(db)
    t_LQTEXT_Database *db;
{
    int MajorVersion;
    int MinorVersion;
    unsigned char *p;

    p = (unsigned char *) LQT_ReadBlock(db, 0L, (t_WID) 0);

    if (p == 0) { /* shouldn't happen */
	LQT_WriteVersionToDatabase(db);
	return;
    }

    MajorVersion = (
	(unsigned char) p[BLOCK_ZERO_MAJORVERSION0]) * 256 +
	(unsigned char) p[BLOCK_ZERO_MAJORVERSION1];
    MinorVersion = p[BLOCK_ZERO_MINORVERSION];

    /* if MajorVersion is 0, we've never initialised block 0 */
    if (MajorVersion == 0 && p[BLOCK_ZERO_COMPILEFLAGBYTE] == 0) {
	LQT_WriteVersionToDatabase(db);
	return;
    }

    if (p[BLOCK_ZERO_COMPILEFLAGBYTE] != LQ_FLAG_VALUE) {
	Error(E_FATAL,
	    "Mismatch: Database flags %d [%s] compiled %d [%s]",
	    (int) *p,
	    lqFlagString(*p),
	    LQ_FLAG_VALUE,
	    lqFlagString(LQ_FLAG_VALUE)
	);
    }

    if (MajorVersion != LQ_MAJOR_VERSION || MinorVersion != LQ_MINOR_VERSION) {
	Error(E_FATAL,
	    "Database was written with lqtext %d.%d, %d.%d can't read it",
	    MajorVersion, MinorVersion, LQ_MAJOR_VERSION, LQ_MINOR_VERSION
	);
    }
}

