/* prflags.c -- Copyright 1994 Liam R. E. Quin.
 * All Rights Reserved.
 * This code is NOT in the public domain.
 * See the file COPYRIGHT for full details.
 *
 * $Id: prflags.c,v 1.9 2019/04/21 06:06:40 lee Exp $
 *
 * Turn flags into a string.
 */

#include "globals.h" /* defines and declarations for database filenames */
#include "error.h"

#include <sys/types.h> /* for fileinfo.h */
#ifndef FILE
# include <stdio.h>
#endif
#ifdef HAVE_STRING_H
# include <string.h>
#else
# include <strings.h>
#endif

#ifdef HAVE_STDLIB_H
# include <stdlib.h>
#else
# include <malloc.h>
#endif

#include "fileinfo.h"
#include "wordinfo.h"
#include "wordrules.h"
#include "emalloc.h"
#include "lqutil.h"
#include "liblqtext.h"

/** Unix system calls that need declaring: **/
/** Unix/C Library Functions that need declaring: **/
/** lqtext library functions that need declaring: **/
/** Functions within this file that are used before being defined: **/
/** **/

/* <Function>
 *   <Name>LQT_FlagsToString
 *   <Class>Tracing
 *   <Purpose>
 *      <P>Returns a printable string representation of the given flags,
 *	primarily intended for humans to read.
 *	The WordFlagNamePairArray argument is an array of (Name, Value)
 *	pairs; for each such pair, if all set bits in Value are also
 *	set in the Flags argument passed to LQT_FlagsToString, the
 *	corresponding Name string is appended to the result.
 *	The array is terminated by a pair with a null Name pointer; this
 *	is used rather than a count so that the array can be initialised
 *	at compile time.</P>
 *
 *	<P>Adjacent Names in the result are separated with the given
 *	separator.
 *	If the flags are zero, the array is searched for a Value of zero, and,
 *	if one is found, the corresponding Name is used; to have a zero value
 *	return an empty string, use a pair with Name pointing to a zero-length
 *	string, not a null pointer.
 *	If no zero Value is found, the string "none" is used instead.</P>
 *   <Returns>
 *	a pointer to a statically allocated string.
 *   <Errors>
 *	Passing null or invalid values for WordFlagNamePairArray or Separator
 *	will cause unpredictable results.
 *	There must be enough memory to allocate the result, which grows
 *	automatically as needed, but never shrinks.
 *   <SeeAlso>
 *	LQT_StringToFlags
 * </Function>
 */ 
API char *
LQT_FlagsToString(Flags, WordFlagNamePairArray, Separator)
    unsigned long Flags;
    t_FlagNamePair *WordFlagNamePairArray;
    char *Separator;
{
    static char *Result = 0;
    static unsigned int AllocatedResultLength = 0;
    int UsedResultLength = 1; /* for the null */
    int SeparatorLength = strlen(Separator);

    t_FlagNamePair *wp;
    int i;

    if (!AllocatedResultLength) {
	AllocatedResultLength = 20;
	Result = emalloc("FlagsToString buffer", AllocatedResultLength);
    }
    Result[0] = '\0';

    if (Flags == 0) {
	for (i = 0; (wp = &WordFlagNamePairArray[i])->Name; i++) {
	    if (wp->Value == 0) {
		if (AllocatedResultLength < strlen(wp->Name) + 1) {
		    AllocatedResultLength = strlen(wp->Name) + 1;
		    Result = erealloc(Result, AllocatedResultLength);
		}
		(void) strcpy(Result, wp->Name);
		return Result;
	    }
	}
	(void) strcpy(Result, "none");
	return Result;
    }

    for (i = 0; (wp = &WordFlagNamePairArray[i])->Name; i++) {
	unsigned int wpLength;

	if ((Flags & wp->Value) == wp->Value && wp->Value) {
	    wpLength = strlen(wp->Name);

	    if (wpLength + UsedResultLength + SeparatorLength + 1 >=
							AllocatedResultLength
	    ) {
		AllocatedResultLength += UsedResultLength + 2;
		Result = erealloc(Result, AllocatedResultLength);
	    }

	    if (UsedResultLength > 1) {
		(void) strcat(Result, Separator);
		UsedResultLength += SeparatorLength + 1;
	    }

	    (void) strcat(Result, wp->Name);
	    UsedResultLength += wpLength;
	    Flags &= ~(wp->Value);
	    if (!Flags) {
		break;
	    }
	}
    }

    return Result;
}

/* <Function>
 *   <Name>LQT_WordFlagsToString
 *   <Class>Tracing
 *   <Purpose>
 *      Returns a printable string representation of the given
 *	WordFlags, intended for humans to read as well as for
 *	use with the LQT_WordFlagsToString function.
 *	The flags are defined in <h>wordrules.h</h> and are explained
 *	under the Language category.
 *   <Returns>
 *	a pointer to a statically allocated string.
 *   <Errors>
 *	There must be enough memory to allocate the result, which grows
 *	automatically as needed, but never shrinks.
 *   <SeeAlso>
 *	LQT_StringToWordFlags
 *	LQT_FlagsToString
 * </Function>
 */ 
API char *
LQT_WordFlagsToString(db, Flags)
    t_LQTEXT_Database *db;
    t_WordFlags Flags;
{
    /* eventually there will be a per-database set of flags */
    return LQT_FlagsToString(
	(unsigned long) Flags, LQTp_WordFlagArray, LQTpWordFlagSep
    );
}
