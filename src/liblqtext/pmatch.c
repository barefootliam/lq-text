/* pmatch.c -- Copyright 1994, 1996 Liam R. E. Quin.
 * All Rights Reserved.
 * This code is NOT in the public domain.
 * See the file COPYRIGHT for full details.
 *
 * $Id: pmatch.c,v 1.3 2019/04/21 06:06:40 lee Exp $
 *
 * LQT_PrintMatch -- print one complete match
 */

#include "error.h"
#include "globals.h" /* defines and declarations for database filenames */

#ifndef FILE
#include <stdio.h> /* stderr, also for fileinfo.h */
#endif

#include <sys/types.h>

#ifdef HAVE_STRING_H
# include <string.h>
#else
# include <strings.h>
#endif

#include "emalloc.h" /* for efree() */
#include "fileinfo.h" /* for wordinfo.h */
#include "wordinfo.h"
#include "pblock.h"
#include "phrase.h"
#include "lqutil.h"
#include "liblqtext.h"

#ifdef HAVE_STDLIB_H
# include <stdlib.h>
#else
# include <malloc.h>
#endif

#ifdef HAVE_UNISTD_H
# include <unistd.h>
#endif


/** System calls and functions... **/
/** Unix system calls used in this file: **/
/** Unix Library Functions used: **/
/** lqtext library functions: **/
/** functions used before they're defined within this file: **/
/** **/

/* <Function>
 *   <Name>LQT_fPrintOneMatch
 *   <Class>Output
 *   <Purpose>
 *	<P>Prints a single match to the given stdio file descriptor,
 *	in a form that will subsequently be understood by the routines
 *	and programs that read matches.</P>
 *   <Notes>
 *	<P>Not all the information stored in the database index for each match
 *	is printed by LQT_fPrintOneMatch.
 *	A future release will allow you to change the print format (using
 *	a Name Space).</P>
 * </Function>
 */
API void
LQT_fPrintOneMatch(db, theFile, FirstNumber, FileInfo, WordPlace)
    t_LQTEXT_Database *db;
    FILE *theFile;
    int FirstNumber;
    t_FileInfo *FileInfo;
    t_WordPlace *WordPlace;
{
    register char *p;
    unsigned long l;
    register int i;
    char Buffer[50];

    /* Avoid using printf, for speed... */

    /* first number -- words in phrase, or Query Matched */
    p = &Buffer[sizeof(Buffer) - 1];
    *p = '\0';
    if (WordPlace->BlockInFile == 0) {
	*--p = '0';
    } else for (i = FirstNumber; i; i /= 10) {
	*--p = "0123456789"[i % 10];
    }
    (void) fputs(p, theFile);
    putc(' ', theFile);

    /* Block In File */
    p = &Buffer[sizeof(Buffer) - 1];
    *p = '\0';
    if (WordPlace->BlockInFile == 0) {
	*--p = '0';
    } else for (l = WordPlace->BlockInFile; l; l /= 10) {
	*--p = "0123456789"[l % 10];
    }
    (void) fputs(p, theFile);
    putc(' ', theFile);

    p = &Buffer[sizeof(Buffer) - 1];
    *p = '\0';
    l = WordPlace->WordInBlock;
    if (l == 0) {
	*--p = '0';
    } else for (; l; l /= 10) {
	*--p = "0123456789"[l % 10];
    }
    (void) fputs(p, theFile);
    putc(' ', theFile);

    /* FID */
    p = &Buffer[sizeof(Buffer) - 1];
    *p = '\0';
    if (FileInfo->FID == 0) {
	*--p = '0';
    } else for (l = FileInfo->FID; l; l /= 10) {
	*--p = "0123456789"[l % 10];
    }
    (void) fputs(p, theFile);

    if (FileInfo->Name) {
	putc(' ', theFile);
	fputs(FileInfo->Name, theFile);
    }
    putc('\n', theFile);
}
