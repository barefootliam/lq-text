/* Phrase.c -- Copyright 1989, 1994, 1995, 1996 Liam R.E. Quin.
 * All Rights Reserved.
 * This code is NOT in the public domain.
 * See the file COPYRIGHT for full details.
 */

/*
 * Deal with (WID, FID, Offfset) triples
 * Liam Quin, September 1989
 *
 * $Id: freephr.c,v 1.2 2019/04/21 06:06:40 lee Exp $
 *
 */

#include "error.h"
#include "globals.h" /* defines and declarations for database filenames */

#include <stdio.h> /* stderr, also for fileinfo.h */
#include <ctype.h>

#ifdef HAVE_STRING_H
# include <string.h>
#else
# include <strings.h>
#endif

#include <sys/types.h>
#ifdef HAVE_FCNTL_H
# ifdef HAVE_SYSV_FCNTL_H
#  include <sys/stat.h>
# endif
# include <fcntl.h>
#endif

#ifdef HAVE_STDLIB_H
# include <stdlib.h>
#else
# include <malloc.h>
#endif

#include "fileinfo.h" /* for wordinfo.h */
#include "wordinfo.h"
#include "pblock.h"
#include "phrase.h"
#include "wordrules.h"
#include "emalloc.h"
#include "lqutil.h"
#include "liblqtext.h"
#include "lqtrace.h"

/** Unix system calls that need to be declared: **/
/** Unix/C Library Functions: **/
/** lqtext functions: **/
/** functions within this file that need forward declarations **/

/** **/

/* <Function>
 *   <Name>LQT_DestroyPhrase
 *   <Class>Retrieval/Phrases, Memory
 *   <Purpose>
 *      <P>Frees any memory associated with the given phrase,
 *	and then frees the Phrase itself.
 *	After calling LQT_DestroyPhrase, it is an error to attempt to
 *	dereference the Phrase, and the operating system may detect this
 *	and raise an exception or send a fatal signal.</P>
 *   <Notes>
 *	LQT_DestroyPhrase does not follow the Next element of the given Phrase.
 *	A caller doing this should take a copy of Phrase->Next before calling
 *	LQT_DestroyPhrase, as after the call the pointer itself will be
 *	inaccessible.
 *   <SeeAlso>
 *	LQT_StringToPhrase
 * </Function>
 */
API void
LQT_DestroyPhrase(db, Phrase)
    t_LQTEXT_Database *db;
    t_Phrase *Phrase;
{
    t_PhraseItem *PI;
    t_MatchList *ML;

    if (!Phrase) {
	return;
    }
    if (Phrase->OriginalString) {
	efree(Phrase->OriginalString);
    }
    if (Phrase->ModifiedString) {
	efree(Phrase->ModifiedString);
    }

    for (PI = Phrase->Words; PI;) {
	t_PhraseItem *PI_Next = PI->Next;

	if (PI->Word) {
	    LQT_DestroyWordInfo(db, PI->Word);
	}
	efree((char *) PI);
	PI = PI_Next;
    }
    Phrase->Words = (t_PhraseItem *) 0;

    for (ML = Phrase->Matches; ML; ) {
	t_MatchList *ML_Next = ML->Next;

	if (ML->Match) {
	    t_Match *MP;

	    for (MP = ML->Match; MP; ) {
		t_Match *MP_Next = MP->Next;
		efree((char *) MP->Where);
		MP->Where = (t_WordPlace *) 0;
		MP = MP_Next;
	    }

	    ML->Match = (t_Match *) 0;
	}
	ML = ML_Next;
    }

    efree((char *) Phrase);
}

