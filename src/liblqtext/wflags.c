/* wflags.c -- Copyright 1994 Liam R. E. Quin.
 * All Rights Reserved.
 * This code is NOT in the public domain.
 * See the file COPYRIGHT for full details.
 *
 * $Id: wflags.c,v 1.5 2019/04/21 06:06:40 lee Exp $
 *
 * Default word flag names
 */

#include "globals.h" /* defines and declarations for database filenames */
#include "error.h"

#ifndef FILE
# include <stdio.h>
#endif

#include "wordrules.h"

/** Unix system calls that need declaring: **/
/** Unix/C Library Functions that need declaring: **/
/** lqtext library functions that need declaring: **/
/** Functions within this file that are used before being defined: **/
/** **/

t_FlagNamePair LQTp_WordFlagArray[] = {
    { WPF_WASPLURAL, "Plural" },
    { WPF_UPPERCASE, "UpperCase" },
    { WPF_POSSESSIVE, "Possessive" },
    { WPF_LASTHADPUNCT, "LastHadPunct" },
    { WPF_LASTWASCOMMON, "LastWasCommon" },
    { WPF_LASTHADLETTERS, "LastHadLetters" },
    { WPF_HASSTUFFBEFORE, "HasStuffBefore" },
    { WPF_LASTINBLOCK, "LastInBlock" },
    { WPF_NEXTHASPUNCT, "NextHasPunct" },
    { WPF_NEXTISCOMMON, "NextIsCommon" },
    { WPF_XMLATTRIBUTE, "XMLAttributeValue" },
    { WPF_XMLENTITYNAME, "XMLEntityReference" },
    { WPF_ALL, "All" },
    { 0, "none" },
    { 0, "None" },
    { 0, 0 }
};
