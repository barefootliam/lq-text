/* phrreset.c -- Copyright 1989, 1995, 1996 Liam R. Quin.
 * All Rights Reserved.
 * This code is NOT in the public domain.
 * See the file COPYRIGHT for full details.
 */

/*
 * Deal with (WID, FID, Offfset) triples
 * Liam Quin, September 1989
 *
 * $Id: phrreset.c,v 1.2 2019/04/21 06:06:40 lee Exp $
 *
 */

#include "error.h"
#include "globals.h" /* defines and declarations for database filenames */

#include <stdio.h> /* stderr, also for fileinfo.h */
#include <sys/types.h>

#ifdef HAVE_FCNTL_H
# ifdef HAVE_SYSV_FCNTL_H
#  include <sys/stat.h>
# endif
# include <fcntl.h>
#endif

#ifdef HAVE_STRING_H
# include <string.h>
#else
# include <strings.h>
#endif
#include <ctype.h>

#ifdef HAVE_STDLIB_H
# include <stdlib.h>
#else
# include <malloc.h>
#endif


#include "fileinfo.h" /* for wordinfo.h */
#include "wordinfo.h"
#include "pblock.h"
#include "phrase.h"
#include "wordrules.h"
#include "emalloc.h"
#include "lqutil.h"
#include "liblqtext.h"
#include "lqtrace.h"

/** Unix system calls that need to be declared: **/
/** Unix/C Library Functions: **/
/** lqtext functions: **/
/** functions within this file that need forward declarations **/

/** **/

/* <Function>
 *   <Name>LQT_ResetPhraseMatch
 *   <Class>Retrieval/Matching, Retrieval/Phrases
 *   <Purpose>
 *      Resets internal pointers within a phrase so that LQT_MakeMatches
 *	can be called.  This is also called by LQT_MakeMatches and
 *	LQT_MakeMatchesWhere, and is provided so that clients can write
 *	their own phrase matching routines compatibly.
 *   <SeeAlso>
 *	LQT_StringToPhrase
 *	LQT_MakeMatches
 * </Function>
 */
/*ARGSUSED2*/
API void
LQT_ResetPhraseMatch(db, thePhrase)
    t_LQTEXT_Database *db;
    t_Phrase *thePhrase;
{
    t_PhraseItem *Word;

    if (!thePhrase || !thePhrase->Words) return;

    for (Word = thePhrase->Words; Word; Word = Word->Next) {
	Word->SearchIndex = 0;
    }
    thePhrase->NumberOfMatches = 0;
}
