/* Phrase.c -- Copyright 1989, 1994, 1995, 1996 Liam R. E. Quin.
 * All Rights Reserved.
 * This code is NOT in the public domain.
 * See the file COPYRIGHT for full details.
 */

/*
 * Deal with (WID, FID, Offfset) triples
 * Liam Quin, September 1989
 *
 * $Id: phrunkw.c,v 1.1 1996/05/15 00:03:28 lee Exp $
 *
 */

#include "error.h"
#include "globals.h" /* defines and declarations for database filenames */

#ifndef FILE
# include <stdio.h>
#endif
#include <sys/types.h>

#include "liblqtext.h"
#include "phrase.h"
#include "lqtrace.h"

/** Unix system calls that need to be declared: **/
/** Unix/C Library Functions: **/
/** lqtext functions: **/
/** functions within this file that need forward declarations **/

/** **/

/* <Function>
 *   <Name>LQT_UnknownWordsInPhrase
 *   <Class>Retrieval/Phrases
 *   <Purpose>
 *      Returns the number of unrecognised words in the given phrase.
 *	A phrase containing any unrecognised words can never be matched.
 *   <Notes>
 *	This number is not included in the result of LQT_NumberOfWordsInPhrase.
 *   <Returns>
 *      the number of unrecognised words in the phrase.
 * </Function>
 */
/*ARGSUSED2*/
API int
LQT_UnknownWordsInPhrase(db, Phrase)
    t_LQTEXT_Database *db;
    t_Phrase *Phrase;
{
    return Phrase->HasUnknownWords;
}
