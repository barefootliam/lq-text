/* phrnword.c -- Copyright 1995, 1996 Liam R. Quin.
 * All Rights Reserved.
 * This code is NOT in the public domain.
 * See the file COPYRIGHT for full details.
 */

/*
 * $Id: phrnword.c,v 1.2 2019/04/21 06:06:40 lee Exp $
 *
 */

#include "error.h"
#include "globals.h" /* defines and declarations for database filenames */

#ifndef FILE
# include <stdio.h> /* needed on HP/UX */
#endif

#include <sys/types.h>

#include "liblqtext.h"
#include "phrase.h"
#include "lqtrace.h"

/** Unix system calls that need to be declared: **/
/** Unix/C Library Functions: **/
/** lqtext functions: **/
/** functions within this file that need forward declarations **/

/** **/

/* <Function>
 *   <Name>LQT_NumberOfWordsInPhrase
 *   <Class>Retrieval/Phrases
 *   <Purpose>
 *      Returns the number of recognised words in the phrase.
 *	Common words, or other things that the various LQT_ReadWord functions
 *	would skip, are not included in the count.
 *	A phrase containing no recognised words can never be matched.
 *   <Returns>
 *      the number of words in the phrase.
 *   <SeeAlso>
 *	LQT_ReadWordFromStringPointer
 * </Function>
 */
/*ARGSUSED2*/
API int
LQT_NumberOfWordsInPhrase(db, Phrase)
    t_LQTEXT_Database *db;
    t_Phrase *Phrase;
{
    register t_PhraseItem *Word;
    register int Result;

    if (!Phrase || !Phrase->Words) {
	return 0;
    }

    /* This should really be a macro, saving the result
     * in the Phrase structure!
     */

    for (Result = 0, Word = Phrase->Words; Word; Word = Word->Next) {
	++Result;
    }
    return Result;
}
