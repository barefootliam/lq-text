/* SetFID.c -- Copyright 1989, 1993, 1994-1996 Liam R. E. Quin.
 * All Rights Reserved.
 * This code is NOT in the public domain.
 * See the file COPYRIGHT for full details.
 */

/* SetFID.c -- get/set the largest current word number
 * 
 * $Id: setfid.c,v 1.6 1996/05/14 16:29:22 lee Exp $
 */

#include "globals.h" /* defines and declarations for database filenames */
#include "error.h"

#include <stdio.h>
#include <sys/types.h>
#ifdef HAVE_FCNTL_H
# ifdef HAVE_SYSV_FCNTL_H
#  include <sys/stat.h>
# endif
# include <fcntl.h>
#endif

#include "fileinfo.h"
#include "wordinfo.h"
#include "blkheader.h"
#include "block0.h"
#include "liblqtext.h"
#include "lqtrace.h"

/** declarations: **/
/** Unix system calls that need to be declared: **/
/** Unix Library Calls that need to be declared: **/
/** lq-text calls that need to be declared: **/
/** **/

PRIVATE t_FID LQTp_RealGetNextFID(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *,
    int WriteCurrent
#endif
);


/* <Function>
 *   <Name>LQT_GetMaxOrAllocateFID
 *   <Class>Database/Documents
 *   <Purpose>
 *	Allocates a new FID, and writes the new value to disk.
 *	If the <var>WriteCurrent</var> argument is zero, the
 *	value is only written in one on every 1,000 calls.
 *   <SeeAlso>
 *	LQT_SyncDatabase
 * </Function>
 */
API t_FID
LQT_GetMaxOrAllocateFID(db, WriteCurrent)
    t_LQTEXT_Database *db;
    int WriteCurrent;
{
    static int SinceLastUpdate = 0;

    if (WriteCurrent) {
	SinceLastUpdate = 0;
	return LQTp_RealGetNextFID(db, WriteCurrent);
    }

    /* Call the real function sometimes, so that the database does
     * get updated in case of a crash or for other users.
     */
    if (++SinceLastUpdate > 1000) {
	SinceLastUpdate = 0;
	(void) LQTp_RealGetNextFID(db, 1);
	/* LQTp_RealGetNextFID(db, 1) sets LQTp__LastNextFIDVal but doesn't
	 * increment it;
	 * This is ugly and has to be made cleaner... TODO FIXME
	 */
    }

    if (db->LQTp__LastNextFIDVal == (t_FID) 0) {
	db->LQTp__LastNextFIDVal = LQT_GetMaxFID(db);
	SinceLastUpdate = 0;
    }
    ++db->LQTp__LastNextFIDVal;
    return db->LQTp__LastNextFIDVal;
}

/* <Function>
 *   <Name>LQT_WriteCurrentMaxFID
 *   <Class>Database/Documents
 *   <Purpose>
 *	<P>Writes the cached value of the largest allocated FID to disk.
 *	This routine is registered by LQT_OpenDatabase so that it is called
 *	automatically by LQT_CloseDatabase.</P>
 *	<P>It may also be useful to call it
 *	directly for the purpose of debugging a new lq-text client that
 *	updates the database, for example when running under a database.</P>
 *   <SeeAlso>
 *	LQT_WriteCurrentMaxWID
 *	LQT_SyncDatabase
 *	LQT_CloseDatabase
 * </Function>
 */
API void
LQT_WriteCurrentMaxFID(db)
    t_LQTEXT_Database *db;
{
    (void) LQTp_RealGetNextFID(db, 1);
}

/*ARGSUSED1*/
PRIVATE t_FID
LQTp_RealGetNextFID(db, WriteCurrent)
    t_LQTEXT_Database *db;
    int WriteCurrent;
{
    t_FID Result = 0;
    unsigned char *p;
    t_BlockHeader *BH;

    /** Alter the file, so other programs can see the new words...
     **/

    p = LQT_ReadBlock(db, (unsigned long) 0L, (t_WID) 0);

    Result = (t_FID) p[BLOCK_ZERO_MAXFID0]; /* most significant byte */
    Result <<= 8; Result |= (t_FID) p[BLOCK_ZERO_MAXFID1];
    Result <<=8; Result |= (t_FID) p[BLOCK_ZERO_MAXFID2];
    Result <<=8; Result |= (t_FID) p[BLOCK_ZERO_MAXFID3];

    /* if WriteCurrent is set, we should not increment anything */
	++db->LQTp__LastNextFIDVal;
	++Result;

    if (db->LQTp__LastNextFIDVal > Result) {
	Result = db->LQTp__LastNextFIDVal;
    } else {
	db->LQTp__LastNextFIDVal = Result;
    }

    {
	t_FID tmp = Result;

	p[BLOCK_ZERO_MAXFID3] = (tmp & 0xff); /* least significant byte */
	tmp >>= 8; p[BLOCK_ZERO_MAXFID2] = (tmp & 0xff);
	tmp >>= 8; p[BLOCK_ZERO_MAXFID1] = (tmp & 0xff);
	tmp >>= 8; p[BLOCK_ZERO_MAXFID0] = (tmp & 0xff);
    }

    BH = (t_BlockHeader *) p;
    BH->NumberOfBlocks = 1;
    LQT_WriteBlock(db, (unsigned long)0L, p, 1, (t_WID) 0);

#ifdef ASCIITRACE
    LQT_Trace(LQTRACE_FID_ALLOC,
	"Largest File Identifier (\"FID\") now %ld",
	Result
    );
#endif

    return Result;
}

