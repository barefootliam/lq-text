/* wcregex.c -- Copyright 1996 Liam R. E. Quin.
 * All Rights Reserved.
 * This code is NOT in the public domain.
 * See the file COPYRIGHT for full details.
 *
 * wcregex.c -- convert wildcards to regexps, and match them
 * 
 * $Id: wcregex.c,v 1.3 2019/04/21 06:06:40 lee Exp $
 *
 */

#include "globals.h" /* defines and declarations for database filenames */
#include "error.h"
#include <stdio.h>

#ifdef HAVE_STDLIB_H
#include <stdlib.h>
#endif
#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif
#ifdef HAVE_STRING_H
#include <string.h>
#else
#include <strings.h>
#endif
#include <sys/types.h>

#include "fileinfo.h"
#include "smalldb.h"
#include "wordindex.h"
#include "wordinfo.h"
#include "numbers.h"
#include "emalloc.h"
#include "wordrules.h" /* max word length */
#include "pblock.h"
#include "lqutil.h"
#include "liblqtext.h"
#include "lqtrace.h"

/** regular expression matching code **/

#include <hsregex.h> /* Henry Spencer's regex header, renamed by Liam */

typedef struct {
    unsigned char *Expression;
    regex_t *CompiledExpression;
    unsigned char *MinValue;
    unsigned char *MaxValue;
} t_WildCardPatternObject;

PRIVATE unsigned char *
handleminmax(p, PatternObject)
    unsigned char *p; /* the input pattern */
    t_WildCardPatternObject *PatternObject;
{
    /* handle :min, :max, maybe other things later;
     * returns a pointer to the first unhandled character
     * or NULL if nothing was found, as in that case the
     * [...] should be treated as a character class.
     */

    if (strncmp(p, "[:max ", 5) == 0) {
	unsigned char *end;
	if (PatternObject->MaxValue) {
	    Error(E_FATAL, "you cannot use [:max ....] twice on the same word");
	}
	p += 5; /* skip over the keyword and first space */
	while (*p == ' ') { /* more spaces maybe */
	    p++;
	}
	if (*p == ']') {
	    Error(E_FATAL, "[:max ] has missing value, e.g. [:max 1972]");
	}
	for (end = &p[1]; *end; end++) {
	    if (*end == ' ') {
		Error(E_FATAL, "[:max must be followed by a word and then ]");
	    } else if (*end == ']') {
		break;
	    }
	}
	if (*end == ']') {
	    /* p points to the first byte, end to the last. */
	    PatternObject->MaxValue = (unsigned char *) emalloc("MaxValue",
		    						end - p + 1);
	    (void) strncpy(PatternObject->MaxValue, p, end - p);
	    PatternObject->MaxValue[end - p + 1] = '\0';
#ifdef ASCIITRACE
    LQT_Trace(LQTRACE_TERM_EXPANSION, "found max %s", PatternObject->MaxValue);
#endif
	    return end;
	} else {
	    Error(E_FATAL, "[:max ] has missing ], e.g. [:max 1972]");
	}
    } else if (strncmp(p, "[:min ", 5) == 0) {
	unsigned char *end;
	if (PatternObject->MinValue) {
	    Error(E_FATAL, "you cannot use [:min ....] twice on the same word");
	}
	p += 5; /* skip over the keyword and first space */
	while (*p == ' ') { /* more spaces maybe */
	    p++;
	}
	if (*p == ']') {
	    Error(E_FATAL, "[:min ] has missing vale`ue, e.g. [:min 1972]");
	}
	for (end = p; *end; end++) {
	    if (*end == ' ') {
		Error(E_FATAL, "[:min must be followed by a word and then ]");
	    } else if (*end == ']') {
		break;
	    }
	}
	if (*end == ']') {
	    /* p points to the first byte, end to the last. */
	    PatternObject->MinValue = (unsigned char *) emalloc("MinValue",
								end - p + 1);
	    (void) strncpy(PatternObject->MinValue, p, end - p);
	    PatternObject->MinValue[end - p] = '\0';
#ifdef ASCIITRACE
	    LQT_Trace(LQTRACE_TERM_EXPANSION, "found min /%s/",
		PatternObject->MinValue
	    );
#endif
	    return end;
	} else {
	    Error(E_FATAL, "[:min ] has missing ], e.g. [:min 1972]");
	}
    } else {
	return 0;
    }
}

API unsigned char *
LQT_PrepareWildCardForMatching(db, WildCard, Length, PrefixLengthp)
    t_LQTEXT_Database *db;
    unsigned char *WildCard;
    int Length;
    int *PrefixLengthp;
{
    register unsigned char *p, *q;
    t_WildCardPatternObject *PatternObject;
    int needRexexp = 0;

    if (!PrefixLengthp) {
	Error(E_FATAL|E_INTERNAL,
	    "LQT_PrepareWildCardForMatching: null PrefixLengthp not allowed"
	);
    }

    PatternObject = (t_WildCardPatternObject *) emalloc("WildcardObject",
	sizeof(t_WildCardPatternObject)
    );

    q = PatternObject->Expression = (unsigned char *) emalloc(
	"WildCard Expression",
	Length * 2 + 3
	/* times two for the worst case, x --> \x for every character;
	 * plus three for adding ^ and $ and \0
	 */
    );
    PatternObject->CompiledExpression = 0;
    PatternObject->MinValue = 0;
    PatternObject->MaxValue = 0;

    *q++ = '^';

    *PrefixLengthp = 0;

    for (p = WildCard; p - WildCard < Length; p++) {
	switch (*p) {
	case '*':
	    if (!*(PrefixLengthp)) {
		*PrefixLengthp = p - WildCard;
	    }
	    *q++ = '.';
	    *q++ = '*';
	    needRexexp = 1;
	    continue;
	case '?':
	    if (!*(PrefixLengthp)) {
		*PrefixLengthp = p - WildCard;
	    }
	    *q++ = '.';
	    needRexexp = 1;
	    continue;
	case '[':
	    /* special case:
	     * [:min word1]
	     * [:max word1]
	     * [:min word1][:max word2]
	     * set min and max elements and do not imply
	     * a regular expression,
	     * and do not end the prefix
	     */
	    {
		char *tmp = 0;

		if (p[1] == ':') {
		    tmp = handleminmax(p, PatternObject);
		}

		if (tmp) {
		    p = tmp;
		    continue;
		}
	    }

	    needRexexp = 1;
	    if (!*(PrefixLengthp)) {
		*PrefixLengthp = p - WildCard;
	    }
	    do {
		if (*p == '\\' && p - WildCard < Length) {
		    *q++ = *p++;
		}
		*q++ = *p;
		if (*p == ']') {
		    break;
		}
		p++;
	    } while (*p && p - WildCard < Length);
	    continue;
	case '^':
	case '.':
	case '$':
	case '(':
	case ')':
	case '|':
	/* case '*': already handled above */
	case '+':
	/* case '?': already handled above */
	case '{': /* don't need to quote the } it seems */
	case '\\':
	    *q++ = '\\';
	    /* fall through */
	}
	*q++ = (*p);
    }
    *q++ = '$';
    *q = '\0';
    if (!*PrefixLengthp) {
	/* unlikely, this would be a bug, as we shouldn't get here with
	 * a constant string
	 */
	*PrefixLengthp = p - WildCard;
    }

    if (needRexexp) {
	int i;
	
	PatternObject->CompiledExpression = (regex_t *) emalloc(
	    "Compiled regex",
	    sizeof(regex_t)
	);
	i = regcomp(
	    PatternObject->CompiledExpression,
	    PatternObject->Expression,
	    REG_BASIC | /* no enhanced syntax */
	    REG_NOSUB /* we only need to match, not to substitute */
	);

	if (i != 0) {
	    Error(E_FATAL,
"LQT_PrepareWildCardForMatching: regexp compile failed (E%d) for \"%s\"",
		i,
		PatternObject->Expression
	    );
	}
    }
#ifdef ASCIITRACE
    LQT_Trace(LQTRACE_TERM_EXPANSION,
	"Expand %*.*s to %s\n",
	Length, Length, WildCard, 
	PatternObject->Expression
    );
#endif

    return (unsigned char *) PatternObject;
}

API void
LQT_FinishWildCardAfterMatching(db, Argument)
    t_LQTEXT_Database *db;
    unsigned char *Argument;
{
    t_WildCardPatternObject *PatternObject;

    PatternObject = (t_WildCardPatternObject *) Argument;
    regfree(PatternObject->CompiledExpression);
    if (PatternObject->CompiledExpression) {
	efree((char *) PatternObject->CompiledExpression);
    }
    efree((char *) PatternObject->Expression);

    if (PatternObject->MinValue) efree((char *) PatternObject->MinValue);
    if (PatternObject->MinValue) efree((char *) PatternObject->MaxValue);

    efree((char *) PatternObject);
}


API int
LQT_MatcherForWildCards(
    db,
    String,
    StringLength,
    Pattern, 
    PatternLength,
    PrefixLength,
    Argument
)
    t_LQTEXT_Database *db;
    unsigned char *String;
    int StringLength;
    unsigned char *Pattern;
    int PatternLength;
    int PrefixLength;
    unsigned char *Argument;
{
    t_WildCardPatternObject *PatternObject;
    int i;
    regmatch_t pmatch[2];

    if (PrefixLength > 0) {
	if (StringLength < PrefixLength) {
	    return 1;
	}
	i = strncmp(String, Pattern, PrefixLength);
	
	if (i > 0) {
	    return LQT_WIDMATCH_FAILED; /* give up, there are no more */
	}
	if (i < 0) {
	    return i;
	}
	/* assert: i == 0 */
    }
    PatternObject = (t_WildCardPatternObject *) Argument;

    if (PatternObject->CompiledExpression) {
	pmatch[0].rm_so = 0;
	pmatch[0].rm_eo = StringLength;
	i = regexec(
	    PatternObject->CompiledExpression,
	    String,
	    0, /* nmatch array used for substitutions */
	    pmatch,
	    REG_STARTEND
	);

	if (i == REG_NOMATCH) {
	    return 1;
	} else if (i != 0) {
	    Error(E_WARN|E_INTERNAL,
		"regexec returned %d, wildcard error", i
	    );
	    return LQT_WIDMATCH_FAILED;
	}
    }
    if (PatternObject->MinValue) {
	i = strncmp(String, PatternObject->MinValue, StringLength);
	if (i < 0) {
	    return i;
	}
    }
    if (PatternObject->MaxValue) {
	i = strncmp(String, PatternObject->MaxValue, StringLength);
	if (i > 0) {
#ifdef ASCIITRACE
	    LQT_Trace(LQTRACE_TERM_EXPANSION,
		"match: %*.*s > %s, giving up",
		StringLength, StringLength, String,
		PatternObject->MaxValue
	    );
#endif
	    return LQT_WIDMATCH_FAILED; /* give up, there are no more */
	}
    }
    return 0;
}
