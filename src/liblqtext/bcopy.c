/* bcopy.c -- Copyright 1989, 1996 Liam R. Quin.  All Rights Reserved.
 * This code is NOT in the public domain.
 * See the file COPYRIGHT for full details.
 */

/* this is a simple replacement for bcopy() where the native bcopy()
 * does not handle overlapping blocks.
 * see config.dir/.test/c/bcopy for a test of bcopy.
 *
 * $Id: bcopy.c,v 1.4 1996/05/15 22:58:05 lee Exp $
 */

#include "globals.h"

#ifndef HAVE_BCOPY
void
bcopy(src, dest, nbytes)
    char *dest;
    char *src;
    int nbytes;
{
    /* We have to be clever about this...
     * If src < dest then we copy from the top down
     * otherwise, copy from the bottom up...
     */
    
    register char *p, *q;

    if (src < dest) {
	for (p = &src[nbytes - 1], q =&dest[nbytes - 1]; nbytes--; q--, p--) {
	    *q = *p;
	}
    } else {
	for (p = src, q = dest; nbytes--; p++, q++) {
	    *q = *p;
	}
    }
}
#endif
