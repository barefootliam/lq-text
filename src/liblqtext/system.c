/* system.c -- Copyright 1989, 1994 Liam R. Quin.
 * All Rights Reserved.
 * This code is NOT in the public domain.
 * See the file COPYRIGHT for full details.
 *
 * This is not a very portable way of doing things... and certainly not
 * a very fast one.  MUST be re-written.
 * Only for use from within curses.
 *
 * Lee
 *
 * $Id: system.c,v 1.8 2019/04/21 06:06:40 lee Exp $
 */

#include "error.h"
#include <stdio.h>
#include <sys/types.h>
#ifdef HAVE_STDLIB_H
# include <stdlib.h>
#endif
#include "globals.h"

#include <curses.h>
#include "liblqtext.h"

#ifndef echo
extern int echo();
#endif
#ifndef wmove
extern int wmove();
#endif
#ifndef nl
extern int nl();
#endif
#ifndef noecho
extern int noecho();
#endif
#ifndef nonl
extern int nonl();
#endif
#ifndef wrefresh
extern int wrefresh();
#endif
#ifndef waddstr
extern int waddstr();
#endif
#ifndef wclear
extern int wclear();
#endif

/* <Function>
 *   <Name>LQU_CursesSafeSystem
 *   <Class>Other/Terminal
 *   <Purpose>
 *	runs the given string as a system command, using system(3);
 *	the terminal modes are restored before and after.
 *   <Restrictions>
 *	This routine should not be used and will be deleted from the next
 *	release; it is only useful for curses-based clients, and should
 *	be provided separately.
 * </Function>
 */
API void
LQU_CursesSafeystem(string, retvalp)
    char *string;
    int *retvalp;
{
    int status;

    clearok(stdscr, TRUE);
    clear();
    refresh();
    noraw();
    echo();
    nl();
    endwin();
    status = system(string);
    if (retvalp) {
	*retvalp = (status >> 8);
    }
    fprintf(stderr, "\n[press  return  to continue] ");
    sleep(1);
    initscr();
    raw();
    noecho();
    nonl();
    (void) getch();
    clearok(stdscr, TRUE);
    mvwaddstr(stdscr, 10, 10, "                        "); /* ???!?? */
    refresh();
}

