head	1.12;
access;
symbols;
locks;
comment	@# @;


1.12
date	2019.04.21.06.06.40;	author lee;	state Exp;
branches;
next	1.11;

1.11
date	96.08.28.01.29.31;	author lee;	state Exp;
branches;
next	1.10;

1.10
date	96.08.20.18.06.53;	author lee;	state Exp;
branches;
next	1.9;

1.9
date	96.08.15.22.24.56;	author lee;	state Exp;
branches;
next	1.8;

1.8
date	96.06.13.16.46.31;	author lee;	state Exp;
branches;
next	1.7;

1.7
date	96.06.11.13.45.33;	author lee;	state Exp;
branches;
next	1.6;

1.6
date	96.05.26.23.06.37;	author lee;	state Exp;
branches;
next	1.5;

1.5
date	96.05.26.17.58.14;	author lee;	state Exp;
branches;
next	1.4;

1.4
date	96.05.26.00.54.03;	author lee;	state Exp;
branches;
next	1.3;

1.3
date	95.04.25.22.47.11;	author lee;	state Exp;
branches;
next	1.2;

1.2
date	95.04.25.22.01.25;	author lee;	state Exp;
branches;
next	1.1;

1.1
date	94.06.25.23.18.35;	author lee;	state Exp;
branches;
next	;


desc
@t_WordFlag --> t_WordFlags
@


1.12
log
@for migration to CVS
@
text
@/* rdflags.c -- Copyright 1994 Liam R. E. Quin.
 * All Rights Reserved.
 * This code is NOT in the public domain.
 * See the file COPYRIGHT for full details.
 *
 * $Id: rdflags.c,v 1.12 2001/05/31 03:50:13 liam Exp $
 *
 * Turn a flag string sequence into a flag value.
 */

#include "globals.h" /* defines and declarations for database filenames */
#include "error.h"

#include <sys/types.h> /* for fileinfo.h */
#ifndef FILE
# include <stdio.h>
#endif
#ifdef HAVE_STRING_H
# include <string.h>
#else
# include <strings.h>
#endif

#ifdef HAVE_STDLIB_H
# include <stdlib.h>
#else
# include <malloc.h>
#endif


#include "fileinfo.h"
#include "wordinfo.h"
#include "wordrules.h"
#include "emalloc.h"
#include "lqutil.h"
#include "liblqtext.h"
#include "lqtrace.h"

/** Unix system calls that need declaring: **/
/** Unix/C Library Functions that need declaring: **/
/** lqtext library functions that need declaring: **/
/** Functions within this file that are used before being defined: **/
/** **/

/* <Function>
 *   <Name>LQT_StringToFlags
 *   <Class>Tracing
 *   <Purpose>
 *	<P>Tries to reverse the operation of LQT_FlagsToString.
 *	In other words, LQT_StringToFlags takes a string which it assumes
 *	to be a sequence of names of flags found in the given FlagNames
 *	array, separated by the given constant string, and returns the
 *	bitwise `or' of the Value members corresponding to the Names that
 *	are found.</P>
 *	<P>In addition, a leading + or - is used to indicate that the
 *	following flags are to be added (with bitwise or) or removed
 *	(usinbg bitwise and on their negation) from the result.
 *   <Returns>
 *      a pointer to the first unconverted character in String, and the
 *	actual value in Flagp
 *   <SeeAlso>
 *	LQT_StringToWordFlags
 *	LQT_WordFlagsToString
 * </Function>
 */ 
API char *
LQT_StringToFlags(String, Flagp, WordFlagNamePairArray, Separator)
    char *String;
    unsigned long *Flagp;
    t_FlagNamePair *WordFlagNamePairArray;
    char *Separator;
{
    register char *p;
    int SeparatorLength = strlen(Separator);
    char *StartOfNextChunk;
    enum {
	AddValue, SubtractValue, SetValue
    } Action = SetValue;
    unsigned long Result = 0;
    unsigned long Pending = 0;

    if (!String) {
	*Flagp = 0;
	return String;
    }

    if (!*String) {
	*Flagp = (unsigned long) 0;
	return String;
    }

    for (p = StartOfNextChunk = String; *p; /*NULL*/) {
	char *EndOfChunk;
	t_FlagNamePair *wp;
	char *Candidate;
	int SawTrailingSeparator = 0;

	EndOfChunk = 0;
	if (*p == '+' || *p == '-' || *p == '=') {
	    if (Pending) {
		switch (Action) {
		    case AddValue: Result |= Pending; break;
		    case SubtractValue: Result &= ~Pending; break;
		    case SetValue: Result = Pending; break;
		}
		Pending = 0;
	    }
	    switch (*p) {
	    case '+':
		Action = AddValue; break;
	    case '-':
		Action = SubtractValue; break;
	    case '=':
		Action = SetValue; break;
	    default:
		Error(E_FATAL|E_BUG|E_INTERNAL,
		    "%s: %d: unhandled case 0%o=%c in switch", *p, *p
		);
	    }
	    /* skip over the character */
	    StartOfNextChunk = ++p;
	}
	/** Look for a possible flag string **/
	Candidate = StartOfNextChunk;
	for (p = &StartOfNextChunk[1]; *p; p++) {
	    if (strncmp(p, Separator, (unsigned) SeparatorLength) == 0) {
		EndOfChunk = &p[-1];
		/* String = StartOfNextChunk; */
		p = StartOfNextChunk = &p[SeparatorLength];
		if (!*StartOfNextChunk) {
		    SawTrailingSeparator = 1; /* oops */
		}
		break;
	    } else if (*p == '+' || *p == '-' || *p == '=') {
		EndOfChunk = &p[-1];
		/* String = StartOfNextChunk; */
		StartOfNextChunk = &p[1];
		if (!*StartOfNextChunk) {
		    SawTrailingSeparator = 1; /* oops */
		}
		break;
	    }
	}

	if (!*p) {
	    EndOfChunk = &p[-1];
	    /* String = StartOfNextChunk; */
	    StartOfNextChunk = p;
	}

	/** if we didn't get one, we're done **/
	if (!EndOfChunk) {
	    if (Pending) {
		switch (Action) {
		    case AddValue: Result |= Pending; break;
		    case SubtractValue: Result &= ~Pending; break;
		    case SetValue: Result = Pending; break;
		}
	    }
	    *Flagp = Result;
	    return p;
	}

	/** for each known flag value: **/
	for (wp = WordFlagNamePairArray; wp->Name; wp++) {
	    /** if it matches, use the value and stop looking: **/
	    if (strncmp(
		wp->Name,
		Candidate,
		(unsigned) ((EndOfChunk - Candidate) + 1)
	    ) == 0) {
		Pending |= wp->Value;
		break;
	    }
	}

	if (!wp->Name) {
	    /* error: unrecognised string */
	    return Candidate;
	}

	if (SawTrailingSeparator) {
	    /* error: trailing garbage */
	    return &StartOfNextChunk[-1];
	}

    } /* for */

    if (Pending) {
	switch (Action) {
	    case AddValue: Result |= Pending; break;
	    case SubtractValue: Result &= ~Pending; break;
	    case SetValue: Result = Pending; break;
	}
    }

    *Flagp = Result;
#ifdef ASCIITRACE
    LQT_Trace(LQTRACE_DEBUG,
	"string to flags: [%s] -> %d / 0%o / 0x%x -> %s",
	String, Result, Result, Result,
	LQT_FlagsToString(Result, WordFlagNamePairArray, Separator)
    );
#endif
    return p;
}

/* <Function>
 *   <Name>LQT_StringToWordFlags
 *   <Class>Tracing
 *   <Purpose>
 *	Tries to reverse the operation of LQT_WordFlagsToString.
 *	In other words, LQT_StringToWordFlags takes a string which it
 *	assumes to be a sequence of names of flags as defined in the
 *	header file <h>wordrules.h</h> separated by LQTpWordFlagSep (a comma),
 *	and returns the
 *	bitwise `or' of the Word Flags corresponding to the Names that
 *	are found.
 *   <Returns>
 *      a pointer to the first unconverted character in String, and the
 *	actual value in Flagp
 *   <SeeAlso>
 *	LQT_StringToWordFlags
 *	LQT_WordFlagsToString
 * </Function>
 */ 
API char *
LQT_StringToWordFlags(db, String, Flagp)
    t_LQTEXT_Database *db;
    char *String;
    t_WordFlags *Flagp;
{
    extern t_FlagNamePair LQTp_WordFlagArray[];
    /* later, this will be included in the database structure */

    return LQT_StringToFlags(
	String,
	(unsigned long *) Flagp,
	LQTp_WordFlagArray,
	LQTpWordFlagSep
    );
}

/* <Function>
 *   <Name>LQT_ForEachWordFlag
 *   <Class>Tracing
 *   <Purpose>
 *	<P>Calls the given function for each available word flag.
 *	The integer argument IsSet passed to the function is non-zero for
 *	those flags that are set in the current trace flags, and zero
 *	for the others.</P>
 *   <Returns>
 *	zero.
 *   <SeeAlso>
 *	LQT_StringToWordFlags
 * </Function>
 */ 
API int
LQT_ForEachWordFlag(CallMe)
    void (* CallMe)(
#ifdef HAVE_PROTO
	char *Name,
	unsigned int Value
#endif
    );
{
    int i;

    for (i = 0; LQTp_WordFlagArray[i].Name; i++) {
	(* CallMe) (
	    LQTp_WordFlagArray[i].Name,
	    LQTp_WordFlagArray[i].Value
	);
    }
    return 0;
}

@


1.11
log
@Fixed addition of tags.
@
text
@d6 1
a6 1
 * $Id: rdflags.c,v 1.10 96/08/20 18:06:53 lee Exp $
a13 1
#include <malloc.h>
d24 7
d83 1
a83 1
	*Flagp = (unsigned long) 0;
d126 1
a126 1
	    if (STRNCMP(p, Separator, SeparatorLength) == 0) {
d167 5
a171 1
	    if (STRNCMP(wp->Name, Candidate, (EndOfChunk - Candidate) + 1)==0){
d179 1
a179 1
	    return p;
d200 3
a202 2
	"string to flags: [%s] -> %d / 0%o / 0x%x",
	String, Result, Result, Result
d231 1
a231 1
    unsigned long *Flagp;
d238 1
a238 1
	Flagp,
d243 35
@


1.10
log
@Fixed the WordFlags conversion; flags are now unsigned longs.
@
text
@d6 1
a6 1
 * $Id: rdflags.c,v 1.9 96/08/15 22:24:56 lee Exp $
d73 2
a74 2
    t_WordFlags Result = 0;
    t_WordFlags Pending = 0;
d77 1
a77 1
	*Flagp = 0;
d82 1
a82 1
	*Flagp = (t_WordFlags) 0;
d122 1
a122 1
		String = StartOfNextChunk;
d130 2
a131 2
		String = StartOfNextChunk;
		p = StartOfNextChunk = &p[1];
d141 1
a141 1
	    String = StartOfNextChunk;
@


1.9
log
@added debug trace.
@
text
@d6 1
a6 1
 * $Id: rdflags.c,v 1.8 96/06/13 16:46:31 lee Exp $
d63 2
a64 2
    t_WordFlags *Flagp;
    t_WordFlagNamePair *WordFlagNamePairArray;
d67 1
a67 1
    register char *p = String;
d86 1
a86 2
    StartOfNextChunk = String;
    while (*p) {
d88 1
a88 1
	t_WordFlagNamePair *wp;
d115 1
a115 2
	    p++;
	    StartOfNextChunk++;
d123 1
a123 1
		StartOfNextChunk = &p[SeparatorLength];
d131 1
a131 1
		StartOfNextChunk = &p[1];
d189 3
a191 3
    LQTrace(LQTRACE_DEBUG,
	"string to flags: [%s] -> %d / 0%0 / 0x%x",
	String, Result
d220 1
a220 1
    t_WordFlags *Flagp;
d222 1
a222 1
    extern t_WordFlagNamePair LQTp_WordFlagArray[];
@


1.8
log
@added + and - syntax.
@
text
@d6 1
a6 1
 * $Id: rdflags.c,v 1.7 96/06/11 13:45:33 lee Exp $
d31 1
d190 6
@


1.7
log
@t_WordFlag --> t_WordFlags.
@
text
@d6 1
a6 1
 * $Id: rdflags.c,v 1.6 96/05/26 23:06:37 lee Exp $
d42 1
a42 1
 *	Tries to reverse the operation of LQT_FlagsToString.
d47 4
a50 1
 *	are found.
d69 5
a79 2
    *Flagp = (t_WordFlags) 0;

d81 1
d92 26
a118 1
	EndOfChunk = 0;
d129 8
d148 8
a155 1
	    /*NOTREACHED*/
d163 1
a163 1
		*Flagp |= wp->Value;
d170 1
a170 1
	    return Candidate;
d174 1
d180 9
@


1.6
log
@Centerline to the rescue!
@
text
@d6 1
a6 1
 * $Id: rdflags.c,v 1.4 1996/05/26 00:54:03 lee Exp $
d59 1
a59 1
    t_WordFlag *Flagp;
d72 1
a72 1
    *Flagp = (t_WordFlag) 0;
d158 1
a158 1
    t_WordFlag *Flagp;
d160 1
a160 1
    extern t_WordFlagNamePair *LQTp_WordFlagArray;
d166 1
a166 1
	&LQTp_WordFlagArray,
@


1.5
log
@oops, that last change caused a core dump!
@
text
@d166 1
a166 1
	LQTp_WordFlagArray,
@


1.4
log
@Was passing incorrect argument...
@
text
@d6 1
a6 1
 * $Id: rdflags.c,v 1.3 95/04/25 22:47:11 lee Exp $
d166 1
a166 5
	&LQTp_WordFlagArray[0],
	    /* I've used &array[0] rather than just array, because
	     * some broken compilers either generate warnings or
	     * actually do the wrong thing with just `array'.
	     */
@


1.3
log
@fixed the error return value to work properly.
Amazing what testing can do!
@
text
@d6 1
a6 1
 * $Id: rdflags.c,v 1.2 95/04/25 22:01:25 lee Exp $
a28 1
#include "readfile.h"
d40 1
d83 1
d93 3
a99 2
	/** if we didn't get one, we're done **/

d106 1
d126 4
d136 2
a137 1
 *   <Name>LQT_StringToFlags
d139 6
a144 5
 *	Tries to reverse the operation of LQT_FlagsToString.
 *	In other words, LQT_StringToFlags takes a string which it assumes
 *	to be a sequence of names of flags found in the given FlagNames
 *	array, separated by the given constant string, and returns the
 *	bitwise `or' of the Value members corresponding to the Names that
d155 2
a156 1
LQT_StringToWordFlags(String, Flagp)
d161 1
d166 5
a170 1
	LQTp_WordFlagArray,
@


1.2
log
@masses of changes & fixes.
@
text
@d6 1
a6 1
 * $Id: rdflags,v 1.1 94/06/25 23:18:35 lee Exp Locker: lee $
d120 1
a120 1
	    return p;
@


1.1
log
@Initial revision
@
text
@d1 1
a1 1
/* prflags.c -- Copyright 1994 Liam R. E. Quin.
d6 1
a6 1
 * $Id: lqkwic.c,v 1.28 94/02/26 14:55:02 lee Exp $
d8 1
a8 1
 * Read matches from a file stream into memory.
d43 5
d49 5
a53 4
 *      <LIST>
 *        <LI>A pointer to the first unconverted character in String
 *	  <LI>The value, in Flagp
 *	</LIST>
d57 1
a57 1
LQT_StringToFlags(String, Resultp)
d60 2
d64 2
d68 1
a68 1
	*Resultp = 0;
d78 1
d80 3
a82 2
	register char *EndOfChunk;
	char *StartOfNextChunk = String;
d86 3
a88 2
	for (p = StartOfNextChunk; *p; p++) {
	    if (STREQN(p, LQTpWordFlagSep, sizeof(LQTpWordFlagSep) - 1)) {
d91 1
a91 1
		StartOfNextChunk = &p[sizeof[LQTpWordFlagSep];
d110 1
a110 1
	for (wp = LQTp_WordFlagArray; wp->Name; wp++) {
d112 1
a112 1
	    if (STREQN(wp->Name, String, p - String)) {
d126 32
@
