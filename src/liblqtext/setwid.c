/* SetWID.c -- Copyright 1989, 1993-1996 Liam R. E. Quin.
 * All Rights Reserved.
 * This code is NOT in the public domain.
 * See the file COPYRIGHT for full details.
 */

/* SetWID.c -- get/set the largest current word number
 * 
 * $Id: setwid.c,v 1.14 1996/07/09 14:32:53 lee Exp $
 */

#include "globals.h" /* defines and declarations for database filenames */
#include "error.h"

#include <stdio.h>
#include <sys/types.h>
#ifdef HAVE_FCNTL_H
# ifdef HAVE_SYSV_FCNTL_H
#  include <sys/stat.h>
# endif
# include <fcntl.h>
#endif

#include "fileinfo.h"
#include "wordinfo.h"
#include "blkheader.h"
#include "block0.h"
#include "liblqtext.h"
#include "lqtrace.h"

/** declarations: **/

/** Unix system calls that need to be declared: **/
/** Unix Library Calls that need to be declared: **/
/** lq-text calls that need to be declared: **/

/** **/

INTERNAL t_WID LQTp_RealGetNextWID(
#ifdef HAVE_PROTO
    t_LQTEXT_Database *,
    int WriteCurrent
#endif
);

LIBRARY t_WID
LQTp_GetMaxOrAllocateWID(db, WriteCurrent)
    t_LQTEXT_Database *db;
    int WriteCurrent;
{
    static int SinceLastUpdate = 0;

    if (WriteCurrent) {
	SinceLastUpdate = 0;
	return LQTp_RealGetNextWID(db, WriteCurrent);
    }

    /* Call the real function sometimes, so that the database does
     * get updated in case of a crash or for other users.
     */
    if (++SinceLastUpdate > 250) {
	SinceLastUpdate = 0;
	(void) LQTp_RealGetNextWID(db, 1);
    }

    if (db->LQTp__LastNextWIDVal == (t_WID) 0) {
	db->LQTp__LastNextWIDVal = LQT_GetMaxWID(db);
	SinceLastUpdate = 0;
    }
    ++db->LQTp__LastNextWIDVal;
#ifdef ASCIITRACE
    if (LQT_TraceFlagsSet(LQTRACE_WORDINFO)) {
	if (SinceLastUpdate % 100 == 99) {
	    LQT_Trace(LQTRACE_WORDINFO, "MaxWID now %ld", db->LQTp__LastNextWIDVal);
	}
    }
#endif
    return db->LQTp__LastNextWIDVal;
}

/* <Function>
 *   <Name>LQT_WriteCurrentMaxWID
 *   <Class>Database/Words
 *   <Purpose>
 *      <P>Writes the value of the largest allocated WID to disk.
 *	This value is cached for efficiency, so LQT_WriteCurrentMaxWID
 *	must be called after allocating a new WID and before the program
 *	exits.</P>
 *	<P>Since LQT_WriteCurrentMaxWID is registered as an action to
 *	be performed on closing or flushing a database, it will be called
 *	automatically by a call to either LQT_Close or LQT_Sync.</P>
 *	<P>The ignored argument is required by LQT_AddActionOnClose.</P>
 *   <SeeAlso>
 *	LQT_AddActionOnClose, LQT_CloseDatabase, LQT_SyncDatabase
 * </Function>
 */
API int
LQT_WriteCurrentMaxWID(db)
    t_LQTEXT_Database *db;
{
    if (LQT_CurrentlyHaveWriteAccess(db)) {
	(void) LQTp_RealGetNextWID(db, 1); /* the 1 means to save the value */
    }
    return 0;
}

/* Undocumented right now -- sorry
 * This resets the max wid -- but does no integrity checks...
 * You have to know what you're doing!
 *
 * It's used by sortwids to throw away deleted words.
 */
LIBRARY void
LQT_ResetMaximumWID(db, NewMaxWID)
    t_LQTEXT_Database *db;
    t_WID NewMaxWID;
{
    unsigned char *p;

    db->LQTp__LastNextWIDVal = NewMaxWID;
    p = LQT_ReadBlock(db, (unsigned long) 0L, (t_WID) 0);

    {
	t_WID tmp = NewMaxWID;

	p[BLOCK_ZERO_MAXWID3] = (tmp & 0xff); /* least significant byte */
	tmp >>= 8; p[BLOCK_ZERO_MAXWID2] = (tmp & 0xff);
	tmp >>= 8; p[BLOCK_ZERO_MAXWID1] = (tmp & 0xff);
	tmp >>= 8; p[BLOCK_ZERO_MAXWID0] = (tmp & 0xff);
    }

    LQT_WriteBlock(db, (unsigned long) 0L, p, 1, (t_WID) 0);

#ifdef ASCIITRACE
    LQT_Trace(LQTRACE_WORDINFO, "LQT_ResetMaximumWID: maxwid %ld", NewMaxWID);
#endif

}

INTERNAL t_WID
LQTp_RealGetNextWID(db, WriteCurrent)
    t_LQTEXT_Database *db;
    int WriteCurrent; /* simply write the current MaxWID if true */
{
    t_WID Result = 0;
    unsigned char *p;

    /** Alter the file, so other programs can see the new words...
     **/

    p = LQT_ReadBlock(db, (unsigned long) 0L, (t_WID) 0);

    Result = (t_WID) p[BLOCK_ZERO_MAXWID0]; /* most significant byte */
    Result <<= 8; Result |= (t_WID) p[BLOCK_ZERO_MAXWID1];
    Result <<=8; Result |= (t_WID) p[BLOCK_ZERO_MAXWID2];
    Result <<=8; Result |= (t_WID) p[BLOCK_ZERO_MAXWID3];

    /* if WriteCurrent is set, we should not increment anything */
    if (!WriteCurrent) {
	++db->LQTp__LastNextWIDVal;
	++Result;
    }

    /* but use the biggest */

    if (db->LQTp__LastNextWIDVal > Result) {
	Result = db->LQTp__LastNextWIDVal;
    } else {
	db->LQTp__LastNextWIDVal = Result;
    }

    if (WriteCurrent) {
	t_WID tmp = Result;
	p[BLOCK_ZERO_MAXWID3] = (tmp & 0xff); /* least significant byte */
	tmp >>= 8; p[BLOCK_ZERO_MAXWID2] = (tmp & 0xff);
	tmp >>= 8; p[BLOCK_ZERO_MAXWID1] = (tmp & 0xff);
	tmp >>= 8; p[BLOCK_ZERO_MAXWID0] = (tmp & 0xff);
	LQT_WriteBlock(db, (unsigned long) 0L, p, 1, (t_WID) 0);
    }



#ifdef ASCIITRACE
    LQT_Trace(LQTRACE_WORDINFO, "LQTp_RealGetNextWID: maxwid now %ld", Result);
#endif

    return Result;
}

