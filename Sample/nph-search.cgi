#! /usr/local/bin/perl -w
use strict;

use CGI qw/:standard -nph -newstyle_urls/;

$| = 1; # unbuffered output

# hits per page; 100 is a good default
my $perpage = 100;

##############################################################

sub fail($$;$);
sub mkHeader($$);
sub mkSearchForm($$);

sub main
{
    my $cgi = new CGI;

    $cgi->nph(1);

    # change the following:
    my $d = "/usr/home/liam/public_html/spirit/LQTEXTDIR";
    my $b = "/usr/local/lqtext/bin";
    # end of configuration

    $ENV{lQTEXTDIR} = $d;
    $ENV{PATH} = "${b}:" . $ENV{PATH};

    my @names = $cgi->param();
    if (!defined(@names) || $#names < 0) {
	mkSearchForm($cgi, "Searching the Sample Collection");
	$cgi->end_html;
	exit(0);
    }

    my $string = $cgi->param('s');
    my $mode = $cgi->param('mode') || 'full_index';

    if (!defined($string) || $string =~ /^\s*$/) {
	fail(
	    $cgi,
	    "No search string was given",
	    "You must enter a phrase to find, such as \"serial port\"."
	);
    }

    if ($mode eq 'full_index') {
	my $first = $cgi->param('o') || 1;

	if ($first !~ /^[0-9]+$/) {
	    fail $cgi,
	         "Invalid o=$first parameter",
		 "Please don't tamper with the URLs, it gives me a headache.";
	}

	mkSearchForm($cgi, "docindex: $string");

	my $query = $string;
	$query =~ s/"\\\`()/./g;


	my $url = $cgi->url('-path_info' => 1, -query => 1, -full => 1);
	# formats for printing matches:
	my $perfile =
"\n<br><a href=\"${url};mode=fileview;fn=\${FID}\">\${DocTitle}</a>\n";
	
	my $permatch =
'$[${MatchNumber/3r}:${TextBefore/40r 40l}${Gap}<b>${MatchedText}</b>${TextAfter}/120 120r]\\n';

	my $last = $first + $perpage;
	my $range = "-o ${first}-${last}";
	# only print results up to the last one.  We need the first ones,
	# to get numbering right in lqkwic later.
	my $qrange = "-o1-${last}";

	open(INDEX, "$b/lqquery -d $d $qrange \"$query\" |
	    $b/lqkwic -d $d $range -u -S '${perfile}' -s '${permatch}' -c -l200 -r200 -f - |") ||
	    fail $cgi, "Can't run lq-text", "$!";

	

	print "<hr>\n";
	if ($first > 1) {
	    my $back = $url;
	    $back=~ s@\bo=[0-9]+@@;
	    my $prev = $first - $perpage;
	    if ($prev < 1) {
		$prev = 1;
	    }
	    print "<a href=\"${back};o=${prev}\">previous</a>\n";
	}
	print "<a href=\"#feet\">bottom</a>\n";
	print "<hr>\n";

	my $n = 0;

	print "<pre>\n";
	while (<INDEX>) {
	    chomp($_);
	    ++$n;
	    print "$_\n";
	}
	print "</pre>\n";
	close(INDEX);

	print "<a name=\"feet\"><hr></a>\n";
	if ($first > 1) {
	    my $back = $url;
	    $back=~ s@\bo=[0-9]+@@;
	    my $prev = $first - $perpage;
	    if ($prev < 1) {
		$prev = 1;
	    }
	    print "<a href=\"${back};o=${prev}\">previous page</a>\n";
	}
	print "<a href=\"#head\">top</a>\n";
	if ($n >= $perpage) {
	    my $fwd = $url;
	    $fwd=~ s@\bo=[0-9]+@@;
	    $first += $perpage;
	    print "<a href=\"${fwd};o=${first}\">next $perpage matches</a>\n";
	}
	print "<hr>\n";

	print $cgi->end_html;
	exit(0);
    }

    if ($mode eq 'fileview') {
	my $doc = $cgi->param('fn');
	if (!defined($doc)) {
	    fail $cgi, "internal error, no document number",
	         "please do not tamper with the URLs"
	}

	if ($doc !~ /^[0-9]+$/) {
	    fail $cgi, "internal error, invalid document number \"$doc\"",
	         "please do not tamper with the URLs, it hurts."
	}

	# Find out about the document
	open(LQFILE, "$b/lqfile -l -d $d -F ${doc} |") ||
	    fail $cgi, "could not start $b/lqfile", "$!";

	my @fields;

	while (<LQFILE>) {
	    chomp;

	    # take the first matching line
	    # 1       plain   Feb 22 21:13:18 2000    /usr/doc/HOWTO/3Dfx-HOWTO
	    # note: don't use split, because a filename can contain spaces
	    @fields = m/^
		([0-9]+)\s+              # document number
		([^\s]+)\s+              # the file type
		([^\s]+)\s+              # the month name [locale specific!]
		([0-9][0-9]?)\s+         # day
		([0-9:]+)\s+             # time of day
		([0-9]+)\s+              # year
		(.+)$/x;                 # filename
	    last if ($#fields == 6 && $fields[0] eq $doc);
	}
	close(LQFILE);

	if (!defined(@fields) || $#fields != 6) {
	    fail $cgi, "document $doc was not found in the index, sorry.";
	}

	if ($fields[0] ne $doc) {
	    fail $cgi, "document $doc was not found in the index, sorry.",
	               "(did find $fields[0], however)";
	}

	# ok, woohoo, a filename
	#
	my $filename = $fields[6];
	my $type = $fields[1];

	# deliver the file, or try to

	my $cmd = "$b/lqcat -d $d -F $doc |";
	my $TurnURLStoLinks = 0;

	if ($type eq 'HTML' || $filename =~ /\.html?(.g*z)?$/i) {
	    print $cgi->header(
		-type => 'text/html'
	    );
	    print "<h1>type=$type</h1>\n";
	} elsif ($type eq 'troff') {
	    # run man and pipe the output through man2html
	    $filename =~ s/\.g?z$//i;
	    if ($filename =~ /cat*/) {
		$cmd = "$b/lqcat -d $d -F $doc | /usr/local/bin/man2html |";
	    } elsif ($filename =~ m@.*/([^/]+)\.([^\.]+)$@) {
		my ($page, $section) = ($1, $2);
		$cmd = "man $section $page | /usr/local/bin/man2html |";
	    } else {
		$cmd = "$b/lqcat -d $d -F $doc | /usr/local/bin/man2html |";
	    }
	} elsif ($type eq 'plain') {
	    $TurnURLStoLinks = 1;
	    print $cgi->header(
		-type => 'text/plain'
	    );
	} else { # plain or one we don't know
	    $TurnURLStoLinks = 1;
	    print $cgi->header(
		-type => 'text/plain'
	    );
	}
	open(INPUT, $cmd) ||
	    fail $cgi,
		 "could not open lqcat on $doc, <tt>${filename}</tt>",
		 "[$cmd]<br>system error: $!";

	while (<INPUT>) {
	    ## not done yet, would need to use text/html, and then
	    ## people can't save the result
	    ## if ($TurnURLStoLinks && m/[tf]tp/) { # might have http | ftp
		## $_ = makeURLs($_);
	    ## }
	    print;
	}
	close(INPUT);
	exit(0);
    }

    fail($cgi, "unknown form mode [$mode]");
}
main();


##############################################################

# fail - print an error message and exit
my $startedHTML = undef;
my $titlePrefix = "lqtext: ";

sub fail($$;$)
{
    my ($cgi, $whatFailed, $reason) = @_;

    if (!$startedHTML) {
	mkHeader($cgi, "Error: $whatFailed");
    }

    print $cgi->h1("Error");
    print "<p>$whatFailed</p>\n";
    if (defined($reason)) {
	print "<p>$reason</p>\n";
    }
    $cgi->end_html;
    exit(0); # return 0 so httpd doesn't log that the script itself failed
}

sub mkHeader($$)
{
    my ($cgi, $title) = @_;

    print $cgi->header(
	-type => 'text/html'
    );
    $startedHTML = 1;
    print $cgi->start_html(
	  -title => $title,
	-BGCOLOR => '#330000',
	   -TEXT => '#CC9999',
	   -LINK => '#9999FF',
	  -ALINK => '#FF66FF',
	  -VLINK => '#FF6666'
    );
    print "<a name=\"#top\"> \&nbsp; </a>\n";
}

sub mkSearchForm($$)
{
    my ($cgi, $title) = @_;

    mkHeader($cgi, $title);

    print $cgi->h1("Searching the Sample Collection");
    print $cgi->start_form(
        '-method' => 'GET'
    );

    print $cgi->textfield(
	-name => 's',
	-size => 40
    );

    print $cgi->submit(
	 -name => 'search',
	-value => 'Search'
    );

    print $cgi->end_form;
    print $cgi->end_html;
}

## sub oneLink($)
## {
##     my $url = shift;
## 
##     my $after = "";
##     if ($url =~ m@^(.*\.[a-z0-9][a-z0-9/]+)(.*)$@) {
## 	($url, $after) = ($1, $2);
##     }
## 
##     return "<a href=\"${url}\">${url}</a>${after}";
## }
## 
## sub makeURLs($)
## {
##     my $line = shift;
## 
##     $line =~ s@(http|ftp)://[^\s<>]+@oneLink($&)@gxei;
##     return 'URL' . $line;
## }
